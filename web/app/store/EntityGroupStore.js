/* 
 * Entity Group Store
 */

Ext.define('delta3.store.EntityGroupStore', {
    extend: 'Ext.data.Store',
    alias: 'store.entityGroup',
    itemId: 'entityGroupStore',
    model: 'delta3.model.GroupModel',
    entityId: {},
    entityType: {},
    pageSize: 5,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {entity: '{"entities":[{"type":"' + this.entityType
                                + '", "idEntity":"' + JSON.stringify(this.entityId) + '"}]}'};
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Group Relationship WS call");
                }
            },
    proxy: {
        type: 'ajax',
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: 'webresources/admin/addEntityGroup',
            read: 'webresources/admin/getEntityGroup',
            destroy: 'webresources/admin/removeEntityGroup'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'groups'
        },
        writer: {
            writeAllFields: true
        }
    }
});

