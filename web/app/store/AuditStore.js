/* 
 * Audit Store
 */

Ext.define('delta3.store.AuditStore', {
    extend: 'Ext.data.Store',
    alias: 'store.method',
    model: 'delta3.model.AuditModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } 
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getAuditStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            read: 'webresources/admin/getAudits'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,
            rootProperty: 'audits'
        },
        writer: {
            writeAllFields: true
        }
    }
});

