/* 
 * Organization Store
 */


Ext.define('delta3.store.OrganizationStore', {
    extend: 'Ext.data.Store',
    alias: 'store.organizations',
    model: 'delta3.model.OrganizationModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;

                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            
                            if (response.success === false) {
                                
                                /*-----commented code from v3.63--------------------------------
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                                */
                               
                               
                                //--------v3.64 added below code for resolution of bug DELQA-756 for exception -----------------
                                console.log("This is error of foreign key contriants error", response.status.message);
                                if(response.status.message==="DA0008 Exception while deleting organizations: Error while commiting the transaction"){
                                    Ext.Msg.alert("DELTA Error", "Delete cannot be processed. Please contact Organization Administrator.");
                                    
                                if ( response.status.message === "Session timeout.\nPlease login again." ) {
                                    Ext.Msg.alert("DELTA Error",response.status.message );
                                          window.location = 'index.html'; 
                                    }
                                }
                                else{
                                    delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);    
                                }
                                //-------------------------------------------------------------------------------------------------
                                
                            
                            }
                            
                        }
                    }
                },                
                
                
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getOrganizations WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createOrganizations',
            read: 'webresources/admin/getOrganizations',
            update: 'webresources/admin/updateOrganizations',
            destroy: 'webresources/admin/removeOrganizations'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'organizations'
        },
        writer: {
            writeAllFields: true
        }
    }
});


