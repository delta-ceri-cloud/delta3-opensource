/* 
 * Stat Store
 */

Ext.define('delta3.store.StatStore', {
    extend: 'Ext.data.Store',
    alias: 'store.stat',
    model: 'delta3.model.StatModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } 
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                                this.reload();
                            }
                        }
                    }
                },                
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getStatStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/process/createStats',
            read: 'webresources/process/getStats',
            update: 'webresources/process/updateStats',
            destroy: 'webresources/process/removeStats'            
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,
            rootProperty: 'stats'
        },
        writer: {
            writeAllFields: true
        }
    }
});

