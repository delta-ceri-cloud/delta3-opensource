/* 
 * Metadata Tree Store
 */

Ext.define('delta3.store.MetadataTreeStore', {
    extend: 'Ext.data.TreeStore',
    requires: [
        'Ext.Msg',
        'Ext.data.*'
    ],
    alias: 'store.metadata',
    itemId: 'MetadataTreeStore',
    localRemoteFlag: 'local',
    loadMask: Ext.LoadMask(Ext.getBody(), {msg: "Please wait..."}),    
    autoSync: false,
    autoLoad: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}', localRemoteFlag: this.localRemoteFlag};
                },
                load: function(store, record, options) {
                    var theTree = Ext.ComponentQuery.query('#dataSourceTree')[0];
                    theTree.setLoading(false);
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                //delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                                delta3.utils.GlobalFunc.showDeltaMessage(response.status.message);
                            } else {
                                // load different icons for views and leaf tips with column "type"
                                var treeRoot = store.root;
                                treeRoot.set('iconCls', 'db-icon16');
                                var thePanel = Ext.ComponentQuery.query('#metadataTree')[0];
                                treeRoot.set('text', thePanel.dataSource);
                                for (i = 0; i < treeRoot.childNodes.length; i++) {
                                    if (treeRoot.childNodes[i].get('qtip') === 'view') {
                                        treeRoot.childNodes[i].set('iconCls', 'db_view-icon16');
                                    } else {
                                        treeRoot.childNodes[i].set('iconCls', 'db_table-icon16');
                                    }
                                    for (j = 0; j < treeRoot.childNodes[i].childNodes.length; j++) {
                                        treeRoot.childNodes[i].childNodes[j].set('qtip', treeRoot.childNodes[i].data.children[j].type);
                                    }
                                }
                                ModelData.fieldGrid.store.load();
                            }
                        }
                    } else {
                        delta3.utils.GlobalFunc.showDeltaMessage('Unable to establish connection with database');
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getMetadateTreeStore WS call");
                }
            },
    root: {text: 'Database',
        expanded: true
    },
    proxy:
            {
                type: 'ajax',
                timeout: 240000, //4 minutes, extra time needed
                api: {
                    create: 'webresources/model/createModels',
                    read: 'webresources/model/getMetadata',
                    update: 'webresources/model/updateModels'
                },
                reader: {
                    totalProperty: 'totalCount',
                    successProperty: "success",
                    type: 'json',
                    keepRawData: true,                    
                    rootProperty: 'children'
                },
                writer: {
                    writeAllFields: true
                }
            }
});
