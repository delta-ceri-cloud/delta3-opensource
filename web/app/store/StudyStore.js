/* 
 * Study Store
 */


Ext.define('delta3.store.StudyStore', {
    extend: 'Ext.data.Store',
    alias: 'store.study',
    model: 'delta3.model.StudyModel',
    sorters: [{property: 'createdTS', direction: 'DESC'}],    
    remoteFilter: true,
    remoteSort: true,   
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    groupId: 0,    
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {group: this.groupId};
                },                
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getStudies WS call");
                }
            },
    //sorters: { property: 'name', direction : 'ASC' },            
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/study/createStudies',
            read: 'webresources/study/getStudies',
            update: 'webresources/study/updateStudies',
            destroy: 'webresources/study/removeStudies'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'studies'
        },
        writer: {
            writeAllFields: true
        }
    }
});


