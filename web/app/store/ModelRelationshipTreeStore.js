/* 
 * Coping Systems Inc.
 * This is in-memory only store
 */

Ext.define('delta3.store.ModelRelationshipTreeStore', {

    extend: 'Ext.data.TreeStore',    
    requires: [
        'Ext.data.*'
    ],    
    itemId: 'modelRelationshipTreeStore',    
    autoSync: false,
    autoLoad: false,    
    rootProperty: {text     : 'Model',
           expanded : true
        },              
	proxy   : {type: 'memory'}
});
