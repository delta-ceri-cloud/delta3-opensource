/* 
 * Audit Model
 */

Ext.define('delta3.model.AuditModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idAudit', type: 'int'},
        'source',
        'userAlias',
        'objectType',
        'action',
        'objectContent',
        {name: 'organizationId', type: 'int'},
        {name: 'userId', type: 'int'},
        'createdTS'
    ]
});


