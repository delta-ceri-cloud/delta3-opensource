/* 
 * Organization Model
 */

Ext.define('delta3.model.OrganizationModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idOrganization', type: 'int' },
                'guid',
                'name',
                'longName',
                'type',
                'key1',
                'key2',
                {name: 'active', type: 'boolean'},                 
                {name: 'parentId', type: 'int' },
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
