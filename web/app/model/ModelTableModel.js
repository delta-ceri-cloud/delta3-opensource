/* 
 * Model(as in DELTA)Table Model
 */

Ext.define('delta3.model.ModelTableModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idModelTable', type: 'int'},
        'name',
        'description',
        'physicalName',
        {name: 'active', type: 'boolean'}, 
        {name: 'primaryTable', type: 'boolean'},             
        {name: 'modelidModel', type: 'int'},                     
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});