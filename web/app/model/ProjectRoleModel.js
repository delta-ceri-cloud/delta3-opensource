/* 
 * Project-Role Relationship Model
 */

Ext.define('delta3.model.ProjectRoleModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'UseridUser', type: 'int'},
        {name: 'RoleidRole', type: 'int'},       
        {name: 'idGroup', type: 'int'},                   
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});
