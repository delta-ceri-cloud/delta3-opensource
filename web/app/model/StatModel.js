/* 
 * Stat Model
 */

Ext.define('delta3.model.StatModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idStat', type: 'int'},
        'shortName',
        'name',
        'description',
        'configVersion',
        'configuration',
        'visualization',
        {name: 'useProxy', type: 'boolean'},     
        {name: 'active', type: 'boolean'}, 
        {name: 'idOrganization', type: 'int'},
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


