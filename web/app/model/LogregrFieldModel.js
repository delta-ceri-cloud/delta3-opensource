/* 
 * Logistic Regression Field Model
 */

Ext.define('delta3.model.LogregrFieldModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idLogregrField', type: 'int' },
                'name',
                'description',
                'value',
                {name: 'modelColumnidRisk', type: 'int' },       
                {name: 'idLogregr', type: 'int' },             
                {name: 'active', type: 'boolean'},                 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
