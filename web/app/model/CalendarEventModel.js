/* 
 * Calendar Event Model
 */

Ext.define('delta3.model.CalendarEventModel', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'EventId', type: 'int' },
        { name: 'CalendarId', type: 'int' },
        { name: 'idOrganization', type: 'int' },
        { name: 'idUser', type: 'int' },
        { name: 'idStudy', type: 'int'},
        'Title',
        { name: 'IsAllDay', type: 'boolean' },
        'Location',
        'Url',
        'Notes',
        'Reminder',
         { name: 'StartDate', type: 'date' },
         { name: 'EndDate', type: 'date' },
        'CreatedBy',
        { name: 'CreatedTS', type: 'date' },
        'UpdatedBy',
        { name: 'UpdateTS', type: 'date' }
    ]
});

