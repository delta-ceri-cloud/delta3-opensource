/* 
 * Task Model
 */

Ext.define('delta3.model.TaskModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idTask', type: 'int'},
        'name',
        'description',
        'type',        
        'objectType',
        'parameters',
        {name: 'objectId', type: 'int'},        
        {name: 'organizationId', type: 'int'},         
        {name: 'active', type: 'boolean'},   
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        {name: 'idEventTemplate', type: 'int'}
    ]
});



