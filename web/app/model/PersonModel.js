/* 
 * Person Model
 */

Ext.define('delta3.model.PersonModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idPerson', type: 'int' },
                'guid',
                'firstName',
                'lastName',
                'middleName',
                'driverLicenseNumber',
                'ssn',
                'type',
                {name: 'active', type: 'boolean'},                
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS',
                {name:'location', persist:true}               
            ],
    belongsTo: 'Ext.app.models.UserModel',
    hasMany: { model: 'Ext.app.models.LocationModel', name: 'location'}    
});