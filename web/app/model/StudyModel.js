/* 
 * Study Model
 */

Ext.define('delta3.model.StudyModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idStudy', type: 'int', sortType: 'asInt'},
                {name: 'name', type: 'string'},
                {name: 'description', type: 'string'},
                'status',
                {name: 'idOrganization', type: 'int' },
                {name: 'idGroup', type: 'int' },                    
                {name: 'idModel', type: 'int' },       
                {name: 'idOutcome', type: 'int' },
                {name: 'idTreatment', type: 'int' },
                {name: 'idStat', type: 'int' },                 
                {name: 'idMethod', type: 'int' },   
                {name: 'idKey', type: 'int' },     
                {name: 'idDate', type: 'int' },                   
                {name: 'idFilter', type: 'int' },                       
                'methodParams',
                {name: 'idAlert', type: 'int' },  
                'statPackageVer',
                'confidenceInterval',
                'confidenceInterval2',
                {name: 'isMissingDataUsed', type: 'boolean'},    
                {name: 'dumpData', type: 'boolean'},      
                {name: 'sendAllColumns', type: 'boolean'},                         
                {name: 'isChunking', type: 'boolean'},
                {name: 'autoExecute', type: 'boolean'},                  
                'chunkingBy',
                {name: 'isGenericId', type: 'boolean'},                  
                {name: 'originalStudyId', type: 'int' },    
                {name: 'overwriteResults', type: 'boolean'},    
                {name: 'outcomePositive', type: 'boolean'},
                {name: 'localStatPackage', type: 'boolean'},    
                {name: 'remoteStatPackage', type: 'boolean'},                
                //{name: 'processedTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                'processedTS',
                {name: 'startTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'},
                {name: 'endTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u'}, 
                {name: 'active', type: 'boolean'},                 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
