/* 
 * Event Model
 */

Ext.define('delta3.model.EventModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idEvent', type: 'int'},
        'description',
        {name: 'objectId', type: 'int'},
        'objecType',        
        {name: 'active', type: 'boolean'},   
        {name: 'executeBy', type: 'int'},
        'executeTS',        
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        {name: 'idOrganization', type: 'int'},
        {name: 'idEventTemplate', type: 'int'}
    ]
});



