/* 
 * FilterFormula Model
 */

Ext.define('delta3.model.FilterFormulaModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idFilterField', type: 'int'},        
        {name: 'filteridFilter', type: 'int'},
        {name: 'modelColumnidModelColumn', type: 'int'},
        'name',
        'formula',
        'operator',
        'type',
        'negator',
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


