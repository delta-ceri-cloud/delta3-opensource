/* 
 * Permission Container
 */

Ext.define('delta3.view.PermissionContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.permissions',
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,          
    //layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.permission',
                region: 'center'
        }, 
        me.callParent();
    }
});