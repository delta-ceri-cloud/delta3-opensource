/* 
 * Group Container
 */

Ext.define('delta3.view.GroupContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.groups',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.group',
                region: 'center'
        },   
        me.callParent();
    }
});