/* 
 * Web Services Container
 */

Ext.define('delta3.view.WebservicesContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.webservices',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;   
        me.items = {
                xtype:  'panel.webservices',
                region: 'center'
        },   
        me.callParent();
    }
});

