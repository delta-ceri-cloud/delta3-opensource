/* 
 * Health Survey Panel
 */

Ext.define('delta3.view.HealthSurveyPanel', {
    extend: 'Ext.Panel',
    title: 'Health Survey',
    height: 800,
    width: '100%',
    scrollable: 'y',
    alias: 'widget.panel.healthsurvey',    
    itemId: 'surveyPanel',
    layout: 'fit',   
    items : [],      
    closable: false,        
    //html: '<iframe src="/myceri/form30.html"  style="width:100%;height:100%;border:none;"></iframe>',    
    initComponent: function() {
        this.items = [{
                frame: false,
                border: false,
                xtype : "component",
                autoEl : {
                    tag : "iframe",
                    frameborder: 0,
                    src: "/myceri/patientList.php?u=" + delta3.utils.GlobalVars.currentUser + '&lcn=' + '1234567890'
                }
            }];
        this.callParent();
    }
});