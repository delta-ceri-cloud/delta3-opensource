/* 
 * Reccurence Popup
 */

Ext.define('delta3.view.scheduler.ReccurencePopup', {
    extend: 'Ext.Window',
    requires: ['Ext.form.FieldSet', 
            'Ext.form.field.Time',
            'Ext.form.field.Number',
            'Ext.form.field.Date'], 
    alias: 'widget.popup.reccurence',
    layout: 'fit',
    width: 400,
    height: 470,
    labelLength: 100,
    itemId: 'reccurencePopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Scheduler',
    selectedRecord: {},
    taskStore: {},
    taskId: 0,
    reccurenceType: 0,
    entityId: 0,
    entityType: {},
    refreshFunction: {},
    checkBoxOnAction: {},
    checkBoxOffAction: {},
    items: [],
    initComponent: function() {
        var me = this;
        var currTask = me.taskStore.findRecord('objectId',me.entityId);
        var yearlyPick = {
                xtype: 'fieldset',
                fieldLabel: '',
                title: 'Every',
                hidden: true,
                width: 140,
                height: 100,
                labelAlign: 'right',    
                labelLength: me.labelLength,                
                layout: 'vbox',
                items: [{xtype: 'container',
                        layout: 'hbox',
                        items: [{
                            xtype: 'numberfield',
                            name: 'recurEvery',
                            itemId: 'yearsRec',
                            minValue: 0,
                            maxValue: 5,
                            maxWidth: 60,
                            value: 1
                        },{
                            xtype: 'label',
                            text: '  Year(s)'
                        }]
                }]
        };         
        var monthlyPick = {
                xtype: 'fieldset',
                fieldLabel: '',
                title: 'Every',
                hidden: true,
                width: 140,
                height: 100,
                labelAlign: 'right',    
                labelLength: me.labelLength,                
                layout: 'vbox',
                items: [{xtype: 'container',
                        layout: 'hbox',
                        items: [{
                            xtype: 'numberfield',
                            name: 'recurEvery',
                            itemId: 'monthsRec',
                            minValue: 0,
                            maxValue: 11,
                            maxWidth: 60,
                            value: 1
                        },{
                            xtype: 'label',
                            text: '  Month(s)'
                        }]
                    }]
            };         
        var dailyPick = {
                xtype: 'fieldset',
                fieldLabel: '',
                title: 'Every',
                hidden: true,
                width: 140,
                height: 100,
                labelAlign: 'right',       
                labelLength: me.labelLength,
                layout: 'vbox',
                items: [{xtype: 'container',
                        layout: 'hbox',
                        items: [{
                            xtype: 'numberfield',
                            name: 'recurEvery',
                            itemId: 'daysRec',
                            minValue: 0,
                            maxValue: 31,
                            maxWidth: 60,
                            value: 1
                        },{
                            xtype: 'label',
                            text: '  Day(s)'
                        }]
                }]
            };        
        var weeklyPick = {
                xtype: 'fieldset',
                fieldLabel: '',
                title: 'Every',
                hidden: true,
                width: 140,
                height: 230,
                labelAlign: 'left',     
                labelLength: me.labelLength,                
                layout: 'auto',
                items: [{
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                            xtype: 'numberfield',
                            name: 'recurEvery',
                            itemId: 'weeksRec',
                            maxWidth: 60,
                            minValue: 0,
                            maxValue: 52,
                            value: 1
                        },{
                            xtype: 'label',
                            text: 'Week(s)'
                        }]
                    }, {
                        xtype: 'checkboxfield',
                        name: 'checkbox1',
                        itemId: 'mondayRec',
                        boxLabel: 'Monday'
                    },                     {
                        xtype: 'checkboxfield',
                        name: 'checkbox2',
                        itemId: 'tuesdayRec',
                        boxLabel: 'Tuesday'
                    },                     {
                        xtype: 'checkboxfield',
                        name: 'checkbox3',
                        itemId: 'wednesdayRec',
                        boxLabel: 'Wednesday'
                    },                    {
                        xtype: 'checkboxfield',
                        name: 'checkbox4',
                        itemId: 'thursdayRec',
                        boxLabel: 'Thursday'
                    },                    {
                        xtype: 'checkboxfield',
                        name: 'checkbox5',
                        itemId: 'fridayRec',
                        boxLabel: 'Friday'
                    },                    {
                        xtype: 'checkboxfield',
                        name: 'checkbox6',
                        itemId: 'saturdayRec',
                        boxLabel: 'Saturday'
                    },                     {
                        xtype: 'checkboxfield',
                        name: 'checkbox7',
                        itemId: 'sundayRec',
                        boxLabel: 'Sunday'
                    }]};
        
        
            
       
        me.items[0] = new Ext.form.Panel({
            title: 'Select time and frequency of execution',
            itemId: 'taskPanel',
            bodyPadding: 10,
            frame: true,
            renderTo: Ext.getBody(),
            items: [{
                xtype: 'timefield',
                emptyText: 'select value',
                name: 'timeStart',
                itemId: 'timeStart',
                fieldLabel: 'Execution Time'
            }, 
            {
                xtype: 'datefield',
                name: 'dateStart',
                itemId: 'dateStart',
                //value: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
                minValue: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
                fieldLabel: 'Start Date'                                
            }, { 
                xtype: 'fieldcontainer',
                fieldLabel: 'Recurrence pattern',            
                layout: 'hbox',
                itemId: 'recurrenceBox',
                items: [{   
                        xtype: 'fieldset',            
                        layout: 'vbox',
                        items: [{
                            xtype: 'radiofield',
                            name: 'radio1',
                            itemId: 'radio0rec',
                            value: true,
                            handler: function(ctl, val) {
                                if ( val === true ) {
                                    me.reccurenceType = 0;
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[1].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[2].setVisible(false);  
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[3].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[4].setVisible(false);   
                                    Ext.ComponentQuery.query('#recNumber')[0].setVisible(false);
                                }
                            },
                            boxLabel: 'None'
                        }, {
                            xtype: 'radiofield',
                            name: 'radio1',
                            itemId: 'radio1rec',
                            value: false,
                            handler: function(ctl, val) {
                                if ( val === true ) {
                                    me.reccurenceType = 1;
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[1].setVisible(true);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[2].setVisible(false);  
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[3].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[4].setVisible(false);     
                                    Ext.ComponentQuery.query('#recNumber')[0].setVisible(true);
                                }
                            },
                            boxLabel: 'Daily'
                        }, {
                            xtype: 'radiofield',
                            name: 'radio1',
                            itemId: 'radio2rec',
                            value: false,
                            handler: function(ctl, val) {
                                if ( val === true ) {
                                    me.reccurenceType = 2;
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[1].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[2].setVisible(true);  
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[3].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[4].setVisible(false);  
                                    Ext.ComponentQuery.query('#recNumber')[0].setVisible(true);
                                }
                            },
                            boxLabel: 'Weekly'
                        }, {
                            xtype: 'radiofield',
                            name: 'radio1',
                            itemId: 'radio3rec',
                            value: false,
                            handler: function(ctl, val) { 
                                if ( val === true ) {
                                    me.reccurenceType = 3;
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[1].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[2].setVisible(false);  
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[3].setVisible(true);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[4].setVisible(false);   
                                    Ext.ComponentQuery.query('#recNumber')[0].setVisible(true);
                                }
                            },                            
                            boxLabel: 'Monthly'
                        }, {
                            xtype: 'radiofield',
                            name: 'radio1',
                            itemId: 'radio4rec',
                            value: false,
                            handler: function(ctl, val) { 
                                if ( val === true ) {
                                    me.reccurenceType = 4;
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[1].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[2].setVisible(false);  
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[3].setVisible(false);
                                    Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[4].setVisible(true);   
                                    Ext.ComponentQuery.query('#recNumber')[0].setVisible(true);
                                }
                            },
                            boxLabel: 'Yearly'
                        }]
                    }, dailyPick, weeklyPick, monthlyPick, yearlyPick]
            }, {
                xtype: 'numberfield',
                name: 'recNumber',
                itemId: 'recNumber',
                value: 1,
                minValue: 0,
                maxValue: 365,
                maxWidth: 100,
                fieldLabel: 'Number of Reccurences'
            }, 
            
                
            {
                xtype: 'checkbox',
                labelWidth: 150,
                labelAlign: 'left',                              
                fieldLabel: 'Generate Flat Table',
                inputValue: true,
                value: true,
                itemId: 'generateFlatTableCheckbox'                           
            },
            
            {
                xtype: 'label',
                //labelWidth: 150,
                labelAlign: 'left',    
                style: "font-size:11px;",
                html: '<br>***It is always recommended to process model and verify flat table generation before scheduling automatic execution.',
                itemId: 'Instr00model'                           
            },
            
            {
                xtype: 'label',
                //labelWidth: 150,
                labelAlign: 'left',    
                style: "font-size:11px;",
                html: '<br><br>***It is always recommended to run studies and verify results before scheduling automatic execution.',
                itemId: 'Instr00study'                           
            }
           
        
           
        
            
            ],
            buttons: [{
                text: 'Delete this Task',
                itemId: 'deleteTaskButton',
                hidden: true,
                    handler: function() {
                        Ext.Msg.show({
                            title:'DELTA',
                            message: 'You are about to delete Task. Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,                            
                            fn: function(btn) {
                                if (btn !== 'yes') {
                                    return;
                                } else {
                                    me.taskStore.load();
                                    var record = me.taskStore.find('idTask',me.taskId);
                                    me.taskStore.removeAt(record);
                                    me.refreshFunction(null);
                                    Ext.ComponentQuery.query('#reccurencePopup')[0].destroy();
                                }
                            }
                        });                                            
                    }
                }, 
                
                
                
                {
                text: 'Save And Schedule',
                handler: function(b) {
                    var startTime = Ext.ComponentQuery.query('#timeStart')[0].value;
                    var startDate = Ext.ComponentQuery.query('#dateStart')[0].value;
                    var recNumber = Ext.ComponentQuery.query('#recNumber')[0].value;
                    var recType = Ext.ComponentQuery.query('#reccurencePopup')[0].reccurenceType;
                    var recObject = {"type": recType, "period": 0};
                    switch ( recType ) {
                        case 1:
                            recObject = {"type": recType, "period": Ext.ComponentQuery.query('#daysRec')[0].value};
                            break;
                        case 2:
                            recObject = {"type": recType, "period": Ext.ComponentQuery.query('#weeksRec')[0].value, 
                                "monday": Ext.ComponentQuery.query('#mondayRec')[0].value, 
                                "tuesday": Ext.ComponentQuery.query('#tuesdayRec')[0].value,
                                "wednesday": Ext.ComponentQuery.query('#wednesdayRec')[0].value, 
                                "thursday": Ext.ComponentQuery.query('#thursdayRec')[0].value, 
                                "friday": Ext.ComponentQuery.query('#fridayRec')[0].value, 
                                "saturday": Ext.ComponentQuery.query('#saturdayRec')[0].value, 
                                "sunday": Ext.ComponentQuery.query('#sundayRec')[0].value
                            };
                            break;    
                        case 3:
                            recObject = {"type": recType, "period": Ext.ComponentQuery.query('#monthsRec')[0].value};
                            break; 
                        case 4:
                            recObject = {"type": recType, "period": Ext.ComponentQuery.query('#yearsRec')[0].value};
                            break;                        
                    }
                    if ( startTime === null ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Execution Time first');
                        return;                        
                    }
                    var currDate = new Date();
                    if ( startDate.getTime() <= currDate.getTime() ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please specify Start Date in the future');
                        return;                            
                    }  
                    
                    
                    var taskType = me.checkBoxOnAction;
                    if ( Ext.ComponentQuery.query('#generateFlatTableCheckbox')[0].value !== true ) {
                        taskType = me.checkBoxOffAction;
                    }
                    
                    
                    
                    var taskJSON = '{"tasks":[{"type":"' + taskType 
                            + '","idTask":"' + me.taskId
                            + '","objectType":"'+ me.entityType 
                            + '","objectId":'+ me.entityId + '}]' 
                            + ',"schedules":[{"startHH":'+ startTime.getHours()
                            + ',"startmm":' + startTime.getMinutes()
                            + ',"startYYYY":' + startDate.getFullYear()
                            + ',"startMM":' + (1 + startDate.getMonth())
                            + ',"startDD":' + startDate.getDate()
                            + ',"reccurenceNumber":' + recNumber 
                            + ',"reccurence":' + JSON.stringify(recObject) + '}]}';
                    
                    
                    
                    if ( me.taskId > 0 ) {
                        Ext.Msg.show({
                            title:'DELTA',
                            message: 'You are about to overwrite Task. Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn !== 'yes') {
                                    return;
                                } else {
                                    delta3.utils.GlobalFunc.doSaveTaskAndScheduleEvents(taskJSON, me.refreshFunction);
                                    Ext.ComponentQuery.query('#reccurencePopup')[0].destroy();
                                }
                            }
                        });                                  
                    } 
                    
                    else {                       
                        delta3.utils.GlobalFunc.doSaveTaskAndScheduleEvents(taskJSON, me.refreshFunction);
                        Ext.ComponentQuery.query('#reccurencePopup')[0].destroy(); 
                    }
                }
            }, 
            
            
            {
                text: 'Close',
                handler: function() {
                    this.up('#reccurencePopup').destroy();
                }
            }
        
            ]
        }); 
        
        Ext.ComponentQuery.query('#recNumber')[0].setVisible(false);
        //var currTask = me.taskStore.findRecord('objectId',me.entityId);
        
        if(me.entityType==='models')
        {
            
            Ext.ComponentQuery.query('#generateFlatTableCheckbox')[0].setVisible(false);        
            Ext.ComponentQuery.query('#Instr00study')[0].setVisible(false);    
            Ext.ComponentQuery.query('#Instr00model')[0].setVisible(true);  
           
            
           
        }
        else{
           
            Ext.ComponentQuery.query('#generateFlatTableCheckbox')[0].setVisible(true);    
            Ext.ComponentQuery.query('#Instr00model')[0].setVisible(false);  
            Ext.ComponentQuery.query('#Instr00study')[0].setVisible(true);        
            
            
        }
        

        if ( currTask !== null ) {
            var params = JSON.parse(currTask.data.parameters);     
            me.taskId = currTask.data.idTask;
            
            if ( params.type === me.checkBoxOnAction ) {
                Ext.ComponentQuery.query('#generateFlatTableCheckbox')[0].setValue(true);
            } else {
                Ext.ComponentQuery.query('#generateFlatTableCheckbox')[0].setValue(false);
            }
            var dateStart = new Date(params.startYYYY, params.startMM - 1, params.startDD, 0,0,0,0);
            Ext.ComponentQuery.query('#dateStart')[0].setValue(dateStart);
            var timeStart = new Date(params.startYYYY, params.startMM, params.startDD, params.startHH, params.startmm,0,0);
            Ext.ComponentQuery.query('#timeStart')[0].setValue(timeStart);
            Ext.ComponentQuery.query('#recNumber')[0].setValue(params.reccurenceNumber);
            Ext.ComponentQuery.query('#recNumber')[0].setVisible(true);
            Ext.ComponentQuery.query('#recurrenceBox')[0].items.items[params.reccurence.type].setVisible(true);   
            switch (params.reccurence.type) {
                case 1:
                    Ext.ComponentQuery.query('#radio1rec')[0].setValue(true);
                    break;                
                case 2: 
                    Ext.ComponentQuery.query('#radio2rec')[0].setValue(true);
                    Ext.ComponentQuery.query('#mondayRec')[0].setValue(params.reccurence.monday); 
                    Ext.ComponentQuery.query('#tuesdayRec')[0].setValue(params.reccurence.tuesday);
                    Ext.ComponentQuery.query('#wednesdayRec')[0].setValue(params.reccurence.wednesday);
                    Ext.ComponentQuery.query('#thursdayRec')[0].setValue(params.reccurence.thursday);
                    Ext.ComponentQuery.query('#fridayRec')[0].setValue(params.reccurence.friday);
                    Ext.ComponentQuery.query('#saturdayRec')[0].setValue(params.reccurence.saturday);
                    Ext.ComponentQuery.query('#sundayRec')[0].setValue(params.reccurence.sunday);
                    break;
                case 3:
                    Ext.ComponentQuery.query('#radio3rec')[0].setValue(true);
                    break;
                case 4:
                    Ext.ComponentQuery.query('#radio4rec')[0].setValue(true);
                    break;                    
            }
            
            
            Ext.ComponentQuery.query('#deleteTaskButton')[0].setVisible(true);
        } 
        me.callParent();
    }
});

