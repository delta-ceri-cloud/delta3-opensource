/* 
 * Organization Container
 */

Ext.define('delta3.view.OrganizationContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.organizations',
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,              
    //layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.organization',
                region: 'center'
        },   
        me.callParent();
    }
});