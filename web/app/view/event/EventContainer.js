/* 
 * Event Container
 */

Ext.define('delta3.view.event.EventContainer', {
    extend: 'Ext.container.Container',
    itemId: 'eventContainer',
    alias:  'widget.container.events',
//    height: 84+21*delta3.utils.GlobalVars.pageSize,            
//    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        //console.log("initComponent EventContainer");        
        me.items = {
                xtype:  'grid.event',
                region: 'center'
        },   
        me.callParent();
    }
});

