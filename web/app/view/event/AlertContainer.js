/* 
 * Alert Container
 */

Ext.define('delta3.view.event.AlertContainer', {
    extend: 'Ext.container.Container',
    itemId: 'alertContainer',
    alias:  'widget.container.alerts',
//    height: 84+21*delta3.utils.GlobalVars.pageSize,            
//    layout: 'fit',
    initComponent:  function(){ 
        var me = this;   
        me.items = {
                xtype:  'grid.alert',
                region: 'center'
        },   
        me.callParent();
    }
});

