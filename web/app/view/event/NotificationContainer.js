/* 
 * Notification Container
 */

Ext.define('delta3.view.event.NotificationContainer', {
    extend: 'Ext.container.Container',
    itemId: 'notificationContainer',
    alias:  'widget.container.notifications',
//    height: 84+21*delta3.utils.GlobalVars.pageSize,            
//    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.notification',
                region: 'center'
        },   
        me.callParent();
    }
});

