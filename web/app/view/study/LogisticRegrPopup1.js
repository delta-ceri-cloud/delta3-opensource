/* 
 * Logistic Regression Method Popup Window
 */

Ext.define('delta3.view.study.LogisticRegrPopup1', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.popup.methodLRStep1',
    requires: [
        'delta3.model.IndependentVariableModel',
        'delta3.view.study.IndependentVariableSelector'
    ],
    layout: 'fit',
    labelWidth: 160,
    labelAlign: 'left',
    itemId: 'methodLRStep1Panel',
    title: 'LR Step1',
    modal: true,
    closeAction: 'destroy',
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    depVariable: {},
    params: {},
    fieldId: 0,
    modelId: {},
    studyId: {},
    items: [],
    listeners: {
        beforeshow: function (comp, eOpts) {
            var thisPanel = Ext.ComponentQuery.query('#methodLRStep1Panel')[0];
            thisPanel.fieldStore.clearFilter(true);
            thisPanel.fieldStore.filter('fieldClass', 'Risk Factor');
            thisPanel.fieldStore.filter('idModel', thisPanel.modelId);
            thisPanel.fieldStore.filter('insertable', true);
        },
        beforedestroy: function (rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },
    initComponent: function () {
        var me = this;
        me.fieldStore.clearFilter(true);
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index);
        if (uniqueField === null) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            Ext.ComponentQuery.query('#logisticRegrContainer')[0].cleanup();
        }
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if (methodRec === null) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            Ext.ComponentQuery.query('#logisticRegrContainer')[0].cleanup();
        }

        // if params empty retrieve all method parameters, not only for this step
        if ((typeof me.params === 'undefined') || (me.params === null) || (me.params === "")) {
            // initialize from empty params string based on Method template
            var paramArray = JSON.parse(methodRec.data.methodParams);
            me.params = paramArray[0];
        }
        var depVariableContr = null;
        if (typeof me.params.inputs.dependentVariableSelection !== 'undefined') {
            me.fieldStore.clearFilter(true);
            me.fieldStore.filter('fieldClass', 'Outcome');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);
            if (JSON.stringify(me.params.inputs.dependentVariableSelection.values[0]) === "{}" || me.params.inputs.dependentVariableSelection.values[0] === 'value0') {
                me.depVariable = '';
            } else {
                me.depVariable = me.params.inputs.dependentVariableSelection.values[0];
            }
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');
            depVariableContr = {
                xtype: 'combobox',
                fieldLabel: me.params.inputs.dependentVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'depVariable',
                allowBlank: !me.params.inputs.dependentVariableSelection.required,     
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                bind: {
                    value: '{depVariable}'
                },
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function (cmb, rec, idx) {
                        me.depVariable = cmb.getValue();
                        //Ext.ComponentQuery.query('#logisticRegrContainer')[0].tab1Saved = false;
                    }
                }
            };
        }
        var indepVariables = '';
        var cflag = 0;
        for (var i = 0; i < me.params.inputs.independentVariableSelection.values.length; i++) {
            if (me.params.inputs.independentVariableSelection.values[i] !== 'value0') {
                if (cflag > 0) {
                    indepVariables += ',';
                }
                cflag++;
                indepVariables += me.params.inputs.independentVariableSelection.values[i];
            }
        }
        var indepVariableContr = null;
        if (typeof me.params.inputs.independentVariableSelection !== 'undefined') {
            me.fieldStore.clearFilter(true);
            me.fieldStore.filter('fieldClass', 'Risk Factor');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);
            var riskFactorStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.IndependentVariableModel.getFields());
            indepVariableContr = Ext.create('delta3.view.study.IndependentVariableSelector', {riskFactorStore: riskFactorStore, indepVariablesList: indepVariables})
        }
        // following code for AVS allows study to retroactively pick this new parameter w/o clearing all params
        var useAVS = null;
        if (typeof me.params.inputs.automaticVariableSelection !== 'undefined') {
            var currValue = '';
            if (typeof me.params.inputs.automaticVariableSelection.values[0] !== 'undefined') {
                currValue = me.params.inputs.automaticVariableSelection.values[0];
            }
            useAVS = {
                xtype: 'checkbox',
                fieldLabel: me.params.inputs.automaticVariableSelection.inputText,
                itemId: 'avs',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,
                value: currValue,
                allowBlank: true
            };
        } else {
            if (typeof me.methodParams[0].inputs.automaticVariableSelection !== 'undefined') {
                me.params.inputs.automaticVariableSelection = me.methodParams[0].inputs.automaticVariableSelection;
                var currValue = '';
                if (typeof me.params.inputs.automaticVariableSelection.values[0] !== 'undefined') {
                    currValue = me.params.inputs.automaticVariableSelection.values[0];
                }
                useAVS = {
                    xtype: 'checkbox',
                    fieldLabel: me.params.inputs.automaticVariableSelection.inputText,
                    itemId: 'avs',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,
                    value: currValue,
                    allowBlank: true
                };
            }
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    bodyStyle: 'padding:5px 5px 0',
                    autoScroll: true,
                    defaultType: 'displayfield',
                    items: [
                        {
                            fieldLabel: me.params.inputs.studyUniqueId.inputText,
                            allowBlank: !me.params.inputs.studyUniqueId.required,     
                            labelWidth: me.labelWidth,
                            labelAlign: me.labelAlign,
                            name: 'studyId',
                            allowBlank: true,
                            value: uniqueField.get("name")
                        } 
//                        {
//                            xtype: 'checkbox',
//                            labelWidth: me.labelWidth,
//                            labelAlign: me.labelAlign,
//                            fieldLabel: 'Use generic Id',
//                            name: 'genericId',
//                            inputValue: true,
//                            value: me.selectedStudyRecord.get('isGenericId'),
//                            itemId: 'genericIdCheckbox'
//                        }
                        , depVariableContr
                        , indepVariableContr
                        , useAVS,
                        {
                            xtype: 'radiogroup',
                            fieldLabel: 'Initial Training Period',
                            labelWidth: me.labelWidth,
                            labelAlign: me.labelAlign,
                            columns: 4,
                            itemId: 'chunkRadiogroup',
                            anchor: '100%',
                            defaults: {xtype: "radio", name: "chunkingBy"},
                            vertical: false,
                            items: [{
                                    boxLabel: 'Year&nbsp;&nbsp;&nbsp;',
                                    handler: function (ctl, val) {
                                        if (val === true) {
                                            me.calculateStartDate('year');
                                        }
                                    },
                                    inputValue: 'year'
                                }, {
                                    boxLabel: 'Half a Year&nbsp;&nbsp;&nbsp;',
                                    width: 90,
                                    handler: function (ctl, val) {
                                        if (val === true) {
                                            me.calculateStartDate('6months');
                                        }
                                    },
                                    inputValue: '6months'
                                }, {
                                    boxLabel: 'Quarter&nbsp;&nbsp;&nbsp;',
                                    handler: function (ctl, val) {
                                        if (val === true) {
                                            me.calculateStartDate('quarter');
                                        }
                                    },
                                    inputValue: 'quarter'
                                }, {
                                    boxLabel: 'Month',
                                    handler: function (ctl, val) {
                                        if (val === true) {
                                            me.calculateStartDate('month');
                                        }
                                    },
                                    inputValue: 'month'
                                }]
                        }
                    ]
                });
        var chunkRadios = Ext.ComponentQuery.query('#chunkRadiogroup')[0];
        // now set the value of the radio button(s) that match the key/value pair
        chunkRadios.setValue({chunkingBy: me.selectedStudyRecord.get('chunkingBy')});
        me.callParent();
    },
    save: function () {
        var thisWin = Ext.ComponentQuery.query('#methodLRStep1Panel')[0];
        var theItems = Ext.ComponentQuery.query('#indepVariable')[0];
        //var theCheckbox = Ext.ComponentQuery.query('#chunkingCheckbox')[0];
        var theChunkRadio = Ext.ComponentQuery.query('#chunkRadiogroup')[0];
//        var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];
        var listOfVariables = theItems.getRawValue();
        var arrayOfVariables = listOfVariables.split(",");
        thisWin.params.inputs.independentVariableSelection.values = [];
        for (var i = 0; i < arrayOfVariables.length; i++) {
            thisWin.params.inputs.independentVariableSelection.values[i] = arrayOfVariables[i];
        }
        var index = thisWin.selectedStudyRecord.get('idKey');
        thisWin.fieldStore.clearFilter(true);
        var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);
//        if (genericIdCheckbox.value === true) {
//            thisWin.params.inputs.studyUniqueId.values[0] = 'id';
//        } else {
            thisWin.params.inputs.studyUniqueId.values[0] = uniqueField.get("name");
//        }        
//        thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);
        thisWin.params.inputs.dependentVariableSelection.values[0] = thisWin.depVariable;
        thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(thisWin.depVariable));
        var temp = theChunkRadio.getValue();
        thisWin.selectedStudyRecord.set('chunkingBy', temp.chunkingBy);
        if (typeof Ext.ComponentQuery.query('#avs')[0] !== 'undefined') {
            thisWin.params.inputs.automaticVariableSelection.values[0] = Ext.ComponentQuery.query('#avs')[0].value;
        }  
    },
    calculateStartDate: function (period) {
        // this function calls REST APi and updates LR Step 2 configuration tab
        var startDate = this.selectedStudyRecord.get('startTS');
        var dateYear = startDate.getFullYear();
        var dateMonth = startDate.getMonth() + 1;
        var dateDay = startDate.getDate();
        var startDateString = dateYear.toString() + '-';
        if (dateMonth > 9) {
            startDateString += dateMonth.toString() + '-';
        } else {
            startDateString += '0' + dateMonth.toString() + '-';
        }
        if (dateDay > 9) {
            startDateString += dateDay.toString();
        } else {
            startDateString += '0' + dateDay.toString();
        }
        var stringJSON = '{"periodType":"' + period.toString() + '","date":"' + startDateString + '"}';
        Ext.Ajax.request
                (
                        {
                            url: '/Delta3/webresources/study/calculateStartDate',
                            method: "POST",
                            disableCaching: true,
                            params: stringJSON,
                            success: successFunction,
                            failure: failureFunction
                        }
                );
        Ext.MessageBox.wait('Calculating Study Start Date ...');
        return;

        function successFunction(response, options)
        {
            Ext.MessageBox.hide();
            Ext.ComponentQuery.query('#startDate')[0].setValue(new Date(response.responseText));
            Ext.ComponentQuery.query('#startDate')[0].updateLayout();
        }

        function failureFunction(response, options)
        {
            Ext.MessageBox.hide();
            if (response.timedout === true) {
                Ext.Msg.alert("DELTA", response.statusText);
            } else {
                Ext.Msg.alert("DELTA", response.responseText);
            }
        }
    }
}
);

