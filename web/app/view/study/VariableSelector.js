/**
 * CERI-Lahey (c) 2016
 */
Ext.define('delta3.view.study.VariableSelector', {
    extend: 'Ext.container.Container',
    
    requires: [
        'Ext.grid.*',
        'Ext.layout.container.HBox',
        'delta3.model.StudyVariableModel'
    ],    
    xtype: 'dd-grid-to-grid',
    
    
    width: 480,
    height: 200,
    layout: {
        type: 'hbox',
        align: 'stretch',
        padding: 5
    },
    
    sourceStore: {},
    variableStore: {},
    variablesList: {},
    fieldClass: 'Variable',
    
    initComponent: function(){
        var group1 = this.id + 'group1',
            group2 = this.id + 'group2',
            
            columns = [{
                text: 'Variable Name', 
                flex: 1, 
                sortable: true, 
                dataIndex: 'name'
            }, {
                text: 'Kind', 
                width: 70, 
                sortable: true, 
                dataIndex: 'fieldKind'
            }];
        
        
        
        
        this.variableStore = new Ext.data.Store({model: delta3.model.StudyVariableModel}); 
  
        var arrayOfVariables = this.variablesList.split(",");
        console.log("This is array Variable", arrayOfVariables);
        
        for (var i=0; i< arrayOfVariables.length; i++) {
            if ( this.sourceStore.getTotalCount() > 0 ) {
                //var index = this.sourceStore.find('name', arrayOfVariables[i]);
                var index = this.sourceStore.find('name', arrayOfVariables[i], 0, false, false, true);
                
                
                if ( index > -1 ) {
                    var record = this.sourceStore.getAt(index);
                    this.sourceStore.removeAt(index);
                    this.variableStore.add(record);
                }
            }
        }
        
        this.items = [{
            itemId: 'variableGrid',
            flex: 1,
            xtype: 'grid',
            multiSelect: true,
                viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: group1,
                    dropGroup: group2
                },
                listeners: {
                    drop: function(node, data, dropRec, dropPosition) {
                        var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
                    }
                }
            },
            store: this.sourceStore,
            columns: columns,
            title: 'Available Data Model ' + this.fieldClass + 's',
//            tools: [{
//                type: 'refresh',
//                tooltip: 'Reset both grids',
//                scope: this,
//                handler: this.onResetClick
//            }],
            stripeRows: true,        
            margin: '0 5 0 0'
        }, 
        
            
            {
            itemId: 'variable' + this.fieldClass,
            flex: 1,
            xtype: 'grid',
            viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: group2,
                    dropGroup: group1
                },
                listeners: {
                    drop: function(node, data, dropRec, dropPosition) {
                        var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
                    }
                }
            },
            store: this.variableStore,
            columns: columns,
//            tools: [{
//                type: 'search',
//                tooltip: 'Retrieve variable list',
//                scope: this,
//                handler: this.onRetrieveClick
//            }],            
            stripeRows: true,
            title: 'Selected Variables',
            
            
            getRawValue: function() {
                var valueString = '';
                
                for ( var i=0; i<this.store.data.length; i++) {
                    if ( i > 0 ) {
                        valueString += ',';
                    }
                    valueString += this.store.getAt(i).get('name');
                }
                console.log("This is value string", valueString);
                return valueString;
            }            
        }];

        this.callParent();
    },
    
    onResetClick: function(){
        Ext.Msg.show({
            title: 'DELTA',
            message: 'You are about to clear variable list. Would you like to proceed?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    //refresh source grid
                    this.down('#variableGrid').getStore().loadData(this.variableData);
                    //purge destination grid
                    //this.down('#variable').getStore().removeAll();
                    this.down().getStore().removeAll();
                }
            }
        });        
    },
    
    onRetrieveClick: function(){
        Ext.Msg.show({
            title: 'DELTA',
            message: 'You are about to retrieve variable list. Would you like to proceed?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {

                }
            }
        });        
    }    
}
);
