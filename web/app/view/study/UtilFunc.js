/* 
 * set of functions to help process study parameters
 */

Ext.define('delta3.view.study.UtilFunc', {
    requires: [
        'Ext.LoadMask',
        'delta3.model.StudyVariableModel',
        'delta3.view.study.VariableSelector'
    ],   
    statics: {
        checkBoxUI: function(uiElement, me, id) {
            return {
                allowBlank: !uiElement.required,                
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                 
                xtype: 'checkbox',
                margin: '5, 0, 5, 0',
                fieldLabel: uiElement.inputText,
                inputValue: true,
                value: uiElement.values[0],
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: uiElement.description
                      });
                    }
                },                 
                itemId: id                           
            };    
        },

        numberUI: function(uiElement, me, id) {
        return {
                xtype: 'numberfield',
                fieldLabel: uiElement.inputText,
                itemId: id,
                allowBlank: !uiElement.required,        
                width: me.controlWidth,                       
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,     
                margin: '5, 0, 5, 0',
                value: uiElement.values[0],
                minValue: 1,
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: uiElement.description
                      });
                    }
                }
            };    
        },

        textUI: function(uiElement, me, id) {
        return {
                xtype: 'textfield',
                fieldLabel: uiElement.inputText,
                itemId: id,
                allowBlank: !uiElement.required,
                width: me.controlWidth,                      
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,     
                margin: '5, 0, 5, 0',
                value: uiElement.values[0],
                minValue: 1,
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: uiElement.description
                      });
                    }
                }
            };    
        },

        dateUI: function(uiElement, me, id, currValue, readOnly) {            
            var dateField = {
                xtype: 'datefield',
                allowBlank: !uiElement.required,          
                width: me.controlWidth,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                  
                submitFormat: uiElement.values[0],
                format: 'm/d/Y',
                fieldLabel: uiElement.inputText,
                itemId: id,
                margin: '5, 0, 5, 0',
                readOnly: readOnly,
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: uiElement.description
                      });
                    }
                },        
                value: currValue
            };    
            return dateField;
        },
        
        
        dropdownUI: function(uiElement, id, me, dropdownValues) {
            var currValue = '';
            if ( (typeof uiElement.values[0] !== 'undefined') && (uiElement.values[0] !== 'value0') ) {     
                currValue = uiElement.values[0];
            }        
            return {
                xtype: 'combobox',
                fieldLabel: uiElement.inputText,
                store: dropdownValues,
                itemId: id,
                allowBlank: !uiElement.required,   
                width: me.controlWidth,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                margin: '5, 0, 5, 0',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: uiElement.description
                      });
                    },
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            };
        },
        
        
        multipleChoiceUI: function(uiElement, id, me) {
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof uiElement.values[0] !== 'undefined') && (uiElement.values[0] !== 'value0') ) {     
                currValue = uiElement.values[0];
            }        
            if ( (typeof uiElement.type !== 'undefined') && (uiElement.type === "MultipleUnique") ) {
                var values = uiElement.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            return {
                xtype: 'combobox',
                fieldLabel: uiElement.inputText,
                store: dropdownValues,
                itemId: id,
                allowBlank: !uiElement.required,    
                width: me.controlWidth,                   
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                margin: '5, 0, 5, 0',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: uiElement.description
                      });
                    },
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            };
        },
        //==========================================================================================================        
        checkIfBaseParamsPresent: function(me, methodComboBox) {
            if ( me.modelId === 0 ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Please assign Data Model to the study first.");
                me.destroy();
            }           
            var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
            if ( methodRec === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
                me.fieldStore.clearFilter(true);
                me.destroy();
            }              
            me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
            var index = me.selectedStudyRecord.get('idKey');
            var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
            if ( uniqueField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
                me.destroy();
            } 
            index = me.selectedStudyRecord.get('idDate');
            if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
                me.destroy();            
            }      
            var startDate = me.selectedStudyRecord.get('startTS');
            if ( startDate === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Study Start Date is not defined.");
                me.destroy();  
            }       
            var endDate = me.selectedStudyRecord.get('endTS');
            if ( endDate === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Study End Date is not defined.");
                me.destroy(); 
            }             
            return methodRec;
        },
        
        
        
        //==========================================================================================================
        drawStepUI: function(params, me, fieldCont1, fieldCont2) {
            var height = me.heightDefault;
            var singleLine = 24;           
            var panel = fieldCont1;
            if ( params.active === false ) return;
            if ( typeof params.stepDesc !== 'undefined' ) {                           
                panel.add( {
                        xtype: 'label',
                        text: params.stepDesc,
                        forId: 'label1'
                    });                  
            }            
            //-------------------------------------------------------------------------------------------------------            
            if ( typeof params.inputs.studyUniqueId !== 'undefined' ) { 
                var index = me.selectedStudyRecord.get('idKey');
                me.fieldStore.clearFilter(true);                  
                var uniqueField = me.fieldStore.findRecord('idModelColumn', index);
                if (uniqueField === null) {
                    delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
                    Ext.ComponentQuery.query('#logisticRegrContainer')[0].cleanup(); //?
                }            
                var currValue = uniqueField.get("name");                        
                panel.add( {
                        xtype: 'textfield',
                        readOnly: true,
                        fieldLabel: params.inputs.studyUniqueId.inputText,
                        name: 'studyUniqueId',
                        itemId: 'studyUniqueId',                      
                        allowBlank: !params.inputs.studyUniqueId.required,  
                        width: me.controlWidth,                             
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                       
                        value: currValue
                    });  
                height += singleLine;
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}
            }    
            //-------------------------------------------------------------------------------------------------------                      
            if ( typeof params.inputs.sequencingVariableSelection !== 'undefined' ) {     
                var index = me.selectedStudyRecord.get('idDate');
                me.fieldStore.clearFilter(true);                    
                var sequenceField = me.fieldStore.findRecord('idModelColumn', index);      
                if ( sequenceField === null ) {
                    delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exit.");
                    me.destroy();    
                }                 
                var currValue = sequenceField.get("name");                    
                panel.add( {
                        xtype: 'textfield',
                        readOnly: true,
                        fieldLabel: params.inputs.sequencingVariableSelection.inputText,
                        name: 'seqVariable',
                        itemId: 'seqVariable',
                        allowBlank: !params.inputs.sequencingVariableSelection.required,     
                        width: me.controlWidth,                            
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,                       
                        value: currValue
                    });   
                height += singleLine;   
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}       
            }            
            //Newly added-------------------------------------------------------------------------------------------------------                      
            
            
            
            
            
            //-------------------------------------------------------------------------------------------------------             
            if ( typeof params.inputs.model !== 'undefined' ) {
                if ( typeof params.inputs.model.editable === 'undefined' ||  params.inputs.model.editable === true ) {
                    var loading_flag = false;
                    var cmb = Ext.create({
                        xtype: 'combobox',
                        fieldLabel: 'LR Formula',
                        store: {},
                        itemId: 'lrFormula',
                        allowBlank: false,
                        labelWidth: me.labelWidth,
                        labelAlign: me.labelAlign,    
                        width: me.controlWidth,              
                        displayField: 'name',
                        valueField: 'idLogregr',
                        queryMode: 'remote',
                        value: {},
                        multiSelect: false,
                        forceSelection: false,
                        idLogregr: 0,
                        logregrStore: {},
                        listeners: {
                            'select': function(cmb, rec, idx) {
                                this.idLogregr = cmb.getValue();
                            },
                            'afterrender': function() {
                                if ( loading_flag === true ) {
                                    this.setLoading(true);
                                }
                            }
                        }
                    });
                    panel.add(cmb);     
                    loading_flag = true;
                    delta3.utils.GlobalFunc.getLRFforModel(me.modelId, finalDisplay);
                    function finalDisplay(resp, options) {
                        loading_flag = false;
                        cmb.setLoading(false);
                        var values = [];
                        for ( var i=0; i<resp.length; i++) {
                            values[i] = {idLogregr: resp[i].model.id, name: resp[i].model.name};
                        }
                        var cbStore =  Ext.create('Ext.data.Store', {
                            fields: ['idLogregr', 'name'], 
                            data: values 
                        });
                        var lrComboBox = Ext.ComponentQuery.query('#lrFormula')[0];
                        lrComboBox.setStore(cbStore);
                        lrComboBox.setValue(params.inputs.model.name);   
                        lrComboBox.logregrStore = resp;      
                    }    
                    height += singleLine;  
                    if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                    
                }
            }                           
            //-------------------------------------------------------------------------------------------------------             
            if ( typeof params.inputs.outcomeVariableSelection !== 'undefined') {
                if ( typeof params.inputs.outcomeVariableSelection.fieldClass === 'undefined' ) { // backwards compatibility
                    params.inputs.outcomeVariableSelection.fieldClass = 'Outcome';
                }                
                var cflag = 0, variablesList = '';
                for (var i = 0; i < params.inputs.outcomeVariableSelection.values.length; i++) {
                    if ( params.inputs.outcomeVariableSelection.values[i] !== 'value0' ) {
                        if ( cflag > 0 ) {
                            variablesList += ',';
                        }
                        cflag++;
                        variablesList += params.inputs.outcomeVariableSelection.values[i];
                    }
                }            
                me.fieldStore.clearFilter(true);                  
                me.fieldStore.filter('fieldClass', params.inputs.outcomeVariableSelection.fieldClass); 
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);                 
                
                var outcomeStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.StudyVariableModel.getFields());
                if ( params.inputs.outcomeVariableSelection.type === 'ArrayList<String>' ) {
                    panel.add(Ext.create('delta3.view.study.VariableSelector', {sourceStore: outcomeStore, variablesList: variablesList, fieldClass: params.inputs.outcomeVariableSelection.fieldClass.replace(/\s+/, "") }));      
                    height += 9*singleLine;       
                } else {
                    var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');   
                    panel.add(delta3.view.study.UtilFunc.dropdownUI(params.inputs.outcomeVariableSelection, 'variableOutcome', me, outcomeDropdownValues));                                
                    height += singleLine;                          
                }
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }              
            //-------------------------------------------------------------------------------------------------------                  
            if ( typeof params.inputs.dependentVariableSelection !== 'undefined' ) { 
                me.fieldStore.clearFilter(true);        
                me.fieldStore.filter('fieldClass', 'Outcome');
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);         
                var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');   
                panel.add(delta3.view.study.UtilFunc.dropdownUI(params.inputs.dependentVariableSelection, 'depVariable', me, outcomeDropdownValues));                                
                height += singleLine;    
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }    
            //-------------------------------------------------------------------------------------------------------            
            if ( typeof params.inputs.exposureVariableSelection !== 'undefined' ) {            
                me.fieldStore.clearFilter(true);        
                me.fieldStore.filter('fieldClass', 'Treatment');
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);         
                var treatmentDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');                   
                panel.add(delta3.view.study.UtilFunc.dropdownUI(params.inputs.exposureVariableSelection, 'expVariable', me, treatmentDropdownValues));                                            
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }             
            //-------------------------------------------------------------------------------------------------------
            if ( typeof params.inputs.studyStartDate !== 'undefined' ) {     
                me.fieldStore.clearFilter(true);    
                if ( (typeof params.inputs.studyStartDate.values[0] !== 'undefined') 
                        && (params.inputs.studyStartDate.values[0] !== 'Y-m-d H:i:s.u') ) {     
                    me.startDate = params.inputs.studyStartDate.values[0];
                } else {
                    me.startDate = me.selectedStudyRecord.get('startTS');
                    if ( me.startDate === null ) {
                        delta3.utils.GlobalFunc.showDeltaMessage("Study Start Date is not defined.");
                        me.destroy();    
                    }                  
                }             
                panel.add(delta3.view.study.UtilFunc.dateUI(params.inputs.studyStartDate, me, 'studyStartDate', me.selectedStudyRecord.get('startTS'), true));        
                height += singleLine;    
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }     
            if ( typeof params.inputs.studyEndDate !== 'undefined' ) {
                me.fieldStore.clearFilter(true);    
                if ( (typeof params.inputs.studyEndDate.values[0] !== 'undefined') 
                        && (params.inputs.studyEndDate.values[0] !== 'Y-m-d H:i:s.u') ) {     
                    me.endDate = params.inputs.studyEndDate.values[0];
                } else {
                    me.endDate = me.selectedStudyRecord.get('endTS');
                    if ( me.endDate === null ) {
                        delta3.utils.GlobalFunc.showDeltaMessage("Study End Date is not defined.");
                        me.destroy();    
                    }                   
                }     
                panel.add(delta3.view.study.UtilFunc.dateUI(params.inputs.studyEndDate, me, 'studyEndDate', me.selectedStudyRecord.get('endTS'), true));
                height += singleLine;         
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }      
            //-------------------------------------------------------------------------------------------------------               
            if ( typeof params.inputs.followupDateSelection !== 'undefined' ) {
                var followupDateValue = '';    
                if ( (typeof params.inputs.followupDateSelection.values[0] !== 'undefined') 
                        && (params.inputs.followupDateSelection.values[0] !== 'Y-m-d H:i:s.u') ) {     
                    followupDateValue = params.inputs.followupDateSelection.values[0];
                }     
                panel.add(delta3.view.study.UtilFunc.dateUI(params.inputs.followupDateSelection, me, 'followupDate', followupDateValue, false));
                height += singleLine;         
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }       
            //Newly added -------------------------------------------------------------------------------------------------------               
            
            
            
            
            
            //-------------------------------------------------------------------------------------------------------                  
            if ( typeof params.inputs.exposureDateSelection !== 'undefined' ) {     
                me.fieldStore.clearFilter(true);        
                me.fieldStore.filter('fieldClass', 'Sequencer');
                me.fieldStore.filter('fieldKind', 'Date');                
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);         
                var expDateDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');   
                panel.add(delta3.view.study.UtilFunc.dropdownUI(params.inputs.exposureDateSelection, 'exposureDate', me, expDateDropdownValues));                                
                height += singleLine;       
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }              
            //-------------------------------------------------------------------------------------------------------                  
            if ( typeof params.inputs.outcomeDateSelection !== 'undefined' ) {     
                me.fieldStore.clearFilter(true);        
                me.fieldStore.filter('fieldClass', 'Sequencer');
                me.fieldStore.filter('fieldKind', 'Date');                
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);         
                var outcomeDateDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');   
                panel.add(delta3.view.study.UtilFunc.dropdownUI(params.inputs.outcomeDateSelection, 'outcomeDate', me, outcomeDateDropdownValues));                                
                height += singleLine;         
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }   
            
            //-------------------------------------------------------------------------------------------------------                  
            
            if ( typeof params.inputs.lastSeenDateSelection !== 'undefined' ) {     
                me.fieldStore.clearFilter(true);        
                me.fieldStore.filter('fieldClass', 'Sequencer');
                me.fieldStore.filter('fieldKind', 'Date');                
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);         
                
                var lastSeenDateDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');   
                panel.add(delta3.view.study.UtilFunc.dropdownUI(params.inputs.lastSeenDateSelection, 'lastSeenDate', me, lastSeenDateDropdownValues));                                
                height += singleLine;         
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }
            
            
            
            
            
            //-------------------------------------------------------------------------------------------------------               
            if ( typeof params.inputs.durationType !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.durationType, 'durationType', me));
                height += singleLine;       
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }
            
            
            
            
            //-------------------------------------------------------------------------------------------------------           
            if ( typeof params.inputs.independentVariableSelection !== 'undefined') {
                if ( typeof params.inputs.independentVariableSelection.fieldClass === 'undefined' ) { // backwards compatibility
                    params.inputs.independentVariableSelection.fieldClass = 'Risk Factor';
                }                  
                var cflag = 0, variablesList = '';
                for (var i = 0; i < params.inputs.independentVariableSelection.values.length; i++) {
                    if ( params.inputs.independentVariableSelection.values[i] !== 'value0' ) {
                        if ( cflag > 0 ) {
                            variablesList += ',';
                        }
                        cflag++;
                        variablesList += params.inputs.independentVariableSelection.values[i];
                    }
                }            
                me.fieldStore.clearFilter(true);                    
                me.fieldStore.filter('fieldClass', params.inputs.independentVariableSelection.fieldClass);
                me.fieldStore.filter('idModel', me.modelId);
                me.fieldStore.filter('insertable', true);                    
                var riskFactorStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.StudyVariableModel.getFields());
                panel.add( 
                   Ext.create('delta3.view.study.VariableSelector', {sourceStore: riskFactorStore, variablesList: variablesList, fieldClass: params.inputs.independentVariableSelection.fieldClass.replace(/\s+/, "")})
                );  
                height += 9*singleLine;   
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }    
            //-------------------------------------------------------------------------------------------------------                 
            if ( typeof params.inputs.caliper !== 'undefined' ) {                           
                panel.add(delta3.view.study.UtilFunc.textUI(params.inputs.caliper, me, 'caliper'));
                height += singleLine;   
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }    
            
            
            
            if ( typeof params.inputs.caliperType !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.caliperType, 'caliperType', me));              
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }
            
            
            //-------------------------------------------------------------------------------------------------------                 
            if ( typeof params.inputs.matchNumber !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.matchNumber, me, 'matchNumber'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }    
            
            //-------------------------------------------------------------------------------------------------------                 
            if ( typeof params.inputs.matchNumber !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.matchNumber, me, 'matchNumber'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }   
            
            //newly added-------------------------------------------------------------------------------------------------------                 
            if ( typeof params.inputs.matchDays !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.matchDays, me, 'matchDays'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }   
            
            
            /* newly added-------------------------------------------------------------------------------------------------------                 
            if ( typeof params.inputs.followupDuration !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.followupDuration, me, 'followupDuration'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }  */  
            
            
            /* newly added follow up duration string------------------------------------------------------------------------------------------------------- 
            if ( typeof params.inputs.followupDuration !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.followupDuration, 'followupDuration', me));              
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }*/
            
            // newly added follow up duration string text-------------------------------------------------------------------------------------------------------                 
            if ( typeof params.inputs.followupDuration !== 'undefined' ) {                           
                panel.add(delta3.view.study.UtilFunc.textUI(params.inputs.followupDuration, me, 'followupDuration'));
                height += singleLine;   
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }    
            
            
            
            //newly added at stratification----------------
            if ( typeof params.inputs.subClasses !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.subClasses, me, 'subClasses'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            } 
            //----------newly added
            if ( typeof params.inputs.lowerTrim !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.lowerTrim, me, 'lowerTrim'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            } 
            //----------newly added
            if ( typeof params.inputs.upperTrim !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.upperTrim, me, 'upperTrim'));    
                height += singleLine;      
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            } 
            
            
            
            
            if ( typeof params.inputs.matchType !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.matchType, 'matchType', me));  
                height += singleLine;    
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }      
            
            if ( typeof params.inputs.matchOrder !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.matchOrder, 'matchOrder', me));     
                height += singleLine;     
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }      
            if ( typeof params.inputs.replaceMatches !== 'undefined' ) {         
                panel.add(delta3.view.study.UtilFunc.checkBoxUI(params.inputs.replaceMatches, me, 'replaceMatches')); 
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }               
            //-------------------------------------------------------------------------------------------------------            
            if ( typeof params.inputs.matchSet !== 'undefined' ) {     
                var loading_flag = false;
                var cmb = Ext.create({
                    xtype: 'combobox',
                    fieldLabel: 'Match Set',
                    store: {},
                    itemId: 'matchSet',
                    allowBlank: false,                     
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,    
                    width: me.controlWidth,                
                    displayField: 'name',
                    valueField: 'name',
                    queryMode: 'local',
                    value: '',
                    multiSelect: false,
                    forceSelection: false,
                    idMatchset: 0,
                    logregrStore: {},
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            this.idMatchset = cmb.getValue();
                        },
                        'afterrender': function() {
                            if ( loading_flag === true ) {
                                this.setLoading(true);
                            }
                        }
                    }
                });
                panel.add(cmb);    
                loading_flag = true;
                delta3.utils.GlobalFunc.getMatchSetForStudy(me.studyId, finalDisplay);
                function finalDisplay(resp, options) {
                    loading_flag = false;
                    cmb.setLoading(false);
                    var values = [];
                    for ( var i=0; i<resp.matchSets.length; i++) {
                        values[i] = {idMatchset: resp.matchSets[i].idMatchset, name: resp.matchSets[i].description};
                    }
                    var cbStore =  Ext.create('Ext.data.Store', {
                        fields: ['idMatchset', 'description'], 
                        data: values 
                    });
                    var msComboBox = Ext.ComponentQuery.query('#matchSet')[0];
                    msComboBox.setStore(cbStore);
                    if ( params.inputs.matchSet.values[0] !== 'value0' ) {                    
                        msComboBox.setValue(params.inputs.matchSet.values[0]); 
                    }
                }    
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }                  
            
            //-------------------------------------------------------------------------------------------------------               
            if ( typeof params.inputs.reportingPeriod !== 'undefined' ) {    
               panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.reportingPeriod, 'reportingPeriod', me));
               height += singleLine; 
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }  
            if ( typeof params.inputs.estimationPscore !== 'undefined' ) {    
               panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.estimationPscore, 'estimationPscore', me));
               height += singleLine;   
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }        
            
            //-------------------------------------------------------------------------------------------------------             
            if ( typeof params.inputs.dataSplitOption !== 'undefined' ) {  
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.dataSplitOption, 'dataSplitOption', me));  
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }             
            //-------------------------------------------------------------------------------------------------------            
            if ( typeof params.inputs.alphaError !== 'undefined' ) { 
               panel.add(delta3.view.study.UtilFunc.textUI(params.inputs.alphaError, me, 'alphaError'));   
               height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }
            if ( typeof params.inputs.betaError !== 'undefined' ) { 
               panel.add(delta3.view.study.UtilFunc.textUI(params.inputs.betaError, me, 'betaError'));   
               height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }       
            if ( typeof params.inputs.oddsRatio !== 'undefined' ) { 
               panel.add(delta3.view.study.UtilFunc.textUI(params.inputs.oddsRatio, me, 'oddsRatio'));   
               height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }    
            if ( typeof params.inputs.numberPeriods !== 'undefined' ) { 
               panel.add(delta3.view.study.UtilFunc.textUI(params.inputs.numberPeriods, me, 'numberPeriods'));   
               height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
            }           
            if ( typeof params.inputs.alphaSpending !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.checkBoxUI(params.inputs.alphaSpending, me, 'alphaSpending'));     
                height += singleLine;   
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }     
            if ( typeof params.inputs.commonSupport !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.checkBoxUI(params.inputs.commonSupport, me, 'commonSupport'));  
                height += singleLine;    
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }                
            if ( typeof params.inputs.duplicatePscores !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.checkBoxUI(params.inputs.duplicatePscores, me, 'duplicatePscores'));   
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }                
            if ( typeof params.inputs.automaticVariableSelection !== 'undefined' ) {               
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.automaticVariableSelection, 'automaticVariableSelection', me));              
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }   
            
            //---------------newly addded
            if ( typeof params.inputs.allowStabilization !== 'undefined' ) {               
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.allowStabilization, 'allowStabilization', me));              
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }  
            
            //---------------newly addded
            if ( typeof params.inputs.allowTrimming !== 'undefined' ) {               
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.allowTrimming, 'allowTrimming', me));              
                height += singleLine;  
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            }  
            
            
            
            //-------------------------------
            if ( typeof params.inputs.chunkingPeriod !== 'undefined' ) {    
                panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.chunkingPeriod, 'chunkingPeriod', me));  
                height += singleLine;    
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}                
            } 
            //-------------------------------------------------------------------------------------------------------            
            if ( typeof params.inputs.rollingWindowUse !== 'undefined' ) {
                panel.add({
                    allowBlank: !params.inputs.rollingWindowUse.required,   
                    width: me.controlWidth,                       
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                 
                    xtype: 'checkbox',
                    fieldLabel: params.inputs.rollingWindowUse.inputText,
                    inputValue: true,
                    value: params.inputs.rollingWindowUse.values[0],
                    listeners: {
                        render: function(c) {
                          new Ext.ToolTip({
                            target: c.getEl(),
                            html: params.inputs.rollingWindowUse.description
                          });
                        },
                        change: function(c) {
                            if ( c.getValue() === true ) {
                               Ext.ComponentQuery.query('#rollingWindowParams')[0].setVisible(true);  
                            } else {
                               Ext.ComponentQuery.query('#rollingWindowParams')[0].setVisible(false);  
                            }
                        }
                    },                 
                    itemId: 'rollingWindowUseCheckbox'                           
                });     
                height += singleLine;        
                if ( me.height < height ) {panel = fieldCont2; height = 0; me.width *= 2;}               
//                if ( (typeof params.inputs.rollingWindowTimeUnit === 'undefined')
//                    || (typeof params.inputs.rollingWindowUnitQuantity === 'undefined') ) {
//                    delta3.utils.GlobalFunc.showDeltaMessage('Rolling Window parameters are missing in the configuration file.');
//                    me.destroy(); 
//                }                
//                panel.add({
//                    xtype: 'fieldset', 
//                    layout: 'vbox', 
//                    itemId: 'rollingWindowParams',
//                    hidden: !Boolean(homedepotparams.inputs.rollingWindowUse.values[0]),
//                    items:
//                        [
//                            panel.add(delta3.view.study.UtilFunc.dateUI(params.inputs.rollingWindowEndDate, me, 'rollingWindowEndDate', currValue, false)),                
//                            panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.rollingWindowDumpWindowTimeUnit,'rollingWindowDumpWindowTimeUnit', me)),
//                            panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.rollingWindowDumpNumberUnits, me, 'rollingWindowDumpNumberUnits')),
//                            panel.add(delta3.view.study.UtilFunc.multipleChoiceUI(params.inputs.rollingWindowScoringWindow, 'rollingWindowScoringWindow', me)),
//                            panel.add(delta3.view.study.UtilFunc.numberUI(params.inputs.rollingWindowScoringWindowUnits, me, 'rollingWindowScoringWindowUnits'))                       
//                        ]});   
            }     
            return height;           
        },    
        //==========================================================================================================        
        retrieveParams: function(thisWin, params) {          
                          
            // this function retrieves paramaters from all tabs based on field #id
            // having multiple paramaters of the same id would create problems
            
            
            
            var index = thisWin.selectedStudyRecord.get('idKey');
            thisWin.fieldStore.clearFilter(true);
            
            
            var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);  
            if ( typeof params.inputs.studyUniqueId !== 'undefined' ) {
                params.inputs.studyUniqueId.values[0] = uniqueField.get("name");
            }
            
            var theOutcomeItems = Ext.ComponentQuery.query('#variableOutcome')[0];    
            if ( typeof params.inputs.outcomeVariableSelection !== 'undefined' && typeof theOutcomeItems !== 'undefined' && theIndepItems !== null ) {
                var listOfOutcomeVariables = theOutcomeItems.getRawValue();
                var arrayOfOutcomeVariables = listOfOutcomeVariables.split(",");              
                params.inputs.outcomeVariableSelection.values = [];              
                for (var i = 0; i < arrayOfOutcomeVariables.length; i++) {
                    params.inputs.outcomeVariableSelection.values[i] = arrayOfOutcomeVariables[i];
                }
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(params.inputs.outcomeVariableSelection.values.toString()));                                   
            }
            var theIndepItems = Ext.ComponentQuery.query('#variableRiskFactor')[0];    
            if ( typeof params.inputs.independentVariableSelection !== 'undefined' && typeof theIndepItems !== 'undefined' && theIndepItems !== null ) {
                var listOfIndepVariables = theIndepItems.getRawValue();
                var arrayOfIndepVariables = listOfIndepVariables.split(",");              
                params.inputs.independentVariableSelection.values = [];              
                for (var i = 0; i < arrayOfIndepVariables.length; i++) {
                    params.inputs.independentVariableSelection.values[i] = arrayOfIndepVariables[i];
                }
            }
            if ( typeof params.inputs.dependentVariableSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#depVariable')[0] !== 'undefined') {        
                params.inputs.dependentVariableSelection.values[0] = Ext.ComponentQuery.query('#depVariable')[0].value;     
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(Ext.ComponentQuery.query('#depVariable')[0].value));                   
            }
            if ( typeof params.inputs.exposureVariableSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#expVariable')[0] !== 'undefined') {        
                params.inputs.exposureVariableSelection.values[0] = Ext.ComponentQuery.query('#expVariable')[0].value;     
                thisWin.selectedStudyRecord.set('idTreatment', delta3.utils.GlobalFunc.lookupFieldId(Ext.ComponentQuery.query('#expVariable')[0].value));                    
            }      
            if ( typeof params.inputs.followupDateSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#followupDate')[0] !== 'undefined' ) {
                var theValue = Ext.ComponentQuery.query('#followupDate')[0].getValue();
                if ( theValue === null ) {
                    theValue = thisWin.selectedStudyRecord.get('endTS');
                    Ext.ComponentQuery.query('#followupDate')[0].setValue(thisWin.selectedStudyRecord.get('endTS'));
                }
                if ( theValue > new Date() ) {
                    return "Followup Date cannot be in the future";
                }
                if ( theValue < thisWin.selectedStudyRecord.get('startTS') ) {
                    return "Followup Date cannot be prior to Study Start Date";
                }
                if ( theValue > thisWin.selectedStudyRecord.get('endTS') ) {
                    Ext.ComponentQuery.query('#followupDate')[0].setValue(thisWin.selectedStudyRecord.get('endTS'));
                    return "Followup Date cannot pass Study End Date.\nUsing Study End Date as default.";
                }                
                params.inputs.followupDateSelection.values[0] = delta3.utils.GlobalFunc.DateStringYYYYMMDD(theValue);
            }          
            if ( typeof params.inputs.studyStartDate !== 'undefined' && typeof Ext.ComponentQuery.query('#studyStartDate')[0] !== 'undefined' ) {
                params.inputs.studyStartDate.values[0] = delta3.utils.GlobalFunc.DateStringYmdHisu(Ext.ComponentQuery.query('#studyStartDate')[0].value);
            }
            if ( typeof params.inputs.studyEndDate !== 'undefined' && typeof Ext.ComponentQuery.query('#studyEndDate')[0] !== 'undefined' ) {   
                params.inputs.studyEndDate.values[0] = delta3.utils.GlobalFunc.DateStringYmdHisu(Ext.ComponentQuery.query('#studyEndDate')[0].value);
            }
            if ( typeof params.inputs.sequencingVariableSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#seqVariable')[0] !== 'undefined' ) {                   
                params.inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;
            }      
            
            
            if ( typeof params.inputs.exposureDateSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#exposureDate')[0] !== 'undefined') {     
                params.inputs.exposureDateSelection.values[0] = Ext.ComponentQuery.query('#exposureDate')[0].value;                                         
            }  
            if ( typeof params.inputs.outcomeDateSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#outcomeDate')[0] !== 'undefined') {     
                params.inputs.outcomeDateSelection.values[0] = Ext.ComponentQuery.query('#outcomeDate')[0].value;                                         
            }               
            
            if ( typeof params.inputs.lastSeenDateSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#lastSeenDate')[0] !== 'undefined') {     
                params.inputs.lastSeenDateSelection.values[0] = Ext.ComponentQuery.query('#lastSeenDate')[0].value;                                         
            }               
            
            
            if ( typeof params.inputs.durationType !== 'undefined' && typeof Ext.ComponentQuery.query('#durationType')[0] !== 'undefined') {     
                params.inputs.durationType.values[0] = Ext.ComponentQuery.query('#durationType')[0].value;                                         
            }                            
            if ( typeof params.inputs.reportingPeriod !== 'undefined' && typeof Ext.ComponentQuery.query('#reportingPeriod')[0] !== 'undefined' ) {                   
                params.inputs.reportingPeriod.values[0] = Ext.ComponentQuery.query('#reportingPeriod')[0].value;      
            }
            if ( typeof params.inputs.estimationPeriod !== 'undefined' && typeof Ext.ComponentQuery.query('#estimationPeriod')[0] !== 'undefined' ) {                   
                params.inputs.estimationPeriod.values[0] = Ext.ComponentQuery.query('#estimationPeriod')[0].value;      
            }            
            if ( typeof params.inputs.caliper !== 'undefined' && typeof Ext.ComponentQuery.query('#caliper')[0] !== 'undefined' ) {                   
                params.inputs.caliper.values[0] = Ext.ComponentQuery.query('#caliper')[0].value;    
            }
            if ( typeof params.inputs.caliperType !== 'undefined' && typeof Ext.ComponentQuery.query('#caliperType')[0] !== 'undefined' ) {    
                params.inputs.caliperType.values[0] = Ext.ComponentQuery.query('#caliperType')[0].value;                    
            }        
 
            if ( typeof params.inputs.matchNumber !== 'undefined' && typeof Ext.ComponentQuery.query('#matchNumber')[0] !== 'undefined' ) {                   
                params.inputs.matchNumber.values[0] = Ext.ComponentQuery.query('#matchNumber')[0].value;    
            }
            if ( typeof params.inputs.matchDays !== 'undefined' && typeof Ext.ComponentQuery.query('#matchDays')[0] !== 'undefined' ) {                   
                params.inputs.matchDays.values[0] = Ext.ComponentQuery.query('#matchDays')[0].value;    
            }
            
            
            
            if ( typeof params.inputs.followupDuration !== 'undefined' && typeof Ext.ComponentQuery.query('#followupDuration')[0] !== 'undefined' ) {                   
                params.inputs.followupDuration.values[0] = Ext.ComponentQuery.query('#followupDuration')[0].value;    
            }
            
            if ( typeof params.inputs.matchType !== 'undefined' && typeof Ext.ComponentQuery.query('#matchType')[0] !== 'undefined' ) {                   
                params.inputs.matchType.values[0] = Ext.ComponentQuery.query('#matchType')[0].value;    
            }             
            if ( typeof params.inputs.replaceMatches !== 'undefined' && typeof Ext.ComponentQuery.query('#replaceMatches')[0] !== 'undefined' ) {    
                params.inputs.replaceMatches.values[0] = Ext.ComponentQuery.query('#replaceMatches')[0].value;                    
            }   
            if ( typeof params.inputs.matchOrder !== 'undefined' && typeof Ext.ComponentQuery.query('#matchOrder')[0] !== 'undefined' ) {                   
                params.inputs.matchOrder.values[0] = Ext.ComponentQuery.query('#matchOrder')[0].value;    
            }               
            if ( typeof params.inputs.dataSplitOption !== 'undefined' && typeof Ext.ComponentQuery.query('#dataSplitOption')[0] !== 'undefined' ) {                   
                params.inputs.dataSplitOption.values[0] = Ext.ComponentQuery.query('#dataSplitOption')[0].value;    
            }               
            if ( typeof params.inputs.estimationPscore !== 'undefined' && typeof Ext.ComponentQuery.query('#estimationPscore')[0] !== 'undefined' ) {    
                params.inputs.estimationPscore.values[0] = Ext.ComponentQuery.query('#estimationPscore')[0].value;                    
            }                
            if ( typeof params.inputs.alphaError !== 'undefined' && typeof Ext.ComponentQuery.query('#alphaError')[0] !== 'undefined' ) {                   
                params.inputs.alphaError.values[0] = Ext.ComponentQuery.query('#alphaError')[0].value;
            }
            if ( typeof params.inputs.betaError !== 'undefined' && typeof Ext.ComponentQuery.query('#betaError')[0] !== 'undefined' ) {                   
                params.inputs.betaError.values[0] = Ext.ComponentQuery.query('#betaError')[0].value;
            }   
            if ( typeof params.inputs.oddsRatio !== 'undefined' && typeof Ext.ComponentQuery.query('#oddsRatio')[0] !== 'undefined' ) {                   
                params.inputs.oddsRatio.values[0] = Ext.ComponentQuery.query('#oddsRatio')[0].value;
            }   
            if ( typeof params.inputs.numberPeriods !== 'undefined' && typeof Ext.ComponentQuery.query('#numberPeriods')[0] !== 'undefined' ) {                   
                params.inputs.numberPeriods.values[0] = Ext.ComponentQuery.query('#numberPeriods')[0].value;
            }            
            if ( typeof params.inputs.alphaSpending !== 'undefined' && typeof Ext.ComponentQuery.query('#alphaSpending')[0] !== 'undefined' ) {                   
                params.inputs.alphaSpending.values[0] = Ext.ComponentQuery.query('#alphaSpending')[0].value;    
            }
            if ( typeof params.inputs.commonSupport !== 'undefined' && typeof Ext.ComponentQuery.query('#commonSupport')[0] !== 'undefined' ) {    
                params.inputs.commonSupport.values[0] = Ext.ComponentQuery.query('#commonSupport')[0].value;                    
            }   
            if ( typeof params.inputs.duplicatePscores !== 'undefined' && typeof Ext.ComponentQuery.query('#duplicatePscores')[0] !== 'undefined' ) {    
                params.inputs.duplicatePscores.values[0] = Ext.ComponentQuery.query('#duplicatePscores')[0].value;                    
            }    
            if ( typeof params.inputs.automaticVariableSelection !== 'undefined' && typeof Ext.ComponentQuery.query('#automaticVariableSelection')[0] !== 'undefined' ) {    
                params.inputs.automaticVariableSelection.values[0] = Ext.ComponentQuery.query('#automaticVariableSelection')[0].value;                    
            }    
            if ( typeof params.inputs.model !== 'undefined' && typeof Ext.ComponentQuery.query('#lrFormula')[0] !== 'undefined' ) {    
                var thisCB = Ext.ComponentQuery.query('#lrFormula')[0];
                 for (var i=0; i<thisCB.logregrStore.length; i++) {
                    if ( thisCB.logregrStore[i].model.id === thisCB.idLogregr ) {
                        params.inputs.model = thisCB.logregrStore[i].model;      
                        break;
                    }
                }      
            }
            if ( typeof params.inputs.matchSet !== 'undefined' && typeof Ext.ComponentQuery.query('#matchSet')[0] !== 'undefined' ) {    
                params.inputs.matchSet.values[0] = Ext.ComponentQuery.query('#matchSet')[0].value;          
            }   
            if ( typeof params.inputs.chunkingPeriod !== 'undefined' && typeof Ext.ComponentQuery.query('#chunkingPeriod')[0] !== 'undefined' ) {                   
                params.inputs.chunkingPeriod.values[0] = Ext.ComponentQuery.query('#chunkingPeriod')[0].value; 
                thisWin.selectedStudyRecord.set('chunkingBy', Ext.ComponentQuery.query('#chunkingPeriod')[0].value);
            }               
            if ( typeof params.inputs.rollingWindowUse !== 'undefined' && typeof Ext.ComponentQuery.query('#rollingWindowUseCheckbox')[0] !== 'undefined' ) {       
                params.inputs.rollingWindowUse.values[0] = Ext.ComponentQuery.query('#rollingWindowUseCheckbox')[0].value; 
//                params.inputs.rollingWindowEndDate.values[0] = Ext.ComponentQuery.query('#rollingWindowEndDate')[0].value; 
//                params.inputs.rollingWindowDumpWindowTimeUnit.values[0] = Ext.ComponentQuery.query('#rollingWindowDumpWindowTimeUnit')[0].value; 
//                params.inputs.rollingWindowDumpNumberUnits.values[0] = Ext.ComponentQuery.query('#rollingWindowDumpNumberUnits')[0].value; 
//                params.inputs.rollingWindowScoringWindow.values[0] = Ext.ComponentQuery.query('#rollingWindowScoringWindow')[0].value;                
//                params.inputs.rollingWindowScoringWindowUnits.values[0] = Ext.ComponentQuery.query('#rollingWindowScoringWindowUnits')[0].value;  
            }
            return '';
        }          
    }
});


