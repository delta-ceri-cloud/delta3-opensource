/* 
 * Correlation Grid
 */

Ext.define('delta3.view.ds.CorrelationGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.correlationGrid',
    itemId: 'correlationGrid',
    autoScroll: false,
    renderTo: document.body,
    region: 'north',
    selType: 'rowmodel',
    requires: [
        'delta3.model.DStatsModel',
        'Ext.data.Store',
        'Ext.data.Model',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column'
    ],
    border: false,
    tbar: [{
        text: 'Export as CSV',
        iconCls: 'exportCSV-icon16',
        tooltip: delta3.utils.Tooltips.mbBtnExcel,
        handler: function(b, e) {
            b.up('grid').exportGrid(ModelData.modelSelected.name);
        }
    }],    
    dStats: {},
    fieldStore: {},
    initComponent: function () {
        var me = this;
        var modelFields = me.buildGridModelFields(me.dStats[1].statistics);
        me.columns = me.buildGridColumns(me.dStats[1].statistics);
        me.store = me.buildStore(me.dStats[1].statistics, modelFields);              
        me.callParent();
    },
    buildStore: function (data, modelFields) {
        var dim = Math.sqrt(data.length);        
        var store = new Ext.data.Store({
            autoLoad: true,
            model: 'delta3.model.DStatsModel',
            data: {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });
        store.model.addFields(modelFields);
        var dataString = '[';
        for (var i=0; i<data.length; i+=dim) {   
            if ( i > 0 ) {
                dataString += ',';
            }
            var dataRow = '{"column_y":"' + data[i].column_y + '"';
            for (var j=0; j<dim; j++) {
                dataRow += ',"value' + j + '":"' + data[i+j].value + '"';
            }
            dataRow += '}';
            dataString += dataRow;            
        }
        dataString += ']';       

        var storeData = JSON.parse(dataString);
        store.setData(storeData);        
        return store;
    },
    buildGridModelFields: function (data) {
        var modelFields = [{name: 'column_y', type: 'string'}];
        var dim = Math.sqrt(data.length);
        for (var i=0; i<dim; i++) {
            var colTempl = '{"name": "value' + i + '", "type": "string"}';
            modelFields.push(JSON.parse(colTempl));
        }        
        return modelFields;
    },
    buildGridColumns: function (data) {
        var cellRenderer = function (value, metaData, record, rowIndex, colIndex, store, view) {            
            if (parseFloat(value) < 0.002) {
                metaData.style = "background-color:#ff9999 !important;";
            }
            if (parseFloat(value) > 0.5 || value === 'NaN') {
                metaData.style = "background-color:#ffff99 !important;";
            }            
            return value;
        };    
        var gridColumns = [{"text": "", "dataIndex": "column_y", "type": "string", "width": 120}];
        var dim = Math.sqrt(data.length);
        for (var i=0; i<dim; i++) {
            var colTempl = '{"text": "' + data[i].column_x + '", "dataIndex": "value' + i + '", "type": "string", "width": 100, "renderer": ""}';
            var column = JSON.parse(colTempl);
            column.renderer = cellRenderer;
            gridColumns.push(column);
        }
        return gridColumns;
    }
});





/*var drawBox = Ext.create('Ext.draw.Component', {
 viewBox: false,
 items: [{
 type: 'rect',
 fill: '#ffc',
 x: 10,
 y: 6,
 width: 60,
 height: 10
 }]
 });
 
 var drawAxis = Ext.create('Ext.draw.Component', {
 viewBox: false,
 items: [{
 type: 'rect',
 fill: '#ffceef',
 x: 10,
 y: 6,
 width: 60,
 height: 1
 }]
 });
 
 var drawComponent = Ext.create('Ext.draw.Component', {
 viewBox: false,
 items: [{
 type: 'circle',
 fill: '#ffc',
 radius: 6,
 x: 8,
 y: 8
 }]
 }); */

/*var myCircle = drawComponent.surface.add({
 type: 'circle',
 x: 8,
 y: 8,
 radius: 6,
 fill: '#cc5'
 }).show(true);*/