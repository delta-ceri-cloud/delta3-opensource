/* 
 * Logistic Regression Grid
 */

Ext.define('delta3.view.logregr.LogregrGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.logregr',
    itemId: 'logregrGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'Ext.form.DateField',
        'delta3.view.logregr.LogregrFieldPopup',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'      
    ],
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                itemId: 'addLRF',
                text: 'Add LR Formula',
                iconCls: 'add_new-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance
                    var r = delta3.model.LogregrModel.create({
                        active: true,
                        idStudy: 0,
                        idModel: 0,
                        name: '',
                        description: '',
                        status: 'manual',
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.insert(0, r);
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }, {
                itemId: 'cloneLRF',
                text: 'Clone Formula',
                iconCls: 'cloneLRF-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Logistic Regression Formula first.');
                        return;
                    }                  
                    delta3.utils.GlobalFunc.doCloneLRF(sm.getSelection()[0].data, sm.getSelection()[0].data.name + ' Clone', thisGrid);
                }
            }, {
                itemId: 'deleteLRF',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];                
                    thisGrid.plugins[0].cancelEdit();
                    var sm = thisGrid.getSelectionModel();                
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Logistic Regression Formula first.');
                        return;
                    }                    
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to delete Logistic Regression Formula. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                thisGrid.store.reload();                                
                            }
                        }
                    });                  
                }
            }, {
                itemId: 'recordInfo',
                text: 'View Properties',
                iconCls: 'recordInfo-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select logistic regression formula first.');
                        return;
                    }                            
                    var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Logistic Regression Formula"});
                    win.show();
                }
            }]
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No formulas found',
            displayInfo: true
        }],
    modelStore: {},
    initComponent: function() {
        var me = this;  
        me.modelStore = delta3.store.ModelStore.create({pageSize: delta3.utils.GlobalVars.largePageSize});
        me.modelStore.load();        
        me.store = me.buildStore();
        me.plugins = me.buildPlugins();
        me.dockedItems[1].store = me.store;
        me.store.load();          
        me.columns = me.buildColumns(me);
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));      
    },
    buildColumns: function(thisGrid) {
        return [
            {text: 'ID', dataIndex: 'idLogregr', width: 40, locked: true, tooltip: delta3.utils.Tooltips.logregrGridId},
            {text: 'Name', dataIndex: 'name', width: 180, locked: true, tooltip: delta3.utils.Tooltips.logregrGridName, editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 180, tooltip: delta3.utils.Tooltips.logregrGridDescription,
                editor: {xtype: 'textfield',maskRe: /[^<>%]/}},
            {text: 'Type', dataIndex: 'status', width: 50, tooltip: delta3.utils.Tooltips.logregrGridType},
            {text: 'Data Model', dataIndex: 'idModel', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.logregrGridModel,
                editor: new Ext.form.ComboBox({
                    store: thisGrid.modelStore,
                    itemId: 'modelComboBox',
                    displayField: 'name',
                    valueField: 'idModel',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true
                }),                    
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModel'];
                    //var model = thisGrid.modelStore.findRecord('idModel', tableIndex);
                    var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex, undefined, undefined, undefined, true);
                    
                    if (model === null)
                        return null;
                    else {
                        return model.get("name");
                    }
                }},            
            {text: 'Sequence Field', dataIndex: 'modelColumnidSequencer', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.logregrGridDate,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var id = rec.data['modelColumnidSequencer'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord( 'idModelColumn', id);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},          
            {text: 'Study ID', dataIndex: 'idStudy', width: 60, sortable: true, tooltip: delta3.utils.Tooltips.logregrGridStudy},            
            {text: 'Study Name', dataIndex: 'idStudy', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.logregrGridStudy,
//                editor: new Ext.form.ComboBox({
//                    store: delta3.utils.GlobalVars.StudyStore,
//                    itemId: 'studyComboBox',
//                    displayField: 'name',
//                    valueField: 'idStudy',
//                    queryMode: 'local',
//                    multiSelect: false,
//                    forceSelection: true
//                }),                
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var id = rec.data['idStudy'];
                    var study = delta3.utils.GlobalVars.StudyStore.findRecord('idStudy', id);
                    if (study === null)
                        return null;
                    else {
                        return study.get("name");
                    }
                }},
            {xtype: 'actioncolumn',
                text: 'Formula',   
                width: 60,
                tooltip: delta3.utils.Tooltips.logregrGridFormula,
                items: [{
                        //iconCls: '.check_good-icon',
                        icon: 'resources/images/page_link16.png',
                        tooltip: 'Click to define or edit Logistic Regression Formula',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var sequencerId = 0;
                            var study = delta3.utils.GlobalVars.StudyStore.findRecord('idStudy', rec.data.idStudy);
                            if ( study !== null ) {
                                sequencerId = study.get('idDate');                           
                            }
                            var win = new delta3.view.logregr.LogregrFieldPopup({fieldStore: delta3.utils.GlobalVars.FieldStore, 
                                logregrId: rec.data.idLogregr, 
                                logregrName: rec.data.name,
                                logregrFormula: rec.data.formula,
                                modelId: rec.data.idModel,
                                sequencerId: sequencerId
                            });
                            win.show();
                        }
                    }]
            },
            {text: 'Result ID', dataIndex: 'idProcess', width: 80, tooltip: delta3.utils.Tooltips.logregrGridId},      
            //{text: 'Interval Start', dataIndex: 'intervalData', width: 80, tooltip: delta3.utils.Tooltips.logregrGridInterval},        
            //{text: 'IntervalQuery', dataIndex: 'intervalQuery', width: 100},     
            {text: "Processed on", width: 128, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},            
            {text: 'Original Formula', dataIndex: 'originalLogregrId', width: 100, tooltip: delta3.utils.Tooltips.logregrGridOriginId}
            //{text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', width: 50, editor: 'checkboxfield'}
        ];
    },
    buildStore: function() {
        return delta3.store.LogregrStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project});        
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'logregrGridEditor',
            listeners: {
                edit: function(rowEditor, changes, r) {
                    if ( changes.record.data.idModel === null || changes.record.data.idModel === 0 ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Model from dropdown.');
                        return;                       
                    }       
                    var thisGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
                    var recSource = delta3.utils.GlobalVars.ModelStore.findRecord('idModel',changes.record.data.idModel);    
                    thisGrid.store.save();                       
                }
            }
        })];
    }
});

