/* 
 * Group Grid
 */

Ext.define('delta3.view.GroupGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.group',
    itemId: 'groupGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'          
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function(rowEditor, changes, r, rowIndex) {
                    var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0];
                    thisGrid.store.sync();
                }
            }
        })
    ],
    border: false,
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',            
        items: [{
                text: 'Add',
                iconCls: 'add_new-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance
                    var r = delta3.model.GroupModel.create({
                        name: '',
                        longName: '',
                        type: '',
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.insert(0, r);
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }, {
                itemId: 'removeGroup',
                text: 'Remove',
                iconCls: 'delete-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0]
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    thisGrid.store.remove(sm.getSelection());
                    thisGrid.store.sync();
                    sm.select(0);
                }
            }, {
                itemId: 'refreshGroup',
                text: 'Refresh',
                iconCls: 'refresh-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0]
                    thisGrid.store.load();
                }
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No groups found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();      
        me.dockedItems[1].store = me.store;
        me.store.load();        
        me.columns = me.buildColumns();         
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));         
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idGroup', locked: true},
            {text: 'Name', dataIndex: 'name', editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 150, editor: 'textfield'},
            {text: 'Active', disabled: false, xtype: 'checkcolumn', width: 50, dataIndex: 'active',  disabled: true, editor: 'checkboxfield'},   
            {text: 'Type', dataIndex: 'type', width: 100, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.groupTypeComboBoxStore,
                            displayField: 'type',
                            valueField: 'type',
                            emptyText: 'Not Assigned',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#groupGrid')[0];
                                    thisGrid.getSelectionModel().getSelection()[0].set('type', h);
                                }
                            }
                        })
            },            
            //{text: 'Parent ID', dataIndex: 'parentId', editor: 'numberfield'},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        console.log('Setting active record!');
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function() {
        return Ext.create('delta3.store.GroupStore');
    }

});

