/* 
 * Permission Grid
 */

Ext.define('delta3.view.PermissionGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.permission',
    itemId: 'permissionGrid',
    autoScroll: true,
    renderTo: document.body,
    minHeight: 170,    
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'           
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function(rowEditor, changes, r, rowIndex) {
                    var thisGrid = Ext.ComponentQuery.query('#permissionGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                text: 'Add Permission',
                iconCls: 'add_new-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#permissionGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance
                    var r = delta3.model.PermissionModel.create({
                        name: 'New Permission',
                        description: 'new permission',
                        module: 1,
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.insert(0, r);
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No permissions found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[1].store = me.store;
        me.columns = me.buildColumns();
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));  
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idPermissiontemplate', width: 50},
            {text: 'Name', dataIndex: 'name', editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 160},
            {text: 'Active', disabled: false, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield'},
            {text: 'Type', dataIndex: 'type', editor: 'textfield'},
            {text: 'Tag', dataIndex: 'tag', editor: 'textfield'},
            {text: 'Sequence', dataIndex: 'sequence', type: 'int', editor: 'numberfield'},            
            {text: 'Module', dataIndex: 'module', type: 'int', editor: 'numberfield'},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }}
        ];
    },
    buildStore: function() {
        return delta3.utils.GlobalVars.PermissionStore;
    }

});

