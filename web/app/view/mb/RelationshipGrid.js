/* 
 * Relationship Grid
 */

Ext.define('delta3.view.mb.RelationshipGrid', {
    extend: 'Ext.grid.Panel',
    title: 'Step 1C: Select Relationships',
    requires: [
        'delta3.store.RelationshipStore',
        'delta3.view.popup.RelationshipPopup'
    ],
    height: 364,
    width: 400,
    itemId: 'relationshipGrid',
    listeners: {
        beforeshow: function(node, data) {
            var me = this;
            me.store.idModel = ModelData.modelSelected.idModel;
            me.store.proxy.extraParams.model = ModelData.modelSelected.idModel;
            me.store.load();
        }
    },
    initComponent: function() {
        var me = this;
        me.store = new Ext.create('delta3.store.RelationshipStore');
        me.columns = me.buildColumns();
        me.callParent();
    },
    tbar: [{
            text: 'Add Relationship',
            iconCls: 'add_new-icon16',
            tooltip: delta3.utils.Tooltips.relBtnAdd,                 
            handler: function() {
                var modelData = {};
                modelData.modelTableComboStore = Ext.create('delta3.store.ModelTableComboStore');
                modelData.modelKey1ComboStore = Ext.create('delta3.store.ModelKeyComboStore');
                modelData.modelKey2ComboStore = Ext.create('delta3.store.ModelKeyComboStore');
                modelData.tableStore = ModelData.tableStore;
                delta3.utils.GlobalFunc.doUpdateModelDropdowns(showRelationshipPopup);        
                function showRelationshipPopup(response) {
                    delta3.utils.GlobalFunc.updateModelBuilderDropdowns(modelData, response); 
                    var popup = Ext.create('delta3.view.popup.RelationshipPopup', {modelData: modelData});
                    popup.show();
                }
            }
        }, {
            text: 'Delete',
            iconCls: 'delete-icon16',
            tooltip: delta3.utils.Tooltips.relBtnDelete,                 
            handler: function() {
                Ext.Msg.show({
                    title:'DELTA',
                    message: 'You are about to delete Relationship from Model. Would you like to proceed?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.store.remove(sm.getSelection());
                            thisGrid.store.sync();                         
                            sm.select(0);
                        }
                    }
                });                
            }
        }, {
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.relBtnRefresh,                           
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                thisGrid.store.load();
            }
        }],
    buildColumns: function() {
        return [
            {text: 'Table1', dataIndex: 'table1', type: 'int', width: 140, tooltip: delta3.utils.Tooltips.relGridTable1, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelTableComboStore, // use pre-populated in ModelTableStore
                            displayField: 'physicalName',
                            valueField: 'idModelTable',
                            queryMode: 'local',
                            itemId: 'relationshipComboTable1'
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['table1'];
                    var model = ModelData.modelTableComboStore.findRecord('idModelTable', tableIndex);
                    if (model === null)
                        return null;
                    else {
                        return model.get("physicalName");
                    }
                }},
            {text: 'Key1', dataIndex: 'key1', type: 'int', width: 120, tooltip: delta3.utils.Tooltips.relGridKey1, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelKey1ComboStore, // use pre-populated
                            displayField: 'name',
                            valueField: 'idModelColumn',
                            itemId: 'relationshipComboKey1',
                            queryMode: 'local'
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var model = ModelData.modelKey1ComboStore.findRecord('idModelColumn', rec.data['key1']);
                    if (model === null)
                        return null;
                    else
                        return model.get("name");
                }},
            {text: 'Table2', dataIndex: 'table2', type: 'int',  width: 140, tooltip: delta3.utils.Tooltips.relGridTable2, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelTableComboStore, // use pre-populated
                            displayField: 'physicalName',
                            valueField: 'idModelTable',
                            itemId: 'relationshipComboTable2',
                            queryMode: 'local'
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['table2'];
                    var model = ModelData.modelTableComboStore.findRecord('idModelTable', tableIndex);                  
                    if (model === null)
                        return null;
                    else {
                        return model.get("physicalName");
                    }
                }},
            {text: 'Key2', dataIndex: 'key2', type: 'int',  width: 120, tooltip: delta3.utils.Tooltips.relGridKey2, editor:
                        new Ext.form.ComboBox({
                            store: ModelData.modelKey2ComboStore, // use pre-populated
                            displayField: 'name',
                            valueField: 'idModelColumn',
                            itemId: 'relationshipComboKey2',
                            queryMode: 'local'
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var model = ModelData.modelKey2ComboStore.findRecord('idModelColumn', rec.data['key2']);
                    if (model === null)
                        return null;
                    else
                        return model.get("name");
                }},
            {text: 'Type', dataIndex: 'type', width: 100, tooltip: delta3.utils.Tooltips.relGridType, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.relationshipComboBoxStore,
                            displayField: 'type',
                            valueField: 'value',
                            emptyText: '',
                            queryMode: 'local'
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var typeString = delta3.utils.GlobalVars.relationshipComboBoxStore.findRecord('value', rec.data['type']);
                    if (typeString === null)
                        return 'error';
                    else
                        return typeString.get('type');
                }
            }
        ];
    },
    isKeyUsedMoreThanOnce: function(store, key) {
        var count = 0;
        store.each(function (record, index) {
            if (record.data.key1 === key) {
                 count++;;
            }
            if (record.data.key2 === key) {
                 count++;;
            }            
        });    
        return count;
    }
});

