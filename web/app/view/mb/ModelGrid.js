/* 
 * Model Grid
 */

Ext.define('delta3.view.mb.ModelGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,    
    alias: 'widget.grid.model',
    itemId: 'modelGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,
    selModel: {mode: "SINGLE", allowDeselect: true},
    taskStore: {},
    requires: [
        'Ext.data.Store',
        'delta3.store.TaskStore',  
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator',    
        'delta3.utils.Tooltips',
        'delta3.store.GroupEntityStore',
        'delta3.view.mb.ModelBuilderContainer',
        'delta3.view.mb.TableViewerContainer',
        'delta3.view.popup.GroupPopup',
        'delta3.view.popup.ProjectSelectionPopup',
        'delta3.view.popup.ImportPopup',
        'delta3.store.EntityGroupStore',
        'delta3.view.popup.ImportModelFilePopup',
        'delta3.view.scheduler.ReccurencePopup',
        'delta3.view.popup.RecordInfoPopup'
    ],
    configStore: delta3.utils.GlobalVars.ConfigurationStore,
    disallowProjectMove: true,
    
    listeners: 
        {
            
        beforedestroy : function(panel, eOpt) {
            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
            //console.log("before detroy");
            thisGrid.store.removeAll();
        },
        
        afterrender: function() {
            //delta3.utils.GlobalFunc.showDeltaMessage('Please refresh grid for updated view');           
            //console.log("before afterrender");

            Ext.Msg.show({
                    closable: false,
                    msg: 'All entitiles associated with selected project have retrived successfully. Please click "ok" to refresh view.',
                    title: 'Project Change',
                    buttons: Ext.Msg.OK, 
                    width: 300,
                    fn: function(buttonId) {
                      if (buttonId === 'ok') {
 //                       console.log(btn);
                        var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                        //thisGrid.store.clearFilter(true);
                        //thisGrid.store.filter('active', true);              
                        thisGrid.store.reload();
                        thisGrid.taskStore.load();
                        Ext.ComponentQuery.query('#modelGrid')[0].getView().refresh();
                      }
                    }
                  });
        }
    },
    
    
    
    
    
    /*
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                
                beforeedit: function(rowEditor, context, r) {
                    //Ext.ComponentQuery.query('#studyGrid')[0].idEntity = context.record.data.idStudy;
                    //delta3.utils.GlobalVars.modelStore.clearFilter(true);
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    thisGrid.store.load();
                    console.log("This is before edit");
                },
                
                edit: function(rowEditor, context, eOpt) {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    var valid = true;
                    var counter = 0;
                    thisGrid.store.each(function(record, index) {
                        //validating uniqueness of new study name 
                        if (record.data.name === context.newValues.name) {
                            counter++;
                            if (counter > 1) {
                                valid = false;
                                return;
                            }
                        }
                    });
                    if (valid === false) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Data Model names must be unique.');
                        return;
                    }
                    //thisGrid.store.save();   
                    thisGrid.store.sync();     
                    Ext.ComponentQuery.query('#modelGrid')[0].getView().refresh(); 
                    thisGrid.store.clearFilter(true);
                }
                
                
            }
            
        })
    ],
    */
    
    
    
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: []
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No models found',
            pageSize: delta3.utils.GlobalVars.pageSize,   
            displayInfo: true
        }],    
    initComponent: function() {
        var me = this;
        me.projectStore = Ext.ComponentQuery.query('#mainViewPort')[0].projectStore;
        me.store = me.buildStore();
        me.store.load();
        me.dockedItems[1].store = me.store;
        me.taskStore = delta3.store.TaskStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project, groupType: "models"});
        me.taskStore.load();
        
        console.log("project :"+Ext.ComponentQuery.query('#mainViewPort')[0].project);
        console.log("me.disallowProjectMove :"+me.disallowProjectMove);
        console.log("MoveModel :"+delta3.utils.GlobalFunc.isPermitted("MoveModel",Ext.ComponentQuery.query('#mainViewPort')[0].project));
        console.log("ModelDelete :"+delta3.utils.GlobalFunc.isPermitted("ModelDelete",Ext.ComponentQuery.query('#mainViewPort')[0].project));
        console.log("LockModel :"+delta3.utils.GlobalFunc.isPermitted("LockModel",Ext.ComponentQuery.query('#mainViewPort')[0].project));
        
        if (delta3.utils.GlobalFunc.isPermitted("MoveModel",Ext.ComponentQuery.query('#mainViewPort')[0].project) === true) {  
            me.disallowProjectMove = false;
        }
        
        me.columns = me.buildColumns(me);   
        me.plugins = me.buildPlugins();  
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];         
        var deleteModel;
        
        
        if (delta3.utils.GlobalFunc.isPermitted("ModelDelete",Ext.ComponentQuery.query('#mainViewPort')[0].project) === true) {
            deleteModel = {
                itemId: 'ModelDelete',
                text: 'Delete Model',
                iconCls: 'delete-icon16',
                tooltip: delta3.utils.Tooltips.modelBtnDelete,
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                        return;
                    }
                    
                    Ext.Msg.show({
                        title: 'DELTA',
                        message: 'You are about to delete Model. This will delete all Studies that use this model. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                            }
                        }
                    });
                }
            };
        }     
               
               
               
        /*
        var lockUnlockModel;
        if (delta3.utils.GlobalFunc.isPermitted("LockModel",Ext.ComponentQuery.query('#mainViewPort')[0].project) === true) {
            lockUnlockModel = {
                text: 'Lock/Unlock Model',
                iconCls: 'key_folder-icon16',
                tooltip: delta3.utils.Tooltips.modelBtnLockUnlock,
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    if (typeof sm === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                        return;
                    }
                    delta3.utils.GlobalFunc.doLockUnlockModel(sm.getSelection()[0].data, thisGrid);
                }
            };
        }*/
        
        tbr.add({
            text: 'Step 1: Manage Model',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                //width: 100,
                margin: '0 0 10 0',
                items: [
                    {
                        text: 'Add Model',
                        iconCls: 'add_new-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnAdd,
                   
                
                        handler: function() {
                            
                            var projectassign;
                            if(Ext.ComponentQuery.query('#mainViewPort')[0].project!==0){
                                projectassign =1;
                            }
                            else{
                                projectassign =0;
                                
                            }
                            
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            // Create a new record instance
                            var d = new Date();
                                var r = delta3.model.ModelModel.create({
                                name: '',
                                longName: 'New Model',
                                type: '',
                                status: 'New',
                                idGroup: Ext.ComponentQuery.query('#mainViewPort')[0].project,
                                outputName: 'ft' + d.valueOf(),
                                projectAssigned :projectassign,
                                createdTS: '0000-00-00 00:00:00.0',
                                updatedTS: '0000-00-00 00:00:00.0'});
                            thisGrid.store.add(r);
                            thisGrid.plugins[0].startEdit(r);                    
                            }
                            
                    }, 
                    
                    
                    {
                        text: 'Edit Model',
                        iconCls: 'edit-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnEdit,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            var sel = thisGrid.getSelectionModel().getSelection()[0];
                            if (typeof sel === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }
                            thisGrid.plugins[0].startEdit(sel, 0);
                        }
                    }, {
                        itemId: 'cloneModel',
                        text: 'Clone Model',
                        iconCls: 'cloneModel-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnClone,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0]
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }
                            delta3.utils.GlobalFunc.doCloneModel(sm.getSelection()[0].data, thisGrid);
                            thisGrid.store.clearFilter(false);
                        }
                    }, //lockUnlockModel, 
                    
                    deleteModel, {
                        itemId: 'recordInfo',
                        text: 'View Properties',
                        iconCls: 'recordInfo-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnProperties,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }
                            var win = Ext.create('delta3.view.popup.RecordInfoPopup', {record: sm.getSelection()[0], recordType: "Data Model"});
                            win.show();
                        }
                    }]
            })
        });
        tbr.add({
            itemId: 'editModel',
            text: 'Step 2: Build Model Structure',
            iconCls: 'modelbuilder-icon16',
            tooltip: delta3.utils.Tooltips.modelBtnMB,
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                    return;
                }
                var dataSourceName = '';
                var confRec = thisGrid.configStore.findRecord('idConfiguration', sm.getSelection()[0].data.idConfiguration);
                if (confRec !== null) {
                    dataSourceName = confRec.get("name");
                }
                var sel = sm.getSelection()[0];
                var selData = sel.data;
                Ext.MessageBox.wait('Loading Data Model ' + sm.getSelection()[0].data.name + ' ...');                
                delta3.utils.GlobalFunc.openModelTab(selData, dataSourceName);
            }
        });
        tbr.add({
            text: 'Import/Export',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                        itemId: 'exportModel',
                        text: 'Export',
                        iconCls: 'exportObject-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnExport,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model first.');
                                return;
                            }
                            delta3.utils.GlobalFunc.doExportModel(sm.getSelection()[0].data);
                        }
                    }, {
                        itemId: 'importModel',
                        text: 'Quick Import',
                        iconCls: 'importObject-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnImportQuick,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            var win = new delta3.view.popup.ImportPopup();
                            win.show();
                        }
                    }, {
                        itemId: 'importModelFromFile',
                        text: 'Import from File',
                        iconCls: 'importObject-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnImport,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            var win = new delta3.view.popup.ImportModelFilePopup();
                            win.show();
                        }
                    }, {
                        text: 'Export Grid to CSV',
                        iconCls: 'exportCSV-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnExcel,
                        handler: function(b, e) {
                            b.up('grid').exportGrid('DELTA Data Model List');
                        }
                    }]
            })
        });
        tbr.add({
            itemId: 'refreshModel',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.modelBtnRefresh,
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                thisGrid.store.load();
            }
        });
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));  
        
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me})); 
    },
    
    buildColumns: function(me) {
        return [
            {text: 'ID', dataIndex: 'idModel', locked: true, width: 40, tooltip: delta3.utils.Tooltips.modelGridId},
            
            
            {xtype: 'actioncolumn', hidden: me.disallowProjectMove, hideable: false, tooltip: delta3.utils.Tooltips.modelGridProject,
                width: 56,
                text: 'Projects',
                renderer: function (value, metadata, record) {
                    var value = record.get('projectAssigned');        
                      if ( value === 0 ) {
                            this.items[0].iconCls = 'page_link-icon16';
                        } else 
                        {
                            this.items[0].iconCls = 'page_go';
                      }
                  
                },
                items: [
                    {
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.modelGridEntityButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);                            
                            var win = new delta3.view.popup.GroupPopup({selectedRecord: rec, entityId: rec.data.idModel, entityType: 'models'});
                            win.show();
                            
                        }
                    }]
            },
    
            {text: 'Name', itemId: 'nameColumn', dataIndex: 'name', tooltip: delta3.utils.Tooltips.modelGridName, editor: 'textfield', width: 150},
            {text: 'Description', dataIndex: 'description', width: 300, tooltip: delta3.utils.Tooltips.modelGridDescription, 
                editor: {xtype: 'textfield',maskRe: /[^<>%]/}},
            
            {text: 'Source', dataIndex: 'idConfiguration', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.modelGridSource,
                editor: new Ext.form.ComboBox({
                    store: delta3.utils.GlobalVars.ConfigurationStore,
                    //store : delta3.utils.GlobalVars.databaseConnectionTypeComboBoxStore,
                    itemId: 'configurationComboBox',
                    displayField: 'name',
                    valueField: 'idConfiguration',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idConfiguration'];
                    var confRec = delta3.utils.GlobalVars.ConfigurationStore.findRecord('idConfiguration', tableIndex);
                    if (confRec === null)
                        return null;
                    else {
                        return confRec.get("name");
                    }
                }},
            
            {xtype: 'actioncolumn',
                width: 60,
                text: 'Schedule',
                renderer: function (value, metadata, record) {
                        var tasks = Ext.ComponentQuery.query('#modelGrid')[0].taskStore;
                        var currTask = tasks.findRecord('objectId',record.get('idModel'));                    
                        if ( currTask === null ) {
                            this.items[0].iconCls = 'clock_edit-icon16';
                        } else {
                            this.items[0].iconCls = 'clock_go-icon16';
                        }
                    },
                items: [{
                        iconCls: 'clock_edit-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridScheduleButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            if ( rec.data.idModel === 0 ) {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Model');
                                return;                                
                            }                            
                            var win = new delta3.view.scheduler.ReccurencePopup 
                                ({selectedRecord: rec, 
                                    entityId: rec.data.idModel, 
                                    entityType: "models",
                                    refreshFunction: Ext.ComponentQuery.query('#modelGrid')[0].refreshTasks,
                                    taskStore: Ext.ComponentQuery.query('#modelGrid')[0].taskStore,
                                    checkBoxOnAction: 'generateFT'
                                    //checkBoxOffAction: 'runStudy'
                                });
                            win.show();
                        }
                    }]
            },                
            
            {text: 'Status', dataIndex: 'status', width: 300, editor: 'textfield', tooltip: delta3.utils.Tooltips.modelGridStatus, renderer: me.showQtip},
            
            
            {text: 'S', dataIndex: 'isAtomicFinished', disabled: true, xtype: 'checkcolumn', width: 40, tooltip: delta3.utils.Tooltips.modelGridAtomicFinished,
                renderer: function(value, metadata, record, row, col, store, gridView) {
                    if (value === true) {
                        return '<img src="resources/images/bullet_star16_mod.png">';
                    } else {
                        return '<img src="resources/images/bullet_white16_mod.png">';
                    }
                }},
            {text: 'V1', dataIndex: 'isVirtualFinished', disabled: true, xtype: 'checkcolumn', width: 40, tooltip: delta3.utils.Tooltips.modelGridVirtualFinished,
                renderer: function(value, metadata, record, row, col, store, gridView) {
                    if (value === true) {
                        return '<img src="resources/images/bullet_star16_mod.png">';
                    } else {
                        return '<img src="resources/images/bullet_white16_mod.png">';
                    }
                }},
            {text: 'M', dataIndex: 'isMissingFinished', disabled: true, xtype: 'checkcolumn', width: 40, tooltip: delta3.utils.Tooltips.modelGridMissingFinished,
                renderer: function(value, metadata, record, row, col, store, gridView) {
                    if (value === true) {
                        return '<img src="resources/images/bullet_star16_mod.png">';
                    } else {
                        return '<img src="resources/images/bullet_white16_mod.png">';
                    }
                }},
            {text: 'V2', dataIndex: 'isSecondaryVirtualFinished', disabled: true, xtype: 'checkcolumn', width: 40, tooltip: delta3.utils.Tooltips.modelGridSecondaryVirtualFinished,
                renderer: function(value, metadata, record, row, col, store, gridView) {
                    if (value === true) {
                        return '<img src="resources/images/bullet_star16_mod.png">';
                    } else {
                        return '<img src="resources/images/bullet_white16_mod.png">';
                    }
                }},
            
            /*v3.64 Added processing id and Commented below code as it retrives updatedTS*/
            //{text: "Processed on", width: 128, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
           
            /*v3.64 Added processing id DELQA-612*/
            {text: "Last Processed on", width: 128, dataIndex: 'processingId', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            
            {text: 'Processed By', dataIndex: 'updatedBy', width: 90,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }},
            {text: 'Owner', dataIndex: 'createdBy', width: 90, sortable: true, filterInvisible: false,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
                }},
            
              /*v3.64 Added Original model ID DELQA-557*/
            {text: 'Original Model ID', dataIndex: 'originalModelId', width: 100, sortable: false, tooltip: delta3.utils.Tooltips.modelOrigID},
            //{text: 'Locked', dataIndex: 'locked', disabled: false, xtype: 'checkcolumn', width: 50, tooltip: delta3.utils.Tooltips.modelGridLocked},
            {text: 'Flat Table Name', dataIndex: 'outputName', width: 120, tooltip: delta3.utils.Tooltips.modelGridFTName}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    
    buildStore: function() {
        return delta3.store.ModelStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project});        
    },
    
    
    buildPlugins: function() {
        return [
            Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                
                beforeedit: function(rowEditor, context, r) {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    //console.log("This is before edit");
                },
                
                edit: function(rowEditor, context, eOpt) {
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    var valid = true;
                    var counter = 0;
                    thisGrid.store.each(function(record, index) {
                        //validating uniqueness of new study name 
                        if (record.data.name === context.newValues.name) {
                            counter++;
                            if (counter > 1) {
                                valid = false;
                                return;
                            }
                        }
                    });
                    if (valid === false) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Data Model names must be unique.');
                        return;
                    }
                    
                    thisGrid.store.sync();     
                    Ext.ComponentQuery.query('#modelGrid')[0].getView().refresh(); 
                    thisGrid.store.clearFilter(true);
                },
                afterEdit : function(grid, row, field, rowIndex, columnIndex){
                    var thisGrid = Ext.ComponentQuery.query('#modelGrid')[0];
                    //thisGrid.store.load();
                    Ext.ComponentQuery.query('#modelGrid')[0].getView().refresh();
                }
                
            }
            
        })
            
        ];
    },
    
    
    showQtip: function(value, metaData, record, rowIdx, colIdx, store) {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    },
    
    refreshTasks: function(returnedValue) {
        
        var store = Ext.ComponentQuery.query('#modelGrid')[0].taskStore;
        store.load(); 
        if ( returnedValue === null || typeof returnValue !== 'undefined' ) {
                
            store.sync();
        } else 
        {
            // check if this is an update or new record
            var index = store.find("idTask",returnedValue.tasks[0].idTask);
            if ( index !== -1 ) {
                store.insert(index, returnedValue.tasks[0]);
            } else {
                store.add(returnedValue.tasks[0]);
            }
        }
        Ext.ComponentQuery.query('#modelGrid')[0].getView().refresh(); 
    }
    
    
});

/* Ext.data.Store.prototype.sortData = function(f, direction){
 direction = direction || 'ASC';
 var st = this.fields.get(f).sortType;
 var fn = function(r1, r2) {
 var v1 = st(r1.data[f]), v2 = st(r2.data[f]);
 // ADDED THIS FOR CASE INSENSITIVE SORT
 if (v1.toLowerCase) {
 v1 = v1.toLowerCase();
 v2 = v2.toLowerCase();
 }
 return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
 };
 this.data.sort(direction, fn);
 if (this.snapshot && this.snapshot != this.data) {
 this.snapshot.sort(direction, fn);
 }
 } */


