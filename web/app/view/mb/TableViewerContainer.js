/* 
 * Table Viewer Container
 */

Ext.define('delta3.view.mb.TableViewerContainer', {
    extend: 'Ext.container.Container',
    requires: [
        'delta3.view.mb.TableViewerGrid'
    ],
    alias:  'widget.container.tableViewer',
    //itemId: 'tableViewerContainer',
    title: {},
    layout: 'fit',   
    initComponent:  function(){   
        this.items = [];  
        this.items[0] = Ext.create('delta3.view.mb.TableViewerGrid',{
                idModel: this.modelSelected.idModel,
                title: this.title,
                append: this.append,
                region: 'center'
        });    
        this.callParent();
    }
});