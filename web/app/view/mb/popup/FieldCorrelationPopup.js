/* 
 * (C) 2017 CERI-Lahey
 * 
 */
Ext.define('delta3.view.mb.popup.FieldCorrelationPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.correlationParam',
    layout: 'fit',
    requires: [
        'delta3.view.study.VariableSelector'
    ],
    width: 530,
    controlWidth: 464,
    height: 320,
    heightDefault: 230, // used in field container 1 to calculated top margin
    itemId: 'correlationParamPopup',
    panelTitle: 'Correlation parameters field selection',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Parameter Correlation Configuration',
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 200,
    labelAlign: 'left',   
    fieldStore: {},
    selectedFieldRecord: {},
    idModel: {},
    params: {},
    items: [],
    buttons: [
        {
            text: 'Analyze',
            handler: function() {  
                var thisWin = Ext.ComponentQuery.query('#correlationParamPopup')[0];               
                var theOutcomeItemsGrid = Ext.ComponentQuery.query('#variableVariable')[0];  
                var listOfVariableNames = theOutcomeItemsGrid.getRawValue(); 
                if ( listOfVariableNames === "" ) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select at least one Field.');
                    return;                    
                }
                var listOfIDs = delta3.utils.GlobalFunc.mapFieldNamesToIDs(thisWin.fieldStore,listOfVariableNames);
                delta3.utils.GlobalFunc.getCorrelationStats(ModelData.modelSelected, listOfIDs, true);
                thisWin.destroy();              
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#correlationParamPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
         me.items[0] = new Ext.Panel(
            {
                frame: true,
                labelWidth: 90,
                labelAlign: 'right',
                title: 'Drag and Drop Variables for Correlation Verification',
                bodyStyle: 'padding:5px 5px 0',
                width: 420,
                height: 300,
                autoScroll: true,
                itemCls: 'form_row',
                defaultType: 'displayfield',
                items: []
            });
        me.updateLayout();            
        me.fieldStore.clearFilter(true);   
        me.fieldStore.filter('idModel', me.idModel);
        me.fieldStore.filter('insertable', true);          
        var localFieldStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.StudyVariableModel.getFields());  
        var fieldName;
        if ( me.selectedFieldRecord === '' ) {
            fieldName = '';
        } else {
            fieldName = me.selectedFieldRecord.name;           
        }
        me.items[0].add(Ext.create('delta3.view.study.VariableSelector', {sourceStore: localFieldStore, variablesList: fieldName}));      
        me.callParent();    
    }
}
);

