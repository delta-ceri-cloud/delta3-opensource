/* 
 * Model Container
 */

Ext.define('delta3.view.mb.ModelContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.models',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.model',
                region: 'center'
        },   
        me.callParent();
    }
});