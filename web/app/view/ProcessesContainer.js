/* 
 * Processes Container
 */

Ext.define('delta3.view.ProcessesContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.processes',
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,      
    //layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.process',
                region: 'center'
        },   
        me.callParent();
    }
});

