/* 
 * Process (Result) Grid
 */

Ext.define('delta3.view.ProcessGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.process',
    itemId: 'processGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,    
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator',           
        'delta3.view.popup.ResultsPopup',        
        'delta3.view.popup.ImportFilePopup',      
        'delta3.view.popup.MemoPopup',
        'delta3.view.rv.common.ResultsContainer',
        'delta3.store.ProcessStore'
    ],
    border: false,
    dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',            
                items: []
            },{
                xtype: 'pagingtoolbar',
                dock: 'bottom',
                emptyMsg: 'No models found',
                displayInfo: true
        }],
    filterStore: {},
    initComponent: function() {
        var me = this;
        me.filterStore = delta3.store.FilterStore.create({idModel: 0, pageSize: delta3.utils.GlobalVars.largePageSize});
        me.filterStore.load();            
        me.store = me.buildStore();         
        me.plugins = me.buildPlugins();
        me.dockedItems[1].store = me.store;
        me.store.load();           
        me.columns = me.buildColumns(me);
        var deleteProcess;
        if (delta3.utils.GlobalFunc.isPermitted("ProcessDelete") === true) {
            deleteProcess = {
                itemId: 'ProcessDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                tooltip: delta3.utils.Tooltips.resultBtnDelete,                     
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];                            
                    var sel = thisGrid.getSelectionModel().getSelection()[0];
                    if (typeof sel === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                        return;
                    }                        
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to delete Results. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {                          
                            if (btn === 'yes') {
                                var sm = thisGrid.getSelectionModel();
                                thisGrid.plugins[0].cancelEdit();
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                thisGrid.store.reload();                                
                            }
                        }
                    });                    
                }
            };
        }        
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add({
            itemId: 'showResultsD3G',
            text: 'Result Dashboard',
            iconCls: 'show_results2-icon16',
            tooltip: delta3.utils.Tooltips.resultBtnShowResults,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                var selRecord = thisGrid.getSelectionModel().getSelection()[0];
                if (typeof selRecord === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                    return;
                }                  
                delta3.utils.GlobalFunc.getStudyDetails(selRecord.data.idStudy, showTab);               
                function showTab(studyRecord) {    
                    delta3.utils.GlobalFunc.displayDashboard(selRecord.data, studyRecord);                
                } 
            }
        });        
        tbr.add({
            itemId: 'showResults',
            text: 'Result Charts',
            iconCls: 'show_results-icon16',
            tooltip: delta3.utils.Tooltips.resultBtnShowResults,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                var selRecord = thisGrid.getSelectionModel().getSelection()[0];
                if (typeof selRecord === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                    return;
                }                  
                delta3.utils.GlobalFunc.showResultsTab(selRecord.data);
            }
        });         
        /*me.tbar[x++] =  {
            itemId: 'callWS',
            text: 'Call Web Service',
            iconCls: 'callWS-icon16',
            tooltip: delta3.utils.Tooltips.resultBtnImport,
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                var win = new delta3.view.popup.WebServicePopup();
                win.show();
            }
        };*/
        tbr.add({
            text: 'Edit',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                    text: 'Edit Result',
                    iconCls: 'edit-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnEdit,                        
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        thisGrid.plugins[0].startEdit(sel, 0);
                    }
                }, deleteProcess, 
                {
                    itemId: 'recordInfo',
                    text: 'View Properties',
                    iconCls: 'recordInfo-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnProperties,                    
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Process"});
                        win.show();
                    }
                }]
            })
        });       
        tbr.add({
            text: 'Advanced',
            iconCls: 'spec-icon16',
            //tooltip: 'Step 2',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                    itemId: 'getStatus',
                    text: 'Get Stage Status',
                    iconCls: 'statProxy-getStatus-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnGetStatus,                        
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        doGetStatus(sm.getSelection());    
                    }
                },  {
                    itemId: 'retrieveResults',
                    text: 'Retrieve Results',
                    iconCls: 'statProxy-getResults-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnRetrieveResults,                    
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                            
                        doGetStatResults(sm.getSelection());
                    }
                }, {
                    itemId: 'processResults',
                    text: 'Process Results',
                    iconCls: 'process_results-icon16',
                    tooltip: delta3.utils.Tooltips.resultBtnShowResults,                
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                            return;
                        }                  
                        delta3.utils.GlobalFunc.doProcessResults(sel.data);  
                    }
                }]
            })
        });   
        tbr.add({
            text: 'Import/Export',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [
                    /*
                    {
                        itemId: 'importResults',
                        text: 'Quick Import',
                        iconCls: 'importObject-icon16',
                        tooltip: delta3.utils.Tooltips.resultBtnImport,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            var win = new delta3.view.popup.ResultsPopup();
                            win.show();
                        }
                    }, {
                        itemId: 'importResultsFromFile',
                        text: 'Import from File',
                        iconCls: 'importObject-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnImport,
                        handler: function() {                         
                            //var win = new delta3.view.popup.ImportFilePopup();
                            var win = Ext.create('delta3.view.popup.ImportFilePopup', {
                                            title: 'DELTA Import Results from File',
                                            url: '/Delta3/webresources/process/saveResultsFile'
                                        });
                            win.show();
                        }
                    }, {
                        itemId: 'exportResultsJSON',
                        text: 'Export Output',
                        iconCls: 'exportObject-icon16',
                        tooltip: delta3.utils.Tooltips.modelBtnExport,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Result first.');
                                return;
                            }
                            var process = sm.getSelection()[0];
                            var win = Ext.create('delta3.view.popup.ExportPopup');
                            var textArea = win.down('#exportDataString');
                            textArea.setValue(process.data.data);
                            win.show();                                      
                        }
                    },*/ 
                    
                    {
                        text: 'Export Grid to CSV',
                        iconCls: 'exportCSV-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnExcel,
                        handler: function(b, e) {
                            b.up('grid').exportGrid('DELTA Result List');
                        }
                    }
                
                ]
            })
        });        
        tbr.add({
            itemId: 'refreshStudy',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.resultBtnRefresh,                
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                thisGrid.store.load();
            }
        });                  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));  
    },
    buildColumns: function(thisGrid) {
        var modelStore = delta3.store.ModelStore.create({pageSize: delta3.utils.GlobalVars.largePageSize});
        modelStore.load();             
        
        
        return [
            {text: 'ID', dataIndex: 'idProcess', locked: true, width: 70},
            {text: 'Study Stage', dataIndex: 'name', width: 140, locked: true, tooltip: delta3.utils.Tooltips.resultGridName},
            
             /*
             {xtype: 'actioncolumn',
                width: 60,
                text: 'Memo',
                renderer: function (value, metadata, record) {
                        
                    },
                items: [
                    {
                        iconCls: 'resultMemo',
                        handler: function(grid, rowIndex, colIndex) {               
                           
                        }
                    }]
            }, */      
            
            {text: 'Status', dataIndex: 'status', width: 70, tooltip: delta3.utils.Tooltips.resultGridStatus},
            {text: 'Progress %', dataIndex: 'progress', width: 70, tooltip: delta3.utils.Tooltips.resultGridProgress},                 
            {text: 'Message', dataIndex: 'message', width: 160, tooltip: delta3.utils.Tooltips.resultGridMessage, renderer: function(val, cell, rec, r_idx, c_idx, store) {
                        cell.tdAttr = 'data-qtip="' + val + '"';
                        return val;
                    }},          
            {text: 'Study ID', dataIndex: 'idStudy', width: 60, tooltip: delta3.utils.Tooltips.resultGridStudyId},            
            //{text: 'Project ID', dataIndex: 'idGroup', width: 60, tooltip: delta3.utils.Tooltips.resultGridStudyId},            
          
            {text: 'Data Model', dataIndex: 'idModel', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.resultGridModel,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModel'];
                    //var model = modelStore.findRecord('idModel', tableIndex);
                    
                    var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex, undefined, undefined, undefined, true);
                    
                    
                    if (model === null)
                        return null;
                    else {
                        return model.get("name");
                    }
                }},
            {text: "Submitted on", width: 128, dataIndex: 'submittedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},            
            {text: "Processed on", width: 128, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},            
             {text: 'Owner', dataIndex: 'createdBy', width: 90, sortable: true,filterInvisible: false,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
            }},
            {text: 'Processed By', dataIndex: 'updatedBy', width: 90,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }},
        
            {text: 'Filter', dataIndex: 'idModel', width: 80, sortable: false, tooltip: delta3.utils.Tooltips.resultGridModel,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {          
                    var tableIndex = rec.data['idFilter'];                  
                    var filter = thisGrid.filterStore.findRecord('idFilter', tableIndex);                
                    if (filter === null)
                        return null;
                    else {
                        var tooltip = '';
                        var fields = filter.data['fields'];
                        var tooltip = '';
                        for (var i = 0; i < fields.length; i++) {
                            if (i > 0) {
                                tooltip += ', ';
                            }
                            tooltip = delta3.utils.GlobalFunc.getFilterWhereClause(fields, delta3.utils.GlobalVars.FieldStore);
                        }
                        cell.tdAttr = 'data-qtip="' + tooltip + '"';
                        return filter.get("name");
                    }
                }},     
            {text: 'Submit SQL', dataIndex: 'filterClause', width: 120, editor: 'textfield', tooltip: delta3.utils.Tooltips.resultGridSubmitSql, renderer: function(val, cell, rec, r_idx, c_idx, store) {
                        cell.tdAttr = 'data-qtip="' + val + '"';
                        return val;
                    }},                
            {text: 'Record Count', dataIndex: 'rowCount', width: 86, tooltip: delta3.utils.Tooltips.resultGridRowCount},                   
            {text: 'Description', dataIndex: 'description', width: 120, editor: 'textfield', tooltip: delta3.utils.Tooltips.resultGridDescription},              
            {text: 'Manual', disabled: true, xtype: 'checkcolumn', dataIndex: 'manualExecution', width: 50, tooltip: delta3.utils.Tooltips.resultGridManualExecution},            
            {text: 'Input', dataIndex: 'input', width: 280, editor: 'textfield', tooltip: delta3.utils.Tooltips.resultGridInput},      
            {text: 'Output', dataIndex: 'data', width: 280, editor: 'textfield', tooltip: delta3.utils.Tooltips.resultGridData},
            
            {text: 'Local Stats', dataIndex: 'localStatPackage', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.resultLocalStatPackage,  disabled: true},  
            {text: 'Remote Stats', dataIndex: 'remoteStatPackage', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.resultRemoteStatPackage,  disabled: true}              
        ];
    },
    buildPlugins: function() {
        return [ Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'processGridEditor',
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#processGrid')[0];
                    thisGrid.store.save();
                }
            }
        })];
    },
    buildStore: function() {
        //return Ext.create('delta3.store.ProcessStore');
        return delta3.store.ProcessStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project});
    }
});

//------------------------------------------------------------ call to get results
function doGetStatResults(selections)
{
    var processes = '';
    var guids = '';

    for (var i = 0; i < selections.length; i++) {
        if (i !== 0) {
            processes += ',';
            guids += ',';
        }
        processes += selections[i].data.idProcess;
        guids += selections[i].data.guid;
    }
    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/process/getStatResult',
                        method: "GET",
                        disableCaching: true,
                        params: {processId: processes, guid: guids},
                        success: doGetStatResultsSuccess,
                        failure: doGetStatResultsFailed
                    }
            );
    function doGetStatResultsSuccess(response, options)
    {
        console.log('calling getStatResults success ' + response.responseText);
        //Ext.Msg.alert("DELTA", response.responseText);
    }

    function doGetStatResultsFailed(response, options)
    {
        console.log('calling getStatResults failed ' + response.responseText);
        Ext.Msg.alert("DELTA", response.responseText);
    }    
}

//------------------------------------------------------------ call to get jobs status
function doGetStatus(selections)
{
    var processes = '';
    var guids = '';

    for (var i = 0; i < selections.length; i++) {
        if (i !== 0) {
            processes += ',';
            guids += ',';
        }
        processes += selections[i].data.idProcess;
        guids += selections[i].data.guid;
        console.log('calling OCEANS getStatus for process id ' + selections[i].data.idProcess);
    }

    Ext.Ajax.request
            (
                    {
                        url: '/Delta3/webresources/process/getJobStatus',
                        method: "GET",
                        disableCaching: true,
                        params: {processId: processes, guid: guids},
                        success: doGetStatusSuccess,
                        failure: doGetStatusFailed
                    }
            );
    function doGetStatusSuccess(response, options)
    {
        console.log('calling getStatus success ' + response.responseText);
    }

    function doGetStatusFailed(response, options)
    {
        console.log('calling getStatus failed ' + response.responseText);
        Ext.Msg.alert("DELTA", response.responseText);
    }    
}


