/* 
 * Method Container
 */

Ext.define('delta3.view.MethodContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.methods',
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,       
    //layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.method',
                region: 'center'
        }, 
        me.callParent();
    }
});