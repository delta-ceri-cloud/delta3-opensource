/* 
 * Audit Grid (read only)
 */

Ext.define('delta3.view.AuditGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.audit',
    itemId: 'auditGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'        
    ],
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                itemId: 'refreshMethod',
                text: 'Refresh',
                iconCls: 'refresh-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#auditGrid')[0]
                    thisGrid.store.load();
                }
            }]
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No audit records found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[1].store = me.store;
        me.store.load();        
        me.columns = me.buildColumns();
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));   
    },
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idAudit', locked: true, width: 80},    
            {text: 'User', dataIndex: 'userAlias', width: 120},
            {text: 'Object Type', dataIndex: 'objectType', width: 80},
            {text: 'Action', dataIndex: 'action', width: 80},
            {text: 'Object ID', dataIndex: 'objectId', width: 80},
            {text: 'Organization ID', dataIndex: 'organizationId', width: 60},
            {text: 'User ID', dataIndex: 'userId', width: 60},
            {text: 'Record Created TS', width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Source', dataIndex: 'source', editor: 'textfield', width: 80}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.AuditStore');
    }

});


