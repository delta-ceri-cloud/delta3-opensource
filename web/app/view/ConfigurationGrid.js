/* 
 * Configuration Grid
 */

Ext.define('delta3.view.ConfigurationGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.configuration',
    itemId: 'configurationGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,       
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator',         
        'delta3.view.mb.ModelBuilderContainer',
        'delta3.view.mb.TableViewerContainer'
    ],
    
    
    /*
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                beforeedit: function(rowEditor, e, eOpts) {                    
                     var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];      
                     //e.record.data.dbUser='';
                     e.record.data.dbPassword='';
                     //delta3.utils.GlobalFunc.showDeltaMessage('For security reason, any change in this information will require to re-enter username and password once again.');
                    
                },
                
                edit: function(rowEditor, changes, r) 
                 {
                    var valid = true;
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];       
                    thisGrid.store.each(function(record, index) {
                        //validating uniqueness of new DB name 
                        if (index !== changes.rowIdx && record.data.name === changes.newValues.name) {
                            valid = false;
                            return;
                        }
                        
                    });
                    
                    if (valid === false) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Database name must be unique.');
                        return;
                    }
                    
                    if ( changes.record.data.name === '' ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database name before saving.');
                        return;                            
                    }   
                    
                    if ( changes.record.data.type === '' ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database type');
                        return;                            
                    }   
                    
                    
                    if (changes.record.data.type ==='MySQL')
                    {   
                        if (changes.record.data.connectionInfo.startsWith("jdbc:mysql://")===false) {
                        valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage
                        ('MySQL Database connection is Invalid. JDBC connection should should start with \"jdbc:mysql://".\n eg. jdbc:mysql://localhost:3306/d3_test_datasets?autoReconnect=true');    
                        return;                            
                        }
                    }
                    if (changes.record.data.type ==='MS-SQL')
                    {   
                        if (changes.record.data.connectionInfo.startsWith("jdbc:sqlserver://")===false) {
                        valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage('MS-SQL Database connection is Invalid. JDBC connection should should start with \"jdbc:sqlserver://\".\n Example. jdbc:sqlserver://127.0. 0.1:1433;databaseName=TestDatabase;  Please refer DELTA <a href="https://ceri-lahey.atlassian.net/wiki/spaces/DUM/overview">User Manual</a> for more information. Thank you for using DELTA.');
                        return;                            
                        }
                    }
                     
                    if ( changes.record.data.dbUser === '' ) {
                        valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database username.');
                        return;                            
                    }
                    
                    if ( changes.record.data.dbPassword === '' ) {
                        valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter/re-enter database password.');
                        return;                            
                    }     
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.store.save();

                },
               
                

//                //old code improvements and fixes https://ceri-lahey.atlassian.net/browse/DELQA-893
//                
//                edit: function(rowEditor, changes, r) {    
//                    if ( changes.record.data.name === '' ) {
//                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database name before saving.');
//                        return;                            
//                    }             
//                    
//                    var valid = true;
//                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];                    
//                    thisGrid.store.each(function(record, index) {
//                        //validating uniqueness of new DB name 
//                        if (index !== changes.rowIdx && record.data.name === changes.newValues.name) {
//                            valid = false;
//                            //delta3.utils.GlobalFunc.showDeltaMessage('Database names must be unique.');
//                            return;
//                        }
//                    });
//                    
//                    if (valid === false) {
//                        delta3.utils.GlobalFunc.showDeltaMessage('Database names must be unique.');
//                        return;
//                    }  
//                    
//                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
//                    thisGrid.store.save();
//                    
//                },
                
                canceledit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.store.load();
                }
            }
        })
    ],
    
    */
    
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                text: 'Add Connection',
                iconCls: 'add_new_config-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance
                    var r = delta3.model.ConfigurationModel.create({
                        name: '',
                        longName: 'New Configuration',
                        type: '',
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.insert(0, r);
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }, 
            
            //v3.64 added code for delete connection https://ceri-lahey.atlassian.net/browse/DELQA-593
            {
                text: 'Delete Connection',
                iconCls: 'delete-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select configuration first.');
                        return;
                    }
                    
                    Ext.Msg.show({
                        title: 'DELTA',
                        message: 'You are about to delete configuration. This will delete your database connection and all confugurations. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                            }
                        }
                    });
                    
                }
            }, 
                
            {
                itemId: 'refreshConfiguration',
                text: 'Refresh',
                iconCls: 'refresh-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0]
                    thisGrid.store.load();
                }
            }]
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No configurations found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        
        
        me.dockedItems[0].store = me.store;
        me.store.load();        
        me.columns = me.buildColumns();
        me.plugins = me.buildPlugins();  
        
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));         
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idConfiguration', locked: true, width: 30},
            {text: 'Name', dataIndex: 'name', editor: 'textfield', width: 100},
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 120},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', editor: 'checkboxfield', width: 40},
            {text: 'Type', dataIndex: 'type', width: 100, editor:
            
            new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.databaseConnectionTypeComboBoxStore,
                            displayField: 'type',
                            emptyText: 'Not Assigned',
                            allowBlank: false,
                            queryMode: 'local',
                            forceSelection: true
                            
                        })
            },
           
            
            //{text: 'Type', dataIndex: 'type', editor: 'textfield', width: 50},
            {text: 'Connection Information', dataIndex: 'connectionInfo', editor: 'textfield', width: 260},
            {text: 'Databse User', dataIndex: 'dbUser', editor: 'textfield', width: 120},
            {text: 'Database Password', dataIndex: 'dbPassword', editor: 'textfield', width: 120,
            
            renderer: function(value, metaData) {
                    //console.log(arguments);
                    //console.log(value);
                   metaData.tdAttr = 'data-qtip="For security reason, any update or change made in record requires to re-enter password."';
                   return value;
            }
            
            },
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        console.log('Setting active record!');
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    
    
    buildStore: function() {
        //return Ext.create('delta3.store.ConfigurationStore');
        return delta3.utils.GlobalVars.ConfigurationStore;
    },
    
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                beforeedit: function(rowEditor, e, eOpts) {                    
                     var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];      
                     //e.record.data.dbUser='';
                     e.record.data.dbPassword='';
                     //delta3.utils.GlobalFunc.showDeltaMessage('For security reason, any change in this information will require to re-enter username and password once again.');
                    
                },
                
                edit: function(rowEditor, changes, r) 
                 {
                    var valid = true;
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];       
                    thisGrid.store.each(function(record, index) {
                        //validating uniqueness of new DB name 
                        if (index !== changes.rowIdx && record.data.name === changes.newValues.name) {
                            valid = false;
                            return;
                        }
                        
                    });
                    
                    if (valid === false) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Database name must be unique.');
                        return;
                    }
                    
                    if ( changes.record.data.name === '' ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database name before saving.');
                        return;                            
                    }   
                    
                    if ( changes.record.data.type === '' ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database type');
                        return;                            
                    }   
                    
                    
                    if (changes.record.data.type ==='MySQL')
                    {   
                        if (changes.record.data.connectionInfo.startsWith("jdbc:mysql://")===false) {
                        //valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage
                        ('MySQL Database connection is Invalid. JDBC connection should should start with \"jdbc:mysql://".\n eg. jdbc:mysql://localhost:3306/d3_test_datasets?autoReconnect=true');    
                         //return;                            
                        }
                    }
                    if (changes.record.data.type ==='MS-SQL')
                    {   
                        if (changes.record.data.connectionInfo.startsWith("jdbc:sqlserver://")===false) {
                        //valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage('MS-SQL Database connection is Invalid. JDBC connection should should start with \"jdbc:sqlserver://\".\n Example. jdbc:sqlserver://127.0. 0.1:1433;databaseName=TestDatabase;  Please refer DELTA <a href="https://ceri-lahey.atlassian.net/wiki/spaces/DUM/overview">User Manual</a> for more information. Thank you for using DELTA.');
                 //       return;                            
                        }
                    }
                     
                    if ( changes.record.data.dbUser === '' ) {
                        valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database username.');
                        return;                            
                    }
                    
                    if ( changes.record.data.dbPassword === '' ) {
                        valid = false;
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter/re-enter database password.');
                        return;                            
                    }     
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.store.save();

                },
               
                
                /*
                //old code improvements and fixes https://ceri-lahey.atlassian.net/browse/DELQA-893
                
                edit: function(rowEditor, changes, r) {    
                    if ( changes.record.data.name === '' ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please enter database name before saving.');
                        return;                            
                    }             
                    
                    var valid = true;
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];                    
                    thisGrid.store.each(function(record, index) {
                        //validating uniqueness of new DB name 
                        if (index !== changes.rowIdx && record.data.name === changes.newValues.name) {
                            valid = false;
                            //delta3.utils.GlobalFunc.showDeltaMessage('Database names must be unique.');
                            return;
                        }
                    });
                    
                    if (valid === false) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Database names must be unique.');
                        return;
                    }  
                    
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.store.save();
                    
                },
                */
                canceledit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    thisGrid.store.load();
                },
                afterEdit : function(grid, row, field, rowIndex, columnIndex){
                    var thisGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
                    //thisGrid.store.load();
                    Ext.ComponentQuery.query('#configurationGrid')[0].getView().refresh();
                }
                
                
            }
        })
    
       ];
        
    }

});


