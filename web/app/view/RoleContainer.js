/* 
 * User Container
 */

Ext.define('delta3.view.RoleContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.roles',
    height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,        
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.role',
                region: 'center'
        },  
        me.callParent();
    }
});



