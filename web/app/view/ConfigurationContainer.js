/* 
 * Configuration Container
 */

Ext.define('delta3.view.ConfigurationContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.configurations',
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,     
    requires:   [
        'Ext.layout.container.Border'
    ],
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.configuration',
                region: 'center'
        }, 
        me.callParent();
    }
});