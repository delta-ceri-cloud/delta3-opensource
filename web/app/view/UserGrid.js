/* 
 * User Grid
 */

Ext.define('delta3.view.UserGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.user',
    itemId: 'userGrid',
    autoScroll: false,
    autoDestroy: true,
    renderTo: document.body,
    minHeight: 170,
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.grid.plugin.RowEditing',
        'delta3.view.popup.UserProjectRolePopup',
        'Ext.toolbar.Paging',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.form.ComboBox',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator',   
        'delta3.model.UserModel'
    ],
    plugins: [],
    border: false,
    tbar_filter: {},
    tbar_paginator: {},
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
                text: 'Add User',
                iconCls: 'add_new-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance
                    var r = delta3.model.UserModel.create({
                        organization_idOrganization: thisGrid.store.getAt(0).data['organization_idOrganization'],
                        person: 0,
                        type: 'USER',
                        graphPackage: 'd3',
                        passFailureMax: 3,
                        passFailureCount: 0,
                        securityHandler: 1,
                        status: '',
                        locale: 'eng_US',
                        passChangedTS: '0000-00-00 00:00:00.0',
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.add(r);
                    thisGrid.plugins[0].startEdit(r);
                }
            },{
                text: 'Edit User',
                iconCls: 'edit-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];                           
                    var sel = thisGrid.getSelectionModel().getSelection()[0];
                    if (typeof sel === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                        return;
                    }                            
                    thisGrid.plugins[0].startEdit(sel, 0);
                }
            },{
                text: 'Roles/Projects',
                iconCls: 'roles-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];                           
                    var sel = thisGrid.getSelectionModel().getSelection()[0];
                    if (typeof sel === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                        return;
                    }     
                    var currUserRoles = "";          // comma delimited string of existing roles
                    var currRoleId = new Array();    // array to hold ids of existing roles      
                    var idUser = thisGrid.getSelectionModel().getSelection()[0].data.idUser;
                    var alias = thisGrid.getSelectionModel().getSelection()[0].data.alias;
                    delta3.utils.GlobalFunc.getUserAuthInfo(alias, idUser, function(response, options) {
                        var responseObject = JSON.parse(response.responseText);
                        if (responseObject.success === true) {
                            var ii = 0;
                            for (var j = 0; j < responseObject.userAuth.projects.length; j++) {
                                for (var i = 0; i < responseObject.userAuth.projects[j].roles.length; i++) {
                                    if (i > 0) {
                                        currUserRoles += ", ";
                                    }
                                    currUserRoles += responseObject.userAuth.projects[j].roles[i].name;
                                    currRoleId[ii++] = responseObject.userAuth.projects[j].roles[i].idRole;
                                }
                            }
                        }               
                        var win = Ext.create('delta3.view.popup.UserProjectRolePopup',{selRecord:thisGrid.getSelectionModel().getSelection()[0]});
                        win.show();
                    });
                }
            }, {
                text: 'Reset Password',
                iconCls: 'pass_reset-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof thisGrid.getSelectionModel().getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                        return;
                    }                
                    var alias = thisGrid.getSelectionModel().getSelection()[0].data.alias;
                    delta3.utils.GlobalFunc.resetPassword(alias);
                }
            }, {
                itemId: 'recordInfo',
                text: 'View Properties',
                iconCls: 'recordInfo-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0]
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select user first.');
                        return;
                    }                            
                    var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "User"});
                    win.show();
                }
            },
             {
                itemId: 'refreshUser',
                text: 'Refresh',
                iconCls: 'refresh-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                    thisGrid.store.load();
                }
            }
        
            
            ]},{
                xtype: 'pagingtoolbar',
                dock: 'bottom',
                emptyMsg: 'No users found',
                displayInfo: true
            }],  
    beforeDestroy: function() {
        var x = Ext.ComponentQuery.query('#tbarDyn')[0];   
    },    
    onDestroy: function() {
        Ext.ComponentQuery.query('#orgComboBox')[0].destroy();
        Ext.ComponentQuery.query('#typeComboBox')[0].destroy();   
        //Ext.ComponentQuery.query('#graphPackageComboBox')[0].destroy();     
        this.callParent();
    },
    initComponent: function() {
        var me = this;
        var combo_validation_required;
        
        me.plugins = me.buildPlugins();         
        me.store = me.buildStore();
        me.store.load();         
        me.dockedItems[1].store = me.store;
        me.columns = me.buildColumns(me.store);   
        me.callParent();           
        // optional, permission driven buttons hookup follows 
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];
        
        
        if (delta3.utils.GlobalFunc.isPermitted("Pseudo") === true) {
           tbr.add({
                itemId: 'Pseudo',
                text: 'Pseudo',
                iconCls: 'pseudo-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0]
                    //var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    var idUser = thisGrid.getSelectionModel().getSelection()[0].data.idUser;
                    var alias = thisGrid.getSelectionModel().getSelection()[0].data.alias;
                    delta3.utils.GlobalFunc.doPseudo(alias, idUser, function() {
                        // following code does not prevent AdminUsers from failing while in Pseudo mode
//                        var userContainer = thisGrid.ownerCt;
//                        thisGrid.destroy();
//                        userContainer.destroy();
//                        var userCtrl = delta3.app.getController('delta3.controller.Users');
//                        userCtrl.destroy();
                        // ----------------------------------------------------------------------------
                        Ext.ComponentQuery.query('#mainViewPort')[0].destroy();
                        Ext.create('delta3.utils.GlobalVars');
                        Ext.create('delta3.utils.GlobalFunc');
                        Ext.create('delta3.utils.Tooltips');         
                        delta3.utils.GlobalFunc.getCurrentUserAuthInfo(initializeGlobalVariables);
                        function initializeGlobalVariables() {
                            // pre-load in-memory data stores for fast lookups
                            delta3.utils.GlobalVars.OrgStore = Ext.create('delta3.store.OrganizationStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
                            delta3.utils.GlobalVars.OrgStore.load();
                            delta3.utils.GlobalVars.RoleStore = Ext.create('delta3.store.RoleStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
                            delta3.utils.GlobalVars.RoleStore.load();
                            delta3.utils.GlobalVars.PermissionStore = Ext.create('delta3.store.PermissionStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
                            delta3.utils.GlobalVars.PermissionStore.load();
                            delta3.utils.GlobalVars.FieldStore = Ext.create('delta3.store.FieldStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
                            delta3.utils.GlobalVars.FieldStore.load();
                            delta3.utils.GlobalVars.UserStore = Ext.create('delta3.store.UserStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
                            delta3.utils.GlobalVars.UserStore.load();        
                            delta3.utils.GlobalVars.StudyStore = Ext.create('delta3.store.StudyStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
                            delta3.utils.GlobalVars.StudyStore.load();           
                            delta3.utils.GlobalVars.ModelStore = Ext.create('delta3.store.ModelStore', {pageSize: delta3.utils.GlobalVars.largePageSize});                         
                            delta3.utils.GlobalVars.ModelStore.load();                      
                            setTimeout(gotoMain, 300);   
                        }  
                        function gotoMain() {
                            Ext.MessageBox.hide();
                            // Set the localStorage value to true
                            localStorage.setItem("DeltaLoggedIn", true);
                            Ext.widget('view-main');
                            Ext.create('delta3.controller.Main');
                        }
                    }
                    );
                }
            });
        }          
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me})); 
//        tbr.add({
//            xtype: 'fieldcontainer',            
//            renderTo: Ext.getBody(),
//            margin: "1 0 1 1",       // top, right, bottom, left               
//            width: 420,
//            layout: 'hbox',
//            gridToReload: me,
//            items: [{
//                    xtype: 'textfield',
//                    itemId: 'recNumText',
//                    name: 'recNumText',
//                    fieldLabel: 'Records per page',
//                    maxWidth: 240,
//                    labelWidth: 120,
//                    labelAlign: 'right',               
//                    emptyText: 'Enter number ..'
//                }, {
//                    xtype: 'button',
//                    text: 'Change2',
//                    handler: function() {
//                        var text = this.up().down('#recNumText').getValue();
//                        this.up().down('#recNumText').setValue('');
//                        if ( isNaN(text) ) {
//                            return;
//                        }
//                        var grid = this.up().gridToReload;       
//                        grid.dockedItems.items[0].pageSize = parseInt(text);
//                        grid.getStore().pageSize = parseInt(text);
//                        grid.getStore().reload({params: {
//                            start: 0,
//                            limit: text
//                        }}); 
//                    }
//                }]
//            });          
        tbr.updateLayout();            
    },
    
    
    buildColumns: function(userStore) {
        delta3.utils.GlobalVars.roleTypeComboBoxStore.filterBy(function(record, scope)
        {
            for (var i = 0; i < delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects[0].roles.length; i++) {
                if (delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects[0].roles[i].type === 'SADMIN') 
                {
                    combo_validation_required = false;
                    return true; // sys admin can pick any type
                }
            }
            if (record.get("type") === 'SADMIN') {
                combo_validation_required = true;
                return false; // org admin can pick any type except sys adminhttp://localhost:8080/Delta3/index.html
            }
            return true;
        });
        
        return [
            {text: 'ID', dataIndex: 'idUser', width: 50, locked: true},
            //{text: 'ID', dataIndex: 'idUser', width: 50},           
            {text: 'Alias', dataIndex: 'alias', locked: true, editor: 'textfield'},
            //{text: 'Alias', dataIndex: 'alias', editor: 'textfield'},            
            {text: 'Active', width: 50, disabled: false, xtype: 'checkcolumn', dataIndex: 'active',  disabled: true, editor: 'checkboxfield'},
            {text: 'Organization', dataIndex: 'Organization_idOrganization', editor: //'numberfield'},
                        new Ext.form.ComboBox({
                            //store: 'OrganizationStore',
                            store: delta3.utils.GlobalVars.OrgStore, // use pre-loaded
                            itemId: 'orgComboBox',
                            displayField: 'name',
                            valueField: 'idOrganization',
                            queryMode: 'local',
                            forceSelection: true,
                            listeners: {
                                'select': function(cmb, rec, idx) {
                                    var h = cmb.getValue();
                                    var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                                    var rec = thisGrid.store.getNewRecords();
                                    if (rec.length > 0) { // process only new record
                                        rec[0].data.organization_idOrganization = h;
                                    }
                                }
                            }
                        }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var model = delta3.utils.GlobalVars.OrgStore.findRecord('idOrganization', rec.data['organization_idOrganization']);
                    return model.get("name");
                }},
            
            
            {text: 'Type', dataIndex: 'type', width: 100, editor:
                new Ext.form.ComboBox({
                    store: delta3.utils.GlobalVars.roleTypeComboBoxStore,
                    itemId: 'typeComboBox',
                    displayField: 'type',
                    valueField: 'type',
                    emptyText: '',
                    queryMode: 'local',
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                            var rec = thisGrid.store.getNewRecords();
                            if (rec.length > 0) { // process only new record
                                rec[0].data.type = h;
                            }                            
                        }
                    }
                })
            },
            //{text: "Passord Expiration TS", width: 120, sortable: true, dataIndex: 'passExpirationTS', field: 'datefield', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')},
            {text: "Password Expiration TS", width: 120, sortable: true, dataIndex: 'passExpirationTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u', editor: 'datefield', 
                renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')},
            //{text: "Passord Expiration TS", width: 120, sortable: true, dataIndex: 'passExpirationTS', type:'date', dateFormat:'Y-m-d H:i:s',editor: 'textfield'},
            {text: "Password Changed TS", width: 120, sortable: true, dataIndex: 'passChangedTS', type: 'date', dateFormat: 'Y-m-d H:i:s.u', renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')},
//            {text: 'Pass Failure Max', dataIndex: 'passFailureMax', type: 'int', editor: 'numberfield'},
//            {text: 'Pass Failure Count', dataIndex: 'passFailureCount', type: 'int', editor: 'numberfield'},
//            //{text: 'GUID', dataIndex: 'guid'},
//            {text: 'Security Handle', dataIndex: 'securityHandler', editor: 'numberfield'},
//            {text: 'Status', dataIndex: 'status', editor: 'textfield'},
//            {text: 'Locale', dataIndex: 'locale', editor: 'textfield'},
            {text: 'First Name', dataIndex: 'firstName', editor: 'textfield'},
            {text: 'Last Name', dataIndex: 'lastName', editor: 'textfield'},
            {text: 'Email Address 1', dataIndex: 'emailAddress1', editor: 'textfield'},
            {text: 'Email Address 2', dataIndex: 'emailAddress2', editor: 'textfield'},
            {text: 'URI1', dataIndex: 'uri1', editor: 'textfield'},
            {text: 'URI2', dataIndex: 'uri2', editor: 'textfield'},
            
            
            /*v3.64 removed item from grid https://ceri-lahey.atlassian.net/browse/DELQA-472
            {text: 'Graphical Package', dataIndex: 'graphPackage', width: 80,  editor:
                new Ext.form.ComboBox({
                    store: delta3.utils.GlobalVars.graphPackageComboBoxStore,
                    itemId: 'graphPackageComboBox',
                    displayField: 'package',
                    valueField: 'package',
                    emptyText: '',
                    queryMode: 'local',
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                            thisGrid.getSelectionModel().getSelection()[0].set('graphPackage', h);
                        }
                    }
                })
            }, 
            */
            
            
            {text: 'Phone Number 1', dataIndex: 'phoneNumber1', editor: 'textfield'},
            {text: 'Phone Number 2', dataIndex: 'phoneNumber2', editor: 'textfield'},
            {text: 'Alert Prefernce 1', dataIndex: 'alertPreference1', editor: 'textfield'},
            {text: 'Alert Preference 2', dataIndex: 'alertPreference2', editor: 'textfield'}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.UserStore');
        //return delta3.utils.GlobalVars.UserStore;
    },
    buildPlugins: function() {
        return [        
                Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2,
                autoCancel: true,
                removeUnmodified: true,
                listeners: {
                    beforeedit: function(rowEditor, e, eOpts) {    
                        if(combo_validation_required === true)
                        {
                            if(e.record.data.type==='SADMIN'){
                                delta3.utils.GlobalFunc.showDeltaMessage('You can not edit SADMIN user');
                                return false;
                            }
                        }
                        
                        if(e.record.data.alias !== '')
                        {
                            delta3.utils.GlobalFunc.showDeltaMessage('Caution : Please notify user if you are changing existing username or any other information');
                            return;  
                        }    
                    
                    },
                    
                    edit: function(rowEditor, changes, r) {
                        
                        if ( changes.record.data.alias === '' ) {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please enter user alias before saving.');
                            return;                            
                        }
                        
                        if ( changes.record.data.type === '' ) {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please enter user type before saving.');
                            return;                            
                        }
                        
                        var valid = true;
                        var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];       
                        thisGrid.store.each(function(record, index) {
                        //validating uniqueness of new alias name 
                        if (index !== changes.rowIdx && record.data.alias === changes.newValues.alias) {
                            valid = false;
                            return;
                        }
                        
                        });
                    
                        if (valid === false) {
                        delta3.utils.GlobalFunc.showDeltaMessage('User alias must be unique.');
                        return;
                        }
                        
                        var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                        thisGrid.store.sync();
                    },
                    canceledit: function(rowEditor, changes, r) {
                        var thisGrid = Ext.ComponentQuery.query('#userGrid')[0];
                        thisGrid.store.load();
                    }                    
                }
            })
        ];
    }
});

