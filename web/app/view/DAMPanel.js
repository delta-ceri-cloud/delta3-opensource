/* 
 * Import Popup Window
 */
var maxParamWidth = 200;

Ext.define('delta3.view.DAMPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.panel.dam',
    requires: ['delta3.view.popup.DAMSetupPopup'],    
    layout: 'vbox',
    width: '100%',
    height: '100%',
    itemId: 'DAMPanel',
    requestHttpType: 'POST',
    tbar: [{
            text: 'Start DELTAlytics',
            handler: function() {		
                delta3.utils.GlobalFunc.callLocalStatPackage('start', updateUI);
                function updateUI(response) {
                    var textArea = Ext.ComponentQuery.query('#DAMStatus')[0];
                    textArea.setValue(response.responseText);                      
                }
            }
        },{
            text: 'DELTAlytics Status',
            handler: function() {		
                delta3.utils.GlobalFunc.callLocalStatPackage('status', updateUI);
                function updateUI(response) {
                    var textArea = Ext.ComponentQuery.query('#DAMStatus')[0];
                    textArea.setValue(response.responseText);                      
                }
            }
        },{
            text: 'Stop DELTAlytics',
            handler: function() {		
                delta3.utils.GlobalFunc.callLocalStatPackage('stop', updateUI);
                function updateUI(response) {
                    var textArea = Ext.ComponentQuery.query('#DAMStatus')[0];
                    textArea.setValue(response.responseText);                      
                }
            }
        },{
            text: 'Get Output',
            handler: function() {		
                delta3.utils.GlobalFunc.callLocalStatPackage('refreshOutput', updateUI);
                function updateUI(response) {
                    var textArea = Ext.ComponentQuery.query('#DAMOutput')[0];
                    textArea.setValue(textArea.getValue() + response.responseText);                      
                }
            }
        },{
            text: 'Get Errors',
            handler: function() {		
                delta3.utils.GlobalFunc.callLocalStatPackage('refreshErrors', updateUI);
                function updateUI(response) {
                    var textArea = Ext.ComponentQuery.query('#DAMErrors')[0];
                    textArea.setValue(textArea.getValue() + response.responseText);                      
                }
            }
        }, {
            text: 'Setup',
            handler: function() {
                var win = Ext.create('delta3.view.popup.DAMSetupPopup');
                win.show();
            }
        }],
    items: [{
                xtype: 'label',
                text: 'Status',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 800,
                height: 18,            
                margin: '5 5 5 5',
                grow: true,
                itemId: 'DAMStatus',
                value: '',
                allowBlank: true
            }, {
                xtype: 'label',
                text: 'Output',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 800,
                height: 160,            
                margin: '5 5 5 5',
                grow: true,
                itemId: 'DAMOutput',
                value: '',
                allowBlank: true
            }, {
                xtype: 'label',
                text: 'Errors',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 800,
                height: 160,         
                margin: '5 5 5 5',
                grow: true,
                itemId: 'DAMErrors',
                value: '',
                allowBlank: true
            }]
});


