/* 
 * Import from file Popup Window
 */

Ext.define('delta3.view.popup.ImportFilePopup', {
    extend: 'Ext.Window',
    requires: ['Ext.form.field.File'], 
    alias: 'widget.popup.importFile',
    layout: 'fit',
    width: 470,
    height: 430,
    itemId: 'importFilePopup',
    url: '/Delta3/webresources/process/saveResultsFile',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Import',
    items: [],
    initComponent: function() {
        var me = this;
         me.items[0] = new Ext.form.Panel({
            //title: me.title,
            width: 450,
            bodyPadding: 10,
            frame: true,
            renderTo: Ext.getBody(),
            items: [{
                xtype: 'filefield',
                name: 'importFile',
                tabIndex: 1,
                fieldLabel: 'File ',
                labelWidth: 50,
                labelAlign: 'left',
                msgTarget: 'side',
                allowBlank: false,
                buttonOnly: true,
                buttonText: 'Select file...'
            }, {
                xtype: 'textareafield',
                tabIndex: 2,
                width: 430,
                height: 300,            
                grow: true,
                itemId: 'importFeedback',
                value: '',
                allowBlank: true
            }],

            buttons: [{
                        text: 'Upload',
                        tabIndex: 3,
                        handler: function(b) {
                            Ext.ComponentQuery.query('#importFeedback')[0].setValue('');
                            var form = this.up('form').getForm();
                            if(form.isValid()){
                                form.submit({
                                    url: this.up('#importFilePopup').url,
                                    waitMsg: 'Uploading file...',
                                    success: function(f, a) {
                                        Ext.ComponentQuery.query('#importFeedback')[0].setValue(a.result.status.message);
                                    },
                                    failure: function(f,a){
                                        Ext.ComponentQuery.query('#importFeedback')[0].setValue(a.response.responseText);
                                    }                            
                                });
                            }
                        }
                    }, {
                        text: 'Close',
                        tabIndex: 4,
                        handler: function() {
                            this.up('#importFilePopup').destroy();
                        }
                    }]
        }); 
        me.callParent();
    }
});


