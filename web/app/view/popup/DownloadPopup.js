/* 
 * Download Popup Window
 */

Ext.define('delta3.view.popup.DownloadPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.download',
    layout: 'vbox',
    width: 520,
    height: 154,
    itemId: 'downloadPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Results Download',
    filetype: 'png',
    tag: '_pd',
    idProcess: '',
    items: [{
                xtype: 'fieldcontainer',                            
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'Title',
                    itemId: 'fileName',
                    labelWidth: 80,
                    labelAlign: 'left',     
                    minWidth: 320,
                    margin: '5 5 5 5',
                    value: '',
                    allowBlank: true
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'Result Index',
                    itemId: 'resultIndex',
                    labelWidth: 80,
                    labelAlign: 'left',     
                    //minWidth: 40,
                    maxWidth: 120,
                    margin: '5 5 5 5',
                    value: '0',
                    allowBlank: true
                }] 
            }, {
                xtype: 'fieldcontainer',                            
                layout: 'hbox',
                items: [{
                    xtype: 'combobox',
                    margin: '5 5 5 5',
                    fieldLabel: 'Artifact Type',
                    itemId: 'artifactType',
                    labelWidth: 80,
                    labelAlign: 'left',    
                    minWidth: 320,
                    //maxWidth: 340,
                    store: delta3.utils.GlobalVars.artifactTypeComboBoxStore,
                    displayField: 'artifact',
                    valueField: 'tag',
                    //emptyText: '',
                    queryMode: 'local',
                    forceSelection: true,
                    multiSelect: false,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            this.up('#downloadPopup').tag = cmb.getValue();
                        }
                    }
                }, {
                    xtype: 'combobox',
                    margin: '5 5 5 5',
                    fieldLabel: 'File Type',
                    itemId: 'fileType',
                    labelWidth: 80,
                    labelAlign: 'left',    
                    maxWidth: 150,
                    store: delta3.utils.GlobalVars.fileTypeComboBoxStore,
                    displayField: 'type',
                    valueField: 'type',
                    //emptyText: 'png',
                    value: 'png',
                    queryMode: 'local',
                    forceSelection: true,
                    multiSelect: false,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            this.up('#downloadPopup').fileType = cmb.getValue();
                        }
                    }
                }] 
            }],
    buttons: [
        {
            text: 'Download',
            handler: function() {
                //var divId = delta3.utils.GlobalVars.resultId + Ext.ComponentQuery.query('#downloadPopup')[0].idProcess 
                //        + '_' + delta3.utils.GlobalVars.resultId + Ext.ComponentQuery.query('#downloadPopup')[0].idProcess 
                //        + Ext.ComponentQuery.query('#downloadPopup')[0].tag + Ext.ComponentQuery.query('#resultIndex')[0].value;                
                var divId = delta3.utils.GlobalVars.resultId + Ext.ComponentQuery.query('#downloadPopup')[0].idProcess 
                        + Ext.ComponentQuery.query('#downloadPopup')[0].tag + Ext.ComponentQuery.query('#resultIndex')[0].value;
                var div = document.getElementById(divId);
                //var panel = Ext.ComponentQuery.query('#Results_' + Ext.ComponentQuery.query('#downloadPopup')[0].idProcess)[0];
                //var offsets = panel.getOffsetsTo(divId);
                //var scrollPos = panel.getScrollY();
                //panel.setScrollY(0);
                //panel.setScrollY(-offsets[1]);
                var params = {
                        filename: "DELTA " + Ext.ComponentQuery.query('#fileName')[0].value, 
                        format: Ext.ComponentQuery.query('#fileType')[0].value,
                        width: delta3.utils.GlobalVars.chartWidth,
                        height: delta3.utils.GlobalVars.chartHeight
                    };
                if ( params.format !== "pdf" ) {
                    if (divId.indexOf('Table') > -1 ) {
                        downloadPictureFromServer(div,params);
                        //downloadTableToPicture(div,params);
                    } else {
                        ////downloadPictureFromServer(div,params);
                        //downloadChartToPictureFromServer(div,params);  
                        downloadSVGToPicture(div,params);
                        ////downloadChartToPicturePopupWindow(div,params); 
                    }
                } else {
                    if (divId.indexOf('Table') > -1 ) {
                        downloadTableToPDF(div,params);
                        //downloadChartToPDF(div,params);
                    } else {
                        downloadChartToPDF(div,params);
                    }
                }
            }
        },
        {
            text: 'Close',
            handler: function() {
                this.up('#downloadPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});


