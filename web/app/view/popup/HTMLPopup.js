/* 
 * HTML (printable) Popup Window
 */

Ext.define('delta3.view.popup.HTMLPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.html',
    layout: 'fit',
    width: 450,
    height: 460,
    itemId: 'htmlPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Information Window',
    windowContent: '',
    items: [],
    buttons: [
        {
            text: 'Print',
            handler: function() {
                //var printContents = document.getElementById("printableArea").innerHTML;
                //var originalContents = document.body.innerHTML;
                //swapElements(originalContents, printContents);
                //document.body.innerHTML = printContents;
                window.print();
                //swapElements(printContents, originalContents)
                //document.body.innerHTML = originalContents;                             
            }
        }, {
            text: 'Close',
            handler: function() {
                this.up('#htmlPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.update(me.windowContent);
        me.callParent();
    }
});

function swapElements(obj1, obj2) {
    // create marker element and insert it where obj1 is
    var temp = document.createElement("div");
    obj1.parentNode.insertBefore(temp, obj1);

    // move obj1 to right before obj2
    obj2.parentNode.insertBefore(obj1, obj2);

    // move obj2 to right before where obj1 used to be
    temp.parentNode.insertBefore(obj2, temp);

    // remove temporary marker node
    temp.parentNode.removeChild(temp);
}

