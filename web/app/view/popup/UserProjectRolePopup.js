/* 
 * User-Project-Role selection Popup Window, 
 */

Ext.define('delta3.view.popup.UserProjectRolePopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.userprojectrole',
    requires: ['delta3.store.RoleStore',
                'delta3.store.ProjectRoleStore',
                'delta3.store.ProjectStore'],
    width: '60%',
    height: '90%',
    layout: 'fit',
    itemId: 'userProjectRolePopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    selRecord: {},
    projectStore: {},
    projectRoleStore: {},
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            delta3.utils.GlobalVars.RoleStore.clearFilter(true);
           
        }
        
        
    },     
    items: [],
    buttons: [{
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#userProjectRolePopup')[0];
                thisWin.destroy();
            }
        }],
    
    
    initComponent: function() {
        var me = this;
        me.projectStore = Ext.create('delta3.store.ProjectStore',{orgId: me.selRecord.data.organization_idOrganization});
        me.projectStore.load();    
        
        me.projectRoleStore = Ext.create('delta3.store.ProjectRoleStore',{userId: me.selRecord.data.idUser});
        //me.projectRoleStore.loadData([],false);
        me.projectRoleStore.load();     
        
        
        
        var roleColumns = [{
                text: 'Role Name', 
                flex: 1, 
                sortable: true, 
                dataIndex: 'name'
            }, {
                text: 'Description', 
                flex: 1,
                sortable: true, 
                dataIndex: 'description'
            }];    

        
        var projectColumns = [{
                text: 'Project Name', 
                flex: 1, 
                sortable: true, 
                dataIndex: 'name'
            }, {
                text: 'Description', 
                width: '50%', 
                sortable: true, 
                dataIndex: 'description'
            }];      
        
        var projectRoleColumns = [ 
            //{text: 'ID', flex: 0.2, dataIndex: 'idGroup',sortable: true},
            //v3.64 updated following code https://ceri-lahey.atlassian.net/browse/DELQA-518
            {   text: 'Project',
                flex: 1,
                sortable: true, 
                autoLoad : true,
                dataIndex: 'projectName',
                autoScroll: true,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    //var projectIndex = rec.data.userHasRolePK.idGroup;
                    var projectIndex = rec.data.idGroup;
                    var project = me.projectStore.findRecord('idGroup', projectIndex);
                    if(projectIndex===0){
                        return 'All';
                    }
                    else {
                        return project.get('name');
                    }
                }   
                
            }, {text: 'Role', flex: 1, sortable: true, dataIndex: 'roleName',
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    //var roleIndex = rec.data.userHasRolePK.roleidRole;
                    var roleIndex = rec.data.RoleidRole;                    
                    var role = delta3.utils.GlobalVars.RoleStore.findRecord('idRole', roleIndex);
                    if (role === null )
                        return null;
                    else {
                        return role.get("name");
                    }
                }                 
            }];        
        
       
       
       //Newly added
       // me.projectRoleColumns = me.buildColumns(me.projectRoleStore);   
       
        me.items[0] = new Ext.Panel(
            {
                frame: true,                  
                bodyStyle: 'padding:5px 5px 0',
                layout: {
                    type: 'auto',
                    pack: 'center'
                },                    
                title: 'Role and Project selection',
                columns: roleColumns,                    
                autoScroll: true
            }); 
            
            
            
        //---------------------------------- System Roles available grid -------                    
        // show only system roles
        delta3.utils.GlobalVars.RoleStore.filterBy(function(record, scope)
        {
            if (me.selRecord.data.type === 'SADMIN') { // user setup for SADMIN
                if (record.get("type") === 'SADMIN' || record.get("type") === 'OADMIN') {
                    return true; // org admin can pick any system role
                } else {               
                    return false; // do not show user roles
                }
            }
            if (me.selRecord.data.type === 'OADMIN') { // user setup for OADMIN
                if (record.get("type") === 'OADMIN') {
                    return true; // org admin can pick any system role except sys admin
                } else {
                    return false; // do not show user roles
                }
            }
            return false;
        });    
        
        
        me.items[0].add({
                xtype: 'label',
                fieldStyle: 'font-weight: bold; color: #003168;',
                text: '\uf05a Please select system role assigned to User. If you dont see desired role please contact Administrator',
                margin: '5 5 5 5'
                
             });
        
        
        var storeAdminRoles = me.copyStore(delta3.utils.GlobalVars.RoleStore);          
        me.items[0].add({
            itemId: 'systemRoleGrid',
            flex: 1,
            xtype: 'grid',
            multiSelect: true,
            store: storeAdminRoles,
            title: 'Available System Roles',
            columns: roleColumns,                    
            stripeRows: true,        
            margin: '2 2 2 2'
        });    
        me.items[0].add({
            xtype: 'fieldset',       
            border: false,
            layout: {
                type: 'hbox',
                pack: 'center'
            }, 
            padding: '5,5,10,5',
            items: [{
                xtype: 'button',            
                text: 'Assign System Role',
                handler: function() {
                    var roleGrid = Ext.ComponentQuery.query('#systemRoleGrid')[0];                           
                    var selSRole = roleGrid.getSelectionModel().getSelection()[0];                        
                    if (typeof selSRole === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select System Role first.');
                        return;
                    }  
                    var idRoleArray = new Array();
                    idRoleArray[0] = selSRole.data.idRole;
                    var thisWin = Ext.ComponentQuery.query('#userProjectRolePopup')[0];         
                    thisWin.saveProjectRoleUserRelationship(thisWin.selRecord.data.alias, thisWin.selRecord.data.idUser, 0, idRoleArray, saved);      
                    
                    function saved(response) {
                        var responseObject = JSON.parse(response.responseText);
                        if ( responseObject.success === true ) {
                            var projectRoleGrid = Ext.ComponentQuery.query('#roleProjectGrid')[0]; 
                            var record = Ext.create(delta3.model.ProjectRoleModel);
                            record.data.RoleidRole = selSRole.data.idRole;    
                            record.data.UseridUser = thisWin.selRecord.data.idUser;                           
                            record.data.idGroup = 0;
                            projectRoleGrid.getStore().add(record);                        
                            projectRoleGrid.getView().refresh();
                            projectRoleGrid.updateLayout();
                        } else {
                            delta3.utils.GlobalFunc.showDeltaMessage(responseObject.status.message);
                        }
                        return;                        
                    }
                }
            }]
        });        
        //---------------------------------- User Roles available grid ---------        
        // show only User Roles
        delta3.utils.GlobalVars.RoleStore.clearFilter(true);
        delta3.utils.GlobalVars.RoleStore.filter('type', 'USER');            
        var storeUserRoles = me.copyStore(delta3.utils.GlobalVars.RoleStore);          
        
        
          me.items[0].add({
                xtype: 'label',
                labelStyle : 'font-weight: bold; color: #003168;',
                text: '\uf05a Please select project/group and role to assigned to User. If you dont see desired project or role contact Administrator',
                margin: '5 5 5 5'
                
         }),   
        
        
        me.items[0].add({
            xtype: 'fieldset',                            
            layout: {
                type: 'hbox',
                pack: 'center'
            }, 
            padding: '5,5,10,5',
            items: [{
                itemId: 'projectGrid',
                flex: 1,
                xtype: 'grid',
                multiSelect: false,            
                store: me.projectStore,      
                title: 'Available Projects',                    
                columns: projectColumns,      
                stripeRows: true,
                margin: '2 2 2 2'                    
            }, 
            
                
                {
                itemId: 'userRoleGrid',
                flex: 1,
                xtype: 'grid',
                multiSelect: true,
                store: storeUserRoles,
                title: 'Available User Roles',
                columns: roleColumns,                    
                stripeRows: true,        
                margin: '2 2 2 2'
            }]
        });     
        
        
        
        
        me.items[0].add({
            xtype: 'fieldset',       
            border: false,
            layout: {
                type: 'hbox',
                pack: 'center'
            }, 
            padding: '5,5,5,5',
            items: [{
                xtype: 'button',            
                text: 'Assign User Role',
                handler: function() {
                    var projectGrid = Ext.ComponentQuery.query('#projectGrid')[0];                           
                    var selProject = projectGrid.getSelectionModel().getSelection()[0];  
                    var projectId = 0; 
                    if (typeof selProject !== 'undefined') {
                        projectId = selProject.data.idGroup;
                    }                    
                    var roleGrid = Ext.ComponentQuery.query('#userRoleGrid')[0];                           
                    var selURole = roleGrid.getSelectionModel().getSelection()[0];                    
                    if (typeof selURole === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select User Role first.');
                        return;
                    }   
                    var idRoleArray = new Array();
                    idRoleArray[0] = selURole.data.idRole;
                    var thisWin = Ext.ComponentQuery.query('#userProjectRolePopup')[0];         
                    thisWin.saveProjectRoleUserRelationship(thisWin.selRecord.data.alias, thisWin.selRecord.data.idUser, projectId, idRoleArray, saved);      
                    function saved(response) {
                        var responseObject = JSON.parse(response.responseText);
                        if ( responseObject.success === true ) {                        
                            var projectRoleGrid = Ext.ComponentQuery.query('#roleProjectGrid')[0]; 
                            var record = Ext.create(delta3.model.ProjectRoleModel);
                            record.data.RoleidRole = selURole.data.idRole;    
                            record.data.moduleId = selURole.data.moduleId;
                            record.data.UseridUser = thisWin.selRecord.data.idUser;                           
                            record.data.idGroup = projectId;
                            
                            projectRoleGrid.getStore().add(record);                        
                            projectRoleGrid.getView().refresh();
                            projectRoleGrid.updateLayout();
                            delta3.utils.GlobalFunc.showDeltaMessage(responseObject.status.message);
                            //var thisWin = Ext.ComponentQuery.query('#userProjectRolePopup')[0];
                            //thisWin.destroy();
                            
                            
                            
                        } else {
                            delta3.utils.GlobalFunc.showDeltaMessage(responseObject.status.message);
                        }
                        return;                        
                    }
                }
            }]
        });
        
        
        
        
        
        me.items[0].add({
                xtype: 'label',
                fieldStyle: 'font-weight: bold; color: #003168;',
                text: '\uf05a Please click on \"Refresh/View\" to retrive updated assigned roles.',
                margin: '5 5 5 5'
                
         }),   
        
        
        
        //---------------------------------- Roles currently persisted grid ----
        delta3.utils.GlobalVars.RoleStore.clearFilter(true);   
        
        me.items[0].add({
            itemId: 'roleProjectGrid',
            //flex: 1,
            xtype: 'grid',
            multiSelect: true,
            store: me.projectRoleStore,
            title: 'Assigned Projects and Roles',
            columns: projectRoleColumns,     
            autoLoad : true,
            stripeRows: true,        
            margin: '5 5 5 5'
        }); 
        
        
        
         
              
        
        
        
        //------------------------------------------------- Delete button ------
        
        me.items[0].add({
            xtype: 'fieldset',       
            frame: false,
            border: false,
            layout: {
                type: 'hbox',
                pack: 'center'
            }, 
            padding: '5,5,5,5',
            margin: '5 5 5 5',
            items: [
                 
             {
                xtype: 'button',    
                text: 'Delete',
                handler: function() {
                    var roleGrid = Ext.ComponentQuery.query('#roleProjectGrid')[0];                           
                    var selRPRole = roleGrid.getSelectionModel().getSelection()[0];                    
                    if (typeof selRPRole === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Project/Role first.');
                        return;
                    }  
                    
                    var thisWin = Ext.ComponentQuery.query('#userProjectRolePopup')[0];  
                    var idRoleArray = new Array();
                    idRoleArray[0] = selRPRole.data.RoleidRole;                    
                    thisWin.removeProjectRoleUserRelationship(thisWin.selRecord.data.alias, thisWin.selRecord.data.idUser, selRPRole.data.idGroup, idRoleArray, removed);      
                    
                    function removed(response) {
                        var responseObject = JSON.parse(response.responseText); 
                        Ext.Msg.confirm("Confirmation", "Caution : This feature will remove project(s) asigned to user. Do you still want to proceed?", function(btnText){
                        if(btnText === "no"){
                            var thisWin = Ext.ComponentQuery.query('#userProjectRolePopup')[0];
                            thisWin.destroy();
                        }
                        
                        else if(btnText === "yes"){
                                //Ext.Msg.alert("Alert", "You have confirmed 'Yes'.");
                                if(responseObject.userProjectRoles[0].userHasRolePK.idGroup===0)
                                {
                                var projectRoleGrid = Ext.ComponentQuery.query('#roleProjectGrid')[0]; 
                                projectRoleGrid.getStore().removeAll();
                                projectRoleGrid.getView().refresh();
                                projectRoleGrid.updateLayout();
                                return;
                                }

                           else{
                                var projectRoleGrid = Ext.ComponentQuery.query('#roleProjectGrid')[0]; 
                                projectRoleGrid.getStore().remove(selRPRole);
                                projectRoleGrid.getView().refresh();
                                projectRoleGrid.updateLayout();
                                return;          
                           }
                        }
                        }, this);
                        
                        
                       
                        
                        
                                                
                    }                                     
                }
            },
        
            {
                xtype: 'button',            
                text: 'Refresh/View',
                handler: function() {
                     var projectRoleGrid = Ext.ComponentQuery.query('#roleProjectGrid')[0]; 
                        projectRoleGrid.getStore().load();
                        projectRoleGrid.getView().refresh();
                        projectRoleGrid.updateLayout();
                }
            }   
            ]
          
         
            
        });
        
             
        me.callParent();
    },
    
    
    
    //--------------------------------------------- utility functions ----------
   
    
    
    copyStore : function (store){
	var records = [];
	var newStore = new Ext.data.Store({model : store.model});
	store.each(
	    function (r)
	    {
		records.push (r.copy());
	    });	
	newStore.loadRecords(records);
	return newStore;
    },
    
    
    
    saveProjectRoleUserRelationship: function (userAlias, idUser, idProject, idRoleArray, callback)
    {
        
        var userRoleJSONString = '{"users":[{"alias":"' + userAlias
                + '", "idUser":"' + idUser
                + '"}], "entitlements":[{"project":' + JSON.stringify(idProject) + ',"roles":' + JSON.stringify(idRoleArray)
                + '}]}';
        
        
        Ext.Ajax.request
            ({
                url: '/Delta3/webresources/admin/addUserProjectRole',
                method: "POST",
                disableCaching: true,
                params: userRoleJSONString,
                success: callback,
                failure: saveRoleUserRelationshipFailed
            });
        function saveRoleUserRelationshipFailed(response, options)
        {
            delta3.utils.GlobalFunc.showDeltaMessage('Saving Role failed: ' + response.responseText);
            return;            
        }   
    },
    
    
    
    
    removeProjectRoleUserRelationship: function (userAlias, idUser, idProject, idRoleArray, callback)
    {
        var userRoleJSONString = '{"users":[{"alias":"' + userAlias
                + '", "idUser":"' + idUser
                + '"}], "entitlements":[{"project":' + JSON.stringify(idProject) + ',"roles":' + JSON.stringify(idRoleArray)
                + '}]}';
        
        Ext.Ajax.request
            ({
                url: '/Delta3/webresources/admin/removeUserProjectRole',
                method: "POST",
                disableCaching: true,
                params: userRoleJSONString,
                success: callback,
                failure: removeRoleUserRelationshipFailed
            });
        function removeRoleUserRelationshipFailed(response, options)
        {
            delta3.utils.GlobalFunc.showDeltaMessage('Deleting Role failed: ' + response.responseText);
            return;  
        }
    }   
});
