/* 
 * (C) CERI Lahey Clinic
 * D3 Results Container
 */
Ext.define('delta3.view.rv.common.ResultsContainer', {
    extend: 'Ext.tab.Panel',
    autoDestroy: true,
    requires: [
        'Ext.tab.Panel',
        'delta3.view.rv.common.DescriptiveStatsGrid',
        'delta3.view.rv.common.MatchedDescriptiveStatsGrid',
        'delta3.view.rv.common.UtilFunc',
        'delta3.view.rv.common.ObservedExpectedGrid',
        'delta3.view.rv.common.RiskAdjustedSPRTGrid',           
        'delta3.view.rv.common.PropDiffGrid',
        'delta3.view.rv.common.LogisticRegressionFormulaGrid'
    ],
    items: [],
    resultsData: {},
    processId: {},
    packageName: 'extjs',
    outcomeName: {},
    filterName: {},
    initComponent: function () { 
        var me = this;
        /*Ext.Loader.loadScript({
            url: 'http://d3js.org/d3.v3.min.js',
            scope: this,                   // scope of callbacks
            onLoad: function() {           // callback fn when script is loaded
                console.log("d3 script loaded");
            },
            onError: function() {          // callback fn if load fails 
                console.log("d3 script load error");
            } 
        });    */    
        if ( (me.resultsData === '') || (typeof me.resultsData === 'undefined') ) {
            delta3.utils.GlobalFunc.showDeltaMessage("There are no results for this study");
            me.destroy();
            this.callParent();  
            return;
        }
        var propDiffStoreArray = delta3.view.rv.common.UtilFunc.buildPropDiffChartStore(me.resultsData);
        var observedExpectedStoreArray = delta3.view.rv.common.UtilFunc.buildObservedExpectedChartStore(me.resultsData);        
        var riskAdjustedSPRTStore = delta3.view.rv.common.UtilFunc.buildRiskAdjustedSPRTChartStore(me.resultsData);    
        if ( typeof me.description === 'undefined' ) {
            me.description = '';
        }        
        var itemsCount = 0;
        me.items = [];        
        if ( (typeof propDiffStoreArray !== 'undefined') && (propDiffStoreArray.length > 1) ) {   
            for (var i=0; i<propDiffStoreArray.length; i++) {
                //var c = Ext.create('delta3.view.rv.' + me.packageName + '.PropDiffChart',{
                var c = Ext.create('delta3.view.rv.extjs.PropDiffChart',{                    
                        title: 'Cumulative Proportional Graph', 
                        chartWidth: 1200,
                        chartHeight: 440,
                        yAxisTitle: 'Cumulative Proportional ' + me.outcomeName,
                        store: propDiffStoreArray[i]
                    });      
                me.items[itemsCount] = delta3.view.rv.common.UtilFunc.buildGraphPanel('Cumulative Proportional Graph',c);
                me.items[itemsCount++].add(c);                
                c.setGraphTitle(me);
            }
            me.items[itemsCount++] = {
                            xtype:  'grid.propDiff',
                            title: 'Cumulative Proportional Table',
                            resultsData: this.resultsData
                    };             
        }   
        if ( (typeof observedExpectedStoreArray !== 'undefined') && (observedExpectedStoreArray.length > 1) ) {        
            for (var i=0; i<propDiffStoreArray.length; i++) {            
                var c = Ext.create('delta3.view.rv.extjs.ObservedExpectedChart',{
                        xtype:  'chart.observedExpectedChart',
                        title: 'Cumulative Standard Graph',    
                        chartWidth: 1200,
                        chartHeight: 440,
                        yAxisTitle: 'Cumulative ' + me.outcomeName,
                        store: observedExpectedStoreArray[0]
                    });     
                me.items[itemsCount] = delta3.view.rv.common.UtilFunc.buildGraphPanel('Cumulative Standard Graph',c);                               
                me.items[itemsCount++].add(c);          
                c.setGraphTitle(me);  
            }
            me.items[itemsCount++] = {
                            xtype:  'grid.observedExpected',
                            title: 'Cumulative Standard Table',
                            resultsData: this.resultsData
                    };              
        }    
        if ( (typeof riskAdjustedSPRTStore !== 'undefined') && (riskAdjustedSPRTStore.data.length > 1) ) {           
            var c = Ext.create('delta3.view.rv.extjs.RiskAdjustedSPRTChart', {
                    title: 'Risk Adjusted SPRT Graph',   
                    chartWidth: 1200,
                    chartHeight: 440,
                    store: riskAdjustedSPRTStore
                });
            me.items[itemsCount] = delta3.view.rv.common.UtilFunc.buildGraphPanel('Risk Adjusted SPRT Graph',c);                
            me.items[itemsCount++].add(c);    
            c.setGraphTitle(me);           
            me.items[itemsCount++] = {             
                xtype:  'grid.riskAdjustedSPRT',
                title: 'Risk Adjusted SPRT Table',
                resultsData: this.resultsData
            };            
        }       
        if ( delta3.view.rv.common.UtilFunc.checkIfResultsPresent(me.resultsData,'DescriptiveStatisticsOutput') === true ) {
            me.items[itemsCount++] = {          
                xtype:  'grid.descriptiveStats',
                title: 'Descriptive Statistics Table',
                resultsData: this.resultsData
            };         
        } 
        if ( delta3.view.rv.common.UtilFunc.checkIfResultsPresent(me.resultsData,'PropensityMatchDescriptiveStats') === true ) {        
            me.items[itemsCount++] = {              
                xtype:  'grid.matchedDescriptiveStats',
                title: 'Matched Data Descriptive Statistics Table', 
                resultsData: this.resultsData
            };   
        }
        if ( delta3.view.rv.common.UtilFunc.checkIfResultsPresent(me.resultsData,'LogisticRegressionFormulaOutput') === true ) {        
            me.items[itemsCount++] = {
                xtype:  'grid.logisticRegressionFormula',
                title: 'Logistic Regression Formula Table', 
                resultsData: this.resultsData
            };               
        }   
        this.callParent();       
    }
});



