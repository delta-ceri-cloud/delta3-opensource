/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('delta3.view.rv.common.ObservedExpectedGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.observedExpected',
    scrollable: true,
    viewConfig: {
        enableTextSelection: true,
        getRowClass: function(record, rowIndex, rowParams, store) {
            if (record.get('RowId') === "1")
                return 'firstRowBackground';
        }        
    },
    resultsData: {},
    height: delta3.utils.GlobalVars.tabHeight,
    requires: [
        'Ext.data.Store',
        'Ext.data.Model'
    ],
    layout: 'fit',
    width: '100%',
    columnLines: true,
    items: [],
    initComponent: function() {
        var me = this;
        var modelFields = delta3.view.rv.common.UtilFunc.buildGridModelFields('ObservedExpectedGraphOutput',me.resultsData);
        me.store = delta3.view.rv.common.UtilFunc.buildGridStore('ObservedExpectedGraphOutput',me.resultsData, modelFields);
        me.columns = eval("(" + delta3.view.rv.common.UtilFunc.buildGridColumns('ObservedExpectedGraphOutput',me.resultsData) + ")");        
        me.tbar = []; 
        me.tbar[0] = {
            text: 'Export to CSV',
            iconCls: 'exportCSV-icon16',
            tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
            handler: function(b, e) {
                b.up('grid').exportGrid('DELTA ' + me.title);
            }
        };        
        me.callParent();
    }
});


