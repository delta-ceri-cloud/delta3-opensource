/**
 * Proportional Difference Chart uses candle stick chart
 */
//var tipWidth = 152;
//var tipHeight = 46;

Ext.define('delta3.view.rv.extjs.PropDiffChart', {
    animate: true,    
    extend: 'Ext.chart.CartesianChart',
    //alias: 'widget.chart.propDiffChart',
    requires: [
        'Ext.chart.series.CandleStick',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category'
    ],
    height: 460,
    width: '100%',
    insetPadding: 20,
    innerPadding: 20,
    yAxisTitle: 'Cumulative Proportional ',
    background: 'white',       
    sprites: [{
        id: 'spriteTitle',
        type  : 'text',
        //font: '14px Arial',
        text: 'Chart Title',
        x: 200,
        y: 16,
        width: 200,
        height: 40 
    }],
    initComponent: function() {
        var me = this;
        me.turnOnAlerts(me.store);        
        me.axes[0].setTitle(me.yAxisTitle);
        me.axes[0].setMinimum(me.calcStoreMinYValue(me.store));
        me.axes[0].setMaximum(me.calcStoreMaxYValue(me.store));
        me.callParent();
    },
    axes: [{
            type: 'numeric',
            position: 'left',
            fields: ['low', 'medium', 'high', 'open', 'close', 'zero'],        
            grid: true
        }, {
            type: 'category',
            position: 'bottom',
            fields: ['time'],
            title: 'Time period'
        }],
    series: [
        {
            type: 'candlestick',
            xField: 'time',
            openField: 'open',
            highField: 'high',
            lowField: 'low',
            closeField: 'close',
            tooltip: {
                trackMouse: true,
                renderer: function(storeItem, item) {
                    if (typeof storeItem.get('alert').description !== 'undefined') {
                        this.width = 150;
                        this.hight = 60;
                        this.setTitle(storeItem.get('alert').description + '<br>'
                                + 'Upper CI: ' + storeItem.get('high') + '<br>'
                                + 'Mean: ' + storeItem.get('medium') + '<br>'
                                + 'Lower CI: ' + storeItem.get('low'));
                    } else {
                        this.width = 110;
                        this.hight = 50;
                        this.setTitle('Upper CI: ' + storeItem.get('high') + '<br>'
                                + 'Mean: ' + storeItem.get('medium') + '<br>'
                                + 'Lower CI: ' + storeItem.get('low'));
                    }
                }
            },
            style: {
                barWidth: 10,
                barHeight: 10,
                opacity: 0.9,
                dropStyle: {
                    fill: '#ff0000',
                    stroke: '#ff0000',
                    'stroke-width': 2
                },
                raiseStyle: {
                    fill: '#333333',
                    stroke: '#333333',
                    'stroke-width': 2
                }
            },
            aggregator: {
                strategy: 'time'
            }
        }, {
            type: 'line', // artificial red zero line    
            style: {
                stroke: '#FF0000',
                opacity: 0.7,
                lineWidth: 1
            },
            highlight: {
                scale: 0.9
            },
            xField: 'time',
            yField: 'zero'
        }
    ],
    calcStoreMinYValue: function (store) {
        var bottomMargin = 0.2;
        var tmp = store.getAt(0).data.low;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.low < tmp) {
                tmp = store.getAt(i).data.low;
            }
        }
        return (tmp - bottomMargin);
    },
    calcStoreMaxYValue: function (store) {
        var topMargin = 0.2;
        var tmp = store.getAt(0).data.high;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.high > tmp) {
                tmp = store.getAt(i).data.high;
            }
        }
        return (tmp + topMargin);
    },
    setGraphTitle: function(container) {
        var surface = this.getSurface('chart');
        var sprite = surface.get('spriteTitle');
        var title = container.name;
        if ( container.description !== '' && container.description !== null ) {
            title += ' - ' + container.description;
        }
        if ( container.filterName !== '' && container.filterName !== null ) {
            title += ' - Filter [' + container.filterName + ']';
        }
        sprite.setText(title);         
    },
    turnOnAlerts: function(store) {
        // following loop is to notify the user, by changing color, about unusual record
        // candle graph, used out of the box, requires this artificial operation 
        // unfortunately it does not work when eveked from intiComponent        
        for (var i=0; i<store.data.length; i++) {
            if ( typeof store.getAt(i).data.alert.type === 'undefined' ) {
                store.getAt(i).set('open',store.getAt(i).data.medium - 0.001);
                store.getAt(i).set('close',store.getAt(i).data.medium + 0.001);
            } else {
                store.getAt(i).set('open',store.getAt(i).data.medium + 0.001);
                store.gatAt(i).set('close',store.getAt(i).data.medium - 0.001);            
            }           
        }           
    }
});