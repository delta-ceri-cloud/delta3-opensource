/* 
 * samples and not successful pieces
 */

function downloadPictureFromServer(div, params) {
    // this one is very picky when it comes to SVG code (loosing parts) and fails with error on IE10
    //<script type="text/javascript" src="modules/delta3graph/lib/rgbcolor.js"></script> 
    //<script type="text/javascript" src="modules/delta3graph/lib/StackBlur.js"></script>     
    //<script type="text/javascript" src="modules/delta3graph/lib/canvg.js"></script> 
    var svg = div.outerHTML;
    var canvas = document.createElement("canvas");
    canvas.setAttribute("style","display:none");
    document.body.appendChild(canvas);
    canvg(canvas, svg);
    var cs = new pictureServerSaver('/Delta3/ConvertCanvasServlet', params.format);
    cs.submitToServer(canvas,params.filename); 
    document.body.removeChild(canvas) ;
};

function CanvasSaver(url) {    
    this.url = url;    
    this.savePNG = function(cnvs, fname) {
        if(!cnvs || !url) return;
        fname = fname || 'picture';
         
        var data = cnvs.toDataURL("image/png");
        data = data.substr(data.indexOf(',') + 1).toString();
         
        var dataInput = document.createElement("input") ;
        dataInput.setAttribute("name", 'imgdata') ;
        dataInput.setAttribute("value", data);
        dataInput.setAttribute("type", "hidden");
         
        var nameInput = document.createElement("input") ;
        nameInput.setAttribute("name", 'name') ;
        nameInput.setAttribute("value", fname + '.png');
         
        var myForm = document.createElement("form");
        myForm.method = 'post';
        myForm.action = url;
        myForm.appendChild(dataInput);
        myForm.appendChild(nameInput);
         
        document.body.appendChild(myForm) ;
        myForm.submit() ;
        document.body.removeChild(myForm) ;
    };
     
    this.generateButton = function (label, cnvs, fname) {
        var btn = document.createElement('button'), scope = this;
        btn.innerHTML = label;
        btn.style['class'] = 'canvassaver';
        btn.addEventListener('click', function(){scope.savePNG(cnvs, fname);}, false);        
        document.body.appendChild(btn);       
        return btn;
    };
}

function downloadChartToPictureIE10(div, params) {
    // this works for external URL not for base64 URI
    var html = new XMLSerializer().serializeToString(div);
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();    
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var url = URL.createObjectURL(this.response);
        // here you can use img for drawing to canvas and handling
        image.onload = function() {
            canvas.width = params.width;
            canvas.height = params.height;                                
            context.drawImage(image, 0, 0, params.width, params.height);     
            // don't forget to free memory up when you're done (you can do this as soon as image is drawn to canvas)
            URL.revokeObjectURL(url);            
        };
        image.src = imgsrc; 
    };
    xhr.open('GET', imgsrc, true);
    xhr.responseType = 'blob';
    xhr.send();
}

function downloadChartToPictureFromServer(div, params) {
    //var html = div.outerHTML;
    var html = new XMLSerializer().serializeToString(div);    
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        function receiveResponse(responseText) {
            var responseReceived = responseText;
        }
        canvas.width = image.width;
        canvas.height = image.height;                                
        context.drawImage(image, 0, 0);     
        var data = canvas.toDataURL("image/" + params.format);
        data = data.substr(data.indexOf(',') + 1).toString();
        var str = "name=" + encodeURIComponent(params.filename) + "&imgdata=" + encodeURIComponent(data);
        // do this despite the fact that AJAX response cannot be "downloaded" (without re-post)
        ajaxPOST("/Delta3/ConvertCanvasServlet", str, receiveResponse);   
        //var cs = new pictureServerSaver('/Delta3/ConvertCanvasServlet', params.format);
        //cs.submitToServer(canvas,params.filename);        
        imgsrc = null;
        canvas = null;
        context = null;
    };  
    image.setAttribute('crossOrigin','anonymous');    
    image.setAttribute('Access-Control-Allow-Origin', '*');
    image.src = imgsrc;
};

// sample for Canvas2Image use
function downloadTableToPicture2(div, params) {
    html2canvas(div, {
        onrendered: function(canvas) {
                //theCanvas = canvas;
                document.body.appendChild(canvas);
                // Convert and download as image 
                Canvas2Image.saveAsImage(canvas, canvas.width, canvas.height, params.format);    
                //Canvas2Image.saveAsPNG(canvas);
                // Clean up 
                document.body.removeChild(canvas);
        }
    });    
};

// this is not used
function downloadPDFFromHTML(source, params) {
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    var pdf = new jsPDF('p', 'pt', 'letter');
    
    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    var specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    var margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        },

        function (dispose) {
            // dispose: object with X, Y of the last line add to the PDF 
            //          this allow the insertion of new lines after html
            pdf.save(params.filename);
        }, margins);
}

function ajaxGET(page, str, callback) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            callback(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET",page + "?" + str,true);
    xmlhttp.send();
}

function ajaxPOST(page, str, callback) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            callback(xmlhttp.responseText);    
        }
    };
    xmlhttp.open("POST", page, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(str);
}
