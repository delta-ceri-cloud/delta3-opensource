Ext.define('delta3.utils.GlobalVars', {
    statics: {
        //browser_height: Ext.getBody().getViewSize().height,
        //browser_width: Ext.getBody().getViewSize().width,
        MsgBox: Ext.MessageBox,
        currentUser: 'Not Authorized',
        logoutURL: 'index.html',
        currentUserAuthInfo: null,     
        currentUserPrefInfo: null,
        mainMenu: [],
        tabHeight: 640,
        borderHeight: 640,   
        fontFamily: 'Arial, sans-serif',     
        fontSize: '12px',
        chartWidth: 900,
        chartHeight: 500,
        chartWidthDash: 540,
        chartHeightDash: 300,        
        gridMarginHeight: 100,
        pageSize: 20,
        gridRowHeight: 21,
        largePageSize: 1000,
        veryLargePageSize: 10000,       
        OrgStore: null,
        RoleStore: null,
        PermissionStore: null,
        MetadataStore: null,
        FieldStore: null,
        UserStore: null,
        StudyStore: null,
        ModelStore: null,
        ConfigurationStore: null,
        loadSemaphore: 0,
        resultId: 'deltaId',
        // following section is for Logregr grid
        idLength: 46,
        uiControlLength: 238,
        dateLength: 70,
        initHeight: 96,
        initWidth: 330,
        dateColumnZero: 3,
        supportedDBColumnTypes: ["VARCHAR","CHAR","BIT","SMALLINT", "INT", "BIGINT", "DECIMAL", "FLOAT", "DOUBLE", "DATE", "DATETIME", "TIMESTAMP"],
        colorLowArray: ["RoyalBlue","LimeGreen","Purple","Navy", "Red","SaddleBrown", "SlateGray","Orchid", "RebeccaPurple", "OliveDrab"],
        colorArray: ["#3366ff","#00cc66","#ff0000","#ffff00", "#800000", "#9900ff", "#0099cc", "#333300", "#ff9900", "#993333"],
        maxOutcomesPerChart: 10,
        //------------------------------------------   
        graphPackageComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['package'], 
                data: [ 
                    {package: 'd3'}, 
                    {package: 'extjs'}
                ] 
            }),              
        notificationPriorityComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['priority','value'], 
                data: [ 
                    {priority: 'Normal', value: 1}, 
                    {priority: 'High', value: 2}
                ] 
            }),                 
        relationshipComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type','value'], 
                data: [ 
                    {type: 'Left Join', value: 1}, 
                    {type: 'Inner Join', value: 2},
                    {type: 'Scalar', value: 3}                    
                ] 
            }),            
        roleTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'USER'}, 
                    {type: 'OADMIN'},
                    {type: 'SADMIN'}
                ] 
            }),     
        statPackageComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'DELTAlytics'}
                ] 
            }),              
        fileTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'png'}, 
                    {type: 'jpeg'},
                    {type: 'pdf'}
                ] 
            }),      
        artifactTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['artifact', 'tag'], 
                data: [ 
                    {artifact: 'Risk Adjusted Observed Expected Chart', tag: '_raoeChart'},
                    {artifact: 'Event Rate Comparison Chart', tag: '_pdChart'}, 
                    {artifact: 'Cumulative Standard Chart', tag: '_oeChart'},
                    {artifact: 'SPRT Chart', tag: '_sprtChart'},
                    {artifact: 'Risk Model Coefficient Chart', tag: '_rmcChart'},
                    {artifact: 'Event Rate Comparison Chart Data', tag: '_pdTable'},
                    {artifact: 'Cumulative Standard Chart Data', tag: '_oeTable'},
                    {artifact: 'Risk Adjusted SPRT Chart Data', tag: '_sprtTable'},
                    {artifact: 'Descriptive Statistics', tag: '_dsTable'}, 
                    {artifact: 'Propensity Matched Descriptive Statistics', tag: '_pmdsTable'}, 
                    {artifact: 'Logistic Regression Formula Data', tag: '_lrTable'},
                    {artifact: 'Survival Matched Chart', tag: '_srvmChart'}, 
                    {artifact: 'Survival Chart', tag: '_srvChart'},
                    {artifact: 'Survival Chart Data', tag: '_srvTable'}     
                ] 
            }),              
        formulaOperatorComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['operator'], 
                data: [ 
                    {operator: 'AND'}, 
                    {operator: 'OR'}                   
                ] 
            }),             
        formulaTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'Include'}, 
                    {type: 'Exclude'}                   
                ] 
            }),    
        groupTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'project'}                  
                ] 
            }),            
        loggingComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['level'], 
                data: [ 
                    {level: 'SEVERE'}, 
                    {level: 'INFO'},
                    {level: 'FINE'}
                ] 
            }),  
        httpMethodComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['method'], 
                data: [ 
                    {method: 'POST'}, 
                    {method: 'GET'}
                ] 
            }),     
        fontFamilyComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['fontFamily'], 
                data: [ 
                    {fontFamily: 'Arial, Helvetica, sans-serif'}, 
                    {fontFamily: '"Arial Black", Gadget, sans-serif'},                     
                    {fontFamily: 'Georgia serif'}, 
                    {fontFamily: 'Impact, Charcoal, sans-serif'}, 
                    {fontFamily: '"Times New Roman", Times, serif'}, 
                    {fontFamily: 'Tahoma, Geneva, sans-serif'},
                    {fontFamily: 'Verdana, Geneva, sans-serif'},
                    {fontFamily: '"Trebuchet MS", Helvetica, sans-serif'},    
                    {fontFamily: '"Lucida Console", Monaco, monospace'},                   
                    {fontFamily: '"Courier New", Courier, monospace'}
                ] 
            }),      
        fontSizeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['fontSize'], 
                data: [ 
                    {fontSize: '8px'}, 
                    {fontSize: '9px'},
                    {fontSize: '10px'},
                    {fontSize: '11px'}, 
                    {fontSize: '12px'},
                    {fontSize: '14px'},
                    {fontSize: '16px'}, 
                    {fontSize: '18px'}                  
                ] 
            }),                 
        fieldClassComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['classField'], 
                data: [ 
                    {classField: 'Case ID'}, 
                    {classField: 'Sequencer'}, 
                    //{classField: 'Category'},
                    {classField: 'Risk Factor'},
                    {classField: 'Treatment'},
                    {classField: 'Outcome'},
                    //v3.64 added Identifier DELQA-743
                    {classField: 'Identifier'},
                    {classField: 'Not Assigned'}                    
                ] 
            }),
            
        orgTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'IT'},
                    {type: 'Research'},
                    {type: 'Hospital'}  
                ] 
            }),
        
         databaseConnectionTypeComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['type'], 
                data: [ 
                    {type: 'MySQL'},
                    {type: 'MS-SQL'}
                ] 
            }),
            
            
        fieldKindComboBoxStore: Ext.create('Ext.data.Store', {
                fields: ['kind'], 
                data: [ 
                    {kind: 'Dichotomous'}, 
                    {kind: 'Continuous'}, 
                    {kind: 'Enumerated'},
                    {kind: 'Date'},
                   // {kind: 'Identifier'},                    
                    {kind: 'Not Assigned'}                    
                ] 
            })
    }
});

