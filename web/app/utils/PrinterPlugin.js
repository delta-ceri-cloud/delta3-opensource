Ext.define("delta3.utils.PrinterPlugin", {
    statics: {
        print: function(htmlElement, printAutomatically) {
            var win = window.open('', 'Print Panel');

            win.document.open();
            win.document.write(htmlElement.outerHTML);
            win.document.close();

            if (printAutomatically) {
                win.print();
            }

            if (this.closeAutomaticallyAfterPrint) {
                if (Ext.isIE) {
                    window.close();
                } else {
                    win.close();
                }
            }
        }

    }
});

