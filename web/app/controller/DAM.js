/* 
 * DAM controller
 */

Ext.define('delta3.controller.DAM', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.DAM',
    id: 'delta3.controller.DAM',
    requires: [
        'delta3.view.DAMPanel',
        'delta3.view.DAMContainer' 
    ],     
    views: [
        'DAMPanel',
        'DAMContainer'
    ]  
});

