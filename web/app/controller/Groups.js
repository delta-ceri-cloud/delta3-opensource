/* 
 * User controller
 */

Ext.define('delta3.controller.Groups', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Groups',       
    id: 'delta3.controller.Groups',
    requires: [
        'delta3.view.GroupGrid',
        'delta3.view.GroupContainer',
        'delta3.model.GroupModel',
        'delta3.store.GroupStore'],          
    models: [
        'GroupModel'
    ],
    stores: [
        'GroupStore'
    ],
    views: [
        'GroupGrid',
        'GroupContainer'
    ],
    addContnet: function() {
        console.log('Group controller Ready!');
    }    
});
