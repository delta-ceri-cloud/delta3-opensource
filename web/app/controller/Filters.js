/* 
 * Filters controller
 */


Ext.define('delta3.controller.Filters', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Filters',       
    id: 'delta3.controller.Filters',
    requires: [
        'delta3.view.filter.FilterGrid',
        'delta3.model.FilterModel',
        'delta3.store.FilterStore'],           
    models: [
        'FilterModel'
    ],
    stores: [
        'FilterStore'
    ],
    views: [
        'FilterGrid',
        'FilterContainer'
    ]   
});