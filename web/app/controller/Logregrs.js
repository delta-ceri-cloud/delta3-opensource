/* 
 * Logistic Regression controller
 */

Ext.define('delta3.controller.Logregrs', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Logregrs',    
    id: 'delta3.controller.Logregrs',
    requires: [
        'delta3.view.logregr.LogregrGrid',
        'delta3.view.logregr.LogregrContainer',
        'delta3.model.LogregrModel',
        'delta3.store.LogregrStore'
    ],        
    models: [
        'LogregrModel'
    ],
    stores: [
        'LogregrStore'
    ],
    views: [
        'LogregrGrid',
        'LogregrContainer'
    ]  
});
