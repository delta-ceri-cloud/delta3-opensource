/* 
 * Organization controller
 */

Ext.define('delta3.controller.Organizations', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Organizations',       
    id: 'delta3.controller.Organizations',
    requires: [
        'delta3.view.OrganizationGrid',
        'delta3.view.OrganizationContainer',
        'delta3.model.OrganizationModel',
        'delta3.store.OrganizationStore'],          
    models: [
        'OrganizationModel'
    ],
    stores: [
        'OrganizationStore'
    ],
    views: [
        'OrganizationGrid',
        'OrganizationContainer'
    ]   
});