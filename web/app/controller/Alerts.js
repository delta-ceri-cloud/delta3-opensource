/* 
 * Alert controller
 */

Ext.define('delta3.controller.Alerts', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Alerts',
    id: 'delta3.controller.Alerts',
    requires: [
        'delta3.view.event.AlertGrid',
        'delta3.view.event.AlertContainer',
        'delta3.model.AlertModel',
        'delta3.store.AlertStore'        
    ],    
    models: [
        'AlertModel'
    ],
    stores: [
        'AlertStore'
    ],    
    views: [
        'AlertGrid',
        'AlertContainer'
    ]  
});

