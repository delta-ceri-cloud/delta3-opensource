/* 
 * Stats (statistical engine configurator) controller
 */


Ext.define('delta3.controller.Stats', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Stats',       
    id: 'delta3.controller.Stats',
    requires: [
        'delta3.view.statPackage.StatGrid',
        'delta3.view.statPackage.StatContainer',
        'delta3.model.StatModel',
        'delta3.store.StatStore'],           
    models: [
        'StatModel'
    ],
    stores: [
        'StatStore'
    ],
    views: [
        'StatGrid',
        'StatContainer'
    ]   
});