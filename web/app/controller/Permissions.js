/* 
 * Permission controller
 */


Ext.define('delta3.controller.Permissions', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Permissions',       
    id: 'delta3.controller.Permissions',
    requires: [
        'delta3.view.PermissionGrid',
        'delta3.view.PermissionContainer',
        'delta3.model.PermissionModel',
        'delta3.store.PermissionStore'],          
    models: [
        'PermissionModel'
    ],
    stores: [
        'PermissionStore'
    ],
    views: [
        'PermissionGrid',
        'PermissionContainer'
    ]
});