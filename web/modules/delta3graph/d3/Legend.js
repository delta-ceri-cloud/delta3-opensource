/* 
 * (C) 2016 CERI-Lahey
 * 
 */

function legend(chart, store, width, outcomeVariables, that, changeResultOpacity) {
    chart.append("svg:g")
        .append("rect")
            .style('fill', 'white')
            .style('stroke', 'white')  
            .style('stroke-opacity', that.param.lowerOpacity)
            .attr('class', 'legend')            
            .attr('width', that.param.legendWidth)
            .attr('height', store.length * that.param.legendSpacing + that.param.legendSpacingMargin)
            .attr("x", width - that.param.legendWidth - that.param.legendSpacingMargin - 1)
            .attr("y", that.param.legendSpacingMargin + 1);   
    
    for (var ii=0; ii<store.length; ii++) {
       chart.append("svg:g")
            .append("rect")
                .style('fill', delta3.utils.GlobalVars.colorLowArray[ii])
                .style('stroke', delta3.utils.GlobalVars.colorLowArray[ii])  
                .style('stroke-opacity', that.param.lowerOpacity)
                .attr('class', 'rectMap')            
                .attr('id', 'rectMap' + ii)
                .attr('width', that.param.legendRectSize)
                .attr('height', that.param.legendRectSize)
                .attr("x", width - that.param.legendWidth + 2*that.param.legendSpacingMargin)
                .attr("y", ii * that.param.legendSpacing + 2*that.param.legendSpacingMargin)
                .on("mouseover", function () {
                        var index = parseInt(this.id.substring(7));
                        changeResultOpacity(index, that.param.higherOpacity);
                    })
                .on("mouseout", function () {
                        var index = parseInt(this.id.substring(7));                    
                        changeResultOpacity(index, that.param.lowerOpacity);                    
                    });        
  
        chart.append("svg:g")
            .append('text') 
                .attr('id', 'textMap' + ii)
                .style("font-family",that.param.fontFamily)
                .style("font-size",that.param.legendFontSize)   
                .style('fill', delta3.utils.GlobalVars.colorLowArray[ii])
                .style('stroke', delta3.utils.GlobalVars.colorLowArray[ii])   
                .style('stroke-opacity', that.param.lowerOpacity)
                .style("stroke-width","1")
                //.style("shape-rendering","crispEdges")     
                .attr("x", width - that.param.legendWidth + that.param.legendSpacing + 2*that.param.legendSpacingMargin)
                .attr("y", ii * that.param.legendSpacing + that.param.legendSpacing)          
                .text(outcomeVariables[ii])
                .on("mouseover", function () {
                        var index = parseInt(this.id.substring(7));
                        changeResultOpacity(index, that.param.higherOpacity);
                    })
                .on("mouseout", function () {
                        var index = parseInt(this.id.substring(7));                    
                        changeResultOpacity(index, that.param.lowerOpacity);                    
                    });          
        if ( ii === delta3.utils.GlobalVars.maxOutcomesPerChart - 1 ) break;        
    }    
}

module.exports.legend = legend;
