/**
 * D3 Logistic Regression Chart
 * Derived source for SVG Rendering: https://www.dashingd3js.com/svg-basic-shapes-and-d3js
 * Derived Source: http://fiddle.jshell.net/7KJC7/2/
 * Derived Source: http://bl.ocks.org/mbostock/3885705
 * Derived Source: http://bl.ocks.org/mbostock/7555321
 * Derived Source: https://www.dashingd3js.com/svg-group-element-and-d3js
 * Slider Copyright (c) 2013, Bjorn Sandvik
 */

var RiskModelCoeffChart = function () {
    var id,
        blockId,
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth,
        browser,
        graphTitle,
        graphSubTitle,
        yAxisTitle;
};

RiskModelCoeffChart.prototype.init = function (graphTitle, graphSubTitle, yAxisTitle, id, width, height, executeFromBrowser) {
    this.graphTitle = graphTitle;
    this.graphSubTitle = graphSubTitle;
    this.yAxisTitle = yAxisTitle;
    this.chartWidth = width;
    this.chartHeight = height;
    this.browser = executeFromBrowser;
    this.id = id;
    this.blockId = '_rmc';
    this.combinedSVGId = id + this.blockId + "Chart";
    this.combinedDIVId = id + this.blockId + "Chart";
};

RiskModelCoeffChart.prototype.getMarkup = function (index, store, cell, d3) {

    console.log("d3 RiskModelCoeffChart generation started.");
    try {
        // define draw and change function ahead of time to satisfied FireFox
        function draw(svg, data) {
            svg.selectAll("g.valueGroup").remove();

            var groups = svg.selectAll(".valueGroup").data(data);
            groups.exit().remove();
            groups.enter().append("g").attr("class", "valueGroup").attr("x", xMap).attr("y", yMap);

            var circle1 = groups.append("circle")
                    .attr("class", "pointValue")
                    .attr("r", 5.5)
                    .attr("cy", yMap)
                    .attr("cx", xMap)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .style("fill", "green")
                    .append("svg:title")
                        .text(function(d) { 
                            if ( typeof d.alert.description === 'undefined') {    
                                return '  Odds Ratio: ' 
                                       + d.OddsRatio + ' ';
                            } else {
                                return '  Odds Ratio: ' 
                                       + d.OddsRatio + ' ';    
                            }
                        });            

            var confidenceInterval = groups.append("svg:line")
                    .attr("class", "ciLineWide")
                    .attr("x1", xMapLCI)
                    .attr("y1", yMap)
                    .attr("x2", xMapUCI)
                    .attr("y2", yMap)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .style("stroke", "rgb(6,120,155)");

            var lowerCILine = groups.append("svg:line")
                    .attr("class", "ciTickLow")
                    .attr("x1", xMapLCI)
                    .attr("y1", yMapCITop)
                    .attr("x2", xMapLCI)
                    .attr("y2", yMapCIBottom)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .style("stroke", "rgb(6,120,155)");

            var upperCILine = groups.append("svg:line")
                    .attr("class", "ciTickHigh")
                    .attr("x1", xMapUCI)
                    .attr("y1", yMapCITop)
                    .attr("x2", xMapUCI)
                    .attr("y2", yMapCIBottom)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .style("stroke", "rgb(6,120,155)");
            // there is a known error related to anti-aliasing with some scaling for the lines
        }

        function change() {
            var selectedValue = d3.event.target.value;
            var selectedIndex = d3.event.target.selectedIndex;

            var y0 = dropDownSort(selectedValue);

            svg.selectAll(".dot").sort(function (a, b) {
                return y0(a.Variable) - y0(b.Variable);
            });

            var transition = svg.transition().duration(750),
                    delay = function (d, i) {
                        return i * 50;
                    };

            transition.selectAll(".pointValue")
                    .delay(delay)
                    .attr("cy", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand());
                    });

            transition.selectAll(".ciLineWide")
                    .delay(delay)
                    .attr("y1", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand());
                    })
                    .attr("y2", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand());
                    });

            transition.selectAll(".ciTickLow")
                    .delay(delay)
                    .attr("y1", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand()) + (0.05 * y.rangeBand());
                    })
                    .attr("y2", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand()) - (0.05 * y.rangeBand());
                    });

            transition.selectAll(".ciTickHigh")
                    .delay(delay)
                    .attr("y1", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand()) + (0.05 * y.rangeBand());
                    })
                    .attr("y2", function (d) {
                        return y0(d.Variable) + (0.5 * y.rangeBand()) - (0.05 * y.rangeBand());
                    });
            // this part moves the values on the axis
            transition.select(".y.axis")
                    .call(yAxis)
                    .selectAll("g")
                    .delay(delay);
        }    

        function dropDownSort(selectedValue) {
            if (selectedValue === "Alphabetical") {
                return y.domain(data.sort(function (a, b) {
                    return d3.ascending(a.Variable, b.Variable);
                })
                        .map(function (d) {
                            return d.Variable;
                        }))
                        .copy();
            } else if (selectedValue === "ValueAscending") {
                return y.domain(data.sort(function (a, b) {
                    return b.OddsRatio - a.OddsRatio;
                })
                        .map(function (d) {
                            return d.Variable;
                        }))
                        .copy();
            } else if (selectedValue === "ValueDescending") {
                return y.domain(data.sort(function (a, b) {
                    return a.OddsRatio - b.OddsRatio;
                })
                        .map(function (d) {
                            return d.Variable;
                        }))
                        .copy();
            } else {
                return y.domain(data.sort(function (a, b) {
                    return a.Variable - b.Variable;
                })
                        .map(function (d) {
                            return d.Variable;
                        }))
                        .copy();
            }
        }

        var me = this;
        var margin = {top: 40, right: 10, bottom: 40, left: 220},
            width = this.chartWidth - margin.left - margin.right,
            height = this.chartHeight - margin.top - margin.bottom;

        var theDiv = document.getElementById(cell);
        if ( this.browser === true ) {  
            var sortDiv = document.getElementById("divSort" + index);
            sortDiv.insertAdjacentHTML('beforeend','<select id="ddSort' + index + '"name="mydropdown"><option value="Unsorted">Unsorted</option><option value="Alphabetical">Alphabetical By Name</option><option value="ValueAscending">Ascending By Value</option><option value="ValueDescending">Descending By Value</option></select>');
        }
        var svg = d3.select(theDiv).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")      
            .attr("class", "delta3chart")
            .attr("width", this.chartWidth)
            .attr("height", this.chartHeight)  
            .attr("id", this.combinedSVGId + index)
            .style("background-color", "white")      
            .style("font-family","Arial, sans-serif")
            .style("shape-rendering","crispEdges")    
            .style("font-size","14px")
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        /*svg.append("svg:g")
            .append("rect")
            .attr("fill","#f2f2f2")
            .attr("width", width)
            .attr("height", height);*/

        /* 
         * value accessor - returns the value to encode for a given data object.
         * scale - maps value to a visual display encoding, such as a pixel position.
         * map function - maps from data value to display value
         * axis - sets up axis
         */
        var data = store.data;

        var x = d3.scale.linear().range([0, width]);    
        x.domain([this.calcStoreMinYValue(data), this.calcStoreMaxYValue(data)]);
        var xMap = function (d) {
            return x(d.OddsRatio);
        }; 
        var xMapLCI = function (d) {
            return xMap(d) - (xMap(d) * 0.10);
        }; 
        var xMapUCI = function (d) {
            return xMap(d) + (xMap(d) * 0.10);
        };  
        var xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(10);


        var y = d3.scale.ordinal().rangeRoundBands([0, height], .1);
        y.domain(data.map(function (d) {
            return d.Variable;
        }));    
        var yMap = function (d) {
            return y(d.Variable) + 0.5 * y.rangeBand();
        };
        var yAxis = d3.svg.axis().scale(y).orient("left");

        var yMapCITop = function (d) {
            return yMap(d) + (0.05 * y.rangeBand());
        };  
        var yMapCIBottom = function (d) {
            return yMap(d) - (0.05 * y.rangeBand());
        }; 

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

        svg.selectAll('.axis line')
             .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
        svg.selectAll('.axis path')
             .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});

        var clip = svg.append("defs").append("svg:clipPath")
                .attr("id", "clip" + cell)
                .append("svg:rect")
                .attr("id", "clip-rect" + cell)
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", width)
                .attr("height", height);

        // Add the line by appending an svg:path element with the data line we created above
        // do this AFTER the axes above so that the line is above the tick-lines
        var path = svg.append("svg:path").attr("class", "path").attr("clip-path", "url(#clip" + cell + ")").attr("d", draw(svg, data));
        //add this event listener using d3 to the drop down sort element (as a whole)
        if ( this.browser === true ) {    
            d3.select("#ddSort" + index).on("change", change);
        }

        if ( this.browser === true ) {  
            function sliderFunc(event, ui) {
                //var maxXValue = d3.max(data, function(d) { return d.OddsRatio + (d.OddsRatio * 0.10); });
                var maxXValue = me.calcStoreMaxYValue(data);
                var minXValue = me.calcStoreMinYValue(data);
                var maxv = d3.min([ui[1], maxXValue]);
                var minv = d3.max([ui[0], minXValue]);
                x.domain([minv, maxv]);
                svg.transition().duration(750).select(".x.axis").call(xAxis);
                svg.transition().duration(750).select(".path").attr("d", draw(svg, data));
            };        
            var sliderDiv = d3.select(theDiv).append("div").attr("id", "slider");
            var slider = d3.slider().axis(true)
                    .on("slide", sliderFunc)
                    //.value([0,d3.max(data, function(d) {return d.OddsRatio;})])
                    .value([me.calcStoreMinYValue(data),me.calcStoreMaxYValue(data)])
                    .min(me.calcStoreMinYValue(data))
                    //.max(d3.max(data, function(d) {return d.OddsRatio;}))
                    .max(me.calcStoreMaxYValue(data))
                    .step(1);
            d3.select('#slider').call(slider);
            svg.append("svg:g")
                .append("text")
                    .attr("x", width/2)
                    .attr("y", 2*margin.top/3) 
                    .text( this.graphSubTitle )
                    .attr("text-anchor", "middle")
                    .attr("font-size", this.param.fontSize);              
            svg.append("svg:g")
                .append("text")
                .attr("x", width/2)
                .attr("y", margin.top/3)
                .text( this.graphTitle )
                .attr("text-anchor", "middle")        
                .attr("font-size", "14px");                  
        }         
    } catch (err) {
        console.log("DELTA3G Error: " + err.message);
    }    
    console.log("d3 RiskModelCoeffChart generation finished.");
    return svg;
};

RiskModelCoeffChart.prototype.calcStoreMinYValue = function (data) { 
    var tmp = 0;
    for (var i = 0; i < data.length; i++) {
        data[i].OddsRatio = +data[i].OddsRatio;
        if ( data[i].OddsRatio > 0 ) {
            if (data[i].OddsRatio - (data[i].OddsRatio * 0.10) < tmp) {
                    tmp = data[i].OddsRatio - (data[i].OddsRatio * 0.10);
                }
        } else {
            if (data[i].OddsRatio + (data[i].OddsRatio * 0.10) < tmp) {
                    tmp = data[i].OddsRatio + (data[i].OddsRatio * 0.10);
                }            
        }
    }
    return tmp;
};

RiskModelCoeffChart.prototype.calcStoreMaxYValue = function (data) { 
    var tmp = 0;
    for (var i = 0; i < data.length; i++) {
        data[i].OddsRatio = +data[i].OddsRatio;
        if ( data[i].OddsRatio > 0 ) {        
            if (data[i].OddsRatio + (data[i].OddsRatio * 0.10) > tmp) {
                tmp = data[i].OddsRatio + (data[i].OddsRatio * 0.10);
            }
        } else {
            if (data[i].OddsRatio - (data[i].OddsRatio * 0.10) > tmp) {
                tmp = data[i].OddsRatio - (data[i].OddsRatio * 0.10);
            }            
        }
    }
    return tmp;
};

RiskModelCoeffChart.prototype.download = function (params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML;
    var imgsrc = 'data:image/svg+xml;base64,' + btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function () {
        canvas.width = image.width;
        canvas.height = image.height;
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" + +params.format + "\"");
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };
    image.src = imgsrc;

};

module.exports.RiskModelCoeffChart = RiskModelCoeffChart;
module.exports.getMarkup = RiskModelCoeffChart.prototype.getMarkup;
module.exports.init = RiskModelCoeffChart.prototype.init;
module.exports.download = RiskModelCoeffChart.prototype.download;
