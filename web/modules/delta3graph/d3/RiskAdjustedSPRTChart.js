/**
 * D3 Risk Adjusted SPRT Chart local
 */
    
var RiskAdjustedSPRTChart = function() {    
        var id,
        blockId,            
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth,
        browser,
        graphTitle,
        graphSubTitle,
        yAxisTitle;
    };
        
RiskAdjustedSPRTChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, id, chartParams, executeFromBrowser) {
    this.graphTitle = graphTitle; 
    this.graphSubTitle = graphSubTitle;     
    this.yAxisTitle = yAxisTitle;
    this.param = chartParams;
    this.browser = executeFromBrowser;
    this.id = id;
    this.blockId = '_sprt';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
};   

RiskAdjustedSPRTChart.prototype.getMarkup = function (index, store, cell, d3) {
    console.log('d3 RiskAdjustedSPRTChart generation started.');

    var colors = d3.scale.category20();
    // Create Margins and Axis and hook our zoom function
    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},      
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .domain([0, store.data.length-1])
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([this.calcStoreMinYValue(store), this.calcStoreMaxYValue(store)])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(-height)
            .tickPadding(10)
            .tickSubdivide(true)
            .orient("bottom");

    var yAxis = d3.svg.axis()
            .scale(y)
            .tickPadding(10)
            .tickSize(-width)
            .tickSubdivide(true)                
            .orient("left"); 
    // Create D3 line object and draw data on our SVG object
    var svg, zoom;
    if ( this.browser === true ) {
        zoom = d3.behavior.zoom()
            .x(x)
            .y(y)
            .scaleExtent([1, 10])
            .on("zoom", zoomedSPRT);
    
        var div = document.getElementById(cell);
        svg = d3.select(div).append("svg")
            .call(zoom)
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")       
            .attr("class", "delta3chart")    
            .attr("width", this.param.width)
            .attr("height", this.param.height)    
            .style("background-color", "white")      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index);       
    } else {
        var div = document.getElementById(cell);  
        svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")    
            .attr("class", "delta3chart")    
            .attr("width", this.param.width)
            .attr("height", this.param.height)  
            .style("background-color", "white")      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index); 
    }

    
    var rect = svg.append("g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("x", margin.left)
        .attr("y", margin.top)
        .attr("width", width)
        .attr("height", height);

    var chart = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    chart.append("svg:g")                
        .attr("class", "x axis")  
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis) 
        .append("text")
            .attr("class", "Observation Count")
            .attr("y", margin.bottom-2)	  
            .attr("x", width/2)
        .text('Observation Count');

    chart.append("svg:g")         
        .attr("class", "y axis")    
        .call(yAxis)              
        .append("text")
            .attr("class", "Cumulative Log Likelihood Ratio")
            .attr("transform", "rotate(-90)")
            .attr("y", (-margin.left) + 14)
            .attr("x", -2*height/3)      
        .text('Cumulative Log Likelihood Ratio');	

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});

    chart.append("clipPath")
        .attr("id", "clip" + cell)
        .append("rect")
            .attr("width", width)
            .attr("height", height);

    var line = d3.svg.line()          
        .x(function(d) {return x(d.RowId); })
        .y(function(d) {return y(d.riskAdjustedValue); })
        .interpolate("step-after");

    var lineUpper = d3.svg.line()          
        .x(function(d) {return x(d.RowId); })
        .y(function(d) {return y(d.upper); })
        .interpolate("step-after");

    var lineLower = d3.svg.line()          
        .x(function(d) {return x(d.RowId); })
        .y(function(d) {return y(d.lower); })
        .interpolate("step-after");	

    var groupSPRTGraphs = chart.append("svg:g")            
                            .attr("class","SPRT Graphs")
                            .attr("stroke-width",1)
                            .style("fill-opacity", 1)
                            .attr("clip-path", "url(#clip" + cell + ")");

    groupSPRTGraphs.append("svg:path")
            .attr("class", "SPRT_1")
            .attr("clip-path", "url(#clip" + cell + ")")
            //.attr('stroke', function(d,i){ return colors(0);})      
            .attr("stroke", "steelblue") 
            .attr("stroke-width",2)
            .attr("fill","none")
            .attr("d", line(store.data));
    
    groupSPRTGraphs.append("svg:path")
            .attr("class", "SPRT_U")
            .attr("clip-path", "url(#clip" + cell + ")")
            .attr("stroke-dasharray",("3,3"))
            .attr("stroke", "steelblue") 
            .attr("stroke-width",2)
            .attr("fill","none")
            .attr("d", lineUpper(store.data));    
    
    groupSPRTGraphs.append("svg:path")
            .attr("class", "SPRT_L")
            .attr("clip-path", "url(#clip" + cell + ")")
            .attr("stroke-dasharray",("3,3"))
            .attr("stroke", "steelblue") 
            .attr("stroke-width",2)
            .attr("fill","none")
            .attr("d", lineLower(store.data));    
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", 2*margin.top/3)
            .text( this.graphSubTitle )
            .attr("text-anchor", "middle")     
            .attr("font-size", this.param.fontSize);  
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", margin.top/3)
            .text( this.graphTitle )
            .attr("text-anchor", "middle")     
            .attr("font-size", this.param.fontSize);            
    console.log('d3 RiskAdjustedSPRTChart generation finished.');
    return svg;    
    
    function zoomedSPRT() {
        //var t = zoom.translate();
        //var tx = t[0];
        //var ty = t[1];
        //tx = Math.min(tx,0);
        //zoom.translate([tx,ty]);
        chart.select(".x.axis").call(xAxis);
        chart.select(".y.axis").call(yAxis);
        chart.select(".SPRT_1").attr('d', line(store.data));
        chart.select(".SPRT_U").attr('d', lineUpper(store.data));
        chart.select(".SPRT_L").attr('d', lineLower(store.data));        
    }                 
};                  
 
RiskAdjustedSPRTChart.prototype.calcStoreMinYValue = function(store) {
    if ( store.data.length === 0 ) return 0;
    var bottomMargin = 1;
    var tmp = store.data[0].lower;
    for (var i=0; i< store.data.length; i++) {
        if (store.data[i].riskAdjustedValue < tmp) {
            tmp = store.data[i].riskAdjustedValue;
        }
    }
    return (tmp - bottomMargin);
};
    
RiskAdjustedSPRTChart.prototype.calcStoreMaxYValue = function(store) {
    if ( store.data.length === 0 ) return 0;
    var topMargin = 1;
    var tmp = store.data[0].upper;
    for (var i=0; i< store.data.length; i++) {
        if (store.data[i].riskAdjustedValue > tmp) {
            tmp = store.data[i].riskAdjustedValue;
        }
    }
    return (tmp + topMargin);
};  
    
RiskAdjustedSPRTChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML; 
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                                
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");
        //var _img = '<img src="'+canvasdata+'">'; 
        //d3.select("#pngdataurl").html(_img);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };      
    image.src = imgsrc;    
};     

module.exports.RiskAdjustedSPRTChart = RiskAdjustedSPRTChart;
module.exports.getMarkup = RiskAdjustedSPRTChart.prototype.getMarkup;
module.exports.init = RiskAdjustedSPRTChart.prototype.init;
module.exports.download = RiskAdjustedSPRTChart.prototype.download;