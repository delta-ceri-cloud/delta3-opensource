/* 
 * (C) CERI Lahey Clinic
 * Results Processor - to be executed in the browser
 * requires d3.js libraries to be loaded prior to loading this script
 */

var data, jsonData;
var fontFamily= 'Arial, sans-serif';     
var fontSize= '12px';
var chartWidth = 900;
var chartHeight = 500;

function downloadResults(divId, format) {
    var div = document.getElementById(divId);    
    var params = {
            filename: divId, 
            format: format,
            width: chartWidth,
            height: chartHeight
        };
    switch (params.format) {
        case 'png':
        case 'jpeg':
            if (div.id.indexOf('Table') > -1 ) {
                downloadPictureFromServer(div,params);
            } else {
                downloadSVGToPicture(div,params);
            }
            break;
        case 'pdf':
            if (div.id.indexOf('Table') > -1 ) {
                downloadTableToPDF(div,params);
            } else {
                downloadChartToPDF(div,params);
            }
            break;
        case 'csv':
            if (div.id.indexOf('Table') > -1 ) {
                downloadTableToCSV(div,params);
            }           
        }    
}
//=====================================================================================================
function processResult(resultsData, inputParams, studyName, executeFromBrowser, cell, index) { 
 try {
    if ( (resultsData === '') || (typeof resultsData === 'undefined') ) {
        return ("There are no results for this study"); 
    }
    var id = 'DELTA3G', 
    packageName = 'd3', 
    outcomeName = 'Outcome', 
    description = 'Graph Data'; 
    var chartParams = {
        'marginTop': 40,
        'marginRight': 70,
        'marginBottom': 50,
        'marginLeft': 50,
        'fontSize': fontSize,
        'backgroundColor':  'white',
        'fontFamily': fontFamily,            
        'width': 900,
        'height': 500,
        'legendFontSize': '9px',
        'legendWidth': 90,
        'legendSpacing': 12,
        'legendSpacingMargin': 1,
        'legendRectSize': 10,
        'lowerOpacity': 0.4,
        'higherOpacity': 1.0};  
       
    console.log('params: ' + JSON.stringify(resultsData.params));
    
    if ( typeof resultsData.params !== 'undefined' ) {
        if ( typeof resultsData.params.id !== 'undefined') id = resultsData.params.id;
        if ( typeof resultsData.params.package !== 'undefined') packageName = resultsData.params.package;    
        if ( typeof resultsData.params.outcome !== 'undefined') outcomeName = resultsData.params.outcome;
        if ( typeof resultsData.params.description !== 'undefined') description = resultsData.params.description;    
        if ( typeof resultsData.params.width !== 'undefined') chartParams.width = resultsData.params.width;
        if ( typeof resultsData.params.height !== 'undefined') chartParams.height = resultsData.params.height;   
        if ( typeof resultsData.params.fontFamily !== 'undefined') chartParams.fontFamily = resultsData.params.fontFamily;    
        if ( typeof resultsData.params.fontSize !== 'undefined') chartParams.fontSize = resultsData.params.fontSize;     
        if ( typeof inputParams !== 'undefined') chartParams.inputParams = inputParams;            
    }
           
    var propDiffStoreArray = buildPropDiffChartStore(resultsData);
    var observedExpectedStoreArray = buildObservedExpectedChartStore(resultsData);        
    var riskAdjustedSPRTStoreArray = buildRiskAdjustedSPRTChartStore(resultsData);    
    var riskModelCoeffStoreArray = buildRiskModelCoeffChartStore(resultsData);
    var survivalStoreArray = buildSurvivalChartStore(resultsData, inputParams);    

    if ( typeof description === 'undefined' ) {
        description = '';
    }        
       
    document.getElementById(cell).style.margin = "10px 50px 20px 20px";
    if ( (typeof observedExpectedStoreArray !== 'undefined') && (observedExpectedStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {   
            if ( resultsData.results[i].name === "ObservedExpectedGraphOutput" && resultsData.results[i].data.length > 0 ) {   
                if ( j === index ) {
                    var riskAdjustedOEChart = new RiskAdjustedOEChart(); 
                    riskAdjustedOEChart.init(studyName,'Risk Adjusted Observed Expected Chart','Cumulative Proportional ' + resultsData.results[i].Y_Axis, id, chartParams);            
                    var idTag = riskAdjustedOEChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);    
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';              
                    document.getElementById(cell).appendChild(iDiv);         
                    riskAdjustedOEChart.getMarkup(j, observedExpectedStoreArray[j++], iDiv.id, d3);       
                } else {
                    j++;
                }
            }
        }           
    }      
    if ( (typeof propDiffStoreArray !== 'undefined') && (propDiffStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {
            if ( resultsData.results[i].name === "ProportionalDifferenceGraphOutput" && resultsData.results[i].data.length > 0 ) {                     
                if ( j === index ) {               
                    var propDiffChart = new PropDiffChart(); 
                    propDiffChart.init(studyName,'Event Rate Comparison Chart','Cumulative Proportional ' + resultsData.results[i].Y_Axis, id, chartParams);            
                    var idTag = propDiffChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);               
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';
                    //iDiv.setAttribute("style","width:950px;height:550px");
                    document.getElementById(cell).appendChild(iDiv);         
                    propDiffChart.getMarkup(j, propDiffStoreArray[j++], iDiv.id, d3); 
                } else {
                    j++;
                }
            }
        }           
    }   
    if ( (typeof observedExpectedStoreArray !== 'undefined') && (observedExpectedStoreArray.length > 0) ) {           
        for (var i=0, j=0; i<resultsData.results.length; i++) {    
            if ( resultsData.results[i].name === "ObservedExpectedGraphOutput" && resultsData.results[i].data.length > 0  ) {   
                if ( j === index ) {                
                    var observedExpectedChart = new ObservedExpectedChart();     
                    observedExpectedChart.init(studyName,'Cumulative Standard Chart','Cumulative ' + resultsData.results[i].Y_Axis, id, chartParams);               
                    var idTag = observedExpectedChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);               
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';
                    document.getElementById(cell).appendChild(iDiv);            
                    observedExpectedChart.getMarkup(j, observedExpectedStoreArray[j++], iDiv.id, d3);   
                } else {
                    j++;
                }
            }
        }            
    }    
    if ( (typeof riskAdjustedSPRTStoreArray !== 'undefined') && (riskAdjustedSPRTStoreArray.length > 0) ) {              
        for (var i=0, j=0; i<resultsData.results.length; i++) { 
            if ( resultsData.results[i].name === "RaSprtGraphOutput" && resultsData.results[i].data.length > 0  ) {  
                if ( j === index ) {                   
                    var riskAdjustedSPRTChart = new RiskAdjustedSPRTChart();
                    riskAdjustedSPRTChart.init(studyName,'Risk Adjusted SPRT Chart','Risk Adjusted SPRT ' + outcomeName, id, chartParams, executeFromBrowser);                    
                    var idTag = riskAdjustedSPRTChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);               
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';            
                    document.getElementById(cell).appendChild(iDiv);        
                    riskAdjustedSPRTChart.getMarkup(j, riskAdjustedSPRTStoreArray[j++], iDiv.id, d3);  
                } else {
                    j++;
                }
            }
        }
    } 
    if ( (typeof riskModelCoeffStoreArray !== 'undefined') && (riskModelCoeffStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {
            if ( resultsData.results[i].name === "RiskModelCoeffGraphOutput" && resultsData.results[i].data.length > 0  ) {    
                if ( j === index ) {                      
                    var riskModelCoeffChart = new RiskModelCoeffChart(); 
                    riskModelCoeffChart.init(studyName,'Risk Model Coefficient Chart','General Risk Model ' + outcomeName, id, chartParams.width, chartParams.height, executeFromBrowser);                
                    var idTag = riskModelCoeffChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);               
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';
                    document.getElementById(cell).appendChild(iDiv);   
                    var iDivSort = document.createElement('div');
                    iDivSort.id = "divSort" + j;    
                    document.getElementById(iDiv.id).appendChild(iDivSort);                 
                    riskModelCoeffChart.getMarkup(j, riskModelCoeffStoreArray[j++], iDiv.id, d3);    
                } else {
                    j++;
                }
            }
        }           
    }      
    if ( (typeof survivalStoreArray !== 'undefined') && (survivalStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {
            if ( resultsData.results[i].name === "SurvivalGraphOutput" && resultsData.results[i].data.length > 0  ) {
                if ( j === index ) {                      
                    var survivalChart = new SurvivalChart(); 
                    survivalChart.init(studyName, 'Survival Chart','Survival ' + resultsData.results[i].Y_Axis, resultsData.results[i].X_Axis, id, chartParams, executeFromBrowser);            
                    var idTag = survivalChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);               
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';
                    document.getElementById(cell).appendChild(iDiv);   
                    var iDivSort = document.createElement('div');
                    iDivSort.id = "divSort" + j;    
                    document.getElementById(iDiv.id).appendChild(iDivSort);                 
                    survivalChart.getMarkup(j, survivalStoreArray[j++], iDiv.id, d3);   
                } else {
                    j++;
                }
            }
            if ( resultsData.results[i].name === 'SurvivalMatchedGraphOutput' && resultsData.results[i].data.length > 0  ) {
                if ( j === index ) {                      
                    var survivalMatchedChart = new SurvivalMatchedChart(); 
                    survivalMatchedChart.init(studyName,'Matched Survival Chart','Survival ' + resultsData.results[i].Y_Axis, resultsData.results[i].X_Axis, id, chartParams, executeFromBrowser);            
                    var idTag = survivalMatchedChart.combinedDIVId + j;             
                    var iDivDownload = document.createElement('div');
                    iDivDownload.id = "download_" + idTag;       
                    iDivDownload.align = 'right';      
                    iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                    iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                    iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                    document.getElementById(cell).appendChild(iDivDownload);               
                    var iDiv = document.createElement('div');
                    iDiv.id = cell + "_" + idTag;
                    iDiv.align = 'center';
                    document.getElementById(cell).appendChild(iDiv);   
                    var iDivSort = document.createElement('div');
                    iDivSort.id = "divSort" + j;    
                    document.getElementById(iDiv.id).appendChild(iDivSort);                 
                    survivalMatchedChart.getMarkup(j, survivalStoreArray[j++], iDiv.id, d3);   
                } else {
                    j++;
                }
            }            
        }           
    }      
    
    for (var ii=0; ii<tablesConfig().length; ii++) {
        if ( checkIfResultsPresent(resultsData,tablesConfig()[ii].tag) === true ) {      
            var idTag = id + '_' + tablesConfig()[ii].htmlId + 'Table' + tablesConfig()[ii].index;
            var iDivDownload = document.createElement('div');
            iDivDownload.id = "download_" + idTag;       
            iDivDownload.align = 'right';      
            iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
            iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
            iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
            iDivDownload.innerHTML += '<img src="resources/images/office-excel-csv32.png" title="Download as CSV" onclick="downloadResults(\''+ String(idTag) + '\',\'csv\')">';
            document.getElementById(cell).appendChild(iDivDownload);  
            var iDiv = document.createElement('div');
            iDiv.id = id + '_' + idTag;
            iDiv.align = 'center';           
            document.getElementById(cell).appendChild(iDiv);   
            var table = new HTMLTable();
            table.init(tablesConfig()[ii], id + '_' + tablesConfig()[ii].htmlId + 'Table' + tablesConfig()[ii].index++, chartParams.width);            
            var html = table.getMarkup(resultsData, index);    
            document.getElementById(iDiv.id).insertAdjacentHTML('beforeend',html);         
        }        
    }          
    return;
} catch (err) {
    console.log("DELTA3G Error: " + err.message);
    var iDiv = document.createElement('div');
    iDiv.id = 'DELTA3G_error';
    document.getElementById(cell).appendChild(iDiv);         
    document.getElementById(iDiv.id).insertAdjacentHTML('beforeend',"<h3>DELTA3G Error: " + err.message + "</h3>"); 
    return ("<h3>DELTA3G Error: " + err.message + "</h3>"); 
}
}

function showDashboard(resultsData, inputParams, studyName, executeFromBrowser, cell, idProcess) { 
 try {
    if ( (resultsData === '') || (typeof resultsData === 'undefined') ) {
        return ("There are no results for this study"); 
    }
    var id = 'DELTA3G', 
        packageName = 'd3', 
        outcomeName = 'Outcome', 
        description = 'Graph Data';
    var chartParams = {
        'marginTop': 40,
        'marginRight': 20,
        'marginBottom': 50,
        'marginLeft': 50,
        'fontSize': fontSize,
        'backgroundColor':  'white',
        'fontFamily': fontFamily,        
        'width': 450,
        'height': 250,
        'legendFontSize': '9px',
        'legendWidth': 90,
        'legendSpacing': 12,
        'legendSpacingMargin': 1,
        'legendRectSize': 10,
        'lowerOpacity': 0.4,
        'higherOpacity': 1.0,
        'inputParams': ''};       
    
    if ( typeof resultsData.params !== 'undefined' ) {
        if ( typeof resultsData.params.id !== 'undefined') id = resultsData.params.id;
        if ( typeof resultsData.params.idStudy !== 'undefined') chartParams.idStudy = resultsData.params.idStudy;        
        if ( typeof resultsData.params.package !== 'undefined') packageName = resultsData.params.package;    
        if ( typeof resultsData.params.outcome !== 'undefined') outcomeName = resultsData.params.outcome;
        if ( typeof resultsData.params.description !== 'undefined') description = resultsData.params.description;    
        if ( typeof resultsData.params.width !== 'undefined') chartParams.width = resultsData.params.width;
        if ( typeof resultsData.params.height !== 'undefined') chartParams.height = resultsData.params.height;   
        if ( typeof resultsData.params.fontFamily !== 'undefined') chartParams.fontFamily = resultsData.params.fontFamily;    
        if ( typeof resultsData.params.fontSize !== 'undefined') chartParams.fontSize = resultsData.params.fontSize;   
        if ( typeof inputParams !== 'undefined') chartParams.inputParams = inputParams;         
    }
            
    var propDiffStoreArray = buildPropDiffChartStore(resultsData);
    var observedExpectedStoreArray = buildObservedExpectedChartStore(resultsData);        
    var riskAdjustedSPRTStoreArray = buildRiskAdjustedSPRTChartStore(resultsData);    
    var riskModelCoeffStoreArray = buildRiskModelCoeffChartStore(resultsData);
    var survivalStoreArray = buildSurvivalChartStore(resultsData, inputParams);    

    if ( typeof description === 'undefined' ) {
        description = '';
    }        
       
    document.getElementById(cell).style.margin = "10px 50px 20px 20px";
    var containerDiv = document.createElement('div');
    containerDiv.id = cell + "_container";
    //containerDiv.align = 'left';  
    containerDiv.style="overflow: hidden;";
    document.getElementById(cell).appendChild(containerDiv);       
    
    if ( (typeof observedExpectedStoreArray !== 'undefined') && (observedExpectedStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {   
            if ( resultsData.results[i].name === "ObservedExpectedGraphOutput" && resultsData.results[i].data.length > 0 ) {      
                var riskAdjustedOEMultiChart = new RiskAdjustedOEMultiChart(); 
                riskAdjustedOEMultiChart.init(studyName,'Risk Adjusted Observed Expected',getAllYAxisDescriptions(resultsData,"ObservedExpectedGraphOutput"), id, chartParams, idProcess);            
                var idTag = riskAdjustedOEMultiChart.combinedDIVId + j;  
                var subContainerDiv = document.createElement('div');  
                subContainerDiv.id = cell + "_" + idTag + '_sub';    
                subContainerDiv.style="float: left;";      
                document.getElementById(containerDiv.id).appendChild(subContainerDiv);                     
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image16.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture16.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document16.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(subContainerDiv.id).appendChild(iDivDownload);                                
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;
                document.getElementById(subContainerDiv.id).appendChild(iDiv);              
                riskAdjustedOEMultiChart.getMarkup(j, observedExpectedStoreArray, iDiv.id, d3, delta3.utils.GlobalFunc.showResultsTabZoom);        
                break; // one chart shows all data                
            }
        }           
    }      
    if ( (typeof propDiffStoreArray !== 'undefined') && (propDiffStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {
            if ( resultsData.results[i].name === "ProportionalDifferenceGraphOutput" && resultsData.results[i].data.length > 0 ) {      
                var propDiffMultiChart = new PropDiffMultiChart(); 
                propDiffMultiChart.init(studyName,'Event Rate Comparison Chart',getAllYAxisDescriptions(resultsData,"ProportionalDifferenceGraphOutput"), id, chartParams, idProcess);            
                var idTag = propDiffMultiChart.combinedDIVId + j;    
                var subContainerDiv = document.createElement('div');  
                subContainerDiv.id = cell + "_" + idTag + '_sub';     
                subContainerDiv.style="float: left;";                     
                document.getElementById(containerDiv.id).appendChild(subContainerDiv);                     
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image16.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture16.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document16.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(subContainerDiv.id).appendChild(iDivDownload);                                
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;  
                document.getElementById(subContainerDiv.id).appendChild(iDiv);                            
                propDiffMultiChart.getMarkup(j, propDiffStoreArray, iDiv.id, d3, delta3.utils.GlobalFunc.showResultsTabZoom);   
                break;
            }
        }           
    }   
    if ( (typeof observedExpectedStoreArray !== 'undefined') && (observedExpectedStoreArray.length > 0) ) {           
        for (var i=0, j=0; i<resultsData.results.length; i++) {    
            if ( resultsData.results[i].name === "ObservedExpectedGraphOutput" && resultsData.results[i].data.length > 0  ) {                      
                var observedExpectedMultiChart = new ObservedExpectedMultiChart();     
                observedExpectedMultiChart.init(studyName,'Cumulative Standard Chart',getAllYAxisDescriptions(resultsData,"ObservedExpectedGraphOutput"), id, chartParams, idProcess);               
                var idTag = observedExpectedMultiChart.combinedDIVId + j;
                var subContainerDiv = document.createElement('div');  
                subContainerDiv.id = cell + "_" + idTag + '_sub';   
                subContainerDiv.style="float: left;";                     
                document.getElementById(containerDiv.id).appendChild(subContainerDiv);                     
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image16.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture16.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document16.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(subContainerDiv.id).appendChild(iDivDownload);                
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;       
                document.getElementById(subContainerDiv.id).appendChild(iDiv);                       
                observedExpectedMultiChart.getMarkup(j, observedExpectedStoreArray, iDiv.id, d3, delta3.utils.GlobalFunc.showResultsTabZoom);   
                break;
            }
        }            
    }    
    
    if ( (typeof riskAdjustedSPRTStoreArray !== 'undefined') && (riskAdjustedSPRTStoreArray.length > 0) ) {           
        var riskAdjustedSPRTChart = new RiskAdjustedSPRTChart();
        riskAdjustedSPRTChart.init(studyName,'Risk Adjusted SPRT Chart', 'Risk Adjusted SPRT ' + outcomeName, id, chartParams, executeFromBrowser);        
        for (var i=0, j=0; i<resultsData.results.length; i++) { 
            if ( resultsData.results[i].name === "RaSprtGraphOutput" && resultsData.results[i].data.length > 0  ) {                   
                var idTag = riskAdjustedSPRTChart.combinedDIVId + j;             
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(cell).appendChild(iDivDownload);               
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;
                iDiv.align = 'center';            
                document.getElementById(cell).appendChild(iDiv);        
                riskAdjustedSPRTChart.getMarkup(j, riskAdjustedSPRTStoreArray[j++], iDiv.id, d3);    
            }
        }
    } 
    if ( (typeof riskModelCoeffStoreArray !== 'undefined') && (riskModelCoeffStoreArray.length > 0) ) {      
        var riskModelCoeffChart = new RiskModelCoeffChart(); 
        riskModelCoeffChart.init(studyName,'Risk Model Coefficient Chart','General Risk Model ' + outcomeName, id, chartParams.width, chartParams.height, executeFromBrowser);
        for (var i=0, j=0; i<resultsData.results.length; i++) {
            if ( resultsData.results[i].name === "RiskModelCoeffGraphOutput" && resultsData.results[i].data.length > 0  ) {            
                var idTag = riskModelCoeffChart.combinedDIVId + j;             
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(cell).appendChild(iDivDownload);               
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;
                iDiv.align = 'center';
                document.getElementById(cell).appendChild(iDiv);   
                var iDivSort = document.createElement('div');
                iDivSort.id = "divSort" + j;    
                document.getElementById(iDiv.id).appendChild(iDivSort);                 
                riskModelCoeffChart.getMarkup(j, riskModelCoeffStoreArray[j++], iDiv.id, d3);     
            }
        }           
    }      
    if ( (typeof survivalStoreArray !== 'undefined') && (survivalStoreArray.length > 0) ) {      
        for (var i=0, j=0; i<resultsData.results.length; i++) {
            if ( resultsData.results[i].name === "SurvivalGraphOutput" && resultsData.results[i].data.length > 0  ) {
                var survivalChart = new SurvivalChart(); 
                survivalChart.init(studyName,'Survival Chart','Survival ' + resultsData.results[i].Y_Axis, resultsData.results[i].X_Axis, id, chartParams, executeFromBrowser);            
                var idTag = survivalChart.combinedDIVId + j;             
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(cell).appendChild(iDivDownload);               
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;
                iDiv.align = 'center';
                document.getElementById(cell).appendChild(iDiv);   
                var iDivSort = document.createElement('div');
                iDivSort.id = "divSort" + j;    
                document.getElementById(iDiv.id).appendChild(iDivSort);                 
                survivalChart.getMarkup(j, survivalStoreArray[j++], iDiv.id, d3);   
            }
            if ( resultsData.results[i].name === "SurvivalMatchedGraphOutput" && resultsData.results[i].data.length > 0  ) {                 
                var survivalMatchedChart = new SurvivalMatchedChart(); 
                survivalMatchedChart.init(studyName,'Matched Survival Chart','Survival ' + resultsData.results[i].Y_Axis, resultsData.results[i].X_Axis, id, chartParams, executeFromBrowser);            
                var idTag = survivalMatchedChart.combinedDIVId + j;             
                var iDivDownload = document.createElement('div');
                iDivDownload.id = "download_" + idTag;       
                iDivDownload.align = 'right';      
                iDivDownload.innerHTML = '<img src="resources/images/image32.png" title="Download as PNG" onclick="downloadResults(\''+ String(idTag) + '\',\'png\')">';
                iDivDownload.innerHTML += '<img src="resources/images/picture32.png" title="Download as JPEG" onclick="downloadResults(\''+ String(idTag) + '\',\'jpeg\')">';            
                iDivDownload.innerHTML += '<img src="resources/images/pdf_document32.png" title="Download as PDF" onclick="downloadResults(\''+ String(idTag) + '\',\'pdf\')">';
                document.getElementById(cell).appendChild(iDivDownload);               
                var iDiv = document.createElement('div');
                iDiv.id = cell + "_" + idTag;
                iDiv.align = 'center';
                document.getElementById(cell).appendChild(iDiv);   
                var iDivSort = document.createElement('div');
                iDivSort.id = "divSort" + j;    
                document.getElementById(iDiv.id).appendChild(iDivSort);                 
                survivalMatchedChart.getMarkup(j, survivalStoreArray[j++], iDiv.id, d3);   
            }               
        }           
            //window.addEventListener('resize', reRender());  
    }              
    return;
} catch (err) {
    console.log("DELTA3G Error: " + err.message);
    var iDiv = document.createElement('div');
    iDiv.id = 'DELTA3G_error';
    document.getElementById(cell).appendChild(iDiv);         
    document.getElementById(iDiv.id).insertAdjacentHTML('beforeend',"<h3>DELTA3G Error: " + err.message + "</h3>"); 
    return ("<h3>DELTA3G Error: " + err.message + "</h3>"); 
}
}

