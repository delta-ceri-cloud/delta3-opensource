/**
 * D3 Risk Adjusted Observed Expected Chart local
 */
    
var RiskAdjustedOEChart = function() {    
        var id,
        blockId,       
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth, 
        graphTitle,
        graphSubTitle,
        yAxisTitle;
    };

RiskAdjustedOEChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, id, chartParams) {
    this.graphTitle = graphTitle;
    this.graphSubTitle = graphSubTitle;
    this.yAxisTitle = yAxisTitle;
    this.param = chartParams;
    this.id = id;
    this.blockId = '_raoe';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
};                    
 
RiskAdjustedOEChart.prototype.getMarkup = function (index, store, cell, d3) {
    console.log('d3 RiskAdjustedOEChart generation started.');
    var div = document.getElementById(cell);
    var svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")           
            .attr("width", this.param.width)
            .attr("height", this.param.height) 
            .attr("class", "delta3chart")
            .style("background-color", "white")      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index);  

    // Create Margins and Axis and hook our zoom function
    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},      
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var timeLabels = [];
    var drawPrimaryCI = false;
    var drawSecondaryCI = false;
    timeLabels[0] = '';
    for (var i=0; i<store.data.length; i++) {
        timeLabels[i+1] = store.data[i].time;
        if ( store.data[i].expLow !== 0.0 && store.data[i].expHigh !== 0.0 ) drawPrimaryCI = true;
        if ( store.data[i].exp2Low !== 0.0 && store.data[i].exp2High !== 0.0 ) drawSecondaryCI = true;        
    }
    timeLabels[i+2] = '';

    var x = d3.scale.ordinal()
        .domain(timeLabels)
        .rangeRoundPoints([0, width]);

    var maxY = this.calcStoreMaxYValue(store, drawPrimaryCI, drawSecondaryCI);
    var minY = this.calcStoreMinYValue(store, drawPrimaryCI, drawSecondaryCI);
    var y = d3.scale.linear()
        .domain([0, maxY+Math.abs(maxY/10)])
        .range([height, 0]); 

    var yCount = d3.scale.linear()
        .domain([this.calcStoreMinYRightValue(store, drawPrimaryCI, drawSecondaryCI), 
                this.calcStoreMaxYRightValue(store, drawPrimaryCI, drawSecondaryCI)])
        .range([height, 0]);

    var chart = svg.append("svg:g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");      

    var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(-height)     
            .tickPadding(10)
            .tickSubdivide(true)
            .orient("bottom");         

    var yAxis = d3.svg.axis()
            .scale(y)
            //.tickPadding(0.1)
            .tickSize(-width)
            .tickSubdivide(true) 
            .tickPadding(6)
            .orient("left");    

    var yAxisRight = d3.svg.axis()
            .scale(yCount)                               
            .orient("right");
 
    chart.append("svg:g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("width", width)
        .attr("height", height);
    
//    chart.append("svg:g")
//            .attr("class", "x axis")
//            .attr("transform", "translate(0," + height + ")")
//            .call(xAxis)
//            .selectAll("text")  
//                .style("text-anchor", "end")
//                .attr("dx", "-.8em")
//                .attr("dy", ".15em")
//                .attr("transform", "rotate(-45)" );    

    chart.append("svg:g")                
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)    
        .append("text")     
            .attr("class", "Time Period")
            .attr("y", margin.bottom-2)	  
            .attr("x", width/2)
            .text('Time Period');

    chart.append("svg:g")         
        .attr("class", "y axis")    
        .call(yAxis)           
        .append("text")
            .attr("class", "Cumulative")
            .attr("transform", "rotate(-90)")
            .attr("y", (-margin.left) + 12)
            .attr("x", -5*height/6)      
            .text(this.yAxisTitle);	

    chart.append("svg:g")         
        .attr("class", "y axis")          
        .attr("transform", "translate(" + width + ",0)")       
        .call(yAxisRight)        
        .append("text")
            .attr("class", "Case Count")
            .attr("transform", "rotate(-90)")
            .attr("y", margin.right-12)
            .attr("x", -2*height/3)      
            .text('Case Count');	

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});


    chart.append("clipPath")
        .attr("id", "clip" + cell)
        .append("rect")
            .attr("width", width)
            .attr("height", height);

    var lineExpCount = d3.svg.line()          
        .x(function(d) {return x(d.time); })
        .y(function(d) {return yCount(d.expCount); })        
        .interpolate("basis");              

    var groupGraphs = chart.append("svg:g")            
                            .attr("class","OE Graphs")
                            .attr("stroke","white")
                            .attr("stroke-width",1)
                            .style("fill-opacity", 1)
                            .attr("fill","none")
                            .attr("clip-path", "url(#clip" + cell + ")");

    groupGraphs.append("svg:path")
        .style("shape-rendering","auto")         
        .attr("class", "COUNT_1")
        .attr("clip-path", "url(#clip" + cell + ")")
        .attr("stroke", "green") 
        .attr("stroke-width",1)
        .attr("fill","none")
        .attr("d", lineExpCount(store.data));          
        
var expRectangles = chart.selectAll("expMarker")
    .data(store.data)
    .enter()
    .append("rect")
        .attr("class","expMarker")
        .style("fill",function(d) { return "rgb(0,0,0)"; } )  
        .style("stroke-width","1")
        .style("stroke","black")
        .attr("width", 10 )
        .attr("height", 10 )
        .attr("x", function (d) {
            var ret = x(d.time)-5;
            return ret;
        })
        .attr("y", function (d) {
            var ret = y(d.expMedium)-5;
            return ret;
        });
            
    //Unadjusted Vertical Confidence Interval of Expectation
var expLine = chart.selectAll("expLine")
        .data(store.data)
        .enter()
        .append("line")
            .attr("class", "expLine")
            .style("fill","none" )  
            .style("stroke-width","3")
            .style("stroke","black")
            .attr("x1", function(d) {  return x(d.time); })
                    .attr("y1", function(d) { return y(d.expHigh); })			
            .attr("x2", function(d) {  return x(d.time); })
                    .attr("y2", function(d) { return y(d.expLow); });

if ( drawSecondaryCI === true ) {
    //Alpha Adjusted Upper Vertical Confidence Interval of Expectation        
    var expDashLineUpper = chart.selectAll("expDashLineUpper")
            .data(store.data)
            .enter()
            .append("line")
                .attr("class", "expDashLineUpper")
                .style("fill","none" )  
                .style("stroke-dasharray",("2,2"))
                .style("stroke-width","3")
                .style("stroke","black")
                .attr("x1", function(d) {  return x(d.time); })
                        .attr("y1", function(d) { return y(d.exp2High); })			
                .attr("x2", function(d) {  return x(d.time); })
                        .attr("y2", function(d) { return y(d.expHigh); });
                
        //Alpha Adjusted Lower Vertical Confidence Interval of Expectation
    var expDashLineLower = chart.selectAll("expDashLineLower")
            .data(store.data)
            .enter()
            .append("line")
                .attr("class", "expDashLineLower")
                .style("fill","none" )  
                .style("stroke-dasharray",("2,2"))
                .style("stroke-width","3")
                .style("stroke","black")
                .attr("x1", function(d) {  return x(d.time); })
                        .attr("y1", function(d) { return y(d.exp2Low); })			
                .attr("x2", function(d) {  return x(d.time); })
                        .attr("y2", function(d) { return y(d.expLow); });
    }            
            
var obsCircles = chart.selectAll("obsMarkerCircle")
    .data(store.data)
    .enter()
    .append("circle")
        .style("stroke-width","1")
        .style("stroke","black")
        .attr("class","obsMarkerCirle")
        .attr("fill", function (d) {
//            alertColor( d.alert.type );            
            switch ( d.alert.type ) {
                case 2: 
                    return "yellow";
                case 4:
                    return "gray";
                case 3:
                    return "red";
                case 5:
                    return "blue";
                default:
                    return "lightgreen";      
            }
        })
        .attr("cx", function (d) {
            var ret = x(d.time);
            return ret;
        })
        .attr("cy", function (d) {
            var ret = y(d.obsMedium);
            return ret;
        })
        .attr("r", function (d) {
            return 6;
        })
        .append("svg:title")
            .text(function(d) { 
                if ( typeof d.alert.description === 'undefined') {    
                    return d.time                            
                        + '\n  Observed Rate: ' 
                        + d.obsMedium + ', '  
                        + '\n  Expected Rate: ' 
                        + d.exp2Low + ', '  
                        + d.expLow + ', '   
                        + d.expMedium + ', '     
                        + d.expHigh + ', '  
                        + d.exp2High + ' ';
                } else {
                    return d.alert.description + '\n'
                        + d.time                           
                        + '\n  Observed Rate: ' 
                        + d.obsMedium + ', '  
                        + '\n  Expected Rate: ' 
                        + d.exp2Low + ', '  
                        + d.expLow + ', '   
                        + d.expMedium + ', '     
                        + d.expHigh + ', '  
                        + d.exp2High + ' ';      
                }
            });

    var labels = chart.selectAll("labels")
            .data(store.data)
            .enter()
            .append("text")
                .attr("class","labels")    
                .attr("font-family", this.param.fontFamily)
                .attr("font-size", this.param.fontSize)
                .attr("fill", "black")
                .text(function(d) { return d.obsMedium + " %"; })
                .attr("x", function(d) { return x(d.time)+4; })
                .attr("y", function(d) { return y(d.obsMedium)-4; });
        
    // display graph title
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", 2*margin.top/3)
            .text( this.graphSubTitle )
            .attr("text-anchor", "middle")        
            .attr("font-size", this.param.fontSize);       
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", margin.top/3)
            .text( this.graphTitle )
            .attr("text-anchor", "middle")       
            .attr("font-size", this.param.fontSize);                
    
    console.log('d3 RiskAdjustedOEChart generation finished.');
    return svg;      
};                  

RiskAdjustedOEChart.prototype.calcStoreMinYValue = function (store, primaryCI, secondaryCI) {
    var tmp;
    if ( secondaryCI === true ) {
        tmp = store.data[0].exp2Low;
    } else {
        tmp = store.data[0].expLow;
    }    
    for (var i=0; i< store.data.length; i++) {
        if (primaryCI === true && store.data[i].obsLow < tmp) {
            tmp = store.data[i].obsLow;
        }
        if (secondaryCI === true && store.data[i].exp2Low < tmp) {
            tmp = store.data[i].exp2Low;
        }            
    }
    return tmp;
};
    
RiskAdjustedOEChart.prototype.calcStoreMaxYValue = function (store, primaryCI, secondaryCI) {
    var tmp;
    if ( secondaryCI === true ) {
        tmp = store.data[0].exp2High;
    } else {
        tmp = store.data[0].expHigh;
    }
    for (var i=0; i< store.data.length; i++) {
        if (primaryCI === true && store.data[i].obsHigh > tmp) {
            tmp = store.data[i].obsHigh;
        }
        if (secondaryCI === true && store.data[i].exp2High > tmp) {
            tmp = store.data[i].exp2High;
        }            
    }
    return tmp;
};        

RiskAdjustedOEChart.prototype.calcStoreMinYRightValue = function (store) {
    return 0;
};

RiskAdjustedOEChart.prototype.calcStoreMaxYRightValue = function (store) {
    var topMargin = 1;
    var tmp = store.data[0].expCount;
    for (var i=0; i< store.data.length; i++) {
        if (store.data[i].expCount > tmp) {
            tmp = store.data[i].expCount;
        }          
    }
    return (tmp + topMargin);
};
    
RiskAdjustedOEChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML;     
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                              
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };     
    image.src = imgsrc;    

};      

module.exports.RiskAdjustedOEChart = RiskAdjustedOEChart;
module.exports.getMarkup = RiskAdjustedOEChart.prototype.getMarkup;
module.exports.init = RiskAdjustedOEChart.prototype.init;
module.exports.download = RiskAdjustedOEChart.prototype.download;