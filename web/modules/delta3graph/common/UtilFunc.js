/* 
 * set of functions to help render DELTA3 graphs
 */
var  colorLowArray = ["RoyalBlue","LimeGreen","Purple","Navy", "Red","SaddleBrown", "SlateGray","Orchid", "RebeccaPurple", "OliveDrab"];
var  colorArray =  ["#3366ff","#00cc66","#ff0000","#ffff00", "#800000", "#9900ff", "#0099cc", "#333300", "#ff9900", "#993333"];
var  maxOutcomesPerChart = 10;

function buildPropDiffChartStore(resultsArray) {
    var resultStoreArray = [];
    var resultStore;
    var data;
    var dataRow;
    var resultCounter = 0;

    for (var i=0; i<resultsArray.results.length; i++) {      
        if (resultsArray.results[i].name === 'ProportionalDifferenceGraphOutput' && resultsArray.results[i].data.length > 0) { 
             resultStore = {fields: []}; 
            var modelFields = '["id","time","low","medium","high","alert"]'; 
            var storeFields = JSON.parse(modelFields);     
            storeFields[findColumn(resultsArray.results[i], 'rowid')] = "id";        
            storeFields[findColumn(resultsArray.results[i], 'period')] = "time";                
            storeFields[findColumn(resultsArray.results[i], 'upper')] = "high";
            storeFields[findColumn(resultsArray.results[i], 'lower')] = "low";                   
            storeFields[findColumn(resultsArray.results[i], 'proportional')] = "medium";     
 
             data = '[';
             for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                  if ( j > 0 ) {
                      data += ',';
                  }
                  dataRow = '{';
                  for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                     if ( z > 0 ) {
                         dataRow += ',';
                     }  
                     if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                         resultsArray.results[i].data[j][z] = '""';
                     }                        
                     if  ( storeFields[z] === "time" ) {
                         dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                     } else {
                         //dataRow += '"' + storeFields[z] + '":' + resultsArray.results[i].data[j][z];  
                         dataRow += '"' + storeFields[z] + '":' + Math.round(10000 * resultsArray.results[i].data[j][z])/100;                           
                     }
                  }
                  dataRow += ',"zero":0,"open":0,"close":0,"alert":' + this.getVisualAlert(resultsArray.results[i].alerts, j) + '}';
                  data += dataRow;
             }
             data += ']';
             var storeData = JSON.parse(data);
             resultStore.data = storeData;                  
             resultStoreArray[resultCounter++] = resultStore;
        }   
    }

    return resultStoreArray;        
 };
        
function buildObservedExpectedChartStore(resultsArray) {
    var resultStoreArray = [];           
    var resultStore;         
    var data;
    var dataRow;
    var resultCounter = 0;

    for (var i=0; i<resultsArray.results.length; i++) {      
        if (resultsArray.results[i].name === 'ObservedExpectedGraphOutput' && resultsArray.results[i].data.length > 0) { 
             resultStore = {fields: []};   
            var modelFields = '["rowId","time","obsCount","obsEvent","obsHigh","obsLow","obsMedium","expCount","expEvent","expHigh","expLow","expMedium","obs2High","obs2Low","exp2High","exp2Low"]';  
            var storeFields = JSON.parse(modelFields);    
            storeFields[findColumn(resultsArray.results[i], 'rowid')] = "id";        
            storeFields[findColumn(resultsArray.results[i], 'period')] = "time";                
            storeFields[findColumn(resultsArray.results[i], 'observed count')] = "obsCount";
            storeFields[findColumn(resultsArray.results[i], 'observed event')] = "obsEvent";                   
            storeFields[findColumn(resultsArray.results[i], 'observed upper confidence interval')] = "obsHigh";    
            storeFields[findColumn(resultsArray.results[i], 'observed lower confidence interval')] = "obsLow";    
            storeFields[findColumn(resultsArray.results[i], 'observed proportion')] = "obsMedium";    
            storeFields[findColumn(resultsArray.results[i], 'expected count')] = "expCount";
            storeFields[findColumn(resultsArray.results[i], 'expected event')] = "expEvent";                   
            storeFields[findColumn(resultsArray.results[i], 'expected upper confidence interval')] = "expHigh";    
            storeFields[findColumn(resultsArray.results[i], 'expected lower confidence interval')] = "expLow";    
            storeFields[findColumn(resultsArray.results[i], 'expected proportion')] = "expMedium";          
            
            storeFields[findColumn(resultsArray.results[i], 'observed secondary upper confidence interval')] = "obs2High";    
            storeFields[findColumn(resultsArray.results[i], 'observed secondary lower confidence interval')] = "obs2Low";    
            storeFields[findColumn(resultsArray.results[i], 'expected secondary upper confidence interval')] = "exp2High";    
            storeFields[findColumn(resultsArray.results[i], 'expected secondary lower confidence interval')] = "exp2Low";    
            
             data = '[';
             for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                  if ( j > 0 ) {
                      data += ',';
                  }
                  dataRow = '{';
                  for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                     if ( z > 0 ) {
                         dataRow += ',';
                     }  
                     if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                         resultsArray.results[i].data[j][z] = '""';
                     }                        
                     if  ( storeFields[z] === "time" ) {
                         dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                     } else {
                         if ( storeFields[z] === "id" || storeFields[z] === "expCount" || storeFields[z] === "obsCount"  || storeFields[z] === "expEvent" || storeFields[z] === "obsEvent") {
                             dataRow += '"' + storeFields[z] + '":' + resultsArray.results[i].data[j][z];  
                         } else {
                             dataRow += '"' + storeFields[z] + '":' + Math.round(10000 * resultsArray.results[i].data[j][z])/100;  
                         }
                     }
                  }
                  dataRow += ',"zero":0,"open":0,"close":0,"alert":' + this.getVisualAlert(resultsArray.results[i].alerts, j) + '}';
                  data += dataRow;
             }
         data += ']';

         var storeData = JSON.parse(data);
         resultStore.data = storeData;               
         resultStoreArray[resultCounter++] = resultStore; // this allows multiple results sets to be processed
        }
    }    
    return resultStoreArray;        
 };
        
function buildRiskAdjustedSPRTChartStore(resultsArray) {          
    var resultStoreArray = [];           
    var resultStore;         
    var data;
    var resultCounter = 0;

    for (var i=0; i<resultsArray.results.length; i++) {               
        if (resultsArray.results[i].name === 'RaSprtGraphOutput' && resultsArray.results[i].data.length > 0 ) {  
            resultStore = {fields: []};   
            data = '[';            
            for (var j=0; j<resultsArray.results[i].data.length; j++) {   
               if ( j > 0 ) {
                   data += ',';
               }
               data += '{"RowId":' + resultsArray.results[i].data[j][0];
               data += ',"riskAdjustedValue":' + resultsArray.results[i].data[j][1];
               data += ',"upper":' + resultsArray.results[i].data[j][2];
               data += ',"lower":' + resultsArray.results[i].data[j][3];
               data += ',"alert":""}';
            }
            data += ']';
            var storeData = JSON.parse(data);
            resultStore.data = storeData;
            resultStoreArray[resultCounter++] = resultStore; // this allows multiple results sets to be processed
        }
    }
    return resultStoreArray;        
 };
        
function buildSurvivalChartStore(resultsArray, inputParams) {          
    var resultStoreArray = [];           
    var resultStore;         
    var data;
    var resultCounter = 0;

    for (var i=0; i<resultsArray.results.length; i++) {               
        if ((resultsArray.results[i].name === 'SurvivalMatchedGraphOutput' || resultsArray.results[i].name === 'SurvivalGraphOutput') && resultsArray.results[i].data.length > 0) {  
            resultStore = {fields: []};   
            resultsArray.results[i] = adjustColumnsBasedOnParams(resultsArray.results[i], inputParams);
            data = '[';
            for (var j=0; j<resultsArray.results[i].data.length; j++) {
                // skip empy rows of data returned by DELTAlytics based on probability been ''
                if ( resultsArray.results[i].data[j][1] !== '' ) {
                    if ( j > 0 ) {
                        data += ',';
                    }                    
                    data += '{';
                    for (var z=0; z<resultsArray.results[i].columns.length; z++) {
                        if ( z > 0 ) {
                           data += ',';
                        }       
                        var tmp = resultsArray.results[i].data[j][z];
                        var prepend = '';
                        // expand name with partent column name and replace spaces with underscores
                        if ( typeof resultsArray.results[i].columns[z].parentcolumn !== 'undefined' && resultsArray.results[i].columns[z].parentcolumn.name !== '' ) {
                            prepend = '_' + resultsArray.results[i].columns[z].parentcolumn.name.replace(/\s/g,"_").toLowerCase();
                        }
                        if ( isNaN(tmp) ) {
                            data += '"' + resultsArray.results[i].columns[z].name.replace(/\s/g,"_").toLowerCase() + prepend + '":"' + tmp.trim() + '"';
                        } else {
                            data += '"' + resultsArray.results[i].columns[z].name.replace(/\s/g,"_").toLowerCase() + prepend + '":"' + tmp + '"';                    
                        }
                    }
                    data += '}';       
                }
            }
            data += ']';
            var storeData = JSON.parse(data);       
            resultStore.data = storeData;
            resultStoreArray[resultCounter++] = resultStore; // this allows multiple results sets to be processed
        }
    }
    return resultStoreArray;        
 };
 
function buildRiskModelCoeffChartStore(resultsArray) {          
    var resultStoreArray = [];           
    var resultStore;         
    var data;
    var resultCounter = 0;

    for (var i=0; i<resultsArray.results.length; i++) {               
        if (resultsArray.results[i].name === 'RiskModelCoeffGraphOutput' && resultsArray.results[i].data.length > 0 ) {  
            resultStore = {fields: []};   
            data = '[';
            for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                if ( j > 0 ) {
                    data += ',';
                }
                data += '{"RowId":"' + resultsArray.results[i].data[j][0].toString().replace(/\\"/g/ "") + '"';
                data += ',"Variable":"' + resultsArray.results[i].data[j][1].toString().replace(/\\"/g/ "") + '"';
                data += ',"OddsRatio":"' + resultsArray.results[i].data[j][2].toString().replace(/\\"/g/ "") + '"';
                data += ',"LCI":"' + resultsArray.results[i].data[j][3].toString().replace(/\\"/g/ "") + '"';
                data += ',"UCI":"' + resultsArray.results[i].data[j][4].toString().replace(/\\"/g/ "") + '"';                 
                data += ',"alert":""}';
            }
            data += ']';
            var storeData = JSON.parse(data);
            resultStore.data = storeData;
            resultStoreArray[resultCounter++] = resultStore; // this allows multiple results sets to be processed
        }
    }
    return resultStoreArray;        
 };
 
function getAllYAxisDescriptions(resultsData,tag) {
    var description = '';
    for (var i=0, j=0; i<resultsData.results.length; i++) {   
        if ( resultsData.results[i].name === tag && resultsData.results[i].data.length > 0 ) {      
            if ( j > 0 ) description += ',';
            description += resultsData.results[i].Y_Axis;
            if ( typeof description === 'undefined' ) return '';
            j++;
        }
    } 
    return description;
}; 

function getOutcomesMaxNumber(resultsData) {
    // following code calulates max number of repetition among all Results JSON tables
    var jMax = 0;
    for (var ii=0; ii<tablesConfig().length; ii++) {
        if ( checkIfResultsPresent(resultsData,tablesConfig()[ii].tag) === true ) {         
            for (var i=0, j=0; i<resultsData.results.length; i++) {   
                if ( resultsData.results[i].name === tablesConfig()[ii].tag && resultsData.results[i].data.length > 0 ) {      
                    j++;
                }
            if ( j > jMax ) jMax = j;
            } 
        }
    }
    return jMax;
};
 
function checkIfResultsPresent(resultsArray, resultType) {
    //var resultsArray = JSON.parse(resultsData);     
    for (var i=0; i<resultsArray.results.length; i++) {      
        if (resultsArray.results[i].name === resultType) { 
            return true;
        }
    }
    return false;             
 };
 
function findColumn(results, phrase) {
    for (var i=0; i<results.columns.length; i++) {
        if ( results.columns[i].name.toLowerCase().indexOf(phrase) !== -1 )
            return i;
    }
    return i;
}; 
        
function getVisualAlert(alertArray, index) {
    if ( typeof alertArray === 'undefined' || alertArray.length === 0 )  return '""';
    var maxTypeOffset = 0;
    var maxType;
    var hit = false;
    for (var i=0; i<alertArray.length; i++) {
        // pick the highest type among all alert types for this row
        if ( alertArray[i].idRow === index ) {
            if ( hit === false ) {
                maxType = alertArray[i].type;
                hit = true;
            }
            if ( alertArray[i].type >= maxType ) {
                maxType = alertArray[i].type;
                maxTypeOffset = i;
            }
        }
    }
    if ( hit === true ) {
        return JSON.stringify(alertArray[maxTypeOffset]);
    } else {
        return '""';
    }
};
        
function buildGridStore(type, resultsArray, modelFields) {
    //var resultsArray = JSON.parse(resultsData);
    var resultCounter = 0;
    var data = '{"dsData":[';
    var dataRow;

    for (var i = 0; i < resultsArray.results.length; i++) {
        if (resultsArray.results[i].name === type) {
            if ( resultCounter > 0 ) {
                data += ',';
            }
            for (var j = 0; j < resultsArray.results[i].data.length; j++) {
                if (j > 0) {
                    data += ',';
                }
                dataRow = '[';
                for (var z = 0; z < resultsArray.results[i].data[j].length; z++) {
                    if (z > 0) {
                        dataRow += ',';
                    }
                    dataRow += '"' + resultsArray.results[i].data[j][z] + '"';
                }
                dataRow += ']';
                data += dataRow;
            }
            resultCounter++;
        }
    }
    data += ']}';

    var fields = eval("(" + modelFields + ")");
    var model = Ext.define(null, {
        extend: 'Ext.data.Model',
        fields: fields
    });

    var store = new Ext.data.Store({
        autoLoad: true,
        model: model,
        data: {},
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                rootProperty: 'dsData'
            }
        }
    });

    store.loadRawData(eval("(" + data + ")")); // re-load data since metadata changed
    return store;
};
    
function buildGridModelFields(type, resultsArray) {
    //var resultsArray = JSON.parse(resultsData);
    var j, i;
    for (j = 0; j < resultsArray.results.length; j++) {
        if (resultsArray.results[j].name === type) {
            var hString = '[';
            for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                if (i > 0) {
                    hString += ',';
                }
                hString += '{"name":"' + resultsArray.results[j].columns[i].name + '", "type":"string"}';
            }
            return hString += ']';
        }
    }
    return null;
};
    
function buildGridColumns(type, resultsArray) {
    //var resultsArray = JSON.parse(resultsData);
    var hString = '[';
    var j, i;
    for (j = 0; j < resultsArray.results.length; j++) {
        if (resultsArray.results[j].name === type) {
            var hString = '[';
            for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                if (i > 0) {
                    hString += ',';
                }
                hString += '{"text":"' + resultsArray.results[j].columns[i].name + '", "dataIndex":"' + resultsArray.results[j].columns[i].name + '", "type":"string", "width": 130}';
            }
            return hString += ']';
        }
    }
    return null;
};    

function adjustColumnsBasedOnParams(results, inputParams) {
    // check inputParams
    if ( typeof inputParams.inputs.alphaSpending === 'undefined' || inputParams.inputs.alphaSpending.values[0] === false ) {
        for (var i = results.columns.length; i > 0; i--) {
            var j = results.columns[i-1].name.toLowerCase().indexOf('alpha spending');
            if ( j !== -1 ) {
                results.columns.splice(i-1,1);
                for ( var z = 0; z < results.data.length; z++) {
                    results.data[z].splice(i-1,1);
                }
            } 
        }
    }
    return results;
}

function downloadChartToPDF(div, params) { // works for PDF in Chrome
    var html = new XMLSerializer().serializeToString(div);    
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = params.width;
        canvas.height = params.height;           
        changeResolution(canvas, 730/image.width);        
        context.drawImage(image, 0, 0);   
        var pdf = new jsPDF();
        var marginLeft=5;
        var marginRight=5;
        var canvasdata = canvas.toDataURL("image/jpeg");
        pdf.addImage(canvasdata,"jpeg",marginLeft,marginRight);
        pdf.save(params.filename);
        document.body.removeChild(canvas);           
        canvas = null;  
        context = null;
        pdf = null;
    };  
    image.setAttribute('crossOrigin','anonymous');    
    image.setAttribute('Access-Control-Allow-Origin', '*');    
    image.src = imgsrc;
};

function downloadTableToPDF(div, params) { // does not work for PDF in Chrome - black rectangle
    html2canvas(div, {
        //logging: true,
        allowTaint: true,        
        onrendered: function(canvas) {      
            //canvas.width = params.width;
            //canvas.height = params.height;              
            //changeResolution(canvas, 730/params.width); - this wipes out the content!
            var pdf = new jsPDF('landscape');
            var marginLeft=5;
            var marginRight=5;
            var canvasdata = canvas.toDataURL("image/jpeg");     
            pdf.addImage(canvasdata,"jpeg",marginLeft,marginRight);
            pdf.save(params.filename);
            document.body.removeChild(canvas);   
            canvas = null;
            canvasdata = null;
            pdf = null;
        }
    });
}

function downloadTableToCSV(div, params) { 
    var header = '';
    for (var i =0; i < div.childNodes[1].childNodes[0].childNodes.length; i++) {
        if ( i !== 0 ) header += ',';
        header += div.childNodes[1].childNodes[0].childNodes[i].innerText;
    }
    var data = ''; 
    for (var i =0; i < div.childNodes[2].childNodes.length; i++) {
        for (var j =0; j < div.childNodes[2].childNodes[i].childNodes.length; j++) {
            if ( j !== 0) data += ',';
            data += div.childNodes[2].childNodes[i].childNodes[j].innerText;
        }
        data += '\r\n';  
    }    
    var csv = header + '\r\n' + data;
    if (navigator.userAgent.search("MSIE") >= 0) {
        var IEwindow = window.open();
        IEwindow.document.write('sep=,\r\n' + csv);
        IEwindow.document.close();
        IEwindow.document.execCommand('SaveAs', true, params.filename + ".csv");
        IEwindow.close();
    } else {
        var uri = 'data:text/csv;charset=utf-8,' + escape(csv);
        var downloadLink = document.createElement("a");
        downloadLink.href = uri;
        downloadLink.download = params.filename + ".csv";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }    
}

function downloadPictureFromServer(div, params) { // works for PNG and JPEG tables
    html2canvas(div, {
        logging: true,
        allowTaint: true,        
        onrendered: function(canvas) {
            var cs = new pictureServerConverter('/Delta3/ConvertCanvasServlet', params.format);
            cs.submitToServer(canvas,params.filename);
        }
    });    
};

function downloadSVGToPicture(div, params) { // works for PNG and JPEG charts
    var cs = new SVGServerConverter('/Delta3/ConvertSVGServlet', params.format);
    var imgData = new XMLSerializer().serializeToString(div);
    cs.submitToServer(imgData,params.filename);  
};

function SVGServerConverter(url, type) {    
    this.url = url;    
    this.submitToServer = function(data, fname) {
        fname = fname || 'jpeg';      
        var dataInput = document.createElement("input") ;
        dataInput.setAttribute("name", "imgdata") ;
        dataInput.setAttribute("value", data);
        dataInput.setAttribute("type", "hidden");
         
        var typeInput = document.createElement("input"); 
        typeInput.setAttribute("name", "imgtype") ;
        typeInput.setAttribute("value", type);
        
        var nameInput = document.createElement("input") ;
        nameInput.setAttribute("name", "name") ;
        nameInput.setAttribute("value", fname + "." + type);
         
        var myForm = document.createElement("form");
        myForm.method = 'post';
        myForm.action = url;
        myForm.appendChild(dataInput);
        myForm.appendChild(typeInput);
        myForm.appendChild(nameInput);
         
        document.body.appendChild(myForm);
        myForm.submit();
        document.body.removeChild(myForm);
    };
}

function pictureServerConverter(url, type) {    
    this.url = url;    
    this.submitToServer = function(cnvs, fname) {
        if(!cnvs || !url) return;
        fname = fname || 'picture';
         
        var data = cnvs.toDataURL("image/" + type);
        data = data.substr(data.indexOf(',') + 1).toString();
         
        var dataInput = document.createElement("input") ;
        dataInput.setAttribute("name", "imgdata") ;
        dataInput.setAttribute("value", data);
        dataInput.setAttribute("type", "hidden");
         
        var nameInput = document.createElement("input") ;
        nameInput.setAttribute("name", "name") ;
        nameInput.setAttribute("value", fname + "." + type);
         
        var myForm = document.createElement("form");
        myForm.method = 'post';
        myForm.action = url;
        myForm.appendChild(dataInput);
        myForm.appendChild(nameInput);
         
        document.body.appendChild(myForm);
        myForm.submit();
        document.body.removeChild(myForm);
    };
}

function changeResolution(canvas, scaleFactor) {
    // Set up CSS size if it's not set up already
    if (!canvas.style.width)
        canvas.style.width = canvas.width + 'px';
    if (!canvas.style.height)
        canvas.style.height = canvas.height + 'px';

    canvas.width = Math.ceil(canvas.width * scaleFactor);
    canvas.height = Math.ceil(canvas.height * scaleFactor);
    var ctx = canvas.getContext('2d');
    ctx.scale(scaleFactor, scaleFactor);
}

function setDPI(canvas, dpi) {
    // Set up CSS size if it's not set up already
    if (!canvas.style.width)
        canvas.style.width = canvas.width + 'px';
    if (!canvas.style.height)
        canvas.style.height = canvas.height + 'px';

    var scaleFactor = dpi / 96;
    canvas.width = Math.ceil(canvas.width * scaleFactor);
    canvas.height = Math.ceil(canvas.height * scaleFactor);
    var ctx = canvas.getContext('2d');
    ctx.scale(scaleFactor, scaleFactor);
}

function popUpWindowWithPicture(dataURL, title, width, height) {
        newWindow("<img src=\"" + dataURL + "\"/>");
        function newWindow(content) {
            top.consoleRef = window.open("", title,
                "width="+ (width+20) +",height="+ (height+20)
                + ",top=100, left=160"
                + ",menubar=0"
                + ",toolbar=1"
                + ",status=0"
                + ",scrollbars=1"
                + ",resizable=1")
            top.consoleRef.document.writeln(
                "<!DOCTYPE html><html><head><title>" + title + "</title></head>"
                + "<body bgcolor=white onLoad=\"self.focus()\">"
                + content
                + "</body></html>"
            )
            top.consoleRef.document.close()
        }       
}

function log(text) {
    var time = new Date(), formattedDate, formattedTime;
    
    formattedDate = time.getFullYear() + '/' + (1+time.getMonth()) + "/" + time.getDate();
    formattedTime = time.getHours() + ":" + padLeft(time.getMinutes(),"0",2) + ":" + padLeft(time.getSeconds(),"0",2);    
    console.log(formattedDate + ' ' + formattedTime + ' ' + text);
    
    function padLeft(input,padString,length){
        var toReturn = input;
        while(toReturn.length < length){
            toReturn = padString + toReturn;
        }
        return toReturn;
    };
}

function alertColor(alertType) {
    switch ( alertType) {
        case 2: 
            return "yellow";
        case 4:
            return "gray";
        case 3:
            return "red";
        case 5:
            return "blue";
        default:
            return "lightgreen";          
    }
}

function tablesConfig() {
    return [
    {tag: 'ProportionalDifferenceGraphOutput', htmlId: 'pd', description: 'Event Rate Comparison Chart Data', index: 0, everyTab: false},
    {tag: 'ObservedExpectedGraphOutput', htmlId: 'oe', description: 'Observed Expected Chart Data', index: 0, everyTab: false},
    {tag: 'RaSprtGraphOutput', htmlId: 'sprt', description: 'Risk Adjusted SPRT Chart Data', index: 0, everyTab: false},
    {tag: 'SurvivalGraphOutput', htmlId: 'srvg', description: 'Survival Chart Data', index: 0, everyTab: false},  
    {tag: 'SurvivalMatchedGraphOutput', htmlId: 'srmg', description: 'Matched Survival Chart Data', index: 0, everyTab: false},      
    {tag: 'SurvivalEventOutput', htmlId: 'srve', description: 'Survival Event Data', index: 0, everyTab: false},    
    {tag: 'SurvivalMatchedEventOutput', htmlId: 'smrve', description: 'Survival Event Data', index: 0, everyTab: false},        
    {tag: 'DescriptiveStatisticsOutput', htmlId: 'ds', description: 'Descriptive Statistics', index: 0, everyTab: true}, 
    {tag: 'PropensityMatchDescriptiveStats', htmlId: 'pmds', description: 'Covariate Matched Statistics', index: 0, everyTab: true}, 
    {tag: 'LogisticRegressionFormulaOutput', htmlId: 'lr', description: 'Logistic Regression Formula Data', index: 0, everyTab: true},
    {tag: 'LogModelPerformanceStats', htmlId: 'lmps', description: 'Logistic Regression Formula Diagnostics', index: 0, everyTab: true},       
    {tag: 'Outcomes', htmlId: 'otms', description: 'Outcomes Data', index: 0, everyTab: true},
    {tag: 'SurvivalCurvesComparison', htmlId: 'scurv', description: 'Survival Curves Comparison', index: 0, everyTab: true},
    {tag: 'CoxPropHazardModel', htmlId: 'cox', description: 'Cox Proportional Hazard Model', index: 0, everyTab: true}, 
    {tag: 'ModelEvalDiag', htmlId: 'meval', description: 'Model Evaluation and Diagnostics', index: 0, everyTab: true}];
}

module.exports.buildPropDiffChartStore = buildPropDiffChartStore;
module.exports.buildObservedExpectedChartStore = buildObservedExpectedChartStore;
module.exports.buildRiskAdjustedSPRTChartStore = buildRiskAdjustedSPRTChartStore;
module.exports.buildRiskModelCoeffChartStore = buildRiskModelCoeffChartStore;
module.exports.buildSurvivalChartStore = buildSurvivalChartStore;
module.exports.getAllYAxisDescriptions = getAllYAxisDescriptions;
module.exports.getOutcomesMaxNumber = getOutcomesMaxNumber;
module.exports.checkIfResultsPresent = checkIfResultsPresent;
module.exports.getVisualAlert = getVisualAlert;
module.exports.buildGridStore = buildGridStore;
module.exports.buildGridModelFields = buildGridModelFields;
module.exports.buildGridColumns = buildGridColumns;
module.exports.alertColor = alertColor;
module.exports.tablesConfig = tablesConfig;
module.exports.log = log;



