FROM tomcat:9.0
LABEL maintainer="neha.v.khairnar@lahey.org"

ADD ./dist/Delta3.war /usr/local/tomcat/webapps/

RUN chmod 777 /usr/local/tomcat/webapps

EXPOSE 8080

HEALTHCHECK --interval=1m --timeout=3s CMD curl -f http://deltaapp:8080/Delta3/GetDeltaInfo || exit 1

CMD ["catalina.sh", "run"] 
