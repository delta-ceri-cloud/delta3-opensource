/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.entity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mxm29
 */
@Entity
@Table(name = "d3_metadata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "D3Metadata.findByModelId", query = "SELECT s FROM D3Metadata s WHERE s.idModel = :idModel order by s.idMetadata desc")        
//    @NamedQuery(name = "D3Metadata.findAll", query = "SELECT d FROM D3Metadata d")
//    , @NamedQuery(name = "D3Metadata.findByIdMetadata", query = "SELECT d FROM D3Metadata d WHERE d.idMetadata = :idMetadata")
//    , @NamedQuery(name = "D3Metadata.findByIdModel", query = "SELECT d FROM D3Metadata d WHERE d.idModel = :idModel")
//    , @NamedQuery(name = "D3Metadata.findByName", query = "SELECT d FROM D3Metadata d WHERE d.name = :name")
//    , @NamedQuery(name = "D3Metadata.findByData", query = "SELECT d FROM D3Metadata d WHERE d.data = :data")
//    , @NamedQuery(name = "D3Metadata.findByActive", query = "SELECT d FROM D3Metadata d WHERE d.active = :active")
//    , @NamedQuery(name = "D3Metadata.findByCreatedBy", query = "SELECT d FROM D3Metadata d WHERE d.createdBy = :createdBy")
//    , @NamedQuery(name = "D3Metadata.findByCreatedTS", query = "SELECT d FROM D3Metadata d WHERE d.createdTS = :createdTS")
//    , @NamedQuery(name = "D3Metadata.findByUpdatedBy", query = "SELECT d FROM D3Metadata d WHERE d.updatedBy = :updatedBy")
//    , @NamedQuery(name = "D3Metadata.findByUpdatedTS", query = "SELECT d FROM D3Metadata d WHERE d.updatedTS = :updatedTS")
})
public class D3Metadata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="MODEL_ID")
    @TableGenerator(name="MODEL_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="MODEL_ID", allocationSize=1)
    @Column(name = "idMetadata")
    private Integer idMetadata;
    @Basic(optional = false)
    @Column(name = "idModel")
    private int idModel;
    @Column(name = "name")
    private String name;
    @Column(name = "data")
    private String data;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public D3Metadata() {
    }

    public D3Metadata(Integer idMetadata) {
        this.idMetadata = idMetadata;
    }

    public D3Metadata(Integer idMetadata, int idModel) {
        this.idMetadata = idMetadata;
        this.idModel = idModel;
    }

    public Integer getIdMetadata() {
        return idMetadata;
    }

    public void setIdMetadata(Integer idMetadata) {
        this.idMetadata = idMetadata;
    }

    public int getIdModel() {
        return idModel;
    }

    public void setIdModel(int idModel) {
        this.idModel = idModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMetadata != null ? idMetadata.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof D3Metadata)) {
            return false;
        }
        D3Metadata other = (D3Metadata) object;
        if ((this.idMetadata == null && other.idMetadata != null) || (this.idMetadata != null && !this.idMetadata.equals(other.idMetadata))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ceri.delta.entity.model.D3Metadata[ idMetadata=" + idMetadata + " ]";
    }
    
}
