/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.entity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mxm29
 */
@Entity
@Table(name = "d3_desc_stats")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "D3DescStats.findAll", query = "SELECT d FROM D3DescStats d")
    , @NamedQuery(name = "D3DescStats.findByIdStats", query = "SELECT d FROM D3DescStats d WHERE d.idStats = :idStats")
    , @NamedQuery(name = "D3DescStats.findByIdOrganization", query = "SELECT d FROM D3DescStats d WHERE d.idOrganization = :idOrganization")       
    , @NamedQuery(name = "D3DescStats.findByIdModel", query = "SELECT d FROM D3DescStats d WHERE d.idModel = :idModel and d.flat = :flat order by d.idStats")
    , @NamedQuery(name = "D3DescStats.findByName", query = "SELECT d FROM D3DescStats d WHERE d.name = :name")
    , @NamedQuery(name = "D3DescStats.findByStats", query = "SELECT d FROM D3DescStats d WHERE d.stats = :stats")
    , @NamedQuery(name = "D3DescStats.findByStatus", query = "SELECT d FROM D3DescStats d WHERE d.status = :status")
    , @NamedQuery(name = "D3DescStats.findByActive", query = "SELECT d FROM D3DescStats d WHERE d.active = :active")
    , @NamedQuery(name = "D3DescStats.findByCreatedBy", query = "SELECT d FROM D3DescStats d WHERE d.createdBy = :createdBy")
    , @NamedQuery(name = "D3DescStats.findByCreatedTS", query = "SELECT d FROM D3DescStats d WHERE d.createdTS = :createdTS")
    , @NamedQuery(name = "D3DescStats.findByUpdatedBy", query = "SELECT d FROM D3DescStats d WHERE d.updatedBy = :updatedBy")
    , @NamedQuery(name = "D3DescStats.findByUpdatedTS", query = "SELECT d FROM D3DescStats d WHERE d.updatedTS = :updatedTS")})
public class D3DescStats implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="DESCSTATS_ID")
    @TableGenerator(name="DESCSTATS_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="DESCSTATS_ID", allocationSize=1)
    @Column(name = "idStats")
    private Integer idStats;
    @Basic(optional = false)
    @Column(name = "idOrganization")
    private int idOrganization;
    @Basic(optional = false)
    @Column(name = "idModel")
    private int idModel;
    @Column(name = "name")
    private String name;
    @Column(name = "stats")
    private String stats;
    @Column(name = "status")
    private String status;
    @Column(name = "flat")
    private Boolean flat;    
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;

    public D3DescStats() {
    }

    public D3DescStats(Integer idStats) {
        this.idStats = idStats;
    }

    public D3DescStats(Integer idStats, int idOrganization, int idModel) {
        this.idStats = idStats;
        this.idOrganization = idOrganization;
        this.idModel = idModel;
    }

    public Integer getIdStats() {
        return idStats;
    }

    public void setIdStats(Integer idStats) {
        this.idStats = idStats;
    }

    public int getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(int idOrganization) {
        this.idOrganization = idOrganization;
    }

    public int getIdModel() {
        return idModel;
    }

    public void setIdModel(int idModel) {
        this.idModel = idModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStats() {
        return stats;
    }

    public void setStats(String stats) {
        this.stats = stats;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getFlat() {
        return flat;
    }

    public void setFlat(Boolean flat) {
        this.flat = flat;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return (java.sql.Timestamp)createdTS;
    }

    public void setCreatedTS(java.sql.Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return (java.sql.Timestamp)updatedTS;
    }

    public void setUpdatedTS(java.sql.Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStats != null ? idStats.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof D3DescStats)) {
            return false;
        }
        D3DescStats other = (D3DescStats) object;
        if ((this.idStats == null && other.idStats != null) || (this.idStats != null && !this.idStats.equals(other.idStats))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ceri.delta.entity.model.D3DescStats[ idStats=" + idStats + " ]";
    }
    
}
