/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.entity.matchset;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mxm29
 */
@Entity
@Table(name = "d3_matchset")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "MatchSet.findAll", query = "SELECT m FROM MatchSet m")
    , @NamedQuery(name = "MatchSet.findByIdMatchset", query = "SELECT m FROM MatchSet m WHERE m.idMatchset = :idMatchset")
    , @NamedQuery(name = "MatchSet.findByIdOrganization", query = "SELECT m FROM MatchSet m WHERE m.idOrganization = :idOrganization")
    , @NamedQuery(name = "MatchSet.findByIdModel", query = "SELECT m FROM MatchSet m WHERE m.idModel = :idModel")
    , @NamedQuery(name = "MatchSet.findByIdStudy", query = "SELECT m FROM MatchSet m WHERE m.idStudy = :idStudy")
    , @NamedQuery(name = "MatchSet.findByIdProcess", query = "SELECT m FROM MatchSet m WHERE m.idProcess = :idProcess")
    , @NamedQuery(name = "MatchSet.findByIdGroup", query = "SELECT m FROM MatchSet m WHERE m.idGroup = :idGroup")
    , @NamedQuery(name = "MatchSet.findByGuid", query = "SELECT m FROM MatchSet m WHERE m.guid = :guid")
    , @NamedQuery(name = "MatchSet.findByName", query = "SELECT m FROM MatchSet m WHERE m.name = :name")
    , @NamedQuery(name = "MatchSet.findByDescription", query = "SELECT m FROM MatchSet m WHERE m.description = :description")
    , @NamedQuery(name = "MatchSet.findByRowCount", query = "SELECT m FROM MatchSet m WHERE m.rowCount = :rowCount")
    , @NamedQuery(name = "MatchSet.findByStatus", query = "SELECT m FROM MatchSet m WHERE m.status = :status")
    , @NamedQuery(name = "MatchSet.findByLocalStatPackage", query = "SELECT m FROM MatchSet m WHERE m.localStatPackage = :localStatPackage")
    , @NamedQuery(name = "MatchSet.findByRemoteStatPackage", query = "SELECT m FROM MatchSet m WHERE m.remoteStatPackage = :remoteStatPackage")
    , @NamedQuery(name = "MatchSet.findByActive", query = "SELECT m FROM MatchSet m WHERE m.active = :active")
    , @NamedQuery(name = "MatchSet.findByCreatedBy", query = "SELECT m FROM MatchSet m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MatchSet.findByCreatedTS", query = "SELECT m FROM MatchSet m WHERE m.createdTS = :createdTS")
    , @NamedQuery(name = "MatchSet.findByUpdatedBy", query = "SELECT m FROM MatchSet m WHERE m.updatedBy = :updatedBy")
    , @NamedQuery(name = "MatchSet.findByUpdatedTS", query = "SELECT m FROM MatchSet m WHERE m.updatedTS = :updatedTS")})*/
public class MatchSet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="MATCHSET_ID")
    @TableGenerator(name="MATCHSET_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="MATCHSET_ID", allocationSize=1) 
    @Column(name = "idMatchset")
    private Integer idMatchset;
    @Column(name = "idOrganization")
    private Integer idOrganization;
    @Column(name = "idModel")
    private Integer idModel;
    @Column(name = "idStudy")
    private Integer idStudy;
    @Column(name = "idProcess")
    private Integer idProcess;
    @Column(name = "idGroup")
    private Integer idGroup;
    @Column(name = "GUID")
    private String guid;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "rowCount")
    private Integer rowCount;
    @Column(name = "status")
    private String status;
    @Column(name = "localStatPackage")
    private Boolean localStatPackage;
    @Column(name = "remoteStatPackage")
    private Boolean remoteStatPackage;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTS;
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTS;

    public MatchSet() {
    }

    public MatchSet(Integer idMatchset) {
        this.idMatchset = idMatchset;
    }

    public Integer getIdMatchset() {
        return idMatchset;
    }

    public void setIdMatchset(Integer idMatchset) {
        this.idMatchset = idMatchset;
    }

    public Integer getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(Integer idOrganization) {
        this.idOrganization = idOrganization;
    }

    public Integer getIdModel() {
        return idModel;
    }

    public void setIdModel(Integer idModel) {
        this.idModel = idModel;
    }

    public Integer getIdStudy() {
        return idStudy;
    }

    public void setIdStudy(Integer idStudy) {
        this.idStudy = idStudy;
    }

    public Integer getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(Integer idProcess) {
        this.idProcess = idProcess;
    }

    public Integer getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getLocalStatPackage() {
        return localStatPackage;
    }

    public void setLocalStatPackage(Boolean localStatPackage) {
        this.localStatPackage = localStatPackage;
    }

    public Boolean getRemoteStatPackage() {
        return remoteStatPackage;
    }

    public void setRemoteStatPackage(Boolean remoteStatPackage) {
        this.remoteStatPackage = remoteStatPackage;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Date createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Date updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMatchset != null ? idMatchset.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MatchSet)) {
            return false;
        }
        MatchSet other = (MatchSet) object;
        if ((this.idMatchset == null && other.idMatchset != null) || (this.idMatchset != null && !this.idMatchset.equals(other.idMatchset))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ceri.delta.entity.matchset.MatchSet[ idMatchset=" + idMatchset + " ]";
    }
    
}
