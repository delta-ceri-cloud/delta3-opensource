/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ceri.delta.server;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.property.Property;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Boston Advanced Analytics
 */
public class StartStopContextListener implements ServletContextListener {
    private static Logger logger = Logger.getLogger("");
    //private static Logger logger = Logger.getLogger(StartStopContextListener.class.getName());  
    //private static Logger root = (Logger)Logger.getLogger("");
    //NotificationTimerTask notificationTimer;
    CommunicationTimerTask communicationTimer;
    StatServiceImpl statService;
    private ScheduledExecutorService scheduler;    
    ScriptEngineManager sem;
    ScriptEngine nashorn;
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        //final LoggingMXBean mxBean = LogManager.getLoggingMXBean();
        //mxBean.setLoggerLevel("", Level.SEVERE.getName());
        // Get the root logger
        //Logger rootLogger = Logger.getLogger("");
        //for(Handler handler : rootLogger.getHandlers()) {
          // Change log level of default handler(s) of root logger
          // The paranoid would check that this is the ConsoleHandler ;)
          //handler.setLevel(Level.INFO);
        //}
        // Set root logger level
        //rootLogger.setLevel(Level.INFO);        
        logger.log(Level.SEVERE, "Starting Tomcat DELTA3 context!");        
        //Enumeration allLoggers = LogManager.getLogManager().getLoggerNames();      
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new NotificationTimerTask(), 60, 60, TimeUnit.SECONDS);       
        // create the thread pool
        int cpus = Runtime.getRuntime().availableProcessors();
        int maxThreads = (int) (cpus * 0.5);
        maxThreads = (maxThreads > 0 ? maxThreads : 1);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(maxThreads, maxThreads, 120,
                TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(2*maxThreads));
        sce.getServletContext().setAttribute("executor",executor);        
        //notificationTimer = new NotificationTimerTask();
        //notificationTimer.init();       
        statService = new StatServiceImpl();
        statService.initializeExternalStatPackage("system");
        /*sem = new ScriptEngineManager();
        nashorn = sem.getEngineByName("nashorn");
        try {
            //nashorn.eval("load('src/hello.js')");
            nashorn.eval("print('Hello World!');");
        } catch (ScriptException ex) {
            Logger.getLogger(StartStopContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        long statPollingPeriod;
        String statPollingPeriodString = new Property("statPackagePollingPeriod").getParamValue();
        try {
            statPollingPeriod = Long.parseLong(statPollingPeriodString);
        } catch (Exception e) {
            statPollingPeriod = 1000*60*60*24; // default to 24 hours
        }            
        communicationTimer = new CommunicationTimerTask();
        communicationTimer.init(statPollingPeriod);         
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.log(Level.INFO, "Stopping Tomcat DELTA3 context!");
        if ( communicationTimer != null ) {
            communicationTimer.cancel();
            communicationTimer = null;
        }
        ThreadPoolExecutor executor = (ThreadPoolExecutor) sce.getServletContext().getAttribute("executor");
        executor.shutdown();  
        scheduler.shutdownNow();
        if ( statService.proxy != null ) {
            statService.proxy.shutdown();
        }
        PersistenceManager.closeEntityManagerFactory();
        // deregister sql driver(s)
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                logger.log(Level.INFO, "deregistering jdbc driver: " + driver);
            } catch (SQLException e) {
                logger.log(Level.INFO, "error deregistering jdbc driver: " + driver, e);
            }
        }        
        statService = null;
        logger = null;
    }

}