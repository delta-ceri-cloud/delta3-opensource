/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server.rest;


import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.model.ModelRelationshipJpaController;
import com.ceri.delta.jpa.model.ModelTableJpaController;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.AdminServiceImpl;
import com.ceri.delta.server.ImportServiceImpl;
import com.ceri.delta.server.ModelServiceImpl;
import com.ceri.delta.server.processor.ProcessFlatTable;
import com.ceri.delta.server.processor.ProcessFlatTableNoMissing;
import com.ceri.delta.util.JSON.JSONObject;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc.
 */
@Path("import")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3ImportResource {
    private static final Logger logger = Logger.getLogger(Delta3ImportResource.class.getName());
    private static ModelServiceImpl modelService; 
    private static ImportServiceImpl importService;   
    private static ModelJpaController modelController;
    private String _corsHeaders;    
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Delta3ModelResource
     */
    public Delta3ImportResource() {
        if ( importService == null ) {
            importService = new ImportServiceImpl();
        }      
        if ( modelService == null ) {
            modelService = new ModelServiceImpl();
        }      
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }                  
    }

    @Path("/importObjects")
    @POST
    @Produces("application/json")
    public Response importObjects(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.importObjects]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(importService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = importService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = importService.importObjects(decodedContent, organizationId, userId);                
            }   
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing object: ", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    

    @Path("/importFile")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response importFile(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.importFile]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(importService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("------");
            String decodedContent = payload.substring(beginIndex, endIndex);
            decodedContent = URLDecoder.decode(decodedContent, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = importService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = importService.importObjects(decodedContent, organizationId, userId);                
            }    
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing object from file: ", ex);
            result = "File import error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 

    @Path("/importModelFileAndProcessAll")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response importModelFileAndProcessAll(String payload, @Context HttpServletRequest request) {
        JSONObject responseJSON;        
        String logId = "DELTA3 WS method [admin.importFileAndProcessAll]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(importService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("------WebKitFormBoundary");
            String decodedContent = payload.substring(beginIndex, endIndex);
            decodedContent = URLDecoder.decode(decodedContent, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = importService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = importService.importObjects(decodedContent, organizationId, userId);                
            }  
            responseJSON = new JSONObject(result);
            JSONObject statusJSON = responseJSON.getJSONObject("status");
            if ( responseJSON.getBoolean("success") == true ) {
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());   
                ThreadPoolExecutor executor = (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");
                executor.execute(new ProcessFlatTable(null, result, currentTimestamp.toString(), userId));  
                result = "Data Model imported successfully and Flat Table processing started.\nYou can monitor the process from Model Builder.";                
            } else {
                result = statusJSON.getString("message");
            }
            //result = modelService.processFlatTableAll(result, userId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing and processing Data Model: ", ex);
            result = "File import and processing Flat Table error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 
    
    @Path("/importModelFileAndProcessNoMissing")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response importModelFileAndProcessNoMissing(String payload, @Context HttpServletRequest request) {
        JSONObject responseJSON;        
        String logId = "DELTA3 WS method [admin.importFileAndProcessNoMissing]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(importService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("------WebKitFormBoundary");
            String decodedContent = payload.substring(beginIndex, endIndex);
            decodedContent = URLDecoder.decode(decodedContent, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = importService.importObjects("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = importService.importObjects(decodedContent, organizationId, userId);                
            }  
            responseJSON = new JSONObject(result);
            JSONObject statusJSON = responseJSON.getJSONObject("status");
            if ( responseJSON.getBoolean("success") == true ) {
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());   
                ThreadPoolExecutor executor = (ThreadPoolExecutor) request.getServletContext().getAttribute("executor");
                executor.execute(new ProcessFlatTableNoMissing(null, result, currentTimestamp.toString(), userId));  
                result = "Data Model imported successfully and Flat Table processing started." 
                        + "\nMissing fields processing turned off."
                        + "\nYou can monitor the process from Model Builder.";                
            } else {
                result = statusJSON.getString("message");
            }
            //result = modelService.processFlatTableAll(result, userId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing and processing (no missing) Data Model: ", ex);
            result = "File import and processing (no missing) Flat Table error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 

    
   @OPTIONS
   @Path("import")
   public Response corsMyResourceShare(@HeaderParam("Access-Control-Request-Headers") String requestH) {
      _corsHeaders = requestH;
      return makeCORS(Response.ok(), requestH);
   }
   
   private Response makeCORS(Response.ResponseBuilder req, String returnMethod) {
       Response.ResponseBuilder rb = req.header("Access-Control-Allow-Origin", "*")
          .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

       if (!"".equals(returnMethod)) {
          rb.header("Access-Control-Allow-Headers", returnMethod);
       }

       return rb.build();
    }

    private Response makeCORS(Response.ResponseBuilder req) {
       return makeCORS(req, _corsHeaders);
    }      
}
