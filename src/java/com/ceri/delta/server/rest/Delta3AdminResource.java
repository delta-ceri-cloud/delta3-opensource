/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server.rest;

import com.ceri.delta.entity.Audit;
import com.ceri.delta.entity.Configuration;
import com.ceri.delta.entity.GroupTable;
import com.ceri.delta.entity.Method;
import com.ceri.delta.entity.Organization;
import com.ceri.delta.entity.Permissiontemplate;
import com.ceri.delta.entity.Role;
import com.ceri.delta.entity.User;
import com.ceri.delta.entity.events.Eventtemplate;
import com.ceri.delta.jpa.AuditJpaController;
import com.ceri.delta.jpa.ConfigurationJpaController;
import com.ceri.delta.jpa.GroupJpaController;
import com.ceri.delta.jpa.MethodJpaController;
import com.ceri.delta.jpa.OrganizationJpaController;
import com.ceri.delta.jpa.PermissiontemplateJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.RoleJpaController;
import com.ceri.delta.jpa.UserJpaController;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.AdminServiceImpl;
import com.ceri.delta.server.EventServiceImpl;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc
 */

@Path("admin")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3AdminResource {
    private static final Logger logger = Logger.getLogger(Delta3AdminResource.class.getName());;
    private static AdminServiceImpl adminService;
    private static UserJpaController userController;
    private static OrganizationJpaController organizationController;
    private static RoleJpaController roleController;   
    private static AuditJpaController auditController;         
    private static PermissiontemplateJpaController permissiontemplateController;   
    private static GroupJpaController groupController;
    private static ConfigurationJpaController configurationController;
    private static MethodJpaController methodController;  
    private static EventServiceImpl eventService;

    /**
     * Creates a new instance of Delta3AdminResource
     */
    public Delta3AdminResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }      
        if ( userController == null ) {
            userController = new UserJpaController();
        }      
        if ( organizationController == null ) {
            organizationController = new OrganizationJpaController();
        }    
        if ( roleController == null ) {
            roleController = new RoleJpaController();
        }         
        if ( permissiontemplateController == null ) {
            permissiontemplateController = new PermissiontemplateJpaController();
        }     
        if ( groupController == null ) {
            groupController = new GroupJpaController();
        }          
        if ( configurationController == null ) {
            configurationController = new ConfigurationJpaController();
        }          
        if ( methodController == null ) {
            methodController = new MethodJpaController();
        }      
        if ( auditController == null ) {
            auditController = new AuditJpaController();
        }         
        if ( eventService == null ) {
            eventService = new EventServiceImpl();
        }       
    }

    @Path("/login")
    @GET
    @Produces("application/json")
    public Response loginUser(@Context UriInfo info, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        
        logger.log(Level.SEVERE,"Received WS request to authenticate user");        
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();        
        String userAlias = info.getQueryParameters().getFirst("Username");
        String password = info.getQueryParameters().getFirst("Password");    
        String app = info.getQueryParameters().getFirst("App");           
        String result = adminService.login(userAlias, password, app, request);
        String sessionId = "";          
        try {
            if ( !result.equals("") ) {
                success.put("success", false);                  
                status.put("message", result);                
                success.put("status", status);
            } else {
                success.put("success", true);                  
                status.put("message", "User authenticated.");                
                success.put("status", status);       
                sessionId = request.getSession(false).getId();     
                sessionId = sessionId.substring(0,10)+'C'+sessionId.substring(11);
            }              
            javax.servlet.http.Cookie cookie=new javax.servlet.http.Cookie("DELTA3",sessionId);
            cookie.setHttpOnly(false);
            cookie.setSecure(false);
            cookie.setPath("/");
            response.addCookie(cookie);     // set session simulation cookie            
            return Response.ok(success.toString()).build();                
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "JSON error in WS authUser: ", ex);
        }      
        return Response.ok(result).build(); 
    }

     /**
     * GET method for updating or creating an instance of Delta3AdminResource
     * @return an HTTP response with content of the updated or created resource.
     */
//    @Path("/loginUserAndGetAuth")
//    @GET
//    @Produces("application/json")
//    public Response loginUserAndGetAuth(@Context UriInfo info, @Context HttpServletRequest request) {
//        
//        logger.log(Level.SEVERE,"Received WS request to authenticate and authorize user");        
//        JSONObject success = new JSONObject();
//        JSONObject status = new JSONObject();     
//        String userAlias = info.getQueryParameters().getFirst("Username");
//        String password = info.getQueryParameters().getFirst("Password");
//        request.getServletContext().getContext("/Delta3").setAttribute("userAlias","kipo");           
//        String result = adminService.login(userAlias, password, request);        
//        try {
//            if ( !result.equals("") ) {
//                success.put("success", false);                  
//                status.put("message", result);                
//                success.put("status", status);
//                return Response.status(Response.Status.NOT_FOUND).entity("User " + userAlias + " not authenticated.").build();
//            } else {   
//                result = "Not authorized"; // to be overwritten by call to adminService
//                try {    
//                    result = adminService.getUserAuthInfo(userAlias, request);            
//                } catch (Exception ex) {
//                    logger.log(Level.SEVERE, "Exception in WS loginUserAndGetAuth: ", ex.getMessage()); 
//                }                  
//                return Response.ok(result).build();      
//            }   
//        } catch (JSONException ex) {
//            logger.log(Level.SEVERE, "JSON error in WS authUser: ", ex);
//        }
//        return Response.serverError().build();  
//    }
    
    @Path("/logout")
    @POST
    @Produces("application/json")  
    public Response logoutUser(@Context HttpServletRequest request, @CookieParam("JSESSIONID") Cookie cookie) {
        
        logger.log(Level.SEVERE,"Received WS request to logout user");        
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();       
        try {
            if ( !adminService.logout(request) ) {
                success.put("success", false);                  
                status.put("message", "User session not terminateted");                
                success.put("status", status);
                return Response.status(Response.Status.NOT_FOUND).entity("User session not terminated.").build();
            } else {
                success.put("success", true);                  
                status.put("message", "User session terminated");                
                success.put("status", status);                       
                NewCookie newCookie = new NewCookie(cookie, null, 0, false);  // this seems to be not working on Chrome and Mac!
                return Response.ok(success.toString()).cookie(newCookie).build();      
            }   
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS logoutUser: ", ex);
        }
        return Response.serverError().build();  
    }
        
    @Path("/getCurrentUser")
    @GET
    @Produces("application/json")
    public Response getCurrentUser(@Context HttpServletRequest request) {
        
        String alias = null;
        
        if ( (alias = Auth.getUserAliasFromSession(request)) != null ) {
           return Response.ok(alias).build();
        } else {
           return Response.status(Response.Status.NOT_FOUND).entity("User not logged in").build();            
        }
    }
//    
//    @Path("/auth")
//    @POST
//    @Produces("application/json")
//    public Response authUser(String load) {
//        
//        logger.log(Level.SEVERE,"Received WS request to authenticate user: " + load); 
//        JSONObject success = new JSONObject();
//        JSONObject status = new JSONObject();            
//        JSONObject requestContent;        
//        String userAlias = null;
//        String password = null; 
//        String organization = null;
//        
//        try {
//            requestContent = new JSONObject(load);
//            JSONArray userArray = requestContent.getJSONArray("users");
//            JSONObject user = (JSONObject)userArray.get(0);
//            userAlias = user.getString("userAlias");
//            password = user.getString("password");   
//            organization = user.getString("organization");
//        } catch (JSONException ex) {
//            logger.log(Level.SEVERE, null, ex);
//        }
//        try {
//            String result = adminService.authorize(userAlias, password, organization);
//            if ( result.equals("OK") == true ) {
//                status.put("message", "User authorized");                
//                success.put("status", status);
//                success.put("success", true);          
//                return Response.ok(success.toString()).build();      
//            } else {
//                status.put("message", result);                
//                success.put("status", status);
//                success.put("success", false);  
//                return Response.ok(success.toString()).build();                      
//                //return Response.status(Response.Status.NOT_FOUND).entity("User " + userAlias + "and Organization " + organization + " not authenticated.").build();
//            }   
//        } catch (JSONException ex) {
//            logger.log(Level.SEVERE, "JSON error in WS method [admin.authUser]: ", ex);
//        }
//        return Response.serverError().build();  
//    }
  
    @Path("/getUserAuthInfo")
    @POST
    @Produces("application/json")
    public Response getUserAuthInfo(String payload, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        String userAlias = null;
        String logId = "DELTA3 WS method [admin.getUserAuthInfo]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
         try {
            requestContent = new JSONObject(payload);
            JSONArray userArray = requestContent.getJSONArray("userAuth");
            JSONObject user = (JSONObject)userArray.get(0);
            userAlias = user.getString("alias");    
            result = adminService.getUserAuthInfo(userAlias, payload, request);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }

    @Path("/getCurrentUserAuthInfo")
    @POST
    @Produces("application/json")
    public Response getCurrentUserAuthInfo(String payload, @Context HttpServletRequest request, @Context HttpServletResponse response) {  
        String userAlias = null;
        String logId = "DELTA3 WS method [admin.getCurrentUserAuthInfo]";
        String result = "WA0000 " + logId + " failed";        
        Integer userId = Auth.getUserIdFromSession(request);
        userAlias = Auth.getUserAliasFromSession(request);
        //String app = info.getQueryParameters().getFirst("App");             
        String sessionId = "";
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).cookie(new NewCookie("DELTA3", "")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        try {    
            result = adminService.getUserAuthInfo(userAlias, payload, request);   
            sessionId = request.getSession(false).getId();   
            javax.servlet.http.Cookie cookie=new javax.servlet.http.Cookie("DELTA3",sessionId.substring(0,11)+'C'+sessionId.substring(12));
            cookie.setHttpOnly(false);
            cookie.setSecure(false);
            cookie.setPath("/");
            response.addCookie(cookie); // set session simulation cookie            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " Exception: ", ex.getMessage()); 
            result = logId + " Exception: " + ex.getMessage();
        }         
        return Response.ok(result).build(); 
    }
 
    @Path("/changePassword")
    @POST  
    @Produces("application/json")
    public Response changePassword(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.changePassword]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");                     
            result = adminService.changePassword(decodedContent, userId);       
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
 
    @Path("/resetPassword")
    @POST  
    @Produces("application/json")
    public Response resetPassword(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.resetPassword]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");                     
            result = adminService.resetPassword(decodedContent, userId);       
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/addUserRole")
    @POST
    @Produces("application/json")
    public Response addUserRole(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.addUserRole]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());     

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.createUserRoles(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
   
    @Path("/removeUserRole")
    @POST
    @Produces("application/json")
    public Response removeUserRole(String payload, @Context HttpServletRequest request) {
 
        String logId = "DELTA3 WS method [admin.removeUserRole]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.deleteUserRoles(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
     
    @Path("/addUserProjectRole")
    @POST
    @Produces("application/json")
    public Response addUserProjectRole(String payload, @Context HttpServletRequest request) throws JSONException {
        JSONObject responseJSON = null;
        JSONObject responseJSON2 = null;
        JSONObject payloadJSON = null;
        
        
        String logId = "DELTA3 WS method [admin.addUserProjectRole]";
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        
        System.out.println(" Payload===============================================================");
        System.out.println("This is at payload"+payload);
        payloadJSON = new JSONObject(payload);
        JSONArray payloadArray = payloadJSON.getJSONArray("users");
        JSONObject payloadst = (JSONObject)payloadArray.get(0);
        System.out.println("This is at organizationId"+organizationId);
        System.out.println("This is at main userId"+userId);
        System.out.println("This is JSONObject payloadst:"+payloadst.toString());
        System.out.println("This is JSONObject alias :"+payloadst.get("alias"));
        System.out.println("This is JSONObject created idUser :"+payloadst.get("idUser"));        
        System.out.println("===============================================================");
        String PayloadUserAlias = payloadst.get("alias").toString();
        String PayloadUserID = payloadst.get("idUser").toString();
      
        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());     

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            System.out.println("This is decoded content"+decodedContent);
            
            result = adminService.createUserProjectRoles(decodedContent, userId);
            
        
        
            try {
                responseJSON = new JSONObject(result);          
                if ( responseJSON.getBoolean("success") == true ) {
                    
                    System.out.println(" RESULT===============================================================");
                    System.out.println("This is result :"+result);
                    System.out.println("This is responseJSON :"+responseJSON.toString());
                    JSONArray roleArray = responseJSON.getJSONArray("userRoles");
                    JSONObject role = (JSONObject)roleArray.get(0);
                    System.out.println("This is JSONObject role :"+role.toString());
                    System.out.println("This is JSONObject updatedBy :"+role.get("updatedBy"));
                    System.out.println("This is JSONObject createdBy :"+role.get("createdBy"));
                    //System.out.println("This is JSONObject userHasRolePK :"+role.get("userHasRolePK"));                    
                    JSONObject role2 = new JSONObject(role.get("userHasRolePK").toString());
                   
                    //System.out.println("This is role2 useridUser:"+role2.get("useridUser"));
                    //System.out.println("This is role2 idGroup:"+role2.get("idGroup"));
                    //System.out.println("This is role2 roleidRole:"+role2.get("roleidRole"));
                    
                    
                    Integer AlluserID = role2.getInt("useridUser"); 
                    Integer AllgroupID = role2.getInt("idGroup"); 
                    Integer AllroleID = role2.getInt("roleidRole"); 
                    System.out.println("This is AllgroupID:"+AllgroupID);
                    
                    
                    if(AllgroupID==0){
                        System.out.println(" AT ASSIGNING ALL PROJECTS===============================================================");
                        String result3 = adminService.getOrgObjects(0, 0,  organizationId, groupController, new GroupTable());
                        //System.out.println("Project is 0 hence retrived all projects\n"+result3);
                        responseJSON2 = new JSONObject(result3);
                        JSONArray allGroupArray = responseJSON2.getJSONArray("groups");
                        System.out.println("allGroupArray length"+allGroupArray.length());
                        
                        for(int i=0;i<allGroupArray.length();i++){
                            System.out.println("This is allGroupArray loop \n"+allGroupArray.get(i));
                             JSONObject allGroupArray1 = new JSONObject(allGroupArray.get(i).toString());
                             System.out.println("------------------------------------------");
                             System.out.println("name:"+allGroupArray1.get("name"));
                             System.out.println("AlluserID:"+AlluserID);
                             System.out.println("AllroleID:"+AllroleID);
                             System.out.println("groupid:"+allGroupArray1.get("idGroup"));
                            Integer PayloadProjectID = allGroupArray1.getInt("idGroup");
                             
                            
                            System.out.println(" calling POST ===================================================================)");
                            String payload1 ="{\"users\":[{\"alias\":\""+PayloadUserAlias+"\",\"idUser\":\""+ PayloadUserID+"\"}],\"entitlements\":[{\"project\":"+PayloadProjectID+",\"roles\":["+AllroleID +"]}]}";                              
                            
                            JSONObject payload1json = new JSONObject(payload1);
                                    
                            String decodedContent1 = URLDecoder.decode(payload1, "UTF-8");
                            System.out.println("decodedContent1" +decodedContent1);
                             System.out.println("decodedContent1" +payload1);
                             
                            String result4 = adminService.createUserProjectRoles(decodedContent1, userId);
                            System.out.println("result4"+result4);
                        }
                        
                        
                    }
                    else{
                         System.out.println("This is not all project");
                        
                    }
                   
                    

                    
                    
                    
                    //JSONObject ifuserHasRolePK = role.userHasRolePK;
                    //System.out.println("This is ifuserHasRolePK :"+ifuserHasRolePK);
                   
                    //Integer idGroup = role.getInt("userHasRolePK").;
                    //System.out.println("This is idGroup :"+idGroup);
                    //adminService.createGroupEntities(idGroup, idModel, "models", userId);
                    
                }
            
            } catch (JSONException ex) {
                Logger.getLogger(Delta3ModelResource.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
        return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
   
    @Path("/removeUserProjectRole")
    @POST
    @Produces("application/json")
    public Response removeUserProjectRole(String payload, @Context HttpServletRequest request) throws UnsupportedEncodingException, JSONException {
        JSONObject responseJSON = null;
        JSONObject responseJSON2 = null;
        JSONObject payloadJSON = null;
        payloadJSON = new JSONObject(payload);
        JSONArray payloadArray = payloadJSON.getJSONArray("users");
        JSONObject payloadst = (JSONObject)payloadArray.get(0);
        
        System.out.println("payload"+payload);
        System.out.println(" Payload===============================================================");
        String PayloadUserAlias = payloadst.get("alias").toString();
        String PayloadUserID = payloadst.get("idUser").toString();

        String logId = "DELTA3 WS method [admin.removeUserProjectRole]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.deleteUserProjectRoles(decodedContent, userId);
            //need to add code here
            
            
            try{
              responseJSON = new JSONObject(result);          
                if ( responseJSON.getBoolean("success") == true ) {
                    
                    System.out.println(" RESULT===============================================================");
                    System.out.println("This is result :"+result);
                    System.out.println("This is responseJSON :"+responseJSON.toString());
                    JSONArray roleArray = responseJSON.getJSONArray("userProjectRoles");
                    JSONObject role = (JSONObject)roleArray.get(0);
                    System.out.println("This is JSONObject role :"+role.toString());
                    System.out.println("This is JSONObject updatedBy :"+role.get("updatedBy"));
                    System.out.println("This is JSONObject createdBy :"+role.get("updatedTS"));
                    System.out.println("This is JSONObject updatedBy :"+role.get("userHasRolePK").toString());
                    
                    JSONObject role2 = new JSONObject(role.get("userHasRolePK").toString());
                    System.out.println("This is role2 useridUser:"+role2.get("useridUser"));
                    System.out.println("This is role2 idGroup:"+role2.get("idGroup"));
                    System.out.println("This is role2 roleidRole:"+role2.get("roleidRole"));
                    
                    
                    Integer AlluserID = role2.getInt("useridUser"); 
                    Integer AllgroupID = role2.getInt("idGroup"); 
                    Integer AllroleID = role2.getInt("roleidRole"); 
                    System.out.println("This is AllgroupID:"+AllgroupID);
                    
                    
                    if(AllgroupID==0){
                        System.out.println("This is all project");
                        String result1 = adminService.getProjectRoles(AlluserID);
                        System.out.println("This is result1:"+result1);
                        
                        responseJSON2 = new JSONObject(result1);
                        JSONArray allGroupArray = responseJSON2.getJSONArray("projectRoles");
                        System.out.println("allGroupArray length"+allGroupArray.length());
                        
                        for(int i=0;i<allGroupArray.length();i++){
                            System.out.println("This is allGroupArray loop \n"+allGroupArray.get(i));
                            JSONObject allGroupArray1 = new JSONObject(allGroupArray.get(i).toString());
                            Integer PayloadProjectID = allGroupArray1.getInt("idGroup");
                             System.out.println("groupid:"+PayloadProjectID);
                            
                            System.out.println("now deleting all------------------------------------------");
                     
                            String payload1 ="{\"users\":[{\"alias\":\""+PayloadUserAlias+"\",\"idUser\":\""+ PayloadUserID+"\"}],\"entitlements\":[{\"project\":"+PayloadProjectID+",\"roles\":["+AllroleID +"]}]}";                              
                            
                            JSONObject payload1json = new JSONObject(payload1);
                                    
                            String decodedContent1 = URLDecoder.decode(payload1, "UTF-8");
                            System.out.println("decodedContent1" +decodedContent1);
                            System.out.println("decodedContent1" +payload1);
                             
                            String result4 = adminService.deleteUserProjectRoles(decodedContent1, userId);
                            System.out.println("result4"+result4);
                        }
                        
                        
                        
                        
                    }
                    else{
                         System.out.println("This is not all project");
                    
                    }
                          
            }
            }catch (JSONException ex) {
                Logger.getLogger(Delta3ModelResource.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            return Response.ok(result).build();     
        }catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }



        
        return Response.serverError().build();  
    } 
    
    @Path("/getRolePermissionInfo")
    @POST
    @Produces("application/json")
    public Response getRolePermissionInfo(String load, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        int idRole;
        String logId = "DELTA3 WS method [admin.getRolePermission]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
         try {
            requestContent = new JSONObject(load);
            JSONArray userArray = requestContent.getJSONArray("rolePermissions");
            JSONObject role = (JSONObject)userArray.get(0);
            idRole = role.getInt("idRole");
            if ( adminService == null ) {
                adminService = new AdminServiceImpl();
            }     
            result = adminService.getRolePermissionInfo(idRole);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }
 
    @Path("/addRolePermission")
    @POST
    @Produces("application/json")
    public Response addRolePermission(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.addRolePermission]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.createRolePermissions(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
   
    @Path("/removeRolePermission")
    @POST
    @Produces("application/json")
    public Response removeRolePermission(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.removeRolePermission]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = adminService.deleteRolePermissions(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, "Delta 3 WS method [admin.removeRolePermission] exception while decoding URL", ex);
        }        
        return Response.serverError().build();  
    }
 
    //-------------------------------------------------------------------------- Users    
    @GET
    @Path("/getUsers")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getUsers(@QueryParam(value = "filter") final String filter, @QueryParam(value = "sort") final String sort, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {      
        
        System.out.println("This is fillter"+filter);
                System.out.println("This is sort"+sort);
                        System.out.println("This is page"+page);
                                System.out.println("This is start"+start);
                                        System.out.println("This is limit"+limit);
        String logId = "DELTA3 WS method [admin.getUsers]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);     
        String userType = Auth.getUserTypeFromSession(request);     
        
        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        
        
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        if ( "SADMIN".equals(userType) && (organizationId == 1)  ) { 
            // assuming this is COPSYS sys admin pull all users for all orgs
            result = adminService.getObjectsExtended(Integer.parseInt(start), Integer.parseInt(limit), filter, sort, userController, new User());            
        } else {
            result = adminService.getOrgObjectsExtended(Integer.parseInt(start), Integer.parseInt(limit), filter, sort, organizationId, userController, new User());
        }
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateUsers")
    @Produces("application/json")
    public Response updateUsers(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateUsers]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                if ( organizationId.intValue() == 1 ) { // assume this is COPSYS sys admin                
                    result = adminService.updateObjects("[" + decodedContent + "]", userId, userController, new User());
                } else {
                    result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, userController, new User());                    
                }
            } else {
                if ( organizationId.intValue() == 1 ) { // assume this is COPSYS sys admin                
                    result = adminService.updateObjects(decodedContent, userId, userController, new User());      
                } else {
                    result = adminService.updateOrgObjects(decodedContent, organizationId, userId, userController, new User());                      
                }
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createUsers")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createUsers(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createUsers]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                if ( organizationId == 1 ) { // assume this is COPSYS sys admin
                    result = adminService.createObjects("[" + decodedContent + "]", userId, userController, new User());
                } else {
                    result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, userController, new User());                    
                }
            } else {
                if ( organizationId == 1 ) { // assume this is COPSYS sys admin                
                    result = adminService.createObjects(decodedContent, userId, userController, new User());  
                } else {
                    result = adminService.createOrgObjects(decodedContent, organizationId, userId, userController, new User());                      
                }
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

    //-------------------------------------------------------------------------- Organizations
    @GET
    @Path("/getOrganizations")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getOrganizations(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getOrganizations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);     
        Integer organizationId = Auth.getOrganizationIdFromSession(request);   
        String userType = Auth.getUserTypeFromSession(request);   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());   
        if ( "SADMIN".equals(userType) && (organizationId == 1)  ) { 
            result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationController, new Organization());
        } else {
            result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, organizationController, new Organization());
        }
        return Response.ok(result).build();       
    }    
   
    @POST
    @Path("/updateOrganizations")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateOrganizations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateOrganizations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, organizationController, new Organization());
            } else {
                result = adminService.updateObjects(decodedContent, userId, organizationController, new Organization());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, "Delta 3 WS method [admin.updateOrganizations] exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
      
    @Path("/createOrganizations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createOrganizations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createOrganizations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);       
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, organizationController, new Organization());
            } else {
                result = adminService.createObjects(decodedContent, userId, organizationController, new Organization());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/removeOrganizations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response removeOrganizations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.removeOrganizations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);       
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteObjects("[" + decodedContent + "]", userId, organizationController, new Organization());
            } else {
                result = adminService.deleteObjects(decodedContent, userId, organizationController, new Organization());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    //-------------------------------------------------------------------------- Project Roles
    @GET
    @Path("/getProjectRoles")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getProjectRoles(@QueryParam(value = "userId") final String userId, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getProjectRoles]";
        String result = "WA0000 " + logId + " failed.";
        Integer thisUserId = Auth.getUserIdFromSession(request);   
        if (thisUserId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request for user {0}", userId);      
        result = adminService.getProjectRoles(Integer.parseInt(userId));
        return Response.ok(result).build();  
    }  
    
    //-------------------------------------------------------------------------- Roles
    @GET
    @Path("/getRoles")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getRoles(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getRoles]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), roleController, new Role());
        return Response.ok(result).build();  
    }    
  
    @Path("/createRoles")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createRoles(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createRoles]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, roleController, new Role());
            } else {
                result = adminService.createObjects(decodedContent, userId, roleController, new Role());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateRoles")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateRoles(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateRoles]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, roleController, new Role());
            } else {
                result = adminService.updateObjects(decodedContent, userId, roleController, new Role());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
   
    //-------------------------------------------------------------------------- Permission Templates
    @GET
    @Path("/getPermissiontemplates")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getPermissiontemplates(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getPermissiontemplates]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());         
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), permissiontemplateController, new Permissiontemplate());
        return Response.ok(result).build();  
    }    
  
    @Path("/createPermissiontemplates")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createPermissionTemplates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createPermissiontemplates]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);          
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, permissiontemplateController, new Permissiontemplate());
            } else {
                result = adminService.createObjects(decodedContent, userId, permissiontemplateController, new Permissiontemplate());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updatePermissiontemplates")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updatePermissiontemplates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updatePermissiontemplates]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, permissiontemplateController, new Permissiontemplate());
            } else {
                result = adminService.updateObjects(decodedContent, userId, permissiontemplateController, new Permissiontemplate());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    //-------------------------------------------------------------------------- Groups
    @GET
    @Path("/getGroupsByOrg")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getGroupsByOrg(@QueryParam(value = "orgId") final String orgId, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getGroupsByOrg]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);  
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        Integer idOrg = Integer.parseInt(orgId);
        if ( idOrg != 0 ) {
            organizationId = idOrg;      
        }
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit),  organizationId, groupController, new GroupTable());
        System.out.println("result at getGroupsByOrg"+result.toString());
        return Response.ok(result).build();  
    }    
    
    @GET
    @Path("/getGroups")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getGroups(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getGroups]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);    
        Integer organizationId = Auth.getOrganizationIdFromSession(request);               
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit),  organizationId, groupController, new GroupTable());
        System.out.println("result at getgroups"+result);
        
       
        return Response.ok(result).build();  
    }    
  
    @Path("/createGroups")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createGroups(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createGroups]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);    
        Integer organizationId = Auth.getOrganizationIdFromSession(request);       
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, groupController, new GroupTable());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, groupController, new GroupTable());                
            }              
            
           System.out.println("This is NEW Group ID"+result.toString());
           
           String result2;
           result2 = adminService.projectALL(result, userId, organizationId );
           System.out.println("This is result2"+result2);
           
             
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateGroups")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateGroups(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateGroups]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);   
        Integer organizationId = Auth.getOrganizationIdFromSession(request);               
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, groupController, new GroupTable());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, groupController, new GroupTable());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }         
    
    @POST
    @Path("/deleteGroups")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response deleteGroups(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.deleteGroups]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);   
        Integer organizationId = Auth.getOrganizationIdFromSession(request);               
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteOrgObjects("[" + decodedContent + "]", organizationId, userId, groupController, new GroupTable());
            } else {
                result = adminService.deleteOrgObjects(decodedContent, organizationId, userId, groupController, new GroupTable());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }      
 
    @GET
    @Path("/getGroupsByType")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getGroupsByType(@QueryParam(value = "type") final String type, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getGroupsByType]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);    
        Integer organizationId = Auth.getOrganizationIdFromSession(request);               
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
        result = adminService.getGroupsByType(Integer.parseInt(start), Integer.parseInt(limit), type, organizationId, groupController);
        return Response.ok(result).build();  
    }    

    @Path("/getEntityGroup")
    @GET
    @Produces("application/json")
    public Response getEntityGroup(@QueryParam(value = "entity") final String entityString, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        Integer idEntity;
        String logId = "DELTA3 WS method [study.getEntityGroup]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
         try {
            requestContent = new JSONObject(entityString);
            JSONArray entityArray = requestContent.getJSONArray("entities");
            JSONObject entity = (JSONObject)entityArray.get(0);
            idEntity = entity.getInt("idEntity"); 
            result = adminService.getEntityGroupInfo(idEntity, entity.getString("type"));            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }       
    
    @Path("/addGroupEntity")
    @POST
    @Produces("application/json")
    public Response addGroupEntity(String payload, @Context HttpServletRequest request) {      
        String logId = "DELTA3 WS method [admin.addGroupEntity]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);   
        
        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");    
            System.out.println("This is at add Group Entity decodedContent"+decodedContent);
            
            result = adminService.createGroupEntities(decodedContent, userId);
            
            System.out.println("This is at add Group Entity"+result.toString());
            
            return Response.ok(result).build();        
        
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while processing group ", ex);
        }        
        return Response.ok(result).build();  
    }

    @Path("/removeAndAddGroupEntity")
    @POST
    @Produces("application/json")
    public Response removeAndAddGroupEntity(String payload, @Context HttpServletRequest request) {      
        String logId = "DELTA3 WS method [admin.removeAndAddGroupEntity]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);                
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");  
            result = adminService.deleteAllGroupEntities(decodedContent, userId);            
            result = adminService.createGroupEntities(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while processing group ", ex);
        }        
        return Response.ok(result).build();  
    }
    
    @Path("/removeGroupEntity")
    @POST
    @Produces("application/json")
    public Response removeGroupEntity(String payload, @Context HttpServletRequest request) {
        //JSONObject requestContent;           
        String logId = "DELTA3 WS method [study.removeStudyCategory]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);      
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");              
            //requestContent = new JSONObject(decodedContent);
            //JSONArray studyArray = requestContent.getJSONArray("groups");
            //JSONObject group = (JSONObject)studyArray.get(0);
            //String type = group.getString("type");            
            result = adminService.deleteGroupEntities(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.ok(result).build();  
    }
 
    //-------------------------------------------------------------------------- Audit
    @GET
    @Path("/getAudits")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getAudits(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getAudits]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), auditController, new Audit());
        return Response.ok(result).build();  
    }   
    
    //-------------------------------------------------------------------------- Configurations    
    @GET
    @Path("/getConfigurations")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getConfigurations(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getConfigurations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, configurationController, new Configuration());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateConfigurations")
    @Produces("application/json")
    public Response updateConfigurations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateConfigurations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                      
            }       
            int start = result.indexOf("idConfiguration")-1;
            int end = result.indexOf("}", start);  
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_DB_UPDATE_EVENT, result.substring(start,end));
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createConfigurations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createConfigurations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createConfigurations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());                    
            } else {
               result = adminService.createOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                      
            }
            int start = result.indexOf("idConfiguration")-1;
            int end = result.indexOf("}", start);  
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_DB_CREATE_EVENT, result.substring(start,end));
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    //-------------------------------------------------------------------------- Methods    
    @GET
    @Path("/getMethods")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getMethods(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getMethods]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, methodController, new Method());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateMethods")
    @Produces("application/json")
    public Response updateMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.updateMethods]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, methodController, new Method());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, methodController, new Method());                      
            }               
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_METHOD_UPDATE_EVENT, result);          
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createMethods")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createMethods]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, methodController, new Method());                    
            } else {

               result = adminService.createOrgObjects(decodedContent, organizationId, userId, methodController, new Method());                      
            }      
            int start = result.indexOf("idConfiguration")-1;
            int end = result.indexOf("}", start);
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_METHOD_CREATE_EVENT, result.substring(start,end));
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
   
    @Path("/goPseudo")
    @POST
    @Produces("application/json")
    public Response goPseudo(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.goPseudo]";
        String result = "WA0000 " + logId + " success.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        if ( organizationId != 1 ) {
                result = "WA0002 " + logId + " User not authorized.";
        } else {
            try {
                String decodedContent = URLDecoder.decode(payload, "UTF-8");           
                User user = adminService.goPseudo(decodedContent, userId);       
                Auth.createSuperUserSession(userId, user, "0", "", request); // to-do: apps ("") may need value!
                return Response.ok(result).build();  
            } catch (UnsupportedEncodingException ex) {
                logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            }    
        }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
 
    @GET
    @Path("/getLoggingLevel")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getLoggingLevel(@Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getLoggingLevel]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        result = adminService.getLoggingLevel();
        return Response.ok(result).build();  
    }  
 
    @Path("/changeLoggingLevel")
    @POST
    @Produces("application/json")
    public Response changeLoggingLevel(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.changeLoggingLevel]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session timeout.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        if ( organizationId != 1 ) {
                result = "WA0002 " + logId + " User not authorized.";
        } else {
            try {
                String decodedContent = URLDecoder.decode(payload, "UTF-8");           
                result = adminService.changeLoggingLevel(decodedContent, userId);       
                return Response.ok(result).build();  
            } catch (UnsupportedEncodingException ex) {
                logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            }    
        }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
}
