/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server.rest;


import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.model.ModelColumn;
import com.ceri.delta.entity.model.ModelRelationship;
import com.ceri.delta.entity.model.ModelTable;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.model.ModelRelationshipJpaController;
import com.ceri.delta.jpa.model.ModelTableJpaController;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.AdminServiceImpl;
import com.ceri.delta.server.ModelServiceImpl;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc.
 */
@Path("model")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3ModelResource {
    private static final Logger logger = Logger.getLogger(Delta3ModelResource.class.getName());
    private static ModelServiceImpl modelService; 
    private static AdminServiceImpl adminService;   
    private static ModelJpaController modelController;
    private static ModelColumnJpaController modelColumnController;  
    private static ModelTableJpaController modelTableController;  
    private static ModelRelationshipJpaController modelRelationshipController;
    private String _corsHeaders;    
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Delta3ModelResource
     */
    public Delta3ModelResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }      
        if ( modelService == null ) {
            modelService = new ModelServiceImpl();
        }      
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }    
        if ( modelColumnController == null ) {
            modelColumnController = new ModelColumnJpaController();
        }         
        if ( modelTableController == null ) {
            modelTableController = new ModelTableJpaController();
        }     
        if ( modelRelationshipController == null ) {
            modelRelationshipController = new ModelRelationshipJpaController();
        }           
    }

    @Path("/createFlatTable")
    @POST 
    @Produces("application/json")
    public Response createFlatTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.createDST]";
        String result = "MB0000 " + logId + " failed.";   
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.executeCreateStatement(decodedContent, userId);    
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

    @Path("/setModelStatus")
    @POST  
    @Produces("application/json")
    public Response setModelStatus(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.setModelStatus]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.setModelStatus(decodedContent, userId, "Temp status");      
            
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

       
    //This is new code for project Assigned
    @Path("/setProjectAssigned")
    @POST  
    @Produces("application/json")
    public Response setProjectAssigned(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.setProjectAssigned]";
        String result = "MB0000 " + logId + " failed.";      
        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            //System.out.println("This is at project Assigned"+payload);
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            result = modelService.setProjectAssigned(decodedContent, userId, "test");
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }  
    
    
    
    //This is new code for project Ressigned
    @Path("/resetProjectAssigned")
    @POST  
    @Produces("application/json")
    public Response resetProjectAssigned(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.resetProjectAssigned]";
        String result = "MB0000 " + logId + " failed.";      
        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            //System.out.println("This is at project Reassigned"+payload);
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            result = modelService.resetProjectAssigned(decodedContent, userId, "test");
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    
    
    

    @Path("/getModelStatus")
    @POST  
    @Produces("application/json")
    public Response getModelStatus(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelStatus]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.getModelStatus(decodedContent, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
 
    @Path("/getModelProcessingId")
    @POST  
    @Produces("application/json")
    public Response getModelProcessingId(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelProcessingId]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.getModelProcessingId(decodedContent, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    // this is deprecated method for sybchronous FT generation
    // it is replaced with async servlet
    @Path("/generateDST")
    @POST  
    @Produces("application/json")
    public Response generateDST(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.generateDST]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8"); 
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
            result = modelService.generateStagingTable(decodedContent, currentTimestamp.toString(), userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    @Path("/getJoin")
    @GET  
    @Produces("application/json")
    public Response getJoin(@QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getJoin]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(model, "UTF-8"); 
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
            result = modelService.getJoinSql(decodedContent, currentTimestamp.toString(), userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    @Path("/deleteFlatTable")
    @POST  
    @Produces("application/json")
    public Response deleteFlatTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.deleteFlatTable]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.deleteFlatTable(decodedContent, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

    @Path("/deleteStagingTable")
    @POST  
    @Produces("application/json")
    public Response deleteStagingTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.deleteStagingTable]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.deleteStagingTable(decodedContent, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    @Path("/processVirtualFields")
    @POST  
    @Produces("application/json")
    public Response processVirtualFields(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.generateDST]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            //result = modelService.processVirtualFields(decodedContent, userId);   
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
            result = modelService.processPrimaryVirtualFields(decodedContent, currentTimestamp.toString(), userId);     
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
   
    @Path("/processMissingFields")
    @POST  
    @Produces("application/json")
    public Response processMissingFields(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.processMissingFields]";
        String result = "MB0000 " + logId + " failed.";           
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
            result = modelService.processMissingFields(decodedContent, currentTimestamp.toString(), userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    @Path("/verifyStatement")
    @GET  
    @Produces("application/json")
    public Response verifyStatement(@QueryParam(value = "tableName") final String tableName, @QueryParam(value = "formula") final String formula,  @QueryParam(value = "secondary") final String secondary, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.verifyStatement]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());

        try {
            String decodedContent1 = URLDecoder.decode(tableName, "UTF-8");              
            result = modelService.verifyStatement(decodedContent1, formula, secondary, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while verifying formula statement", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.SEE_OTHER).entity(result).build();
    } 
 
    @GET
    @Path("/getDBObject")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getDBObject(@QueryParam(value = "modelId") final String modelId, @QueryParam(value = "append") final String append, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @QueryParam(value = "orderBy") final String orderBy, @QueryParam(value = "direction") final String direction,  @QueryParam(value = "filterId") final String filterId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getDBObject]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());              

        result = modelService.getDBObject(userId, Integer.parseInt(start), Integer.parseInt(limit), orderBy, direction, Integer.parseInt(filterId), Integer.parseInt(modelId), append);
        return Response.ok(result).build();       
    }  
        
    //-------------------------------------------------------------------------- Models
        /**
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getModels")   
    @Produces("application/json")
    public Response getModels(@QueryParam(value = "filter") final String filter, @QueryParam(value = "sort") final String sort, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit,  @QueryParam(value = "group") final String group, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModels]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
        result = adminService.getOrgObjectsExtendedGroup(Integer.parseInt(start), Integer.parseInt(limit), filter, sort, Integer.parseInt(group), organizationId, modelController, new Model());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);        
        return Response.ok(result).build();  
    }    
  
    @Path("/createModels")
    @POST
    @Produces("application/json")
    public Response createModels(String payload, @Context HttpServletRequest request) throws NonexistentEntityException, Exception {
        JSONObject responseJSON = null;
        String logId = "DELTA3 WS method [model.createModels]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, modelController, new Model());
        } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, modelController, new Model());                
            }
            
            
            
            
        try {
                responseJSON = new JSONObject(result);
                if ( responseJSON.getBoolean("success") == true ) {
                    
                    System.out.println("This is result :"+result);
                    System.out.println("This is responseJSON :"+responseJSON.toString());
                    
                    JSONArray modelArray = responseJSON.getJSONArray("models");
                    JSONObject model = (JSONObject)modelArray.get(0);
                    System.out.println("This is JSONObject model :"+model.toString());
                    
                    Integer idModel = model.getInt("idModel");  
                    System.out.println("This is idModel :"+idModel);
                    Integer idGroup = model.getInt("idGroup");
                    System.out.println("This is idGroup :"+idGroup);
                    adminService.createGroupEntities(idGroup, idModel, "models", userId);
                    
                }
            
            } catch (JSONException ex) {
                Logger.getLogger(Delta3ModelResource.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateModels") 
    @Produces("application/json")
    public Response updateModels(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.updateModels]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());         

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, modelController, new Model());
            } else {
                    result = adminService.updateOrgObjects(decodedContent, organizationId, userId, modelController, new Model());                
            }            
            return Response.ok(result).build();  
        } catch (Exception ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }    
        return Response.ok(adminService.wrapAPI("", "", "", result)).build();        
    }    

    
    
    
    
    
    @Path("/lockUnlockModel")
    @POST
    @Produces("application/json")
    public Response lockUnlockModel(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.lockUnlockModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = modelService.lockUnlockModel("[" + decodedContent + "]", organizationId, userId, modelController, new Model(), request);
            } else {
                result = modelService.lockUnlockModel(decodedContent, organizationId, userId, modelController, new Model(), request);                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/cloneModel")
    @POST
    @Produces("application/json")
    public Response cloneModel(String payload, @Context HttpServletRequest request) throws Exception {
        
        JSONObject payloadJSON = null;
        JSONObject responseJSON = null;
        Integer newlyClonedModel;
        
        String logId = "DELTA3 WS method [model.cloneModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        
        System.out.println("This is cloneModel"+payload);
        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = modelService.cloneModel("[" + decodedContent + "]", organizationId, userId, modelController, new Model());
        } 
        else {
                result = modelService.cloneModel(decodedContent, organizationId, userId, modelController, new Model());                
        }   
        
        try {
                            
                payloadJSON = new JSONObject(payload);                
                System.out.println("This is result :"+result);
                Scanner scan = new Scanner(result);
                scan.useDelimiter(":");
                
                System.out.println("Result String "+scan.next());  
                
                System.out.println("This is payloadJSON :"+payloadJSON.toString());    
                System.out.println("This is payloadJSON idGroup :"+payloadJSON.get("idGroup"));
                System.out.println("This is payloadJSON idModel :"+payloadJSON.get("idModel"));
                
                String[] resultoutput = result.split(":");
                System.out.println("1st String :"+resultoutput[0]);
                System.out.println("2nd String :"+resultoutput[1]);
                
                Integer idGroup = payloadJSON.getInt( "idGroup");
                newlyClonedModel = Integer.parseInt(resultoutput[1]);
                adminService.createGroupEntities(idGroup, newlyClonedModel, "models", userId);         

            
            } catch (JSONException ex) {
                Logger.getLogger(Delta3ModelResource.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        
        return Response.ok(result).build();  
        
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
    }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/exportModel")
    @POST
    @Produces("application/json")
    public Response exportModel(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.exportModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = modelService.exportModel("[" + decodedContent + "]", organizationId, userId, new Model());
            } else {
                result = modelService.exportModel(decodedContent, organizationId, userId, new Model());                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    //-------------------------------------------------------------------------- Model Metadata
    @POST
    @Path("/getModelMetadata")
    @Produces("application/json")
    public Response getModelMetadata(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelMetadata]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.getModelMetadata(decodedContent, false);                         
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }  

    @GET
    @Path("/getMetadata")  
    @Produces("application/json")
    public Response getMetadata(@QueryParam(value = "model") final String model, @QueryParam(value = "localRemoteFlag") final String localRemoteFlag, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getMetadata]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);

        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());         
     
        try {
            String decodedContent = URLDecoder.decode(model, "UTF-8");   
            boolean localFlag = false;
            if ( localRemoteFlag != null && "local".equals(localRemoteFlag) ) {
                localFlag = true;
            }
            result = modelService.getModelMetadata(decodedContent, localFlag);                         
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();     
    }     

    @GET
    @Path("/getModelDetails")  
    @Produces("application/json")
    public Response getModelDetails(@QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelDetails]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());            
      
        try {
            String decodedContent = URLDecoder.decode(model, "UTF-8");           
            result = modelService.getModelDetails(decodedContent);   
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.INFO, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();     
    }     
 
        /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3AdminResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getModelColumns")   
    @Produces("application/json")
    public Response getModelColumns(@QueryParam(value = "model") final String model, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelColumns]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // pagination ignored            
  
        try {
            String decodedContent = URLDecoder.decode(model, "UTF-8");           
            result = modelService.getModelColumns(decodedContent);                         
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();     
    }   
    
    /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3AdminResource
     * @return an instance of java.lang.String
     */
    @POST
    @Path("/getOrgModelColumns")   
    @Produces("application/json")
    //public Response getOrgModelColumns(@QueryParam(value = "model") final String model, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
    public Response getOrgModelColumns(String payload, @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getOrgModelColumns]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // pagination ignored         
   
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = modelService.getOrgModelColumns(decodedContent, organizationId);                         
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();     
    }   
        
    @Path("/createModelColumnsReturnModel")
    @POST
    @Produces("application/json")
    public Response createModelColumnsReturnModel(String payload, @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.createModelColumnsReturnModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, modelColumnController, new ModelColumn());
            } else {
                result = adminService.createObjects(decodedContent, userId, modelColumnController, new ModelColumn());                
            }              
            decodedContent = URLDecoder.decode(model, "UTF-8");  
            modelService.setModelStatus(decodedContent, userId, "Needs processing");
            result = modelService.getModelDetails(decodedContent);
            return Response.ok(result).build();              
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/createModelColumns")
    @POST
    @Produces("application/json")
    public Response createModelColumns(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.createModelColumns]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        //String decodedContent = URLDecoder.decode(payload, "UTF-8");    
        String decodedContent = payload; // decoding skipped because it was removing + and / characters from formula      
        if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
            decodedContent = "[" + decodedContent + "]";
        }
        result = adminService.createObjects(decodedContent, userId, modelColumnController, new ModelColumn());  
        logger.log(Level.SEVERE, logId + " returning: {0}", result);
        modelService.setModelStatusByColumn(decodedContent, userId, "Needs processing");
        return Response.ok(result).build();  
    }    
 
    @Path("/updateModelColumnsReturnModel")
    @POST
    @Produces("application/json")
    public Response updateModelColumnsReturnModel(String payload,  @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.updateModelColumnsReturnModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            //String decodedContent = URLDecoder.decode(payload, "UTF-8");    
            String decodedContent = payload; // decoding skipped because it was removing + and / characters from formula
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, modelColumnController, new ModelColumn());
            } else {
                result = adminService.updateObjects(decodedContent, userId, modelColumnController, new ModelColumn());                
            }  
            decodedContent = URLDecoder.decode(model, "UTF-8");     
            //modelService.setModelStatus(decodedContent, userId, "Needs processing");
            result = modelService.getModelDetails(decodedContent);            
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/updateModelColumns")
    @POST
    @Produces("application/json")
    public Response updateModelColumns(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.updateModelColumns]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String decodedContent = payload; // decoding skipped because it was removing + and / characters from formula            
        if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
            decodedContent = "[" + decodedContent + "]";
        }
        result = adminService.updateObjects(decodedContent, userId, modelColumnController, new ModelColumn());                
        logger.log(Level.SEVERE, logId + " returning: {0}", result);   
        //modelService.setModelStatusByColumn(decodedContent, userId, "Needs processing");        
        return Response.ok(result).build();  
    }

    // this service method deletes model and all tablews. columns and releationships that belong to it
    @Path("/deleteModel")
    @POST
    @Produces("application/json")
    public Response deleteModel(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.deleteModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = modelService.deleteModel("[" + decodedContent + "]", organizationId, userId, modelController, new Model());
            } else {
                result = modelService.deleteModel(decodedContent, organizationId, userId, modelController, new Model());                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
     
    @Path("/removeModelColumnsReturnModel")
    @POST
    @Produces("application/json")
    public Response removeModelColumnsReturnModel(String payload, @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.removeModelColumnsReturnModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteObjects("[" + decodedContent + "]", userId, modelColumnController, new ModelColumn());
            } else {
                result = adminService.updateObjects(decodedContent, userId, modelColumnController, new ModelColumn());                
            }   
            decodedContent = URLDecoder.decode(model, "UTF-8");     
            logger.log(Level.SEVERE, logId + " returning: {0}", result);   
            modelService.setModelStatus(decodedContent, userId, "Needs processing");
            result = modelService.getModelDetails(decodedContent);  
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/removeModelColumns")
    @POST
    @Produces("application/json")
    public Response removeModelColumns(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.removeModelColumns]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = payload;         // decoding removed as it was failing on special chars received
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                decodedContent = "[" + decodedContent + "]";
            }
            result = adminService.deleteObjects(decodedContent, userId, modelColumnController, new ModelColumn());                            
            logger.log(Level.SEVERE, logId + " returning: {0}", result);  
            modelService.cleanUpFiltersByColumn(decodedContent, userId);            
            modelService.setModelStatusByColumn(decodedContent, userId, "Needs processing");
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while removing column ", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
   
    @Path("/createModelTables")
    @POST
    @Produces("application/json")
    public Response createModelTables(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.createModelTables]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                decodedContent = "[" + decodedContent + "]";
            }
            result = adminService.createObjects(decodedContent, userId, modelTableController, new ModelTable());         
            logger.log(Level.SEVERE, logId + " returning: {0}", result);     
            modelService.setModelStatusByTable(decodedContent, userId, "Needs processing");
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/createModelTablesReturnModel")
    @POST
    @Produces("application/json")
    public Response createModelTablesReturnModel(String payload, @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.createModelTablesReturnModel]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, modelTableController, new ModelTable());
            } else {
                result = adminService.createObjects(decodedContent, userId, modelTableController, new ModelTable());                
            }            
            decodedContent = URLDecoder.decode(model, "UTF-8");     
            result = modelService.getModelDetails(decodedContent);
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();              
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
        /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3AdminResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getModelTables")   
    @Produces("application/json")
    public Response getModelTables(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelTables]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
    
        result = adminService.getObjects(Integer.parseInt(start), Integer.parseInt(limit), modelTableController, new ModelTable());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);        
        return Response.ok(result).build();  
    }       
    
    @Path("/updateModelTables")
    @POST
    @Produces("application/json")
    public Response updateModelTables(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.updateModelTables]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                decodedContent = "[" + decodedContent + "]";
            }
            result = adminService.updateObjects(decodedContent, userId, modelTableController, new ModelTable());                
            logger.log(Level.SEVERE, logId + " returning: {0}", result); 
            modelService.setModelStatusByTable(decodedContent, userId, "Needs processing");
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    
    @Path("/updateModelTablesReturnModel")
    @POST
    @Produces("application/json")
    public Response updateModelTablesReturnModel(String payload, @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.updateModelTables]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, modelTableController, new ModelTable());
            } else {
                result = adminService.updateObjects(decodedContent, userId, modelTableController, new ModelTable());                
            }    
            //logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            //return Response.ok(result).build();  
            decodedContent = URLDecoder.decode(model, "UTF-8");     
            modelService.setModelStatus(decodedContent, userId, "Needs processing");
            result = modelService.getModelDetails(decodedContent);            
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();              
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/createModelRelationships")
    @POST
    @Produces("application/json") //ss?
    public Response createModelRelationships(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getOrgModelColumns]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createObjects("[" + decodedContent + "]", userId, modelRelationshipController, new ModelRelationship());
            } else {
                result = adminService.createObjects(decodedContent, userId, modelRelationshipController, new ModelRelationship());                
            }     
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    
    @Path("/updateModelRelationships")
    @POST
    @Produces("application/json")
    public Response updateModelRelationships(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.updateModelRelationships]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateObjects("[" + decodedContent + "]", userId, modelRelationshipController, new ModelRelationship());
            } else {
                result = adminService.updateObjects(decodedContent, userId, modelRelationshipController, new ModelRelationship());                
            }     
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/removeModelRelationships")
    @POST
    @Produces("application/json")
    public Response removeModelRelationships(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.removeModelRelationships]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteObjects("[" + decodedContent + "]", userId, modelRelationshipController, new ModelRelationship());
            } else {
                result = adminService.deleteObjects(decodedContent, userId, modelRelationshipController, new ModelRelationship());                
            }   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/removeModelTables")
    @POST
    @Produces("application/json")
    public Response removeModelTables(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.removeModelTables]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                decodedContent = "[" + decodedContent + "]";
            }
            result = adminService.deleteObjects(decodedContent, userId, modelTableController, new ModelTable());   
            if ( modelService == null ) {
                modelService = new ModelServiceImpl();
            }     
            if ( modelService.cleanupAfterModelTableDelete(decodedContent, userId) == false ) {
                result = logId + ": Clean up after removal of a model table failed. Contact your database administrator.";
            }  
            logger.log(Level.SEVERE, logId + " returning: {0}", result);       
            modelService.setModelStatusByTable(decodedContent, userId, "Needs processing");
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
        /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3AdminResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getModelRelationships")   
    @Produces("application/json")
    public Response getModelRelationships(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @QueryParam(value = "model") final String model, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.getModelRelationship]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
 
        result = adminService.getObjectsById(Integer.parseInt(start), Integer.parseInt(limit), Integer.parseInt(model), modelRelationshipController, new ModelRelationship());
        logger.log(Level.SEVERE, logId + " returning: {0}", result);        
        return Response.ok(result).build();  
    } 
     
   @OPTIONS
   @Path("model")
   public Response corsMyResourceShare(@HeaderParam("Access-Control-Request-Headers") String requestH) {
      _corsHeaders = requestH;
      return makeCORS(Response.ok(), requestH);
   }
   
   private Response makeCORS(Response.ResponseBuilder req, String returnMethod) {
       Response.ResponseBuilder rb = req.header("Access-Control-Allow-Origin", "*")
          .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

       if (!"".equals(returnMethod)) {
          rb.header("Access-Control-Allow-Headers", returnMethod);
       }

       return rb.build();
    }

    private Response makeCORS(Response.ResponseBuilder req) {
       return makeCORS(req, _corsHeaders);
    }      
}
