/*
 * Model Service Implementation
 */
package com.ceri.delta.server;

import com.ceri.delta.db.DBHelper;
import com.ceri.delta.entity.Configuration;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.model.D3DescStats;
import com.ceri.delta.entity.model.D3Metadata;
import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.model.ModelColumn;
import com.ceri.delta.entity.model.ModelRelationship;
import com.ceri.delta.entity.model.ModelTable;
import com.ceri.delta.entity.process.JobRequest;
import com.ceri.delta.entity.process.Process;
import com.ceri.delta.entity.study.FilterField;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.ConfigurationJpaController;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.model.D3DescStatsJpaController;
import com.ceri.delta.jpa.model.D3MetadataJpaController;
import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.model.ModelRelationshipJpaController;
import com.ceri.delta.jpa.model.ModelTableJpaController;
import com.ceri.delta.jpa.study.FilterFieldJpaController;
import com.ceri.delta.property.Property;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.exceptions.EmptyRecordSetException;
import com.ceri.delta.server.exceptions.ImportErrorException;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import com.ceri.delta.util.Util;
import com.copsys.statproxy.Proxy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author Coping Systems Inc.
 */
public class ModelServiceImpl {

    private static int SELECT = 0;
    private static int JOIN = 1;
    private static int INSERT = 2;
    private static int INSERT_BATCH_SIZE = 50;    
    private static final int MAX_TABLES = 10;
    private static final int INSERT_COUNTER_INCREMENT = 1000;    
    private static final int ROLLUP_MAX = 100;    
    private static final int STATUS_FIELD_SIZE = 1023;
    private static final String ROLLUP_DELIMINATOR = "|";
    private static final Logger logger = Logger.getLogger(ModelServiceImpl.class.getName());    
    private static ModelColumnJpaController modelColumnController;
    private static ModelTableJpaController modelTableController;  
    private static FilterFieldJpaController filterfieldController;      
    private static ModelRelationshipJpaController modelRelationshipController;   
    private static ModelJpaController modelController;       
    private static GroupHasEntityJpaController grouphasentityController;
    private static D3MetadataJpaController metadataController;        
    private static ConfigurationJpaController configurationController;
    private static D3DescStatsJpaController descStatsController;
    private static StudyServiceImpl studyService;    
    private static AdminServiceImpl adminService;   
    private static StatServiceImpl statService;
    private Integer tempFieldGen = 1;
    private Integer tempTableGen = 1;     
    
    public static final String flatTableAppendString = "_final";    

    public String executeCreateStatement(String modelJSON, Integer currentUserId) {      
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            String sqlCreate = constructCreateStatement(m.getOutputName(), mc, mt, mr);    
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());             
            DBHelper.executeSqlStatement("DeltaData","DROP TABLE IF EXISTS " + m.getOutputName(),logger);
            AuditLogger.log(currentUserId, 0, "CREATE TABLE", sqlCreate);             
            DBHelper.executeSqlStatement("DeltaData",sqlCreate,logger);   
            setModelStatus(m, currentUserId, "Created", false, false, false, false);
            return "Table created.";
        } catch (PersistenceException ex) {
                String errorString = "MB0001 SQL error while generating Flat Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;
            }
        catch (Exception ex) {
            String errorString = "MB0001 Error while creating Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;            
        }
    }    
    
    public String generateStagingTable(String modelJSON, String timeStamp, Integer currentUserId) {     
        String resultString;
        String sqlCreate  = "";
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            m.setProcessingId(timeStamp); // save thread timestamp/identifier used to cancel the thread
            setModelStatus(m, currentUserId, "Processing of source fields started", false, false, false, false);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            sqlCreate = constructCreateStatement(m.getOutputName(), mc, mt, mr);    
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());             
            DBHelper.executeSqlStatement("DeltaData","DROP TABLE IF EXISTS " + m.getOutputName(),logger);
            AuditLogger.log(currentUserId, 0, "CREATE TABLE", sqlCreate);             
            DBHelper.executeSqlStatement("DeltaData",sqlCreate,logger);   
            setModelStatus(m, currentUserId, "Populating Flat Table with data");
            if ( "Ok".equals(resultString = populateStagingTableJDBC(m, mr, mt, mc, currentUserId)) ) {
                setModelStatus(m, currentUserId, "Processing of source fields finished", true, false, false, false);
                if ( statService == null ) {
                    statService = new StatServiceImpl();
                }    
                setModelStatus(m, currentUserId, "Processing of Staging Table Descriptive Statistics started");      
                String statString = statService.getAllDS(m, m.getOutputName(), false);  
                statService.saveDescStats(statString, currentUserId, m, false);                
                setModelStatus(m, currentUserId, "Processing of Staging Table Descriptive Statistics finished", true, false, false, false);                   
                return "Processing of source fields finished";
            } else {
                setModelStatus(m, currentUserId, resultString);
                return resultString;
            }
        } catch (PersistenceException ex) {
                String errorString = "MB0002 Field parameters or source column datatype is not valid. " + ex.getCause().getCause().toString() + " SQL: " + sqlCreate;
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;
            }
        catch (Exception ex) {
            String errorString = "MB0003 Error while generating Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                
        }
    } 
    
    public String getJoinSql(String modelJSON, String timeStamp, Integer currentUserId) {     
        String[] resultStrings;
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            resultStrings = createJoinStatement(m, mr, mt, mc);
            return resultStrings[0] + " " +  resultStrings[1];
        }
        catch (Exception ex) {
            String errorString = "MB0103 Error while generating Join SQL Statement: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, "");
            //setModelStatus(m, currentUserId, errorString);
            return errorString;                
        }
    } 
    
    public String deleteFlatTable(String modelJSON, Integer currentUserId) {        
        Model m = null;      
        try {
            m = parseModelFromJSON(modelJSON);
            String tableName = m.getOutputName() + flatTableAppendString;
            AuditLogger.log(currentUserId, 0, "drop table", tableName);
            DBHelper.executeSqlStatement("DeltaData","DROP TABLE " + tableName,logger);
            setModelStatus(m, currentUserId, "Flat Table Deleted", true, false, false, false);
            return "Table " + tableName + " deleted.";
        }  catch (PersistenceException ex) {
                String errorString = "MB0004 SQL error while deleting Flat Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;            
            }
        catch (Exception ex) {
            String errorString = "MB0005 Error while deleting Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                               
        }
    } 

    public String deleteStagingTable(String modelJSON, Integer currentUserId) {        
        Model m = null;      
        try {
            m = parseModelFromJSON(modelJSON);
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());
            DBHelper.executeSqlStatement("DeltaData","DROP TABLE " + m.getOutputName(),logger);
            setModelStatus(m, currentUserId, "Staging Table Deleted", false, false, false, false);
            return "Table " + m.getOutputName() + " deleted.";
        }  catch (PersistenceException ex) {
                String errorString = "MB0004 SQL error while deleting Staging Table: " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;            
            }
        catch (Exception ex) {
            String errorString = "MB0005 Error while deleting Staging Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                               
        }
    } 

    public String processMissingFields_old(String modelJSON, String timeStamp, Integer currentUserId) {           
        String resultMessage;
        Model m = null;
        try {
            m = parseModelFromJSON(modelJSON);
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }      
            // followig if plays a role of a semaphore
            m = modelController.findModel(m.getIdModel());
            if ( m.getIsAtomicFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0065 Processing of Source fields not finished. Cannot proceed");
                return "MB0065 Processing of source fields not finished. Cannot proceed";
            }       
            if ( m.getIsVirtualFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0069 Processing of primary virtual fields not finished. Cannot proceed");
                return "MB0069 Processing of primary virtual fields not finished. Cannot proceed";
            }                
            if ( checkIfTableIsEmpty(m.getOutputName() + flatTableAppendString) == true ) {
                setModelStatus(m, currentUserId, "MB0009 Target table is empty. Cannot process");
                return "MB0009 Target table is empty. Cannot process";
            }         
            m.setProcessingId(timeStamp); // save thread timestamp/identifier used to cancel the thread            
            setModelStatus(m, currentUserId, "Processing of missing fields started", true, true, false, false);
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            ModelColumn primaryKey = getPrimaryKeyField(mt, mc);            
            for (ModelColumn modelColumn : mc) {       
                if ( modelColumn.getSub() == true ) {
                    resultMessage = missingDataProcessor(currentUserId, modelColumn, m.getOutputName() + flatTableAppendString, primaryKey);
                    if ( !"Ok".equals(resultMessage) ) {
                        setModelStatus(m, currentUserId, resultMessage);
                        return resultMessage;
                    }
                }
            }  
            setModelStatus(m, currentUserId, "Processing of missing fields finished", true, true, true, false);
            return "Processing of missing fields finished";
        } catch (PersistenceException ex) {
            String errorString = "MB0010 SQL Error while processing missing fields: " + ex.getCause().getCause().toString();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
            setModelStatus(m, currentUserId, errorString);
            return errorString;            
            }
        catch (Exception ex) {
            String errorString = "MB0011 Error while processing missing fields: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                           
        }
    } 

    public String processMissingFields(String modelJSON, String timestamp, Integer currentUserId) {    
        Model m = parseModelFromJSON(modelJSON);
        m = parseModelFromJSON(modelJSON);
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }      
        m = modelController.findModel(m.getIdModel());        
        return processMissingFields(m, timestamp, currentUserId);
    }
    
    public String processMissingFields(Model m, String timeStamp, Integer currentUserId) {        
        String errorString = "";
        //Model m = null;
        try {
            if ( m.getIsAtomicFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0065 Processing of source fields not finished. Cannot proceed");
                return "MB0065 Processing of source fields not finished. Cannot proceed";
            }       
            if ( m.getIsVirtualFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0069 Processing of primary virtual fields not finished. Cannot proceed");
                return "MB0069 Processing of primary virtual fields not finished. Cannot proceed";
            }                
            if ( checkIfTableIsEmpty(m.getOutputName() + flatTableAppendString) == true ) {
                setModelStatus(m, currentUserId, "MB0009 Target table is empty. Cannot process");
                return "MB0009 Target table is empty. Cannot process";
            }         
            m.setProcessingId(timeStamp); // save thread timestamp/identifier used to cancel the thread            
            setModelStatus(m, currentUserId, "Processing of missing fields started", true, true, false, false);
            String tableName = m.getOutputName() + flatTableAppendString;
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> modelColumn = getModelColumns(mt);
            ModelColumn primaryKey = getPrimaryKeyField(mt, modelColumn);          
            // rename original column to _raw column
            try {
                Query queryResult;
                String sqlStmt = "ALTER TABLE " + tableName;
                int i = 0;
                for (ModelColumn mc : modelColumn) {       
                    if ( mc.getSub() == true ) {    
                        if ( i > 0 ) {
                            sqlStmt += ",";
                        }                        
                        //String originalType = getOriginalFieldType(mc, tableName);
                        String originalType = mc.getType();
                        sqlStmt += " CHANGE " + mc.getName() + " " + mc.getName() + "_raw " + originalType;
                        i++;
                    }
                }
                if ( i > 0 ) { // skip if no "missing field" flags found
                    AuditLogger.log(currentUserId, 0, "rename columns", sqlStmt);             
                    DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);
                    setModelStatus(m, currentUserId, "Creating _raw columns done. Processing of missing fields.", true, true, false, false);                   
                    
                    sqlStmt = "ALTER TABLE " + tableName;                    
                    i = 0;
                    for (ModelColumn mc : modelColumn) {       
                        if ( mc.getSub() == true ) {    
                            if ( i > 0 ) {
                                sqlStmt += ",";
                            }                        
                            //String originalType = getRawFieldType(mc, tableName);
                            String originalType = mc.getType();
                            sqlStmt += " ADD " + mc.getName() + " " + originalType;
                            i++;
                        }
                    }       
                    AuditLogger.log(currentUserId, 0, "re-create columns", sqlStmt);             
                    DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);
                    setModelStatus(m, currentUserId, "Creating empty columns done. Processing of missing fields.", true, true, false, false); 
                    ArrayList<String> missColumns = new ArrayList<>();
                    ArrayList<String> missValues = new ArrayList<>();                    
                    
                    
                    System.out.println("This is primaryKey.getPhysicalName()"+primaryKey.getPhysicalName());
                    String pkvirtual = primaryKey.getPhysicalName();
                    
                    if(pkvirtual.equals("virtual")){
                        
                        pkvirtual = primaryKey.getName();
                        System.out.println("This is primaryKey name apply"+pkvirtual);
                    }
                    else{
                    
                       pkvirtual = primaryKey.getPhysicalName();
                       
                    }
                    
                    sqlStmt = "SELECT " + pkvirtual;     
                    //sqlStmt = "SELECT " + primaryKey.getPhysicalName();       
                    // sqlStmt = "SELECT " + primaryKey.getPhysicalName();                    
                   
                   i = 0;
                    for (ModelColumn mc : modelColumn) {       
                        if ( mc.getSub() == true ) {    
                            sqlStmt += ",";                      
                            sqlStmt += " " + mc.getName() + "_raw";
                            missColumns.add(i,mc.getName());
                            missValues.add(i,mc.getFormula());
                            i++;
                        }
                    }  
                    sqlStmt += " FROM " + tableName;
                    setModelStatus(m, currentUserId, "Substituting missing values.", true, true, false, false); 
                    try {
                        queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger);
                        List<Object[]> results = queryResult.getResultList();       
                        setModelStatus(m, currentUserId, "Working on missing fields: " + sqlStmt, true, true, false, false);                         
                    
                        


                        // start big update loop
                        long counter = 0;
                        for (Object[] obj : results) { 
                            boolean dropFlag = false;                            
                            counter++;
                            if ( counter%INSERT_COUNTER_INCREMENT == 0 ) {
                                setModelStatus(m, currentUserId, "Missing fields processed records: " + Long.toString(counter));
                            }                            
                            // prepare update statement
                            String sqlStmtUpdate = "UPDATE " + tableName + " SET ";                            
                            for (int j =0; j< i; j++) {
                                if ( j > 0 ) {
                                    sqlStmtUpdate += ",";
                                }
                                // check if column had data orgiinally and use, otherwise substitute
                                String fieldData = getInsertValuesSingleRow(obj[j+1]);  
                                fieldData = Util.removeLastChar(fieldData);
                                if ( "NULL".equals(fieldData.toUpperCase()) ) {
                                    //----------*************
                                    if ( "drop_null".equals(missValues.get(j).toLowerCase())) {
                                        dropFlag = true;
                                    } else {
                                        fieldData = missValues.get(j);
                                    }
                                }                            
                                sqlStmtUpdate += missColumns.get(j) + "=" + fieldData;
                            }
                            if ( dropFlag == true ) {
                                sqlStmtUpdate += ",drop_record=1";
                            } else {
                                sqlStmtUpdate += ",drop_record=0"; 
                            }
                            try {
                                String sqlStmtWhere = " WHERE " + primaryKey.getName() + "=" + getInsertValuesSingleRow(obj[0]);
                                sqlStmtWhere = Util.removeLastChar(sqlStmtWhere);                      
                                DBHelper.executeSqlStatement("DeltaData",sqlStmtUpdate + sqlStmtWhere,logger);
                            } catch (PersistenceException ex) {
                                errorString = "MB0056 SQL Error while processing missing fields: " + ex.getCause().getCause().toString();
                                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                                setModelStatus(m, currentUserId, errorString);                            
                                return errorString + "\n" + ex.getCause().getCause().toString();
                            } catch (Exception ex) {
                                errorString = "MB0156 SQL Error while processing missing fields: " + ex.getCause().getCause().toString();
                                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                                setModelStatus(m, currentUserId, errorString);              
                                return errorString + "\n" + ex.getCause().getCause().toString();
                            }
                        }                          
                    } catch (PersistenceException ex) {
                        errorString = "MB0054 SQL Error while processing missing fields:" + ex.getCause().toString() + " " + ex.getCause().getCause().toString();
                        Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);      
                        setModelStatus(m, currentUserId, errorString);  
                        return errorString;
                    } catch (Exception ex) {
                        errorString = "MB0154 SQL Error while processing missing fields:" + ex.getCause().toString() + " " + ex.getCause().getCause().toString();                        
                        Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
                        setModelStatus(m, currentUserId, errorString);  
                        return errorString;
                    }                                             
                }
            } catch (Exception ex) {
                errorString = "MB0057 SQL Error while processing missing fields: " + ex.getCause().toString() + " " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);            
                setModelStatus(m, currentUserId, errorString);                
                return errorString;
            }              
            setModelStatus(m, currentUserId, "Processing of missing fields finished", true, true, true, false);
            return "Processing of missing fields finished";
        } catch (PersistenceException ex) {
            errorString = "MB0010 SQL Error while processing missing fields: " + ex.getCause().getCause().toString();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
            setModelStatus(m, currentUserId, errorString);
            return errorString;            
            }
        catch (Exception ex) {
            errorString = "MB0011 Error while processing missing fields: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                           
        }
    } 

    public String processPrimaryVirtualFields(String modelJSON, String timeStamp, Integer currentUserId) {    
        Model m = parseModelFromJSON(modelJSON);        
        return processPrimaryVirtualFields(m, timeStamp, currentUserId);
    }
    
    public String processPrimaryVirtualFields(Model m, String timeStamp, Integer currentUserId) {          

        try {
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }                
            m = modelController.findModel(m.getIdModel());
            // followig if plays a role of a semaphore
            if ( m.getIsAtomicFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0066 Atomic fields not processed. Cannot proceed");
                return "MB0066 Atomic fields not processed. Cannot proceed";
            }
            if ( checkIfTableIsEmpty(m.getOutputName()) == true ) {
                setModelStatus(m, currentUserId, "MB0006 Source table is empty. Cannot process");
                return "MB0006 Target table is empty. Cannot process";
            }
            m.setProcessingId(timeStamp); // save thread timestamp/identifier used to cancel the thread            
            setModelStatus(m, currentUserId, "Processing of primary virtual fields started", true, false, false, false);
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc_atomic = getModelColumns(mt);  
            List<ModelColumn> mc_virt = getModelColumns(mt);
            ModelColumn primaryKey = getPrimaryKeyField(mt, mc_atomic);  
            if ( primaryKey == null ) {
                String errorString = "MB0007 Error while processing primary virtual fields. Primary key is not defined.";
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString + "Model: ", m.getName());   
                setModelStatus(m, currentUserId, errorString);
                return "MB0007 Error while processing primary virtual fields. Primary key is not defined.";
            }
            mc_atomic = filterInsertableAtomicModelColumns(mc_atomic);
            mc_virt = filterPrimaryVirtualModelColumns(mc_virt);
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName() + flatTableAppendString);             
            DBHelper.executeSqlStatement("DeltaData","DROP TABLE IF EXISTS " + m.getOutputName() + flatTableAppendString,logger);            
            String sqlStmt = "CREATE TABLE " + m.getOutputName() + flatTableAppendString;
            sqlStmt += " (PRIMARY KEY (" + primaryKey.getName() +")) ENGINE = MYISAM DEFAULT CHARSET=utf8 SELECT ";
            sqlStmt += "drop_record,";            
            if ( !mc_atomic.isEmpty() ) {         
                for (ModelColumn modelColumn : mc_atomic) {       
                    sqlStmt += modelColumn.getName() + ",";
                }
                for (ModelColumn modelColumn : mc_virt) {       
                    sqlStmt += modelColumn.getFormula() + " AS " + modelColumn.getName() + ",";
                }                
                sqlStmt = Util.removeLastChar(sqlStmt);
//                sqlStmt += " FROM " + m.getOutputName() + " GROUP BY " 
//                        + primaryKey.getName() + " ORDER BY " + primaryKey.getName();
                sqlStmt += " FROM " + m.getOutputName() + " ORDER BY " + primaryKey.getName();                
                DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);                  
            }
            updateVirtualFieldTypes(mc_virt, m.getOutputName());
            setModelStatus(m, currentUserId, "Processing of primary virtual fields finished", true, true, false, false);
            return "Ok";
        } catch (PersistenceException ex) {
            String errorString = "MB0006 Error while processing primary virtual fields: " + ex.getCause().getCause().toString();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
            setModelStatus(m, currentUserId, errorString);
            return errorString;                                          
        } catch (Exception ex) {
            String errorString = "MB0008 Error while processing primary virtual fields: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                          
        }
    } 


    public String processSecondaryVirtualFields(String modelJSON, String timeStamp, Integer currentUserId) {    
        Model m = parseModelFromJSON(modelJSON);        
        return processSecondaryVirtualFields(m, timeStamp, currentUserId);
    }
    
    public String processSecondaryVirtualFields(Model m, String timeStamp, Integer currentUserId) {          
        String resultMessage;
        try {
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }                
            m = modelController.findModel(m.getIdModel());
            // followig if plays a role of a semaphore
            if ( m.getIsAtomicFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0066 Atomic fields not processed. Cannot proceed");
                return "MB0066 Atomic fields not processed. Cannot proceed";
            }
            if ( m.getIsVirtualFinished() == false ) {
                setModelStatus(m, currentUserId, "MB0067 Primary virtual fields not processed. Cannot proceed");
                return "MB0067 Primary virtual fields not processed. Cannot proceed";
            }   
            if ( m.getIsSecondaryVirtualFinished() == true ) {
                setModelStatus(m, currentUserId, "MB0068 Secondary virtual fields processed. Please generate primary virtual fields first");
                return "MB0068 Secondary virtual fields processed. Please generate primary virtual fields first";
            }            
            if ( checkIfTableIsEmpty(m.getOutputName() + flatTableAppendString) == true ) {
                setModelStatus(m, currentUserId, "MB0006 Target table is empty. Cannot process");
                return "MB0006 Target table is empty. Cannot process";
            }
            setModelStatus(m, currentUserId, "Processing of secondary virtual fields started", true, true, false);
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);  
            ModelColumn primaryKey = getPrimaryKeyField(mt, mc);            
            mc = filterSecondaryVirtualModelColumns(mc);
            if ( !mc.isEmpty() ) {         
                for (ModelColumn modelColumn : mc) {       
                    // add columns one at a time to allow virtual fields to refer to other virtual fields
                    String type = getFormulaFieldTypeMySQL(modelColumn,m.getOutputName() + flatTableAppendString);
                    String sqlStmt = "ALTER TABLE " + m.getOutputName() + flatTableAppendString + " ADD " + modelColumn.getName() + " " + type + " NULL";
                    AuditLogger.log(currentUserId, 0, "add column", sqlStmt);             
                    DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);  
                }
                for (ModelColumn modelColumn : mc) {       
                    resultMessage = secondaryVirtualFieldProcessor(currentUserId, modelColumn, m.getOutputName() + flatTableAppendString, primaryKey);
                    if ( !"Ok".equals(resultMessage) ) {
                        setModelStatus(m, currentUserId, resultMessage);
                        return resultMessage;
                    }        
                    if ( Thread.currentThread().isInterrupted() == true) {
                        logger.log(Level.SEVERE, "MB0055 Secondary virtual field generation interrupted");
                        return "MB0055 Secondary virtual field generation interrupted";
                    }                        
                }
            }
            updateVirtualFieldTypes(mc, m.getOutputName() + flatTableAppendString);
            setModelStatus(m, currentUserId, "Done with secondary virtual fields. Processing finished", true, true, true, true);
            return "Done with secondary virtual fields. Processing finished";
        } catch (PersistenceException ex) {
            String errorString = "MB0006 Error while processing secondary virtual fields: " + ex.getCause().getCause().toString();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
            setModelStatus(m, currentUserId, errorString);
            return errorString;                 
        } catch (Exception ex) {
            String errorString = "MB0008 Error while processing secondary virtual fields: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                          
        }
    } 

    public String processFlatTableAll(String modelJSON, String timestamp, Integer currentUserId, boolean processMissing) {    
        //System.out.println("This is at process fltatable all");
        //System.out.println("Parameter modelJSON : "+modelJSON);
        //System.out.println("Parameter timestamp : "+timestamp);
        //System.out.println("Parameter currentUserId : "+currentUserId);
        //System.out.println("processMissing : "+processMissing);
        
        Model m = parseModelFromJSON(modelJSON);
        //System.out.println("This is model m from parsing parsing : "+m.toString());
        return processFlatTableAll(m, timestamp, currentUserId, processMissing);
    }
    
    public String processFlatTableAll(Model m, String timeStamp, Integer currentUserId, boolean processMissing) {     
        String resultString = "Flat Table processing failed";
        try {
            //System.out.println("This is at seconf ProcessFlatTableAll : "+m);
            //m = parseModelFromJSON(modelJSON);
            m.setProcessingId(timeStamp);
            setModelStatus(m, currentUserId, "Processing of source fields started", false, false, false, false);
            List<ModelRelationship> mr = getModelRelationships(m.getIdModel());
            List<ModelTable> mt = getModelTables(m.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            
            String sqlCreate = constructCreateStatement(m.getOutputName(), mc, mt, mr);  
            AuditLogger.log(currentUserId, 0, "drop table", m.getOutputName());             
            DBHelper.executeSqlStatement("DeltaData","DROP TABLE IF EXISTS " + m.getOutputName(),logger);
            
             
            AuditLogger.log(currentUserId, 0, "CREATE TABLE", sqlCreate);             
            //System.out.println("This is before DBHelper.executeSqlStatement sqlCreate: "+ m.getOutputName());
            DBHelper.executeSqlStatement("DeltaData",sqlCreate,logger);   
            setModelStatus(m, currentUserId, "Populating Flat Table with data");
            if ( "Ok".equals(resultString = populateStagingTableJDBC(m, mr, mt, mc, currentUserId)) ) {
                //System.out.println("This is at ok resultString for populateStagingTableJDBC before virtual fields started\n"+resultString);
                setModelStatus(m, currentUserId, "Processing of virtual fields started", true, false, false, false);   
                
                if ( Thread.currentThread().isInterrupted() == true) {
                    String errorString = "MB0053 Flat Table insertion interrupted";
                    logger.log(Level.SEVERE, errorString);
                    setModelStatus(m, currentUserId, errorString);
                    return errorString;
                }                
                
                if ( !"Ok".equals(resultString = processPrimaryVirtualFields(m, timeStamp, currentUserId)) ) {
                    //System.out.println("This is at ok resultString for processPrimaryVirtualFields virtual fields started\n"+resultString);
                    return resultString;
                }
                
                setModelStatus(m, currentUserId, "Processing of virtual fields finished", true, true, false, false);  
                //ModelColumn primaryKey = getPrimaryKeyField(mt, mc);                   
                //System.out.println("Processing of virtual fields finished");
                
                if ( processMissing == true ) {
                    //System.out.println("Processing of missing fields started if true");
                    setModelStatus(m, currentUserId, "Processing of missing fields started");   
                    processMissingFields(m, timeStamp, currentUserId);  
                    setModelStatus(m, currentUserId, "Processing of missing fields finished", true, true, true, false);                                            
                } 
                
                //System.out.println("Processing of processSecondaryVirtualFields");
                processSecondaryVirtualFields(m, timeStamp, currentUserId);    
                
                
                if ( statService == null ) {
                    statService = new StatServiceImpl();
                }    
                
                
                setModelStatus(m, currentUserId, "Processing of Staging Table Descriptive Statistics started");
                //System.out.println("Processing of Staging Table Descriptive Statistics started");
                String statString = statService.getAllDS(m, m.getOutputName(), false);  
                //System.out.println("statService.getAllDS result and saving saveDescStats \n"+statString);
                statService.saveDescStats(statString, currentUserId, m, false);                
                
                setModelStatus(m, currentUserId, "Processing of Flat Table Descriptive Statistics started");                      
                //System.out.println("Processing of Flat Table Descriptive Statistics started \n");
                
                statString = statService.getAllDS(m, m.getOutputName() + flatTableAppendString, true); 
                //System.out.println("statService.getAllDS flatTableAppendString \n"+flatTableAppendString);
                 
                statService.saveDescStats(statString, currentUserId, m, true);   
                setModelStatus(m, currentUserId, "Processing of Descriptive Statistics finished", true, true, true, true);                
            
            } else {
                setModelStatus(m, currentUserId, resultString);
                return resultString;
            }
            
        } catch (PersistenceException ex) {
                String errorString = "MB0002 Field parameters not valid. " + ex.getCause().getCause().toString();
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);    
                setModelStatus(m, currentUserId, errorString);
                return errorString;
            }
        catch (Exception ex) {
            String errorString = "MB0003 Error while generating Flat Table: " + ex.getMessage();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, errorString, ex);
            setModelStatus(m, currentUserId, errorString);
            return errorString;                
        }
        return resultString;
    } 
 
    public String verifyStatement(String tableName, String formula, String secondary, Integer currentUserId) {      
        String sqlTestStmt = null;
        if ( "true".equals(secondary) ) {
            tableName += flatTableAppendString;
        }
        try {
            sqlTestStmt = "SELECT " + formula + " FROM " + tableName + " LIMIT 1";
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
            AuditLogger.log(currentUserId, 0, "verify statement", sqlTestStmt);            
            Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);
            List<Object[]> results = queryResult.getResultList();
            //return getDBObjectType(results.get(0));
            if ( results.size() > 0 ) {
                return "ok";
            } else {
                return "MB0012 Invalid formula.";
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "MB0113 SQL exception while verifying formula ", ex);
            return "MB0113 SQL exception while verifying formula: " + DBHelper.printSQLException(ex);  
        } catch (IllegalArgumentException ex) {
            logger.log(Level.SEVERE, "MB0114 SQL exception while verifying formula ", ex);
            return "MB0113 Illegal argument while verifying formula: " + ex.getCause();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
            return "MB0013 could not execute query against table " + tableName;            
        }    
    } 
    
    public String getDBObject(Integer currentUserId, int start, int limit, String orderBy, String direction, int filterId, int modelId, String append) {
        try {
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }           
            Model model = modelController.findModel(modelId);          
            String filterString = "";
            if ( filterId != 0 ) {
                if ( studyService == null ) {
                    studyService = new StudyServiceImpl();
                }                   
               filterString = studyService.getSqlFromFilterFormulas(filterId);
            }
            String dataString = DBHelper.getDBObjectData(currentUserId, start, limit, orderBy, direction, model.getOutputName() + append, filterString); 
            return dataString;
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String getModelMetadata(String load, boolean localFlag) {     
        //String result = "MB0014 Metadata is missing.";
        
        //updated below msg v3.67 https://ceri-lahey.atlassian.net/browse/DELQA-889
        String result = "MB0014 : Please click on \"Refresh\" button to load data. Data tables will be populate upon successful database connection. If this message persists, please contact Administrator to verify database connection properties and credentials.";
        
        
        int retryCount = 0;
        int retryMax = 2;
        String dataString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();   
        JSONArray metadata;
        Model model = null;
        
        while ( retryCount < retryMax ) { // JDBC connection used here can stale afer a while, retry once
            try {
                JSONObject JSONContent =  new JSONObject(load);
                JSONArray modelArray = JSONContent.getJSONArray("models");
                for (int i=0; i<modelArray.length(); i++) {
                    logger.log(Level.INFO, "Editing Model {0}", modelArray.getJSONObject(i).getString("name"));
                    Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                    String jString = modelArray.getJSONObject(i).toString();
                    model = gson.fromJson(jString, Model.class);  
                    if ( configurationController == null ) {
                        configurationController = new ConfigurationJpaController();
                    }
                    if ( localFlag == true ) {
                        dataString = getAllCachedMetadata(model);
                    } else {
                        Configuration config = configurationController.findConfiguration(model.getIdConfiguration());                        
                        dataString = DBHelper.getMetadata(config.getType(), config.getConnectionInfo(), config.getDbUser(), config.getDbPassword());                       
                        saveAllCachedMetadata(model, dataString);
                    }
                    break; // process just first record, for now
                    }
                retryCount = retryMax;
            } catch (JSONException ex) {
                retryCount = retryMax;
                logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex});
                result = "MB0015 Exception while parsing JSON model description.";
            } catch (EntityNotFoundException ex) {
                retryCount = retryMax;
                logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
                result = "MB0016 Exception while retrieving table metadata. Model not found.";
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Exception while retrieving Model: {0} {1}", new Object[]{model.getName(), ex});
                retryCount++;
                if ( retryCount < retryMax ) {
                   logger.log(Level.SEVERE, "Retrying to retrieve Model {0} retryCount {1}", new Object[]{model.getName(), Integer.toString(retryCount)});
                } else {
                    result = "MB0017 Database connection failed while retrieving table metadata. " + ex.getMessage();
                }
            }  
        }
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Metadata retrieved");   
                //success.put("totalCount", Integer.valueOf(recordCounter));
                metadata = new JSONArray(dataString);
                success.put("totalCount", Integer.valueOf(metadata.length()));
                success.put("children", metadata);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata], problematic JSON: ", dataString);  
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0018 error while processing JSON in WS method [model.getMetadata]: " + dataString;
        }          
       return result;
    } 

    public String getModelDetails(String load) {     
        String result = "MB0019 Model has no details.";
        String fieldString = "";
        String tableString = "";
        String relationshipString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONArray modelArray = null;        
        List<ModelTable> mt = null;
        List<ModelColumn> mc = null;
        List<ModelRelationship> mr = null;
        JSONArray fieldMetadataJSON;
        JSONArray tableJSON;        
        JSONArray relationshipJSON;
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                //logger.log(Level.INFO, "Retrieving details for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                mr = getModelRelationships(model.getIdModel());
                for (int j=0; j<mr.size(); j++) {
                    if ( j != 0 ) {
                        relationshipString += ",";
                    }                    
                    relationshipString = relationshipString + new JSONObject(mr.get(j)).toString();  
                }                    
                mt = getModelTables(model.getIdModel());
                for (int j=0; j<mt.size(); j++) {
                    if ( j != 0 ) {
                        tableString += ",";
                    }                    
                    tableString = tableString + new JSONObject(mt.get(j)).toString();  
                }                
                mc = getModelColumns(mt);
                for (int j=0; j<mc.size(); j++) {
                    if ( j != 0 ) {
                        fieldString += ",";
                    }                    
                    fieldString = fieldString + new JSONObject(mc.get(j)).toString();  
                }
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex});
            result = "MB0020 Exception while parsing JSON model description.";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0021 Exception while retrieving model metadata. Model not found.\n" + ex.getMessage();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB022 Exception while retrieving model metadata.\n" + ex.getMessage();
        }  
        try {
            if ( "".equals(fieldString) && "".equals(tableString) && "".equals(relationshipString) ) {     
                if ( result.contains("Model has no details.") ) {
                    success.put("success", true);   
                } else {
                    success.put("success", false);                       
                }
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Model details retrieved");   
                success.put("model", modelArray.get(0));                   
                fieldMetadataJSON = new JSONArray("["+fieldString+"]");
                success.put("totalCount", Integer.valueOf(mc.size()));
                success.put("modelColumns", fieldMetadataJSON);                
                tableJSON = new JSONArray("["+tableString+"]");
                success.put("modelTables", tableJSON);    
                relationshipJSON = new JSONArray("["+relationshipString+"]");
                success.put("modelRelationships", relationshipJSON);                   
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0023 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    }

    /* evoked by Web Service */
    public String getModelColumns(String load) {     
        String result = "MB0024 Model has no columns.";
        String fieldString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONArray modelArray = null;        
        List<ModelTable> mt = null;
        List<ModelColumn> mc = null;
        List<ModelRelationship> mr = null;
        JSONArray fieldMetadataJSON;
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                //logger.log(Level.INFO, "Retrieving details for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class);                
                mt = getModelTables(model.getIdModel());               
                mc = getModelColumns(mt);
                for (int j=0; j<mc.size(); j++) {
                    if ( j != 0 ) {
                        fieldString += ",";
                    }        
                    fieldString = fieldString + new JSONObject(mc.get(j)).toString();  
                }
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex});
            result = "MB0025 Exception while parsing JSON model description.";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0026 Exception while retrieving model metadata. Model not found.";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0027 Exception while retrieving model metadata.";
        }  
        try {
            if ( "".equals(fieldString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Model columns retrieved");   
                //success.put("model", modelArray.get(0));                   
                fieldMetadataJSON = new JSONArray("["+fieldString+"]");
                success.put("totalCount", Integer.valueOf(mc.size()));
                success.put("modelColumns", fieldMetadataJSON);                                
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0028 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    }
    
    /* evoked by Web Service */
    public String getOrgModelColumns(String load, Integer currentOrgId) {     
        String result = "MB0029 Organization has no models with columns.";
        String fieldString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();       
        List<Object> modelArray = null;
        List<ModelTable> mt = null;
        List<ModelColumn> mc = null;
        JSONArray fieldMetadataJSON;
        int fieldCounter = 0;
        Model model = null;
        
        try {
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }              
            modelArray = modelController.findOrgModelEntities(currentOrgId);
            for (int i=0; i<modelArray.size(); i++) {
                model = (Model)modelArray.get(i);                
                mt = getModelTables(model.getIdModel());               
                mc = getModelColumns(mt);
                if ( (!"".equals(fieldString)) && ( mc.size() > 0) ) {
                    fieldString += ",";
                }                
                fieldCounter += mc.size();
                for (int j=0; j<mc.size(); j++) {
                    // inject model id as it is helpful in UI
                    ModelColumn mcol = mc.get(j);
                    mcol.setIdModel(model.getIdModel());
                    String chunk = new JSONObject(mcol).toString();
                    if ( !chunk.equals("") ) {
                        if ( j != 0 ) {
                            fieldString += ",";
                        }        
                        fieldString = fieldString + chunk;  
                        }
                    else {
                        logger.log(Level.SEVERE, "ModelColumn skipped: JSON object is null");
                    }
                    }
                }
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0030 Exception while retrieving model metadata. Model not found.";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
            result = "MB0031 Exception while retrieving model metadata.";
        }  
        try {
            if ( "".equals(fieldString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", "Model columns retrieved");   
                //success.put("model", modelArray.get(0));                   
                fieldMetadataJSON = new JSONArray("["+fieldString+"]");
                success.put("totalCount", Integer.valueOf(fieldCounter));
                success.put("modelColumns", fieldMetadataJSON);                                
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [model.getMetadata]: ", ex);  
            result = "MB0032 JSON error in WS method [model.getMetadata]";
        }          
       return result;
    }    

    public boolean cleanupAfterModelTableDelete(String JSONString, Integer userId) {
        Integer [] tableIdArray = null;
        
        try {       
            tableIdArray = getTableIdFromTableJSON(JSONString);
            if ( modelColumnController == null ) {
               modelColumnController = new ModelColumnJpaController();
            }     
            for (int i=0; i<tableIdArray.length; i++) {            
                List<ModelColumn> modelColumns = new ArrayList();  
                modelColumns = modelColumnController.findModelColumnByTable(tableIdArray[i]);
                for (ModelColumn mc : modelColumns) {
                    modelColumnController.deleteObject(mc, userId);
                }   
            }
            if ( modelRelationshipController == null ) {
               modelRelationshipController = new ModelRelationshipJpaController();
            }        
            for (int i=0; i<tableIdArray.length; i++) {            
                List<ModelRelationship> modelRelationships = new ArrayList();  
                modelRelationships = modelRelationshipController.findModelRelationshipByTable(tableIdArray[i]);
                for (ModelRelationship mr : modelRelationships) {
                    modelRelationshipController.deleteObject(mr, userId);
                }              
            }
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while cleaning after ModelTable delete {0}", ex);         
            return false;
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Database exception while cleaning after ModelTable delete {0}", ex);
            return false;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while cleaning after ModelTable delete {0}", ex);
            return false;
        }   
        return true;
    }

    public String deleteModel(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        
        String resultString = "Model successfully deleted";       
        Integer oldModelId;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());  
            if ( !Objects.equals(((Model)subject).getOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "MB0047 Import failed not authorized.");
                return "MB0047 Delete failed. Not authorized."; 
            }                 
            if ( ((Model)subject).getLocked() == true ) {
                return wrapAPI(controller.getObjectType(), "", "Object not deleted", "Model is locked");     
            }
            oldModelId = ((Model)subject).getIdModel();
            if ( studyService == null ) {
                studyService = new StudyServiceImpl();
            } 
//            resultString = studyService.findStudiesByModel((Model)subject);
//            if (  !"".equals(resultString) ) {
//                // there are Studies this Model supports - return immediately
//                return wrapAPI(controller.getObjectType(), "", "Studies found. Object not deleted", resultString);                 
//            } else {
//                resultString = "No Studies found. Model deleted.";
//            }
            List<ModelRelationship> modelRelationships = getModelRelationships(oldModelId);                         
            List<ModelTable> modelTables = getModelTables(oldModelId);
            List<ModelColumn> modelColumns = getModelColumns(modelTables);                         
            for (ModelRelationship mr : modelRelationships) {             
                modelRelationshipController.deleteObject(mr, currentUserId);  
            }   
            for (ModelColumn mc : modelColumns) {                          
                modelColumnController.deleteObject(mc, currentUserId);                           
            }            
            for (ModelTable mt : modelTables) {
                modelTableController.deleteObject(mt, currentUserId);
            }             
            controller.deleteObject(subject, currentOrgId, currentUserId);
            resultString = studyService.deleteStudiesByModel((Model)subject);            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            resultString = "MB0033 Exception while deleting model: "  + ex.getMessage();
            JSONString = "";
        }  
        return wrapAPI(controller.getObjectType(), JSONString, "Object deleted", resultString);        
    }

    
    public String lockUnlockModel(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject, HttpServletRequest request) {
        String resultString = "Model successfully locked/unlocked";       
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            if ( !Objects.equals(((Model)subject).getOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "MB0079 Lock/Unlock failed not authorized.");
                return "MB0079 Lock/Unlock failed. Not authorized."; 
            }                  
            if ( !Auth.isPermitted("LockModel", request) ) {
                return "MB0091 Cannot perform the action: no sufficient permissions.";
            }
            if ( !Auth.canAccessObject(((Model)subject).getCreatedBy(), ((Model)subject).getOrganization(), request) ) {    
                return "MB0092 Cannot perfom the action: request from user who is not an owner.";
            } else {
                if ( ((Model)subject).getLocked() == true ) {
                    ((Model)subject).setLocked(false);
                    resultString = "Model unlocked";
                } else {
                    ((Model)subject).setLocked(true);
                    resultString = "Model locked";
                }
            }
            subject = controller.updateObjects(subject, currentOrgId, currentUserId);
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON Exception while locking/unlocking Object: {0} {1}", new Object[]{JSONString, ex});    
            return "MB0095 JSON Parsing exception while locking/unlocking " + controller.getObjectType();            
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Database exception while locking/unlocking Object: {0} {1}", new Object[]{JSONString, ex});
            return "MB0096 Database exception while locking/unlocking" + controller.getObjectType();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while locking/unlocking creating new Object: {0} {1}", new Object[]{JSONString, ex});
            return "MB0097 Exception while locking/unlocking "  + controller.getObjectType();
        } 
        return resultString;
    }
    
    public String cloneModel(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
        
        String resultString = "Model successfully cloned Id :";       
        Integer oldModelId;
        Integer newModelId;
        System.out.println("This is clonning" +JSONString);
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            
            
            if ( !Objects.equals(((Model)subject).getOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "MB0080 Cloning failed. Not authorized.");
                return "MB0080 Cloning failed. Not authorized."; 
            }                  
            
            
            
            oldModelId = ((Model)subject).getIdModel();
            ((Model)subject).setIdModel(0);
            ((Model)subject).setDescription("");
            ((Model)subject).setStatus("New");
            ((Model)subject).setIsAtomicFinished(Boolean.FALSE);
            ((Model)subject).setIsMissingFinished(Boolean.FALSE);
            ((Model)subject).setIsSecondaryVirtualFinished(Boolean.FALSE);
            ((Model)subject).setIsVirtualFinished(Boolean.FALSE);
            /*v3.64 Added Original model ID DELQA-557*/
            ((Model)subject).setOriginalModelId(oldModelId);
            subject = controller.createObjects(subject, currentOrgId, currentUserId);
            
           
        
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON Exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});    
            return "MB0034 JSON Parsing exception while creating new " + controller.getObjectType();            
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Database exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            return "MB0035 Database exception while creating new " + controller.getObjectType();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0} {1}", new Object[]{controller.getObjectType(), ex});
            return "MB0036 Exception while creating new "  + controller.getObjectType();
        }
        
        try {
            Model m = (Model)subject;
            
            newModelId = m.getIdEntity();
                    
            System.out.println("This is model"+m.getIdModel());
            System.out.println("This is model group"+m.getIdGroup());            
            System.out.println("This is model entity"+m.getIdEntity());
            
            
            List<ModelRelationship> modelRelationships = getModelRelationships(oldModelId);                         
            List<ModelTable> modelTables = getModelTables(oldModelId);
            List<ModelColumn> modelColumns = getModelColumns(modelTables);                      
            for (ModelTable mt : modelTables) {
                Integer oldTableId = mt.getIdModelTable();   
                if ( oldModelId.compareTo(mt.getModelidModel()) == 0 ) {
                    mt.setIdModelTable(0);
                    mt.setModelidModel(m.getIdModel());
                    mt = (ModelTable)modelTableController.createObjects(mt, currentUserId);
                }
                for (ModelColumn mc : modelColumns) {  
                    Integer oldColumnId = mc.getIdModelColumn();                            
                    if ( oldTableId.compareTo(mc.getModelTableidModelTable()) == 0 ) {
                        //ModelColumn newMc = mc;    
                        mc.setIdModelColumn(0);           
                        mc.setModelTableidModelTable(mt.getIdModelTable()); 
                        mc.setIdModel(m.getIdModel());
                        mc = (ModelColumn)modelColumnController.createObjects(mc, currentUserId);  
                    }
                    replaceTableKeyInRelationships(modelRelationships,oldColumnId,mc.getIdModelColumn(), mt.getIdModelTable());                             
                }
            }     
            for (ModelRelationship mr : modelRelationships) {             
                mr.setModelidModel(m.getIdModel());     
                mr.setIdModelRelationship(0);
                mr = (ModelRelationship)modelRelationshipController.updateObjects(mr, currentUserId);  
            }                                
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while cloning Model {0}", ex);         
            return "MB0037 JSON syntax exception while cloning Model";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while cloning Model {0}", ex);
            return "MB0038 Entity not found exception while cloning Model";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while cloning Model {0}", ex);
            return "MB0039 Database exception while cloning Model";
        }   
        
        resultString = resultString +newModelId;
        return resultString;
    }
    
    public String exportModel(String JSONString, Integer currentOrgId, Integer currentUserId, Object subject) {
        
        String resultString = "Model not exported";   
        Integer modelId;
        Model model = null;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());              
            modelId = ((Model)subject).getIdModel();
            if ( !Objects.equals(((Model)subject).getOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "MB0045 Export failed not authorized.");
                return "MB0045 Export failed. Not authorized."; 
            }              
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }           
            model = modelController.findModel(modelId);     
            if ( !Objects.equals(model.getOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "MB0081 Export failed not authorized.");
                return "MB0081 Export failed. Not authorized."; 
            }              
            if ( model == null ) {
                logger.log(Level.SEVERE, "MB0044 Export failed. Model with ID {0} not found.", Integer.toHexString(modelId));
                return "MB0044 Export failed. Model with ID "  + Integer.toString(modelId) + " not found.";
            }
            JSONObject theModel = new JSONObject(model);
            String serializedModel = theModel.toString();
            //resultString = "{\"exportModel\":{\"version\":\"300\"},{\"model\":" + serializedModel;
            resultString = "{\"objectType\":\"Model\",\"version\":\"300\",\"model\":" + serializedModel;            
            //subject = controller.createObjects(subject, currentOrgId, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while exporting Model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0040 Exception while exporting Model "  + model.getName();
        }
        
        try {
            List<ModelRelationship> modelRelationships = getModelRelationships(modelId);                         
            List<ModelTable> modelTables = getModelTables(modelId);
            List<ModelColumn> modelColumns = getModelColumns(modelTables);    
            resultString += ",\"modelTables\":[";
            int i = 0; int j = 0;
            for (ModelTable mt : modelTables) {
                JSONObject theTable = new JSONObject(mt);
                String serializedTable = theTable.toString();
                if ( i++ != 0 ) {
                    resultString += ",";
                }                
                resultString += serializedTable;
            }   
            resultString += "],\"modelColumns\":[";            
            for (ModelColumn mc : modelColumns) {  
                JSONObject theColumn = new JSONObject(mc);
                String serializedColumn = theColumn.toString();  
                if ( j++ != 0 ) {
                    resultString += ",";
                }                    
                resultString += serializedColumn;
            }                  
            resultString += "],\"modelTableRelationships\":[";
            i = 0;
            for (ModelRelationship mr : modelRelationships) {             
                JSONObject theRelationship = new JSONObject(mr);
                String serializedRelationship = theRelationship.toString();  
                if ( i++ != 0 ) {
                    resultString += ",";
                }
                resultString += serializedRelationship;
            }  
            resultString += "]}";                
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while exporting Model {0}", ex);         
            return "MB0041 JSON syntax exception while exporting Model";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while exporting Model {0}", ex);
            return "MB0042 Entity not found exception while exporting Model";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while exporting Model {0}", ex);
            return "MB0043 Database exception while exporting Model";
        }   
        return resultString;
    }

    public String importModel(JSONObject impObject, Integer currentOrgId, Integer currentUserId) throws Exception {
        
        String resultString = "Model not imported.";  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        Integer modelId;
        Model model = new Model();
        JSONObject newModel;        
        JSONArray tableArray;
        JSONArray columnArray;
        JSONArray relationshipArray;       
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();    
        List<ModelTable> origMt = new ArrayList();
        List<ModelColumn> origMc = new ArrayList();
        List<ModelTable> newMt = new ArrayList();
        List<ModelColumn> newMc = new ArrayList();        
        
        try {
            JSONObject modelObject = impObject.getJSONObject("model");
            tableArray =  impObject.getJSONArray("modelTables");
            columnArray =  impObject.getJSONArray("modelColumns");
            relationshipArray =  impObject.getJSONArray("modelTableRelationships");            
            // do the preliminary data check for table names
            for (int i=0; i<tableArray.length(); i++) {
                ModelTable mt = new ModelTable();
                mt = gson.fromJson(tableArray.getString(i), mt.getClass());  
                if ( "".equals(mt.getName()) ) {
                    throw new ImportErrorException("Table alias name cannot be blank");
                }
                if ( "".equals(mt.getPhysicalName()) ) {
                    throw new ImportErrorException("Physical table name cannot be blank");
                }                        
            }   
            // do the preliminary data check for column names
            for (int i=0; i<columnArray.length(); i++) {
                ModelColumn mc = new ModelColumn();
                mc = gson.fromJson(columnArray.getString(i), mc.getClass());  
                if ( "".equals(mc.getName()) ) {
                    throw new ImportErrorException("Column alias name cannot be blank");
                }
                if ( "".equals(mc.getPhysicalName()) ) {
                    throw new ImportErrorException("Physical column name cannot be blank");
                }                        
            }
            // import model header data
            String jString = modelObject.toString();
            model = gson.fromJson(jString, model.getClass());  
            // to allow cros-org import following section is commented out
            //if ( !Objects.equals(model.getOrganization(), currentOrgId) ) {
            //    logger.log(Level.SEVERE, "MB0046 Import failed not authorized.");
            //    return "MB0046 Import failed. Not authorized."; 
            //}              
            // re-use existing object to persist into new db record
            model.setIdModel(0);
            //model.setIdConfiguration(0); // force new user to pick db config
            model.setOrganization(currentOrgId);
            model.setName(model.getName());
            model.setStatus("Data Model imported");
            model.setIsAtomicFinished(Boolean.FALSE);
            model.setIsMissingFinished(Boolean.FALSE);
            model.setIsVirtualFinished(Boolean.FALSE);      
            model.setIsSecondaryVirtualFinished(Boolean.FALSE);           
            model.setCreatedTS(currentTimestamp);
            model.setCreatedBy(currentUserId);        
            model.setUpdatedTS(currentTimestamp);
            model.setUpdatedBy(currentUserId);         
            model.setActive(Boolean.TRUE);
            model = modelController.edit(model);     
            modelId = model.getIdModel();
            newModel = new JSONObject(model);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing Model: {0} {1}", new Object[]{model.getName(), ex});
            throw ex;
        }
       
        try {              
            // import tables
            for (int i=0; i<tableArray.length(); i++) {
                ModelTable mt = new ModelTable();
                mt = gson.fromJson(tableArray.getString(i), mt.getClass());  
                origMt.add(i, mt);
                // create new entity
                ModelTable nMt = new ModelTable();               
                nMt.setIdModelTable(0);
                nMt.setModelidModel(modelId);
                // copy old data
                nMt.setActive(mt.getActive());
                nMt.setName(mt.getName());
                nMt.setPhysicalName(mt.getPhysicalName());
                nMt.setPrimaryTable(mt.getPrimaryTable());
                nMt.setDescription(mt.getDescription());
                nMt.setCreatedTS(currentTimestamp);
                nMt.setCreatedBy(currentUserId);        
                nMt.setUpdatedTS(currentTimestamp);
                nMt.setUpdatedBy(currentUserId);                       
                nMt = modelTableController.edit(nMt);     
                newMt.add(i, nMt);       
            }             
            // import columns
            for (int j=0; j<columnArray.length(); j++) {  
                ModelColumn mc = new ModelColumn();
                mc = gson.fromJson(columnArray.getString(j), mc.getClass());   
                origMc.add(j, mc);
                // create new entity
                ModelColumn nMc = new ModelColumn();
                nMc.setIdModelColumn(0);
                nMc.setModelTableidModelTable(translateTableId(origMt, newMt, mc.getModelTableidModelTable()));
                nMc.setIdModel(modelId);
                // copy old data
                nMc.setActive(mc.getActive());
                nMc.setAtomic(mc.getAtomic());
                nMc.setDefaultValue(mc.getDefaultValue());
                nMc.setDefaultValueType(mc.getDefaultValueType());
                nMc.setDescription(mc.getDescription());
                nMc.setFieldClass(mc.getFieldClass());
                nMc.setFieldKind(mc.getFieldKind());
                nMc.setFormula(mc.getFormula());
                nMc.setInsertable(mc.getInsertable());
                nMc.setKeyField(mc.getKeyField());
                nMc.setName(mc.getName());
                nMc.setPhysicalName(mc.getPhysicalName());
                nMc.setSub(mc.getSub());
                nMc.setType(mc.getType());
                nMc.setRollup(mc.getRollup());
                nMc.setVerified(mc.getVerified());
                nMc.setVirtual(mc.getVirtual());
                nMc.setVirtualOrder(mc.getVirtualOrder());              
                nMc.setCreatedTS(currentTimestamp);
                nMc.setCreatedBy(currentUserId);        
                nMc.setUpdatedTS(currentTimestamp);
                nMc.setUpdatedBy(currentUserId);                     
                nMc = modelColumnController.edit(nMc);     
                newMc.add(j, nMc);
            }          
            // import relationships
            for (int i=0; i<relationshipArray.length(); i++) {             
                ModelRelationship mr = new ModelRelationship();
                mr = gson.fromJson(relationshipArray.getString(i), mr.getClass());   
                mr.setIdModelRelationship(0);
                mr.setTable1(translateTableId(origMt, newMt, mr.getTable1()));
                mr.setTable2(translateTableId(origMt, newMt, mr.getTable2()));                
                mr.setKey1(translateKeyId(origMc, newMc, mr.getKey1()));
                mr.setKey2(translateKeyId(origMc, newMc, mr.getKey2()));                 
                mr.setModelidModel(modelId);
                mr.setCreatedTS(currentTimestamp);
                mr.setCreatedBy(currentUserId);        
                mr.setUpdatedTS(currentTimestamp);
                mr.setUpdatedBy(currentUserId);                         
                mr = modelRelationshipController.edit(mr); 
            }  
            resultString = newModel.toString();     
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while importing Model {0}", ex);         
            throw ex;
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while importing Model {0}", ex);
            throw ex;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while importing Model {0}", ex);
            throw ex;
        }  
        model = null;
        tableArray = null;
        columnArray = null;
        relationshipArray = null;       
        gson = null;    
        origMt = null;
        origMc = null;
        newMt = null;
        newMc = null;         
        return resultString;
    }    

    public String importModelWithOverwrite(JSONObject impObject, Integer currentOrgId, Integer currentUserId) throws Exception {
        
        String resultString = "MB0012 Model import failed";  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        Integer modelId;
        Model model = new Model();
        JSONObject newModel;        
        JSONArray tableArray;
        JSONArray columnArray;
        JSONArray relationshipArray;     
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();    
        List<Model> origModel;
        List<ModelTable> dbMt;
        List<ModelColumn> dbMc;
        List<ModelRelationship> dbMr;                 
        List<ModelTable> impMt = new ArrayList();
        List<ModelColumn> impMc = new ArrayList();     
        List<ModelRelationship> impMr = new ArrayList();                  
        
        try {
            JSONObject modelObject = impObject.getJSONObject("model");
            tableArray =  impObject.getJSONArray("modelTables");
            columnArray =  impObject.getJSONArray("modelColumns");
            relationshipArray =  impObject.getJSONArray("modelTableRelationships");            
            String jString = modelObject.toString();
            model = gson.fromJson(jString, model.getClass());    
            // to allow cros-org import following section is commented out
            //if ( !Objects.equals(model.getOrganization(), currentOrgId) ) {
            //    logger.log(Level.SEVERE, "MB0046 Import failed not authorized.");
            //    return "MB0046 Import failed. Not authorized."; 
            //}                 
            if ( modelTableController == null ) {
               modelTableController = new ModelTableJpaController();
            }               
            if ( modelColumnController == null ) {
               modelColumnController = new ModelColumnJpaController();
            }              
            if ( modelRelationshipController == null ) {
               modelRelationshipController = new ModelRelationshipJpaController();
            }              
            if ( modelController == null ) {
               modelController = new ModelJpaController();
            }           
            origModel = modelController.findModelByName(model.getName());                        
            if ( origModel.isEmpty() ) {
                model = null;
                gson = null;   
                impMt = null;
                impMc = null;
                impMr = null;  
                // create brand new model
                return importModel(impObject, currentOrgId, currentUserId);
            }
            model.setIdModel(origModel.get(0).getIdModel());
            model.setStatus("Data Model Imported");    
            model.setIsAtomicFinished(Boolean.FALSE);
            model.setIsMissingFinished(Boolean.FALSE);
            model.setIsVirtualFinished(Boolean.FALSE);
            model.setIsSecondaryVirtualFinished(Boolean.FALSE);
            model.setUpdatedTS(currentTimestamp);
            model.setUpdatedBy(currentUserId);               
            model = modelController.edit(model);   
            AuditLogger.log(currentUserId, model.getIdModel(), "import", model);
            modelId = model.getIdModel();
            newModel = new JSONObject(model);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing Model: {0}", new Object[]{ex.getMessage()});
            throw ex;
        }
       
        try {
            // 1A. parse imported table information
            for (int i=0; i<tableArray.length(); i++) {
                ModelTable mt = new ModelTable();
                mt = gson.fromJson(tableArray.getString(i), mt.getClass());  
                impMt.add(i, mt);
            }
            // 2A. retreive from db original table information
            dbMt = getModelTables(modelId);
            // 3A. Check data for invalid table names
            for (int i=0; i<impMt.size(); i++) {
                if ( "".equals(impMt.get(i).getName()) ) {
                    throw new ImportErrorException("Table alias name cannot be blank.");
                }
                if ( "".equals(impMt.get(i).getPhysicalName()) ) {
                    throw new ImportErrorException("Physical table name cannot be blank");
                }
            }
            // 1B. parse imported column information
            for (int j=0; j<columnArray.length(); j++) {
                ModelColumn mc = new ModelColumn();
                mc = gson.fromJson(columnArray.getString(j), mc.getClass());   
                impMc.add(j, mc);
            }     
            // 2B. retreive from db original column/field information
            dbMc = getModelColumns(dbMt);      
            // 3B. Check data for invalid column names
            for (int i=0; i<impMc.size(); i++) {
                if ( "".equals(impMc.get(i).getName()) ) {
                    throw new ImportErrorException("Column alias name cannot be blank");
                }
                if ( "".equals(impMc.get(i).getPhysicalName()) ) {
                    throw new ImportErrorException("Physical column name cannot be blank");
                }
            }             
            // 4A. save all refreshed tables and check if there are new tables and add them to dtMt list
            for (int i=0; i<impMt.size(); i++) {
                ModelTable nMt = new ModelTable();      
                nMt.setModelidModel(modelId);
                // copy imported data
                nMt.setActive(impMt.get(i).getActive());              
                nMt.setName(impMt.get(i).getName());
                nMt.setPhysicalName(impMt.get(i).getPhysicalName());
                nMt.setPrimaryTable(impMt.get(i).getPrimaryTable());
                nMt.setDescription(impMt.get(i).getDescription());      
                nMt.setUpdatedTS(currentTimestamp);
                nMt.setUpdatedBy(currentUserId);                  
                if ( isTableNew(impMt.get(i), dbMt) == true ) {
                    // for all new imported tables create entity
                    nMt.setIdModelTable(0);
                    nMt.setCreatedTS(currentTimestamp);
                    nMt.setCreatedBy(currentUserId);     
                    nMt = modelTableController.edit(nMt);     
                    AuditLogger.log(currentUserId, nMt.getIdModelTable(), "import insert", nMt);
                    dbMt.add(nMt);                    
                } else {
                    nMt.setIdModelTable(translateOverwrittenTableId(dbMt,impMt,impMt.get(i).getIdModelTable()));
                    nMt.setCreatedTS(impMt.get(i).getCreatedTS());
                    nMt.setCreatedBy(impMt.get(i).getCreatedBy());  
                    nMt = modelTableController.edit(nMt);     
                    AuditLogger.log(currentUserId, nMt.getIdModelTable(), "import update", nMt);                    
                }                       
            }
         
            // 4B. save all refreshed coumns/fields and check if there are new columns and add them to dbMc list
            for (int j=0; j<impMc.size(); j++) {  
                ModelColumn nMc = new ModelColumn();
                nMc.setIdModel(modelId);
                // copy imported data
                nMc.setActive(impMc.get(j).getActive());
                nMc.setAtomic(impMc.get(j).getAtomic());
                nMc.setDefaultValue(impMc.get(j).getDefaultValue());
                nMc.setDefaultValueType(impMc.get(j).getDefaultValueType());
                nMc.setDescription(impMc.get(j).getDescription());
                nMc.setFieldClass(impMc.get(j).getFieldClass());
                nMc.setFieldKind(impMc.get(j).getFieldKind());
                nMc.setFormula(impMc.get(j).getFormula());
                nMc.setInsertable(impMc.get(j).getInsertable());
                nMc.setKeyField(impMc.get(j).getKeyField());
                nMc.setName(impMc.get(j).getName());
                nMc.setPhysicalName(impMc.get(j).getPhysicalName());
                nMc.setSub(impMc.get(j).getSub());
                nMc.setType(impMc.get(j).getType());
                nMc.setVerified(impMc.get(j).getVerified());
                nMc.setRollup(impMc.get(j).getRollup());
                nMc.setVirtualOrder(impMc.get(j).getVirtualOrder());
                nMc.setVirtual(impMc.get(j).getVirtual());   
                nMc.setModelTableidModelTable(translateOverwrittenTableId(dbMt,impMt,impMc.get(j).getModelTableidModelTable()));                                    
                nMc.setUpdatedTS(currentTimestamp);
                nMc.setUpdatedBy(currentUserId);         
                if ( isColumnNew(impMc.get(j), dbMc) == true ) {    
                    nMc.setIdModelColumn(0);                    
                    nMc.setCreatedTS(currentTimestamp);
                    nMc.setCreatedBy(currentUserId);                           
                    nMc = modelColumnController.edit(nMc);    
                    AuditLogger.log(currentUserId, nMc.getIdModelColumn(), "import insert", nMc);
                    //newMc.add(nMc);
                    dbMc.add(nMc);
                } else {
                    nMc.setIdModelColumn((translateOverwrittenKeyId(dbMc, impMc, impMc.get(j).getIdModelColumn())));
                    nMc.setCreatedTS(impMc.get(j).getCreatedTS());
                    nMc.setCreatedBy(impMc.get(j).getCreatedBy());                          
                    nMc = modelColumnController.edit(nMc);    
                    AuditLogger.log(currentUserId, nMc.getIdModelColumn(), "import update", nMc);                        
                }
            }            

            // 1C. parse imported relationship information
            for (int j=0; j<relationshipArray.length(); j++) {
                ModelRelationship mr = new ModelRelationship();
                mr = gson.fromJson(relationshipArray.getString(j), mr.getClass());  
                impMr.add(j, mr);
            } 
            // 2C. retreive from db original relationship information
            dbMr = getModelRelationships(modelId);          
            // 3C. check if there are new relations and save them in db            
            for (int i=0; i<relationshipArray.length(); i++) {        
                if ( isRelationshipNew(dbMt, impMt, impMr.get(i), dbMr) == true ) {                
                    ModelRelationship mr = new ModelRelationship();
                    mr = gson.fromJson(relationshipArray.getString(i), mr.getClass());   
                    mr.setIdModelRelationship(0);
                    mr.setTable1(translateOverwrittenTableId(dbMt, impMt, mr.getTable1()));
                    mr.setTable2(translateOverwrittenTableId(dbMt, impMt, mr.getTable2()));                
                    mr.setKey1(translateOverwrittenKeyId(dbMc, impMc, mr.getKey1()));
                    mr.setKey2(translateOverwrittenKeyId(dbMc, impMc, mr.getKey2()));                 
                    mr.setModelidModel(modelId);
                    mr.setCreatedTS(currentTimestamp);
                    mr.setCreatedBy(currentUserId);        
                    mr.setUpdatedTS(currentTimestamp);
                    mr.setUpdatedBy(currentUserId);                      
                    mr = modelRelationshipController.edit(mr); 
                    AuditLogger.log(currentUserId, mr.getIdModelRelationship(), "import insert", mr);
                }
            }  
            resultString = newModel.toString();           
        } catch (JsonSyntaxException ex) {
            logger.log(Level.SEVERE, "GSON exception while importing Model {0}", ex);         
            throw ex;
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Entity not found exception while importing Model {0}", ex);
            throw ex;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Database exception while importing Model {0}", ex);
            throw ex;
        }  
        model = null;
        tableArray = null;
        columnArray = null;
        relationshipArray = null;       
        gson = null;    
        dbMt = null;
        dbMc = null;
        dbMr = null;
        impMt = null;
        impMc = null;
        impMr = null;        
        return resultString;
    }  
  
    private boolean isTableNew(ModelTable mt, List<ModelTable> dbMtList) {
        boolean isNew = true;
        for (ModelTable dbMtList1 : dbMtList) {
            if (dbMtList1.getName() == null ? mt.getName() == null : dbMtList1.getName().equals(mt.getName())) {
                isNew = false;
                break;
            }
        }
        return isNew;
    } 
  
    private boolean isColumnNew(ModelColumn mc, List<ModelColumn> dbMcList) {
        boolean isNew = true;
        for (ModelColumn dbMcList1 : dbMcList) {
            if (dbMcList1.getName() == null ? mc.getName() == null : dbMcList1.getName().equals(mc.getName())) {
                isNew = false;
                break;
            }
        }
        return isNew;
    } 
  
    private boolean isRelationshipNew(List<ModelTable> dbMt, List<ModelTable> impMt, ModelRelationship impMr, List<ModelRelationship> dbMrList) {
        boolean isNew = true;
        int table1id = translateOverwrittenTableId(dbMt, impMt, impMr.getTable1());
        int table2id = translateOverwrittenTableId(dbMt, impMt, impMr.getTable2()); 
        
        for (ModelRelationship dbMr : dbMrList) {
            if ( (dbMr.getTable1() == table1id) && (dbMr.getTable2() == table2id) ) {
                isNew = false;
                break;
            }
        }
        return isNew;
    }
 
    // this method is invoked from WS
    public String getModelProcessingId(String load, Integer currentUserId) {
        JSONArray modelArray = null;        
        Model model = null;
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                model = modelController.findModel(model.getIdModel());
                break; // process just first record, for now
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting processing id of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0063 Exception while getting processing id of model " + ex.getMessage();
        }       
       return model.getProcessingId();
    }        
    
    // this method is invoked from WS
    public String getModelStatus(String load, Integer currentUserId) {
        JSONArray modelArray = null;        
        Model model = null;
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                model = modelController.findModel(model.getIdModel());
                break; // process just first record, for now
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0063 Exception while reading model status " + ex.getMessage();
        }      
       if ( model != null ) {
           return model.getStatus();
       } else {
           return "Unknown. Model does not exist.";
       }
    }        
    
    //----------------This is new method
    
    public String setProjectAssigned(String load, Integer currentUserId, String status) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         Model model = null;
         String result;
         if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
          
        if ( grouphasentityController == null ) {
           grouphasentityController = new GroupHasEntityJpaController();
        }   
        
        try {
            int inum = Integer.parseInt(load);    
            //result = modelController.updateProjectAssigned(load, currentUserId);
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityEntitiesByIdAndType(inum, "models");
            //.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
            Model resultmodel = modelController.findModel(inum);
            
            resultmodel.setProjectAssigned(ghe.size());
            modelController.updateObjects(resultmodel, currentUserId);
                
        } catch (Exception ex) {
            //logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0064 Exception while updating model status " + ex.getMessage();
        }       
       return status; // mirror back to browser
     }
      
    
     public String resetProjectAssigned(String load, Integer currentUserId, String status) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         Model model = null;
         String result;
         if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
          
        if ( grouphasentityController == null ) {
           grouphasentityController = new GroupHasEntityJpaController();
        }   
        
        try {

            int inum = Integer.parseInt(load);
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityEntitiesByIdAndType(inum, "models");            
            //reset projectflag to 0 if count is <1
            //if(ghe.size()<1)
            //{
            Model resultmodel = modelController.findModel(inum);
            resultmodel.setProjectAssigned(ghe.size());
            modelController.updateObjects(resultmodel, currentUserId);
            //}
        
            
        } catch (Exception ex) {
            //logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0064 Exception while updating model status " + ex.getMessage();
        }       
       return status; // mirror back to browser
     }
    
    
    
    
    // this method is invoked from WS
    public String setModelStatus(String load, Integer currentUserId, String status) {
        JSONArray modelArray = null;        
        Model model = null;
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");
            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                if ( !"Temp status".equals(status) ) {
                    String stat = status.replaceAll("\"","\\\"");
                    if ( stat.length() > STATUS_FIELD_SIZE ) {
                        model.setStatus(stat.substring(0, STATUS_FIELD_SIZE));
                    } else {
                        model.setStatus(stat);                        
                    }
                }
                modelController.updateObjects(model, currentUserId);
                break; // process just first record, for now
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return "MB0064 Exception while updating model status " + ex.getMessage();
        }       
       return status; // mirror back to browser
    }        

    public int setModelStatus(Model model, Integer currentUserId, String status) {           
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            String stat = status.replaceAll("\"","\\\"");
            if ( stat.length() > STATUS_FIELD_SIZE ) {
                model.setStatus(stat.substring(0, STATUS_FIELD_SIZE));
            } else {
                model.setStatus(stat);                        
            }
            modelController.updateObjects(model, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    }  

    
    
    public int setModelStatus(Model model, Integer currentUserId, String status, boolean a, boolean v1, boolean v2) {           
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            model.setIsAtomicFinished(a);
            model.setIsVirtualFinished(v1);
            model.setIsSecondaryVirtualFinished(v2);
            String stat = status.replaceAll("\"","\\\"");
            if ( stat.length() > STATUS_FIELD_SIZE ) {
                model.setStatus(stat.substring(0, STATUS_FIELD_SIZE));
            } else {
                model.setStatus(stat);                        
            }
            modelController.updateObjects(model, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    } 
    
    public int setModelStatus(Model model, Integer currentUserId, String status, boolean a, boolean v1, boolean m, boolean v2) {           
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }          
        try {
            model.setIsAtomicFinished(a);
            model.setIsMissingFinished(m);
            model.setIsVirtualFinished(v1);
            model.setIsSecondaryVirtualFinished(v2);
            String stat = status.replaceAll("\"","\\\"");
            if ( stat.length() > STATUS_FIELD_SIZE ) {
                model.setStatus(stat.substring(0, STATUS_FIELD_SIZE));
            } else {
                model.setStatus(stat);                        
            }
            modelController.updateObjects(model, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    } 
    
    public int setModelStatusByColumn(String JSONColumn, Integer currentUserId, String status) {     
        ModelColumn modelColumn = new ModelColumn();
        ModelTable modelTable = null;
        Model model = null;
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }        
        try {     
            JSONArray objectArray =  new JSONArray(JSONColumn);
            if ( objectArray.length() > 0) {            
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(0).toString();
                modelColumn = gson.fromJson(jString, modelColumn.getClass());
                modelTable = modelTableController.findModelTable(modelColumn.getModelTableidModelTable());
                model = modelController.findModel(modelTable.getModelidModel());
                try {
                    String stat = status.replaceAll("\"","\\\"");
                    if ( stat.length() > STATUS_FIELD_SIZE ) {
                        model.setStatus(stat.substring(0, STATUS_FIELD_SIZE));
                    } else {
                        model.setStatus(stat);                        
                    }               
                    modelController.updateObjects(model, currentUserId);
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
                    return -1;
                }
            }
            return model.getIdModel();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing coluns/tables to update status of model: {0} {1}", new Object[]{model.getName(), ex});
            return -2;
        }  
    }  
 
    public int setModelStatusByTable(String JSONTable, Integer currentUserId, String status) {     
        ModelTable modelTable = new ModelTable();
        Model model = null;
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }        
        try {     
            JSONArray objectArray =  new JSONArray(JSONTable);
            for (int i=0; i<objectArray.length(); i++) {            
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                modelTable = gson.fromJson(jString, modelTable.getClass());
                model = modelController.findModel(modelTable.getModelidModel());
                try {
                    String stat = status.replaceAll("\"","\\\"");
                    if ( stat.length() > STATUS_FIELD_SIZE ) {
                        model.setStatus(stat.substring(0, STATUS_FIELD_SIZE));
                    } else {
                        model.setStatus(stat);                        
                    }
                    modelController.updateObjects(model, currentUserId);
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception while updating status of model: {0} {1}", new Object[]{model.getName(), ex});
                    return -1;
                }
            }
            return model.getIdModel();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing table JSON to update status of model: {0} {1}", new Object[]{model.getName(), ex});
            return -2;
        }  
    } 
 
    public int cleanUpFiltersByColumn(String JSONColumn, Integer currentUserId) {     
        ModelColumn modelColumn = new ModelColumn();
        Model model = null;
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }  
        if ( filterfieldController == null ) {
           filterfieldController = new FilterFieldJpaController();
        }          
        try {     
            JSONArray objectArray =  new JSONArray(JSONColumn);
            for (int i=0; i<objectArray.length(); i++) {            
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                modelColumn = gson.fromJson(jString, modelColumn.getClass());
                List<FilterField> ffList = new ArrayList();
                ffList = filterfieldController.findByModelColumnidModelColumn(modelColumn.getIdModelColumn());
                for (FilterField ff : ffList) {
                    try {
                        filterfieldController.destroy(ff.getIdFilterField());
                    } catch (NonexistentEntityException ex) {
                        logger.log(Level.SEVERE, "Exception while deleteing FilterField records: {0} {1}", new Object[]{ff.getIdFilterField().toString(), ex});
                    }
                }
            }
            return 0;
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing model JSON before deleteing FilterField records: {0} {1}", new Object[]{model.getName(), ex});
            return -2;
        }  
    }  
     
    private Integer translateTableId(List<ModelTable> origMt, List<ModelTable> newMt, int current) {
        for (int i=0; i<origMt.size(); i++) {
            if ( origMt.get(i).getIdModelTable() == current ) {
                return newMt.get(i).getIdModelTable();
            }
        }        
        return 0;
    }

    
    private Integer translateOverwrittenTableId(List<ModelTable> dbMtList, List<ModelTable> impMtList, int current) {
        String nameToSearch = null;
        for (ModelTable impMt : impMtList) {
            if (impMt.getIdModelTable() == current) {
                nameToSearch = impMt.getName();
            }
        }        
        for (ModelTable dbMt : dbMtList) {
            if (dbMt.getName() == null ? nameToSearch == null : dbMt.getName().equals(nameToSearch)) {
                return dbMt.getIdModelTable();
            }
        }        
        return 0;
    }
    
    private Integer translateKeyId(List<ModelColumn> origMc, List<ModelColumn> newMc, int current) {
        for (int i=0; i<origMc.size(); i++) {
            if ( origMc.get(i).getIdModelColumn() == current ) {
                return newMc.get(i).getIdModelColumn();
            }
        }        
        return 0;
    }    
    
    private Integer translateOverwrittenKeyId(List<ModelColumn> dbMcList, List<ModelColumn> impMcList, int current) {
        String nameToSearch = null;
        for (ModelColumn impMc : impMcList) {
            if (impMc.getIdModelColumn() == current) {
                nameToSearch = impMc.getName();
            }
        }          
        for (ModelColumn dbMc : dbMcList) {
            if ( dbMc.getName() == null ? nameToSearch == null : dbMc.getName().equals(nameToSearch) ) {
                return dbMc.getIdModelColumn();
            }
        }        
        return 0;
    }  
    
    private String getColumnsOmitSubs(Model m) {
        String result = "";
        List<ModelTable> mt = getModelTables(m.getIdModel());
        List<ModelColumn> mc = getModelColumns(mt);       
        for (ModelColumn modelColumn : mc) {       
            if ( modelColumn.getSub() == false ) {
                result += modelColumn.getName() + ",";
            }
        }          
        return Util.removeLastChar(result);
    } 
    
    private String getAllColumns(Model m) {
        String result = "";
        List<ModelTable> mt = getModelTables(m.getIdModel());
        List<ModelColumn> mc = getModelColumns(mt);       
        for (ModelColumn modelColumn : mc) {       
                result += modelColumn.getName() + ",";
        }          
        return Util.removeLastChar(result);
    }     
    
    public JSONObject submitFlatTabletoProxy(Process process, String filterWhereClause, String methodName, String orderField, Set<String> columns, Proxy proxy, Boolean dump, Study s) throws Exception {
        JobRequest job = null;
        JSONObject jobObject = null;

        try {
            Query queryResult;
            int colCount = 0;
            if ( modelController == null ) {
                modelController = new ModelJpaController();
            }
            Model model = modelController.findModel(process.getIdModel());
            if ( model == null ) return null;        

            String sqlStmt = "select ";    
            String columnString = "";
            if ( s.getSendAllColumns() == false ) {
                colCount = columns.size();
                if ( colCount < 1 ) {
                    return null; // most likely Flat Table does not exist
                }      
                Iterator<String> iterator = columns.iterator();                
                for (int i=0; i < colCount; i++) {
                    if ( i > 0 ) {
                        columnString += ",";
                    }
                    columnString += iterator.next();
                }

            } else {
                colCount = getTableWidth(model.getOutputName() + flatTableAppendString);
                if ( colCount < 1 ) {
                    return null; // most likely Flat Table does not exist
                }                  
                columnString = DBHelper.getDBObjectColumns(model.getOutputName() + flatTableAppendString, filterWhereClause);                 
            }
            sqlStmt += columnString + " from ";
                
            if ( filterWhereClause == null ) {
                sqlStmt += model.getOutputName() + flatTableAppendString;
            } else {
                sqlStmt += model.getOutputName() + flatTableAppendString + " WHERE " + filterWhereClause; 
            }
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger);
            process.setFilterClause(sqlStmt);    
            
            List<Object[]> resultValues = queryResult.getResultList();
            int rowCount = resultValues.size();
            process.setRowCount(rowCount);
            if ( rowCount == 0 ) {
                throw new EmptyRecordSetException("Empty record set. Job not submitted.");
            }

            logger.log(Level.SEVERE, "Reserving memory for array[rows-{0},columns-{1}]", new Object[]{Integer.toString(rowCount), Integer.toString(colCount)});
            double[] data = new double[rowCount * colCount];
            // changes to double array will be visible after return
            
            //String returnString = DBHelper.getObjectDataIntoDoubleArray(model.getOutputName() + flatTableAppendString, columnString, filterWhereClause, orderField, "ASC", data);
            //New Method for v3.67
            
            //String returnString = DBHelper.getObjectDataIntoDoubleArray(process.getIdFile(), process.getIdProcess(), model.getOutputName() + flatTableAppendString, columnString, filterWhereClause, orderField, "ASC", data); 
            
            String returnString = DBHelper.getObjectDataIntoDoubleArray(process.getIdFile(), process.getIdProcess(), model.getOutputName() + flatTableAppendString, columnString, filterWhereClause, orderField, "ASC", data,colCount); 
            
            if ( !"OK".equals(returnString) ) throw new IOException(returnString);
            String memKey = "";
            
            
            
            if ( s.getRemoteStatPackage() == true ) {
                memKey = proxy.storeDoubleArray(data);   
                if ( memKey == null ) return null;
            }
            String metaDataString= "";
            String[] columnTokens = columnString.split(",");
            List<ModelTable> mt = getModelTables(process.getIdModel());
            List<ModelColumn> mc = getModelColumns(mt);
            for (int ii=0;  ii<colCount; ii++) {
                if ( ii > 0 ) {
                   metaDataString += ","; 
                }                    
                metaDataString = metaDataString + "{\"name\":\"" + columnTokens[ii] 
                        + "\",\"type\":\"" + findColumnType(mc,columnTokens[ii])
                        + "\",\"class\":\"" + findColumnClass(mc,columnTokens[ii])
                        + "\",\"kind\":\"" + findColumnKind(mc,columnTokens[ii]) + "\"}";
            }
            
            
            
            
            job = new JobRequest(process);     
            job.setMemoryKey(memKey);  
            job.setMethod(methodName);
            job.setColumnCount(colCount);
            job.setLastProcess(s.getIdLastProcess());
            job.setColumns("[" + metaDataString + "]");
            
            logger.log(Level.SEVERE, "Metadata sent with submitted job: {0}", new Object[]{metaDataString});                    
            JSONArray JSONcolumns = new JSONArray("[" + metaDataString + "]");
            jobObject = new JSONObject(job);
            jobObject.put("columns", JSONcolumns);  
            
            if ( dump == true ) {
                //Util.dumpDoubleArrayToCSV(process.getIdStudy().toString(), ",",  data, columnTokens, colCount);
                Util.dbDumpDoubleArrayToCSV(process.getIdStudy().toString(), ",",  data, columnTokens, colCount, logger);
                //Util.dumpDoubleArrayToDB(process.getIdStudy().toString(), data, columnTokens, colCount, logger);                
            }
            return jobObject;        
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Persistence error while populating double array before submission: {0}", ex.getMessage());            
            MySQLSyntaxErrorException sqlEx = (MySQLSyntaxErrorException) ex.getCause().getCause();
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Hibernate error while populating double array before submission: {0}", sqlEx.getMessage());
            throw sqlEx;            
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Error while populating double array before submission: (0)", ex.getMessage());
            throw ex;
        }
    }
   
    private List<ModelRelationship> replaceTableKeyInRelationships(List<ModelRelationship> modelRelations, Integer oldColumnId, Integer newColumnId, Integer newTableId) {
        for (ModelRelationship mr: modelRelations) {
            if ( oldColumnId.compareTo(mr.getKey1()) == 0 ) {
                mr.setTable1(newTableId);
                mr.setKey1(newColumnId);                                                            
            }      
            if ( oldColumnId.compareTo(mr.getKey2()) == 0 ) {                           
                mr.setTable2(newTableId);
                mr.setKey2(newColumnId);                                                                        
            }             
        }
        return modelRelations;
    }
    
    /* get ModelId from Model JSON, not used */
    public int getModelId(String load) {     
        JSONArray modelArray = null;        
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("models");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving ModelId for Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class); 
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving ModelId: {0} {1}", new Object[]{model.getName(), ex});
            return 0;
        }       
       return model.getIdModel();
    }
 
    /* get TableId from Column JSON */
    public int getTableId(String load) {     
        JSONArray modelArray = null;        
        ModelColumn column = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(load);
            modelArray = JSONContent.getJSONArray("columns");

            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Retrieving TableId for Column {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                column = gson.fromJson(jString, ModelColumn.class); 
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving ModelId: {0} {1}", new Object[]{column.getName(), ex});
            return 0;
        }  
        
       return column.getModelTableidModelTable();
    }
  
     /* get TableIds from Table JSON */
    public Integer[] getTableIdFromTableJSON(String load) {     
        Integer [] tableIdArray = null;
        try {
            JSONArray objectArray =  new JSONArray(load);
            tableIdArray = new Integer[objectArray.length()];
            for (int i=0; i<objectArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                ModelTable subject = gson.fromJson(jString, ModelTable.class); 
                logger.log(Level.INFO, "Retrieving TableIds from JSON: found id {0}", Integer.toString(subject.getIdModelTable()));                
                tableIdArray[i] = subject.getIdModelTable();
                if ( i == (MAX_TABLES-1) ) break;
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving TableIds and processing JSON: {0}", ex);            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving TableIds from JSON: {0}", ex);
            return null;
        }  
        
       return tableIdArray;
    }
    
    static public Model parseModelFromJSON(String modelJSON) {
        Model model = null;
        
        try {
            JSONObject JSONContent =  new JSONObject(modelJSON);
            JSONArray modelArray = JSONContent.getJSONArray("models");
            for (int i=0; i<modelArray.length(); i++) {
                logger.log(Level.INFO, "Processing Model {0}", modelArray.getJSONObject(i).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = modelArray.getJSONObject(i).toString();
                model = gson.fromJson(jString, Model.class);  
                break; // process just first record, for now
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while editing Model: {0} {1}", new Object[]{model.getName(), ex});
        } 
        return model;
    }
    
    public List<ModelRelationship> getModelRelationships(int modelId)
    {
        if ( modelRelationshipController == null ) {
           modelRelationshipController = new ModelRelationshipJpaController();
        }    
        List<ModelRelationship> modelRelationships = modelRelationshipController.findModelRelationshipByModelId(modelId);
        return modelRelationships;
    }
    
    public List<ModelTable> getModelTables(int modelId)
    {
        if ( modelTableController == null ) {
           modelTableController = new ModelTableJpaController();
        }    
        List<ModelTable> modelTables = modelTableController.findModelTableByModel(modelId);
        return modelTables;
    }
    
    public List<ModelColumn> getModelColumns(List<ModelTable> modelTables)
    {
        if ( modelColumnController == null ) {
           modelColumnController = new ModelColumnJpaController();
        }           
        List<ModelColumn> modelColumns = new ArrayList();     
        
        for (ModelTable mt : modelTables) {
            modelColumns.addAll(modelColumnController.findModelColumnByTable(mt.getIdModelTable()));
        }
        return modelColumns;
    }    
    
    //--------------------------------------------------------------------------
    public int getTableWidth(String tableName) {
        try {          
            //String sqlTestStmt = "SELECT * FROM " + tableName + " LIMIT 1";
            // following is not optimal, but necessare for quick Microsoft fix!
            String sqlTestStmt = "SELECT * FROM " + tableName;
            Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);

            List<Object[]> results = queryResult.getResultList();
            int count = results.get(0).length;         
            return count;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting number of table columns: {0}", ex);
            return -1;
        }
    }    
    
    private ModelTable lookupModelTables(List<ModelTable> modelTables, int id) {
        for (ModelTable mt : modelTables) {
            if ( mt.getIdModelTable() == id ) {
                return mt;
            }
        }
        return null;
    }
    
    private ModelColumn lookupModelColumns(List<ModelColumn> modelColumns, int id) {
        for (ModelColumn mc : modelColumns) {
            if ( mc.getIdModelColumn() == id ) {
                return mc;
            }
        }
        return null;
    }
    
    public List<ModelColumn> filterInsertableAtomicModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getInsertable() == true) && (mc.getAtomic() == true)) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }

    public List<ModelColumn> filterInsertableModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( mc.getInsertable() == true) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }
    
    private List<ModelColumn> filterAtomicModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( mc.getAtomic() == true ) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }
    
    private List<ModelColumn> filterInsertableAtomicNoScalarsModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getInsertable() == true) && (mc.getAtomic() == true) 
                    && (("".equals(mc.getFormula())) || (mc.getSub() == true))) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }    

    private List<ModelColumn> filterAtomicNoScalarsModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getAtomic() == true) && (("".equals(mc.getFormula())) || (mc.getSub() == true))) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }    
    
    private List<ModelColumn> filterPrimaryVirtualModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getVirtual()== true) && (mc.getVirtualOrder() == false) ) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }
  
    private List<ModelColumn> filterSecondaryVirtualModelColumns(List<ModelColumn> modelColumns) {
        List<ModelColumn> newModelColumns = new ArrayList();
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getVirtual()== true)  && (mc.getVirtualOrder() == true) ) {
                newModelColumns.add(mc);
            }
        }
        return newModelColumns;
    }
    
    private int lookupModelByTableId(int id) {
        ModelTable mt = modelTableController.findModelTable(id);
        return mt.getModelidModel();
    }
    
    private int lookupTableByColumnId(int id) {
        ModelColumn mc = modelColumnController.findModelColumn(id);
        return mc.getModelTableidModelTable();
    }
 
    private boolean checkIfTableIsEmpty(String tableName) {
        try {
            String sqlStmt = "SELECT COUNT(*) FROM " + tableName;
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger); 
            List<Object[]> resultCounts = queryResult.getResultList(); 
            String tmp = String.valueOf(resultCounts.get(0));                        
            int totalCount = Integer.parseInt(tmp);
            return totalCount == 0;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while checking if table is empty: {0}", ex);
            return true;
        }
    }
    
    /* get Primary Key for target OST table from Column JSON */
    /* Primary Key is by default the first key of Primary Table, this may need to change */
    private ModelColumn getPrimaryKeyField(List<ModelTable> mt, List<ModelColumn> mc) {  
        int primaryTableId = 0; 
        for (ModelTable modelTable : mt) {
            if ( modelTable.getPrimaryTable() == true ) {
                primaryTableId = modelTable.getIdModelTable();
                break;
            }
        }   
        for (ModelColumn modelColumn : mc) {
            if ( (modelColumn.getKeyField() == true) 
                    && (modelColumn.getModelTableidModelTable() == primaryTableId) ) {
                return modelColumn;
            }
        }       
        return null;
    }
    
    // this is MySQL specific, JPQL does not support create table
    // with in-flight substitution for rollup fields to MEDIUMTEXT type
    private String constructCreateStatement(String tableName, List<ModelColumn> modelColumns, List<ModelTable> modelTables, List<ModelRelationship> modelRelationships) {
        String sqlStmt = "CREATE TABLE `" + tableName + "` (`id` INT(11) NOT NULL AUTO_INCREMENT,";
        // process all the fields but exclude non-insertable and non-atomic ones
        //List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicModelColumns(modelColumns);
        List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicModelColumns(modelColumns);
        for (ModelColumn mc : modelColumnsInsertable) { // needs quotes around strings and time stamps
            if ( mc.getRollup() == true ) {
                sqlStmt = sqlStmt + "`" + mc.getName() + "` MEDIUMTEXT DEFAULT " + mc.getDefaultValue() + ",";
            } else {            
                //sqlStmt = sqlStmt + "`" + mc.getName() + "` " + mc.getType() + " DEFAULT " + mc.getDefaultValue() + ",";
                sqlStmt = sqlStmt + "`" + mc.getName() + "` " + mc.getType() + ",";
            }
        }  
        sqlStmt = sqlStmt + "`drop_record` BIT DEFAULT 0,";
        // process all one-to-many Relationships looking for key2 (many)
        for (ModelColumn mc : modelColumns) { 
            if ( (mc.getAtomic() == true) && (mc.getKeyField() == true) ) {
                for (ModelRelationship mr : modelRelationships) {
                    if ("3".equals(mr.getType()) && (mc.getIdModelColumn().compareTo(mr.getKey2()) == 0) && (!"".equals(mc.getFormula()))) {                
                        try {
                            String tn = lookupModelTables(modelTables,mc.getModelTableidModelTable()).getPhysicalName();
                            String keyFieldType;
                            if ( mc.getRollup() == true ) {
                                keyFieldType = "MEDIUMTEXT";
                            } else {
                                keyFieldType = getFormulaFieldTypeMySQL(mc, tn);
                            }
                            if ( keyFieldType != null ) {
                                sqlStmt = sqlStmt + "`" + mc.getName() + "` " + keyFieldType + ",";
                            }
                        } catch (PersistenceException ex) {
                            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0001 Error while constructing CREATE statement. ", ex);            
                            return "MB0001 SQL Error while constructing CREATE statement.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
                        }  catch (Exception ex) {
                            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0001 SQL Error while constructing CREATE statement.", ex.getMessage());
                        }
                    }   
                }
            }
        }
        //sqlStmt += " PRIMARY KEY (`id`)) ENGINE = INNODB DEFAULT CHARSET=utf8";
        sqlStmt += " PRIMARY KEY (`id`)) ENGINE = MYISAM DEFAULT CHARSET=utf8";
        return sqlStmt;
    }

    private String getOriginalFieldType(ModelColumn mc, String tableName) throws Exception {
         String sqlTestStmt = null;
         try {
             sqlTestStmt = "SELECT " + mc.getName() + " FROM " + tableName + " LIMIT 1";
             logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
             Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);
             queryResult.getSingleResult();
             List<Object[]> results = queryResult.getResultList();
             return getDBObjectType(results.get(0));
         } catch (Exception ex) {
             logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
             throw ex;
         }
    }

    private String getRawFieldType(ModelColumn mc, String tableName) throws Exception {
         String sqlTestStmt = null;
         try {
             sqlTestStmt = "SELECT " + mc.getName() + "_raw" + " FROM " + tableName + " LIMIT 1";
             logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
             Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);
             queryResult.getSingleResult();
             List<Object[]> results = queryResult.getResultList();
             return getDBObjectType(results.get(0));
         } catch (Exception ex) {
             logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
             throw ex;
         }
    }
    
    private String getFormulaFieldTypeMySQL(ModelColumn mc, String tableName) throws Exception {
         String sqlTestStmt = null;
         try {
             sqlTestStmt = "SELECT " + mc.getFormula() + " FROM " + tableName + " LIMIT 1";
             logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
             Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);
             queryResult.getSingleResult();
             List<Object[]> results = queryResult.getResultList();
             return getDBObjectType(results.get(0));
         } catch (Exception ex) {
             logger.log(Level.SEVERE, "Exception in evaluating formula: " + sqlTestStmt, ex);
             throw ex;
         }
    }
    
    private String getDBObjectType(Object obj) {
        if (obj instanceof String) {
            int len = ((String)obj).length();
            return "VARCHAR(256)"; // how to find true size?
        }
        if (obj instanceof Boolean) {
            return "BIT";                 
        }          
        if (obj instanceof Integer) {
            return "INTEGER(11)";
        }   
        if (obj instanceof BigInteger) {
            return "INTEGER(11)";
        }        
        if (obj instanceof BigDecimal) {
            return "DECIMAL(10,2)";
        }         
        if (obj instanceof Double) {
            return "DOUBLE";                 
        } 
        if (obj instanceof Float) {
            return "FLOAT";                 
        }          
        if (obj instanceof Character) {
            return "CHAR";
        }    
        if (obj instanceof Date) {
            return "DATE";
        }                   
        if (obj instanceof Timestamp) {
            return "TIMESTAMP";
        }      
        return "VARCHAR(256)"; // default 
    }

    private String updateVirtualFieldTypes(List<ModelColumn> mcList, String tableName) throws Exception {
        String fieldType = null;
        if ( modelColumnController == null ) {
           modelColumnController = new ModelColumnJpaController();
        }             
        for (int i = 0; i< mcList.size(); i++) {
            ModelColumn mc = mcList.get(i);
            fieldType = getFormulaFieldTypeMySQL(mc, tableName);
            mc.setType(fieldType);
            modelColumnController.edit(mc);
        }
        return "Ok";
    }
    
    private String populateStagingTableJDBC(Model model, List<ModelRelationship> modelRelationships, List<ModelTable> modelTables, List<ModelColumn> modelColumns, Integer currentUserId) {  
        ResultSet queryResult = null;    
        try {           
            String[] stmt = createJoinStatement(model, modelRelationships, modelTables, modelColumns);            

            List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicNoScalarsModelColumns(modelColumns);               
            List<List<String>> modelRollupColumns = extractRollupFields(modelColumnsInsertable, modelTables);      
            
            logger.log(Level.INFO, "Executing SQL statement: {0} {1}", new Object[]{stmt[SELECT], stmt[JOIN]}); 
            setModelStatus(model, currentUserId, "Fetching source fields");
            if ( configurationController == null ) {
                configurationController = new ConfigurationJpaController();
            }            
            Configuration conConf = configurationController.findConfiguration(model.getIdConfiguration());
            
            ResultSet queryCountResult;  
            if ( conConf.getType().equals("MS-SQL") ) {
                queryCountResult = DBHelper.executeJdbcQuery(conConf,"select count_big(*) as recordcount " + stmt[JOIN],logger);                
            } else {
                queryCountResult = DBHelper.executeJdbcQuery(conConf,"select count(*) as recordcount " + stmt[JOIN],logger);
            }
            queryCountResult.next();
            String totalCount = queryCountResult.getString("recordcount");    
            
            DBHelper.executeSqlStatement("DeltaData","ALTER TABLE " + model.getOutputName() + " DISABLE KEYS;",logger);  
            
            queryResult = DBHelper.executeJdbcQuery(conConf,stmt[SELECT] + stmt[JOIN],logger);
            if ( Thread.currentThread().isInterrupted() == true) {
                logger.log(Level.SEVERE, "MB0051 Staging Table creation interrupted");
                return "MB0051 Staging Table creation interrupted";
                }
            stmt[INSERT] += " ) VALUES (";      
            int columnCount = queryResult.getMetaData().getColumnCount();  
            Object[] obj = new Object[columnCount];
            // for each row of data retieved insert new row of data into DST table
            logger.log(Level.INFO, "Starting Staging Table insert loop, INSERT_BATCH_SIZE={0}", new Object[]{Integer.toString(INSERT_BATCH_SIZE)}); 
            long counter = 0;
            String sqlStmtInsertData;   
            boolean resultSetNotEmpty = queryResult.next();
            while (resultSetNotEmpty) {           
                sqlStmtInsertData = "";
                for (int j=0; j<INSERT_BATCH_SIZE; j++) {      
                    if ( j > 0 ) {
                        sqlStmtInsertData += " ,(";
                    }
                    for (int i = 0; i < columnCount; i++) {        
                        obj[i] = queryResult.getObject(i+1);
                    }
                    sqlStmtInsertData += getInsertValues(obj);   
                    sqlStmtInsertData = overwriteRollupFields(j, columnCount, sqlStmtInsertData, modelRollupColumns);
                    sqlStmtInsertData = Util.removeLastChar(sqlStmtInsertData);
                    sqlStmtInsertData += ")";
                    counter++;
                    if ( counter%INSERT_COUNTER_INCREMENT == 0 ) {
                        setModelStatus(model, currentUserId, "Source fields processed: " + Long.toString(counter) + " records of " + totalCount);
                    }
                    if ( Thread.currentThread().isInterrupted() == true) {
                        logger.log(Level.SEVERE, "MB0052 Staging Table insertion interrupted");
                        return "MB0052 Staging Table insertion interrupted";
                    }
                    if ( !(resultSetNotEmpty=queryResult.next()) ) 
                        break;
                }
                // later add ability to populate memory instead of saving to DB
                // which could be followed immediately with call to StatProxy for statistical processing
                DBHelper.executeSqlStatement("DeltaData",stmt[INSERT] + sqlStmtInsertData,logger);  
                if ( resultSetNotEmpty == false )
                    break;
            }
            DBHelper.executeSqlStatement("DeltaData","ALTER TABLE " + model.getOutputName() + " ENABLE KEYS;",logger);  
            return "Ok";
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "MB0049 SQL exception while processing source fields", ex);
            return "MB0049 SQL exception while processing source fields.\n" + DBHelper.printSQLException(ex);  
        } catch (PersistenceException ex) {
            logger.log(Level.SEVERE, "MB0049 Persistence exception while processing source fields", ex);
            return "MB0049 Persistence exception while processing source fields.\n" + DBHelper.printPersistenceException(ex);              
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "MB0049 Error while processing source fields", ex);
            return "MB0049 Error while processing source fields.\n" + ex.toString(); 
        } finally {
            if ( queryResult != null ) {
                try {
                    Statement st = queryResult.getStatement();
                    st.close();
                    queryResult.close();
                } catch (SQLException ex) {
                    logger.log(Level.SEVERE, "Exception while closing db connection of sql query: {0}", new Object[]{ex});
                }
            }
        }
    }
 
    private String[] createJoinStatement(Model model, List<ModelRelationship> modelRelationships, List<ModelTable> modelTables, List<ModelColumn> modelColumns) {

        HashMap<String, String> tableMap = new HashMap<>();        
        String insertBatchSize = new Property("insertBatchSize").getParamValue();
        if ( insertBatchSize != null ) {
            INSERT_BATCH_SIZE = Integer.parseInt(insertBatchSize);
        }
        List<ModelColumn> primaryKeys = new ArrayList();        
        if ( modelTableController == null ) {        
            modelTableController = new ModelTableJpaController();
        }    
        List<ModelTable> modelTablePrime = modelTableController.findModelTableByPrimary(model.getIdModel(), true);

        String primaryTablePhName = modelTablePrime.get(0).getPhysicalName();

        logger.log(Level.INFO, "Found primary table {0} for Model {1}. Ignoring other tables if present.", new Object[]{primaryTablePhName, modelTablePrime.get(0).getModelidModel()});

        String sqlStmtSelect = "SELECT "; 
        String sqlStmtJoin = " FROM ";
        String sqlStmtInsert = "INSERT INTO " + model.getOutputName() + " ("; 

        // extract all primary table keys
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().intValue() == modelTablePrime.get(0).getIdModelTable().intValue()) && (mc.getKeyField() == true) ) {
                primaryKeys.add(mc);
            }
        }        

        sqlStmtJoin += primaryTablePhName + " AS D3ALIAS" + tempTableGen.toString();
        tableMap.put(primaryTablePhName,"D3ALIAS" + tempTableGen.toString());
        tempTableGen++;        
        // join syntax is MySQL specific
        // construct as many JOINs as releationship records   
        // still needs branch for scalar joins
        for (ModelRelationship mr : modelRelationships) {
            if ( "1".equals(mr.getType()) ) { 
                sqlStmtJoin = sqlStmtJoin + " LEFT JOIN " 
                    + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName() + " AS D3ALIAS" + tempTableGen.toString() + " ON ";
                tableMap.put(lookupModelTables(modelTables, mr.getTable2()).getPhysicalName(),"D3ALIAS" + tempTableGen.toString());
                sqlStmtJoin = sqlStmtJoin + " D3ALIAS" + tempTableGen.toString()
                    + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                    + " = " + tableMap.get(lookupModelTables(modelTables, mr.getTable1()).getPhysicalName()) 
                    + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();
            } else {
                if ( "2".equals(mr.getType()) ) { 
                    sqlStmtJoin = sqlStmtJoin + " INNER JOIN " 
                        + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName() + " AS D3ALIAS" + tempTableGen.toString() + " ON ";
                    tableMap.put(lookupModelTables(modelTables, mr.getTable2()).getPhysicalName(),"D3ALIAS" + tempTableGen.toString());                        
                    sqlStmtJoin = sqlStmtJoin + " D3ALIAS" + tempTableGen.toString()
                        + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                        + " = " + tableMap.get(lookupModelTables(modelTables, mr.getTable1()).getPhysicalName()) 
                        + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();
                } else {                    
                    Integer tempFieldGenStart = tempFieldGen;
                    sqlStmtJoin = sqlStmtJoin + " LEFT JOIN ( SELECT " 
                        + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName()
                        + formatAtomicTableColumns(modelColumns, mr.getTable2());
                        //+ formatAtomicNonKeyTableColumns(modelColumns, mr.getTable2());
                    String temp = formatFormulaTableColumnsForScalar(modelColumns, mr.getTable2());
                    if ( !"".equals(temp) ) {
                        sqlStmtJoin += ", " + temp;
                    }
                    sqlStmtJoin = sqlStmtJoin 
                        + " FROM " + lookupModelTables(modelTables, mr.getTable2()).getPhysicalName()
                        + " GROUP BY " + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName()
                        + ") AS D3ALIAS" + tempTableGen.toString() + " ON ";

                    sqlStmtJoin = sqlStmtJoin + "D3ALIAS" + tempTableGen.toString()
                        + "." + lookupModelColumns(modelColumns, mr.getKey2()).getPhysicalName() 
                        + " = " + lookupModelTables(modelTables, mr.getTable1()).getPhysicalName() 
                        + "." + lookupModelColumns(modelColumns, mr.getKey1()).getPhysicalName();   
                    // add fields from scalar functions using their new logical/temp names
                    temp = formatFormulaTableColumns(modelColumns, mr.getTable2());
                    if ( !"".equals(temp) ) {
                        sqlStmtInsert = sqlStmtInsert + formatFormulaTableColumns(modelColumns, mr.getTable2()) + ",";
                    }
                    for ( ;tempFieldGenStart<tempFieldGen; tempFieldGenStart++) {
                        sqlStmtSelect += " D3ALIAS" + tempTableGen.toString() 
                                + ".D3ALIAS_FIELD" + tempFieldGenStart.toString() + ",";
                    }                                   
                    sqlStmtSelect = replaceNameWithAlias(sqlStmtSelect,
                                                lookupModelTables(modelTables, mr.getTable2()).getPhysicalName(),
                                                "D3ALIAS" + tempTableGen.toString());                   
                    tempFieldGen++;                 
                }
            }
            tempTableGen++;
        }           
        
        // include in final SELECT and INSERT all the insertable and atomic fields
        // except those that have formula and are not sub - used for scalar one-to-many operations
        // and those that do not belong to tables actively participating in releationship         
        List<ModelColumn> modelColumnsInsertable = filterInsertableAtomicNoScalarsModelColumns(modelColumns);
        for (ModelColumn mc : modelColumnsInsertable) {
            String aliasTableName = tableMap.get(lookupModelTables(modelTables,mc.getModelTableidModelTable()).getPhysicalName());
            if ( aliasTableName != null ) {
                sqlStmtSelect = sqlStmtSelect + " " + aliasTableName + ".";
                sqlStmtSelect = sqlStmtSelect + mc.getPhysicalName() + ",";
                sqlStmtInsert = sqlStmtInsert + mc.getName() + ",";
            }                
        }          
         
        sqlStmtSelect = Util.removeLastChar(sqlStmtSelect);   
        sqlStmtInsert = Util.removeLastChar(sqlStmtInsert);        
        tableMap.clear();
        tableMap = null;       
        return new String[]{sqlStmtSelect, sqlStmtJoin, sqlStmtInsert};   
    }
    
    private List<List<String>> extractRollupFields(List<ModelColumn> modelColumns, List<ModelTable> modelTables) {
        List<List<String>> rollupFields = new ArrayList();     
        int i = 0; 
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getInsertable() == true) && (mc.getRollup() == true)) {
                List<String> valueSet = new ArrayList();
                valueSet.add(mc.getName());
                String sqlStmt = "SELECT DISTINCT " + mc.getPhysicalName() + " FROM " + lookupModelTables(modelTables,mc.getModelTableidModelTable()).getPhysicalName();
                logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
                Query queryResult;
                try {
                    queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger);
                    String fieldValue = "";
                    List<Object[]> results = queryResult.getResultList();
                    logger.log(Level.INFO, "Rolling up field {0}. {1} values found.", new Object[]{mc.getName(),results.size()});
                    for ( int j=0; j < results.size() && j < ROLLUP_MAX; j++ ) {
                        if ( j > 0 ) {
                            fieldValue += ROLLUP_DELIMINATOR;
                        }
                        if ( results.get(j) == null ) {
                            fieldValue += "null";
                        } else {
                            fieldValue += results.get(j);
                        }
                    }
                    valueSet.add(fieldValue);               
                } catch (Exception iex) {
                    logger.log(Level.SEVERE, "MB0071 Error while extracting rollup fields. ", iex);            
                }    
                rollupFields.add(valueSet);                
            } else {
                rollupFields.add(null);     
            }         
            i++;
        }
        return rollupFields;
    }
    
    private String overwriteRollupFields(int step, int colCount, String sqlStmtInsertData, List<List<String>> modelRollupColumns) {
        int offset = step * colCount;
        String[] tokens = null;
        String resultString = sqlStmtInsertData;
        for (int i=0; i<modelRollupColumns.size(); i++) {
            if ( modelRollupColumns.get(i) != null ) {
                if ( tokens == null ) {
                    tokens = sqlStmtInsertData.split(",");
                }
                tokens[offset+i] = "'" + modelRollupColumns.get(i).get(1) + "'";
            }
        }      
        if ( tokens != null ) {
            resultString = "";
            for (String token : tokens) {
                resultString += token + ",";
            }        
        }
        return resultString;
    }
    
    private String missingDataProcessor(int currentUserId, ModelColumn mc, String tableName, ModelColumn primaryKey) {
        String resultMessage = "MB0005 Error. Missing data processing failed. ";
        Query queryResult;
        String sqlStmtData;
        String sqlStmt;
        String type;
        String originalType;
        List<ModelColumn> modelColumns = new ArrayList();            
        
        // add temp column to target OST table
        try {
            originalType = getOriginalFieldType(mc, tableName);
            type = getFormulaFieldTypeMySQL(mc,tableName);            
            sqlStmt = "ALTER TABLE " 
                    + tableName 
                    + " ADD D3temp_" + mc.getName() + " " + type;
            AuditLogger.log(currentUserId, 0, "add column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0053 Error while processing missing fields. ", ex);            
            return "MB0053 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while adding column {0} to table {1}" + new Object[]{mc.getName(),tableName}, ex);
            return resultMessage;
        }

        sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getName() + ", " + mc.getFormula() + " FROM " + tableName;
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        
        // add 3 columns to parse in the for loop below: primary Key, original and temporary        
        modelColumns.add(primaryKey);
        modelColumns.add(mc);
        ModelColumn D3temp_mc = mc;
        D3temp_mc.setPhysicalName("D3temp_" + mc.getPhysicalName());
        modelColumns.add(D3temp_mc);         
        try {
            queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger);
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0054 Error while processing missing fields. ", ex);            
            return "MB0054 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        }catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return resultMessage;
        }    
        List<Object[]> results = queryResult.getResultList();        
        if ( results.size() == 1 ) {
            // this is function which resulted in one value for all records
            // substitue function with value in the select and re-run query
            // to obtain full set of records
            sqlStmtData = getMissingDataFunctionValue(results.get(0));
            sqlStmtData = Util.removeLastChar(sqlStmtData);             
            sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getName() + ", " + sqlStmtData + " FROM " + tableName;
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
            //Query queryResult;
            try {
                queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger);
            } catch (PersistenceException ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0055 Error while processing missing fields. ", ex);            
                return "MB0055 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
            } catch (Exception ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                return resultMessage;
            }       
            results = queryResult.getResultList();
        }
       
        String sqlStmtUpdate = "UPDATE " + tableName + " SET D3temp_" + mc.getName() + "=";        

        for (Object[] obj : results) {        
            sqlStmtData = getMissingDataFinalValue(obj);
            sqlStmtData = Util.removeLastChar(sqlStmtData);         
            try {
                if ( results.size() == 1 ) {
                    // for aggregate functions update the whole column
                    DBHelper.executeSqlStatement("DeltaData",sqlStmtUpdate + sqlStmtData,logger);                    
                } else {
                    String sqlStmtWhere = " WHERE " + primaryKey.getName() + "=" + getUpdateKey(obj);
                    sqlStmtWhere = Util.removeLastChar(sqlStmtWhere);                      
                    DBHelper.executeSqlStatement("DeltaData",sqlStmtUpdate + sqlStmtData + sqlStmtWhere,logger);
                }
            } catch (PersistenceException ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0056 Error while processing missing fields. ", ex);            
                return "MB0056 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
            } catch (Exception ex) {
                Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                return resultMessage;
            }
        }  
        // rename original column to _raw column
        try {
            sqlStmt = "ALTER TABLE " + tableName 
                    + " CHANGE " + mc.getName() 
                    + " " + mc.getName() + "_raw " + originalType;
            AuditLogger.log(currentUserId, 0, "rename column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0057 Error while processing missing fields. ", ex);            
            return "MB0057 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while renaming column to {0}_raw in table {1}" + new Object[]{mc.getName(),tableName}, ex);
            return resultMessage;
        }          
        // rename temp column to orginal column
        try {
            sqlStmt = "ALTER TABLE " + tableName 
                    + " CHANGE D3temp_" + mc.getName() 
                    + " " + mc.getName() + " " + type;
            AuditLogger.log(currentUserId, 0, "rename column", sqlStmt);             
            DBHelper.executeSqlStatement("DeltaData",sqlStmt,logger);      
        } catch (PersistenceException ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0058 Error while processing missing fields. ", ex);            
            return "MB0058 SQL Error while processing missing fields.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "Exception while renaming column to {0}_raw in table {1}" + new Object[]{mc.getName(),tableName}, ex);
            return resultMessage;
        }        
        return "Ok";
    }
     
    private String secondaryVirtualFieldProcessor(int currentUserId, ModelColumn mc, String tableName, ModelColumn primaryKey) {

        String resultMessage = "MB0059 Error while processing virtual fields.";
        
        String sqlStmtUpdate = "UPDATE " + tableName + " SET " + mc.getName() + " = " + mc.getFormula();             
        try {
            DBHelper.executeSqlStatement("DeltaData",sqlStmtUpdate,logger);                    
        } catch (PersistenceException ex) {
            if ( ex.getCause().getCause().toString().contains("Invalid use of group function") ) {
                String sqlStmt = "SELECT " + primaryKey.getName() + ", " + mc.getFormula() + " FROM " + tableName;
                logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
                Query queryResult;
                try {
                    queryResult = DBHelper.executeSqlQuery("DeltaData",sqlStmt,logger);
                    List<Object[]> results = queryResult.getResultList();
                    Object[] obj = results.get(0); 
                    String sqlStmtData = getUpdateValues(obj);      
                    sqlStmtData = Util.removeLastChar(sqlStmtData);    
                    sqlStmtUpdate = "UPDATE " + tableName + " SET " + mc.getName() + " = " + sqlStmtData;
                    DBHelper.executeSqlStatement("DeltaData",sqlStmtUpdate,logger);   
                    return "Ok";
                } catch (Exception iex) {
                    Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB0061 Error while processing virtual fields. ", iex);            
                    return "MB0061 SQL Error while processing virtual fields.\n" + iex.getCause().toString() + "\n" + iex.getCause().getCause().toString();
                }         
            }
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, "MB00062 Error while processing virtual fields formula: {0}, {1}", new Object[]{mc.getFormula(), ex.getMessage()});            
            return "MB0062 SQL Error while processing virtual fields. Formula: " + mc.getFormula() + " Exception: "+ ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "MB0060 SQL Error while processing virtual fields. Formula: " + mc.getFormula() + " Exception: "+ ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();
        } 
        return "Ok";
    }
   
    private String replaceNameWithAlias(String sqlStatement, String name, String alias) {
        String result = sqlStatement.replace(name, alias);
        return result;
    }  
    
    private String formatAtomicNonKeyTableColumns(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getInsertable() == true) 
                    && (mc.getKeyField() == false && (("".equals(mc.getFormula()))) || (mc.getFormula() == null)) ) {
                sqlStmtColumns += "," + mc.getPhysicalName();
            }
        }  
        return sqlStmtColumns;
    }

    private String formatAtomicTableColumns(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getKeyField() == false && (("".equals(mc.getFormula()))) || (mc.getFormula() == null)) ) {
                sqlStmtColumns += "," + mc.getPhysicalName();
            }
        }  
        return sqlStmtColumns;
    }
    
    private String formatFormulaTableColumnsForScalar(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getInsertable() == true) && (mc.getKeyField() == false) 
                    && (!"".equals(mc.getFormula())) && (mc.getFormula() != null)) {
                sqlStmtColumns += mc.getFormula() + " AS D3ALIAS_FIELD" + tempFieldGen.toString() + ",";
                tempFieldGen++;
            }
        }  
        return Util.removeLastChar(sqlStmtColumns);        
    }
   
    private String formatFormulaTableColumns(List<ModelColumn> modelColumns, Integer tableId) {
        String sqlStmtColumns ="";
        for (ModelColumn mc : modelColumns) {
            if ( (mc.getModelTableidModelTable().compareTo(tableId) == 0) && (mc.getAtomic() == true) 
                    && (mc.getKeyField() == false) && (!"".equals(mc.getFormula())) && (mc.getFormula() != null)) {
                sqlStmtColumns += mc.getName() + ",";
            }
        }  
        return Util.removeLastChar(sqlStmtColumns);        
    }
    
    private String getInsertValues(Object[] obj) {
        String sqlStmtInsertData = "";
        for (int ii=0; ii< obj.length; ii++) {
            sqlStmtInsertData += getInsertValuesSingleRow(obj[ii]);
        }   
        return sqlStmtInsertData;
    }
    
    /* The difference between this method and getInsertValues is that first column (primary key) is skipped */
    private String getUpdateValues(Object[] obj) {
        String sqlStmtInsertData = "";
        for (int ii=1; ii< obj.length; ii++) {
            sqlStmtInsertData += getInsertValuesSingleRow(obj[ii]);
        }   
        return sqlStmtInsertData;
    }

    /* The logic for missing data substitution */
    private String getMissingDataFinalValue(Object[] obj) {
        // check if column had data orgiinally and use, otherwise substitute
        String oldData = getInsertValuesSingleRow(obj[1]);  
        String newData = getInsertValuesSingleRow(obj[2]);  
        if ( "NULL,".equals(oldData) ) {
            return newData;
        }
        return oldData;
    }

    /* Retrieve value retured by function formula */
    private String getMissingDataFunctionValue(Object[] obj) {
        return getInsertValuesSingleRow(obj[2]);  
    }
    
    /* The difference between this method and getInsertValues is that only first column (primary key) is fetched */
    private String getUpdateKey(Object[] obj) {
        return getInsertValuesSingleRow(obj[0]); 
    }    
    
    private String getInsertValuesSingleRow(Object obj) {
        String sqlStmtInsertData = "";
        if ( obj == null || obj.equals("") ) {
            //if ( mc.getDefaultValue() == null ) {
                sqlStmtInsertData += "NULL,";
            //} else {
            //    sqlStmtInsertData += mc.getDefaultValue() + ",";
            //}
        } else {
            

            if (obj instanceof String) {
              //logger.log(Level.INFO, "Processing String field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += "'" + obj + "',";
            }
            else if (obj instanceof Boolean) {
              //logger.log(Level.INFO, "Processing Boolean field {0}", new Object[]{obj[ii]});
                if ( obj.equals(true) ) {
                    sqlStmtInsertData += "1,";
                } else {
                    sqlStmtInsertData += "0,";
                }
            }        
            else if (obj instanceof Byte) {
              //logger.log(Level.INFO, "Processing Byte field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }              
            else if (obj instanceof Short) {
              //logger.log(Level.INFO, "Processing Short field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }           
            else if (obj instanceof Integer) {
              //logger.log(Level.INFO, "Processing Integer field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }    
            else if (obj instanceof BigInteger) {
              //logger.log(Level.INFO, "Processing BigInteger field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            } 
            else if (obj instanceof BigDecimal) {
              //logger.log(Level.INFO, "Processing BigDecimal field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }     
            else if (obj instanceof Float) {
              //logger.log(Level.INFO, "Processing Float field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }              
            else if (obj instanceof Double) {
              //logger.log(Level.INFO, "Processing Double field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += obj + ",";                    
            }  
            else if (obj instanceof Character) {
              //logger.log(Level.INFO, "Processing Character field {0}", new Object[]{obj[ii]});
              sqlStmtInsertData += "'" + obj + "',";
            }    
            else if (obj instanceof Date) {
              //logger.log(Level.INFO, "Processing Date field {0}", new Object[]{obj[ii]});
              //String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj);
              String date =  String.format("%1$TY-%1$Tm-%1$Td %1$TH:%1$TM:%1$TS", (Date)obj); 
              //logger.log(Level.INFO, "Processing Date field after re-formatting {0}", new Object[]{date});
              sqlStmtInsertData += "'" + date + "',";
            }                   
            else if (obj instanceof Timestamp) {
              //logger.log(Level.INFO, "Processing Timestamp field {0}", new Object[]{obj[ii]});
              //String date = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj);
              String date =  String.format("%1$TY-%1$Tm-%1$Td %1$TH:%1$TM:%1$TS", (Timestamp)obj);    
              //logger.log(Level.INFO, "Processing Timestamp field after re-formatting {0}", new Object[]{date});
              sqlStmtInsertData += "'" + date + "',";
            } 
            
            //Adding for handelling unsupported datatypes
            else
            {
                sqlStmtInsertData += "'" + obj + "',";
            }
        }
        return sqlStmtInsertData;
    }    
  
    private boolean saveAllCachedMetadata(Model model, String metadata) throws Exception {
        if ( metadataController == null ) {
           metadataController = new D3MetadataJpaController();
        }        
        D3Metadata md = new D3Metadata();
        md.setIdModel(model.getIdModel());
        md.setName(model.getName());
        md.setData(metadata);
        md.setActive(Boolean.TRUE);
        metadataController.createObjects(md, 1);      
        return true;
    }                          

    private String getAllCachedMetadata(Model model) {
        String dataString = "";      
        if ( metadataController == null ) {
           metadataController = new D3MetadataJpaController();
        }          
        try {
            List<D3Metadata> mdList = metadataController.findD3MetadataByModelId(model.getIdModel());
            dataString = mdList.get(0).getData();
        } catch (Exception ex) {
            Logger.getLogger(ModelServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataString;
    }
    
    private String getCachedMetadata(Model model) {
        String dataString = "[";
        if ( modelColumnController == null ) {
           modelColumnController = new ModelColumnJpaController();
        }            
        List<ModelTable> tables = getModelTables(model.getIdModel());
        for (int j =0; j<tables.size(); j++) {
            if ( j != 0 ) {
               dataString += ",";
            }     
            dataString = dataString + "{\"text\": \""+ tables.get(j).getName() + "\",\"qtip\":\"table\",\"leaf\":false,\"children\":[";          
            List<ModelColumn> columns = new ArrayList();            
            columns.addAll(modelColumnController.findModelColumnByTable(tables.get(j).getIdModelTable()));
            
            for (int i = 0; i < columns.size(); i++) {
                if ( columns.get(i).getAtomic() == true ) {
                    if ( i != 0 ) {
                        dataString += ",";
                    }
                    //logger.log(Level.INFO, "SQL Name: "+ metadata.getColumnLabel(i + 1)+ ", Type: "+ metadata.getColumnTypeName(i + 1));
                    dataString = dataString + "{\"leaf\":true,\"text\":\"" + columns.get(i).getPhysicalName() 
                            + "\",\"type\":\"" + columns.get(i).getType().toUpperCase()
                            + "\",\"size\":\""
                            + "\",\"precision\":\""
                            + "\",\"scale\":\""                  
                            + "\"}"; 
                }
            }
            dataString += "]}";                  
        }
        dataString += "]";      
        return dataString;
    }
    
    private boolean isInRelationship(ModelColumn mc, List<ModelRelationship> relationships) {
        for (int i=0; i<relationships.size(); i++) {
            if ( Objects.equals(mc.getModelTableidModelTable(), relationships.get(i).getTable1()) ||
                 Objects.equals(mc.getModelTableidModelTable(), relationships.get(i).getTable2())   ) {
                return true;
            }
        }
        return false;
    }
    
    private String findColumnType(List<ModelColumn> mc,String columnName) {
        for (ModelColumn modelColumn : mc) {   
            if ( modelColumn.getName() == null ? columnName == null : modelColumn.getName().equals(columnName) )
                return modelColumn.getType();
        } 
        return "";
    }
    
    private String findColumnClass(List<ModelColumn> mc,String columnName) {
        for (ModelColumn modelColumn : mc) {   
            if ( modelColumn.getName() == null ? columnName == null : modelColumn.getName().equals(columnName) )
                return modelColumn.getFieldClass();
        } 
        return "";
    }    
    
    private String findColumnKind(List<ModelColumn> mc,String columnName) {
        for (ModelColumn modelColumn : mc) {   
            if ( modelColumn.getName() == null ? columnName == null : modelColumn.getName().equals(columnName) )
                return modelColumn.getFieldKind();
        } 
        return "";
    }        
    
    /* delegete this work to common method in admin service */
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        if (adminService == null) {
            adminService = new AdminServiceImpl();
        }
        return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
    }    

   
}
