/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server;

import com.ceri.delta.entity.logregr.Logregr;
import com.ceri.delta.entity.logregr.LogregrDate;
import com.ceri.delta.entity.logregr.LogregrDateComp;
import com.ceri.delta.entity.logregr.LogregrDateFieldComp;
import com.ceri.delta.entity.logregr.LogregrField;
import com.ceri.delta.entity.model.ModelColumn;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.logregr.LogregrDateJpaController;
import com.ceri.delta.jpa.logregr.LogregrFieldJpaController;
import com.ceri.delta.jpa.logregr.LogregrJpaController;
import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.process.ProcessJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.server.exceptions.EntityExistsException;
import com.ceri.delta.server.exceptions.StudyParameterException;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Coping Systems Inc.
 */
public class LogregrServiceImpl {

    private static final Logger logger = Logger.getLogger(LogregrServiceImpl.class.getName());
    private static LogregrDateJpaController logregrdateController;
    private static LogregrFieldJpaController logregrfieldController;
    private static LogregrJpaController logregrController;
    private static StudyJpaController studyController;
    private static ModelColumnJpaController modelColumnController;
    private static ProcessJpaController processController;
    private static AdminServiceImpl adminService;

    public LogregrServiceImpl() {
        if (logregrController == null) {
            logregrController = new LogregrJpaController();
        }
        if (logregrfieldController == null) {
            logregrfieldController = new LogregrFieldJpaController();
        }
        if (logregrdateController == null) {
            logregrdateController = new LogregrDateJpaController();
        }
        if (studyController == null) {
            studyController = new StudyJpaController();
        }
        if (modelColumnController == null) {
            modelColumnController = new ModelColumnJpaController();
        }
        if (processController == null) {
            processController = new ProcessJpaController();
        }
    }

    public String createLogregr(String JSONString, Integer currentOrgId, Integer currentUserId) {
        String result = "Logregr Objects(s) not created";
        Object subject = null;
        String dataString = "";
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                logger.log(Level.INFO, "Creating Object {0} " + Integer.toString(i), logregrController.getObjectType());
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, new Logregr().getClass());               
                subject = logregrController.createObjects(subject, currentOrgId, currentUserId);
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(subject).toString();
                LogregrField entField = new LogregrField(); 
                entField.setIdLogregr(((Logregr)subject).getIdLogregr());
                entField.setActive(Boolean.TRUE);
                entField.setName("intercept");
                entField.setValue("C");
                entField.setModelColumnidRisk(0);
                entField = createLogregrField(entField, currentUserId);
                LogregrDate entDate = new LogregrDate(); 
                entDate.setIdLogregr(((Logregr)subject).getIdLogregr());  
                entDate.setActive(Boolean.TRUE);
                entDate.setValue("");
                entDate.setDescription("default");
                entDate.setName("");
                entDate.setIdLogregrField(entField.getIdLogregrField());
                entDate.setModelColumnidSequencer(((Logregr)subject).getModelColumnidSequencer());
                entDate = createLogregrDate(entDate, currentUserId);
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Object: {0} {1}", new Object[]{logregrController.getObjectType(), ex});
            result = "LR0017 Exception while creating new "  + logregrController.getObjectType() + ": " + ex.getMessage();
        }   
        return wrapAPI(logregrController.getObjectType(), dataString, "Object(s) created", result);
    }
         
    public String getLogregrFields(String payload) {

        String dataString = "";
        String result = "No fields found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();
        JSONArray JSONArrayObjects = null;

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Fields for logregr Formula {0}.", Integer.toString(idLogregr));
            dataString = getLogregrFieldJSON(idLogregr);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression object into JSON", ex.getMessage());
            result = "LR0001 Exception parsing Logistic Regression object into JSON";
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                JSONArrayObjects = new JSONArray(dataString);
                success.put("success", true);
                status.put("message", Integer.valueOf(JSONArrayObjects.length()) + " formula fields retrieved");
                success.put("totalCount", Integer.valueOf(JSONArrayObjects.length()));
                success.put("logregrfields", JSONArrayObjects);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "LR0002 JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }

    public String createLogregrFields(String JSONString, Integer currentUserId) {
        String result = "Field for logregr formula not created";
        LogregrField entity = null;
        String dataString = "";

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int ii = 0; ii < anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Field {0} for Logistic Regression Formula", anArray.getJSONObject(ii).getString("name"));
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                entity = gson.fromJson(jString, LogregrField.class);
                if (ii > 0) {
                    dataString = dataString.concat(",");
                }
                //LogregrField ent = createLogregrFieldString(entity, currentUserId);
                dataString = dataString + createLogregrFieldString(entity, currentUserId);
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new field for logregr formula: {0} {1}", new Object[]{entity.getName(), ex});
            result = "LR0003 Exception while creating new Formula:" + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Logistic Regression Formula field created", result);
    }

    public String updateLogregrFields(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not updated";
        String dataString = "";
        LogregrField entity;
        int idLogregr = 0, idModelColumn = 0;

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int i = 0; i < anArray.length(); i++) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(i).toString();
                entity = gson.fromJson(jString, LogregrField.class);
                idModelColumn = entity.getModelColumnidRisk();
                idLogregr = entity.getIdLogregr();
                logger.log(Level.INFO, "Updating Field {0} for logregr Formula {1}", new Object[]{Integer.toString(idModelColumn), Integer.toString(idLogregr)});
                entity = updateLogregrField(entity, currentUserId);
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(entity).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregr), ex});
            result = "LR0004 JSON exception while updating releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Logistic Regression Formula field updated", result);
    }

    public String deleteLogregrField(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not deleted";
        String dataString = "";
        int idLogregrField = 0;

        try {
            JSONObject JSONContentRoot = new JSONObject(JSONString);
            idLogregrField = JSONContentRoot.getInt("idLogregrField");
            logger.log(Level.INFO, "Deleting Formula Field {0}", new Object[]{Integer.toString(idLogregrField)});
            LogregrField ent = deleteLogregrField(idLogregrField, currentUserId);
            dataString = dataString + new JSONObject(ent).toString();
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregrField), ex});
            result = "LR0005 JSON exception while deleting releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Logistic Regression Formula field deleted", result);
    }

    public String deleteLogregrFields(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula field not deleted";
        String dataString = "";
        int idLogregrField = 0;

        try {
            JSONArray objectArray = new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i = 0; i < objectArray.length(); i++) {
                JSONObject JSONContentRoot = objectArray.getJSONObject(i);
                idLogregrField = JSONContentRoot.getInt("idLogregrField");
                LogregrField ent = deleteLogregrField(idLogregrField, currentUserId);
                if (sepCounter++ > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(ent).toString();
                List<LogregrDate> aList = logregrdateController.findLogregrDateByLogregrFieldId(idLogregrField);
                for (LogregrDate t : aList) {
                    logger.log(Level.INFO, "Deleting Formula Date {0}", new Object[]{Integer.toString(t.getIdLogregrDate())});
                    deleteLogregrDate(t.getIdLogregrDate(), currentUserId);
                }
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregrField), ex});
            result = "LR0006 JSON exception while deleting releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Filter Formulas deleted", result);
    }
    
//------------------------------------------------------------------------------ Logregr Date/Value
    public String getLogregrDates(String payload) {

        String dataString = "";
        String result = "No dates found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();
        JSONArray JSONArrayObjects = null;

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Dates for logregr Formula {0}.", Integer.toString(idLogregr));
            dataString = getLogregrDateJSON(idLogregr);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression object into JSON", ex.getMessage());
            result = "LR0007 Exception parsing Logistic Regression object into JSON";
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                JSONArrayObjects = new JSONArray(dataString);
                success.put("success", true);
                status.put("message", JSONArrayObjects.length() + " formula fields retrieved");
                success.put("totalCount", Integer.valueOf(JSONArrayObjects.length()));
                success.put("logregrdates", JSONArrayObjects);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "LR0008 JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }

    public String createLogregrDates(String JSONString, Integer currentUserId) {
        String result = "Date for logregr formula not created";
        LogregrDate entity = null;
        String dataString = "";

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int ii = 0; ii < anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Date/Value {0} for Logistic Regression Formula", anArray.getJSONObject(ii).getString("name"));
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                entity = gson.fromJson(jString, LogregrDate.class);
                LogregrDate ent = createLogregrDate(entity, currentUserId);
                if (ii > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(ent).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new date for logregr formula: {0} {1}", new Object[]{entity.getName(), ex});
            result = "LR0009 Exception while creating new Formula:" + ex.getMessage();
        }
        return wrapAPI("logregrdates", dataString, "Logistic Regression Formula field created", result);
    }

    private LogregrDate updateLogregrDate(LogregrDate ent, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.INFO, "User {0} updating relationship between date {1} and logregr formula {2}",
                new Object[]{Integer.toString((currentUserId)),
                    Integer.toString((ent.getIdLogregrDate())),
                    Integer.toString(ent.getIdLogregr())});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        logregrdateController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrDate(), "update", ent);
        return ent;
    }

    public String updateLogregrDates(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not updated";
        String dataString = "";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();      
        JSONArray JSONArrayObjects = null;        
        LogregrDate entity;
        int idLogregr = 0, idLogregrDate = 0;

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int i = 0; i < anArray.length(); i++) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(i).toString();
                entity = gson.fromJson(jString, LogregrDate.class);
                idLogregrDate = entity.getIdLogregrDate();
                idLogregr = entity.getIdLogregr();
                logger.log(Level.INFO, "Updating Date {0} for logregr Formula {1}", new Object[]{Integer.toString(idLogregrDate), Integer.toString(idLogregr)});
                entity = updateLogregrDate(entity, currentUserId);
            }
            logger.log(Level.INFO, "Retrieving Dates for logregr Formula {0}.", Integer.toString(idLogregr));
            dataString = getLogregrDateJSON(idLogregr);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregr), ex});
            result = "LR0010 JSON exception while updating releationship for logregr formula: " + ex.getMessage();
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                JSONArrayObjects = new JSONArray(dataString);
                success.put("success", true);
                status.put("message", JSONArrayObjects.length() + " formula fields retrieved");
                success.put("totalCount", Integer.valueOf(JSONArrayObjects.length()));
                success.put("logregrdates", JSONArrayObjects);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "LR0008 JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }
    
    public String updateLogregrDates_old(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula not updated";
        String dataString = "";
        LogregrDate entity;
        int idLogregr = 0, idLogregrDate = 0;

        try {
            JSONArray anArray = new JSONArray(JSONString);
            for (int i = 0; i < anArray.length(); i++) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(i).toString();
                entity = gson.fromJson(jString, LogregrDate.class);
                idLogregrDate = entity.getIdLogregrDate();
                idLogregr = entity.getIdLogregr();
                logger.log(Level.INFO, "Updating Date {0} for logregr Formula {1}", new Object[]{Integer.toString(idLogregrDate), Integer.toString(idLogregr)});
                entity = updateLogregrDate(entity, currentUserId);
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(entity).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregr), ex});
            result = "LR0010 JSON exception while updating releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrdates", dataString, "Logistic Regression Formula date updated", result);
    }

    public String deleteLogregrDates(String JSONString, Integer currentUserId) {
        String result = "Logistic Regression Formula date not deleted";
        String dataString = "";
        int idLogregrDate = 0;

        try {
            JSONArray objectArray = new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i = 0; i < objectArray.length(); i++) {
                JSONObject JSONContentRoot = objectArray.getJSONObject(i);
                idLogregrDate = JSONContentRoot.getInt("idLogregrDate");
                logger.log(Level.INFO, "Deleting Formula Date {0}", new Object[]{Integer.toString(idLogregrDate)});
                LogregrDate ent = deleteLogregrDate(idLogregrDate, currentUserId);
                if (sepCounter++ > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(ent).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting releationship for logregr formula: {0} {1}", new Object[]{Integer.toString(idLogregrDate), ex});
            result = "LR0011 JSON exception while deleting releationship for logregr formula: " + ex.getMessage();
        }
        return wrapAPI("logregrfields", dataString, "Filter Formulas deleted", result);
    }

    private LogregrDate deleteLogregrDate(int idLogregrField, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship {1} between field and formula", new Object[]{Integer.toString((currentUserId)), Integer.toString((idLogregrField))});

        LogregrDate ent = new LogregrDate(idLogregrField);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        logregrdateController.destroy(idLogregrField);
        AuditLogger.log(currentUserId, ent.getIdLogregrDate(), "delete", ent);
        return ent;
    }

    public String getLRFString(String payload, Integer currUserId) {

        String dataString = "";
        String result = "No fields found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();
        int colMax = 0;
        int rowMax = 0;

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Fields for logregr Formula {0}.", Integer.toString(idLogregr));

            List<LogregrField> rows = logregrfieldController.findLogregrFieldByLogregrId(idLogregr);
            List<LogregrDate> columns = logregrdateController.findLogregrDateByLogregrId(idLogregr);
            Collections.sort(columns, new LogregrDateComp()); // sort by dates which become header names in UI
            Collections.sort(columns, new LogregrDateFieldComp()); // sort by LogregrField ID

            if ((columns != null) && !columns.isEmpty()) {
                colMax = columns.size();
                logger.log(Level.INFO, "Logistic Regression Formula {0} has dates. Retrieving {1} formula column data.", new Object[]{Integer.toString(idLogregr), Integer.toString(colMax)});
                if ((rows != null) && !rows.isEmpty()) {
                    rowMax = rows.size();
                    logger.log(Level.INFO, "Logistic Regression Formula {0} has fields. Retrieving {1} formula row data.", new Object[]{Integer.toString(idLogregr), Integer.toString(rowMax)});
                    for (int j = 0; j < colMax / rowMax; j++) {
                        Integer sequencer = columns.get(j * rowMax).getModelColumnidSequencer();
                        if (sequencer == 0) {
                            dataString = "LR0017 Manual formula: sequencer field not defined for the study.";
                            break;
                        }
                        ModelColumn mc = modelColumnController.findModelColumn(sequencer);
                        //dataString += "\"" + mc.getName() + "\" >= " + columns.get(j).getName() + "<br>";
                        dataString += columns.get(j).getDescription() + "<br>";                        
                        if ( j + 1 < colMax / rowMax ) {
                            // it is not the last column
                            dataString += "\"" + mc.getName() + "\" < " + columns.get(j+1).getName() + "<br>";
                        }
                        /*if (j == 0) {
                            dataString += "\"" + mc.getName() + "\" < " + columns.get(j).getName() + "<br>";
                        } else {
                            dataString += "\"" + mc.getName() + "\" >= " + columns.get(j - 1).getName()
                                    + " < " + columns.get(j).getName() + "<br>";
                        }*/
                        for (int i = 0; i < rowMax; i++) {
                            if (i > 0) {
                                dataString = dataString.concat(" + ");
                            }
                            LogregrField theRow = getLogregrFieldById(rows, columns.get(i * colMax / rowMax + j).getIdLogregrField());
                            if (theRow.getName().equals("intercept")) {
                                dataString = dataString + columns.get(i * colMax / rowMax + j).getValue();
                            } else {
                                dataString = dataString + columns.get(i * colMax / rowMax + j).getValue() + " * " + theRow.getName();
                            }
                        }
                        dataString += "<br><br>";
                    }
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression Formula", ex.getMessage());
            result = "LR0012 Exception parsing Logistic Regression Formula: " + ex.getMessage();
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                success.put("success", true);
                status.put("message", dataString);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }

    public String cloneLRF(String payload, Integer currUserId) {
        String dataString = "";
        String result = "No fields found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            JSONObject requestContent = new JSONObject(payload);
            JSONArray anArray = requestContent.getJSONArray("logregrs");
            JSONObject logregr = (JSONObject) anArray.get(0);
            Integer idLogregr = logregr.getInt("idLogregr");
            logger.log(Level.INFO, "Retrieving Fields for logregr Formula {0}.", Integer.toString(idLogregr));
            Logregr lr = logregrController.findLogregr(idLogregr);
            Logregr lrNew = new Logregr();
            lrNew.setName(lr.getName() + " Clone");
            lrNew.setIdOrganization(lr.getIdOrganization());
            lrNew.setActive(Boolean.TRUE);
            lrNew.setIdModel(lr.getIdModel());
            lrNew.setIdProcess(0);
            lrNew.setIdStudy(lr.getIdStudy());
            lrNew.setIntervalData(lr.getIntervalData());
            lrNew.setModelColumnidSequencer(lr.getModelColumnidSequencer());
            lrNew.setDescription(lr.getDescription());
            lrNew.setOriginalLogregrId(lr.getIdLogregr());
            lrNew.setStatus("cloned");
            lrNew = (Logregr) logregrController.createObjects(lrNew, currUserId);
            List<LogregrField> rows = logregrfieldController.findLogregrFieldByLogregrId(idLogregr);
            List<LogregrDate> columns = logregrdateController.findLogregrDateByLogregrId(idLogregr);
            Collections.sort(columns, new LogregrDateComp()); // sort by dates which become header names in UI
            Collections.sort(columns, new LogregrDateFieldComp()); // sort by LogregrField ID
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            HashMap<Integer, Integer> modelColumnLookup = new HashMap();
            for (LogregrField lrf : rows) {
                LogregrField lrfNew = new LogregrField();
                lrfNew.setName(lrf.getName());
                lrfNew.setActive(Boolean.TRUE);
                lrfNew.setDescription(lrf.getDescription());
                lrfNew.setModelColumnidRisk(lrf.getModelColumnidRisk());
                lrfNew.setValue(lrf.getValue());
                lrfNew.setIdLogregr(lrNew.getIdLogregr());
                lrfNew.setCreatedTS(currentTimestamp);
                lrfNew.setCreatedBy(currUserId);                 
                lrfNew = logregrfieldController.create(lrfNew);
                modelColumnLookup.put(lrf.getIdLogregrField(), lrfNew.getIdLogregrField());
            }
            for (LogregrDate lrd : columns) {
                LogregrDate lrdNew = new LogregrDate();
                lrdNew.setName(lrd.getName());
                lrdNew.setActive(Boolean.TRUE);
                lrdNew.setValue(lrd.getValue());
                lrdNew.setDescription(lrd.getDescription());
                lrdNew.setModelColumnidSequencer(lrd.getModelColumnidSequencer());
                lrdNew.setIdLogregrField(modelColumnLookup.get(lrd.getIdLogregrField()));
                lrdNew.setIdLogregr(lrNew.getIdLogregr());
                lrdNew.setCreatedTS(currentTimestamp);
                lrdNew.setCreatedBy(currUserId);                   
                logregrdateController.create(lrdNew);
            }
            //dataString = new JSONObject(lrNew).toString();
            dataString = "Logistic Regression Formula cloned";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Logistic Regression Formula", ex.getMessage());
            result = "LR0013 Exception parsing Logistic Regression Formula: " + ex.getMessage();
        }
        try {
            if ("".equals(dataString)) {
                success.put("success", false);
                status.put("message", result);
            } else {
                success.put("success", true);
                status.put("message", dataString);
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [logregr.getLogregrFields]: ", ex);
            result = "JSON error in WS method [logregr.getLogregrFields]";
        }
        return result;
    }
    
    public String getLRFIntervals(String studyId, String type, String chunking, Integer currentUserId) {
            
        String responseString = "";
        Study s = (Study) studyController.findStudy(Integer.parseInt(studyId));
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());
        responseString = "[";
        boolean isChunking = false;
        if ( chunking == "true" ) {
            isChunking = true;
        }
         
        List<String> chunks = getCalendarChunks(isChunking, type, s.getStartTS(), s.getEndTS());
        for (int i=0; i< chunks.size(); i++) {
            responseString += "{\"interval\":\"" + chunks.get(i) + "\",";
            responseString += "\"intervalQuery\":\"" + mc.getName() + " >= '" + chunks.get(i) + "'";
            if ( i + 1 < chunks.size() ) {
                // it is not the last column
                responseString += " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'\"},";
            } else {
                responseString += "\"}";
            }
        }
        return responseString + "]";
    }    

    public String deleteLogregr(String JSONString, Integer currentUserId) {
        String result = "Objects(s) not deleted";
        String dataString = "";
        Logregr subject = new Logregr();
        try {
            JSONArray objectArray = new JSONArray(JSONString);
            for (int i = 0; i < objectArray.length(); i++) {
                logger.log(Level.INFO, "Deleting Object {0} " + Integer.toString(i), logregrController.getObjectType());
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = objectArray.getJSONObject(i).toString();
                subject = gson.fromJson(jString, subject.getClass());
                List<LogregrField> lrf = logregrfieldController.findLogregrFieldByLogregrId(((Logregr) subject).getIdLogregr());
                deleteLogregrFields(lrf);
                List<LogregrDate> lrd = logregrdateController.findLogregrDateByLogregrId(((Logregr) subject).getIdLogregr());
                deleteLogregrDates(lrd);
                logregrController.deleteObject(subject, currentUserId);
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(subject).toString();
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Logistic Regression object: {0}", ex.getMessage());
            result = "LR0014 Exception while deleting Logistic Regression object: " + ex.getMessage();
        }
        return wrapAPI(logregrController.getObjectType(), dataString, "Object(s) deleted", result);
    }
    
    // this method prepares time periods for Logistic Regression formula 
    // based on Study start and end dates and chunking flag
    // chunking is done in month/3-month/12-month increments in this implementation
    // this method is not used as of version 1.87 in favor of getCalendarChunks()
    private List<String> getDateChunks(boolean chunking, String type, Timestamp tsStart, Timestamp tsEnd) {
        List<String> stringChunks = new ArrayList();
        String startYear = new SimpleDateFormat("yyyy").format(tsStart);        
        String monthDay = new SimpleDateFormat("MM/dd").format(tsStart);
        String startMonth = new SimpleDateFormat("MM").format(tsStart);
        String startDay = new SimpleDateFormat("dd").format(tsStart);
        int intervalYearStart = Integer.parseInt(startYear);    
        int intervalMonthStart = Integer.parseInt(startMonth);    
        if ( type == null ) {
            type = "month";
            logger.log(Level.SEVERE, "Type for chunking was not provided. Dafaulting to month");
        }
        type= type.toLowerCase();
        
        if ( chunking == false ) {
            // start with study start date
            stringChunks.add(new SimpleDateFormat("yyyy/MM/dd").format(tsStart));       
            switch ( type ) {
                case "year":
                    // add one year
                    intervalYearStart++;
                    stringChunks.add(Integer.toString(intervalYearStart) + "/" + monthDay);   
                    break;
                case "quarter":
                    // add two months
                    intervalMonthStart += 2;    
                case "month":
                    // add one month
                    intervalMonthStart += 1;
                    if ( intervalMonthStart > 12 ) {
                        intervalMonthStart = intervalMonthStart % 12;
                        intervalYearStart++; 
                    }                    
                    stringChunks.add(Integer.toString(intervalYearStart) + "/" + Integer.toString(intervalMonthStart)
                        + "/" + startDay);   
                    break;                     
            }
        } else {
            switch ( type ) {
                case "year":            
                    int start = Integer.parseInt(startYear);
                    String endYear = new SimpleDateFormat("yyyy").format(tsEnd);
                    int end = Integer.parseInt(endYear);
                    for (int i=0; start <= end; start++, i++) {
                        stringChunks.add(Integer.toString(start) + "/" + monthDay);
                    }
                    break;
            }
        }
        return stringChunks;
    }
  
    // this method prepares time periods for Logistic Regression formula 
    // based on Study start and end dates and chunking flag
    // chunking is done by calendar month, quarter and year in this implementation
    private List<String> getCalendarChunks(boolean chunking, String type, Timestamp tsStart, Timestamp tsEnd) {
        List<String> stringChunks = new ArrayList();     
        String startYear = new SimpleDateFormat("yyyy").format(tsStart);        
        String startMonth = new SimpleDateFormat("MM").format(tsStart);
        String startDay = new SimpleDateFormat("dd").format(tsStart);
        if ( type == null ) {
            type = "month";
            logger.log(Level.SEVERE, "Type for chunking was not provided. Dafaulting to month");
        }
        type = type.toLowerCase();
        
        if ( chunking == false ) {
            // start with study start date
            stringChunks.add(new SimpleDateFormat("yyyy/MM/dd").format(tsStart));      
            String newDateString = calcNewStartDate(type, startYear, startMonth, startDay);
            stringChunks.add(newDateString);   
        } else { // this is not tested/used yet
            switch ( type ) {
                case "year":            
                    int start = Integer.parseInt(startYear);
                    String endYear = new SimpleDateFormat("yyyy").format(tsEnd);
                    int end = Integer.parseInt(endYear);
                    for (int i=0; start <= end; start++, i++) {
                        stringChunks.add(Integer.toString(start) + "/01/01");
                    }
                    break;
            }
        }
        return stringChunks;
    }
 
    // this method is part of REST service, used to calculate "on-the-fly" start of LR Step 2 start date
    public String calculateDate(String requestData) throws JSONException {  
        JSONObject input = new JSONObject(requestData);
        String type = input.getString("periodType");
        String date = input.getString("date");
        if ( type == null ) {
            type = "month";
            logger.log(Level.SEVERE, "Type for chunking was not provided. Dafaulting to month");
        }          
        String startYear = date.substring(0,4);        
        String startMonth = date.substring(5,7);
        String startDay = date.substring(8,10);
        return calcNewStartDate(type, startYear, startMonth, startDay);
    }
    
    //-------------------------------------------------------------------------- LRF parsing, etc
    // this method is called before submitting job to calculate LRF
    // it (should) create logergDate for constant/intercept row for selected Interval
    // right now it defaults to first year with no chunking
    public List<LogregrDate> calculateLRFTrainingPeriod(Study s, com.ceri.delta.entity.process.Process p, Integer currentUserId) {
            
        List<LogregrDate> lrdList = new ArrayList();
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());

        try {
            List<String> chunks = getCalendarChunks(s.getIsChunking(), s.getChunkingBy(),  s.getStartTS(), s.getEndTS());
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            for (int i=0; i< chunks.size(); i++) {
                LogregrDate lrd = new LogregrDate();
                lrd.setActive(Boolean.TRUE);
                lrd.setModelColumnidSequencer(s.getIdDate());
                lrd.setValue("0.0");    
                lrd.setCreatedTS(currentTimestamp);
                lrd.setCreatedBy(currentUserId);        
                lrd.setUpdatedTS(currentTimestamp);
                lrd.setUpdatedBy(currentUserId);                    
                lrd.setName(chunks.get(i));
                String sqlWhereClause = mc.getName() + " >= '" + chunks.get(i) + "'";
                if ( i + 1 < chunks.size() ) {
                    // it is not the last column
                    sqlWhereClause += " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'";
                }          
                lrd.setDescription(sqlWhereClause);
                lrdList.add(lrd);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while persisting LRF Dates for process: {0} {1}", new Object[]{p.getName(), ex.getMessage()});
        }
        return lrdList;
    }

    // method evoked when response is recieved from external stat package
    // first it moves results from Process to existing Logregr or creates a new Logregr
    
    public String saveLRFResults(com.ceri.delta.entity.process.Process p, Study s, String lrfResults) {

        String lrfName = "LRF for " + p.getName();
        try {
            ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());            
            // search by processId replaced with search by name and processId to seperate steps
            List<Logregr> lrList = logregrController.findLogregrByProcessId(p.getIdProcess());
            //List<Logregr> lrList = logregrController.findLogregrByNameAndProcessId(lrfName, p.getIdProcess());
            Logregr lr;
            if (lrList.size() > 0) {
                //lr = lrList.get(lrList.size()-1); // get the last one
                lr = lrList.get(0); // assume one
                lr.setFormula(lrfResults);
                lr = (Logregr) logregrController.updateObjects(lr, p.getIdOrganization(), 0);
            } else {
                lr = new Logregr();
                lr.setIdProcess(p.getIdProcess());
                lr.setIdModel(p.getIdModel());
                lr.setIdStudy(p.getIdStudy());
                lr.setName(lrfName);
                lr.setDescription(p.getName());
                lr.setFormula(lrfResults);
                lr.setModelColumnidSequencer(mc.getIdModelColumn());
                lr.setActive(Boolean.TRUE);
                lr.setOriginalLogregrId(0);
                lr.setStatus("auto");
                lr = (Logregr) logregrController.createObjects(lr, p.getIdOrganization(), 0); 
            }
            if ( "Ok".equals(parseLRFResults(s, mc, lr, 0)) ) {  // 0 means system
                JSONArray params = new JSONArray(s.getMethodParams());
                if ( (params.length() > 1) && !(p.getStepNumber() == params.length()-1) ) { // multistep and not last one
                    // save LFR in study parameters JSON only if multi-setp study, but not in the last step
                    String modelLRF = getLRFforProcess(p.getIdProcess());
                    JSONObject newModel = new JSONObject(modelLRF);
                    newModel.put("name", lrfName); 
                    newModel.put("id", lr.getIdLogregr());                
                    JSONObject inputs = params.getJSONObject(params.length()-1).getJSONObject("inputs");   // last step params
                    inputs.put("model", newModel);
                    params.getJSONObject(params.length()-1).put("inputs",inputs);
                    s.setMethodParams(params.toString());
                    s = (Study) studyController.edit(s);
                }
            } else {
                logger.log(Level.SEVERE, "LR0016 Error: not able to store Logistic Regression Formula received from external statistic package: studyId {0} process {1}", new Object[]{s.getIdStudy(), p.getName()});                
                return "LR0016 Error: not able to store Logistic Regression Formula received from external statistic package";
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving LR from process with ID: {0} {1}", new Object[]{p.getName(), ex});
            return "LR0015 Error saving LRF results: " + ex.getMessage();
        }
        return null;
    }

    public String formatLRFParamsStep2(Study s) {
        String responseStringModel = "{\"type\": \"ArrayList<String>\",\"class\": \"lrValue\",\"inputText\": \"Model values\",\"description\": \"Select all the model values for the study from the list\",\"required\": true,\"values\" : [";    
        List<Logregr> lrList = logregrController.findLogregrByStudyId(s.getIdStudy());    
        // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
        List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(lrList.size()-1).getIdLogregr());
        for (int i=0; i<lrfList.size(); i++) {
            if ( i > 0 ) {
                responseStringModel += ",";
            }
            String fieldName = lrfList.get(i).getName();
            responseStringModel += "{\"name\":\"" + fieldName + "\",\"estimate\":\"";
            List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
            responseStringModel += lrdList.get(0).getValue() + "\"}";
        }
        responseStringModel += "]}";
        // defaut to generic "id"
        String responseStringUniqueId = "\"studyUniqueId\":{\"type\":\"String\",\"class\":\"Case ID\",\"inputText\":\"Unique ID\",\"description\": \"Select the unique id for the study\",\"required\": false,\"values\": [\"id\"]}";
        String responseStringDepend = "\"dependantVariableSelection\":{\"values\":[\"";
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdOutcome());
        responseStringDepend += mc.getName();
        responseStringDepend += "\"],\"inputText\": \"Dependent Variable\",\"description\": \"Select the dependent variable for the study from the list\",\"required\": true,\"type\": \"String\",\"class\": \"Outcome\"}";
        return responseStringModel + "," + responseStringUniqueId + "," + responseStringDepend;
    }

    public JSONObject formatLRFParams(Study s) {
        try {
            List<Logregr> lrList = null;   
            List<LogregrField> lrfList = null;           
            JSONObject model = new JSONObject();
            JSONObject type = new JSONObject();
            type.put("type", "ArrayList<String>");
            JSONObject fieldClass = new JSONObject();
            fieldClass.put("fieldClass", "lrValue");
            JSONObject inputText = new JSONObject();
            inputText.put("inputText", "Model Values");
            JSONObject required = new JSONObject();
            required.put("required", true);
            JSONArray values = new JSONArray();
            Integer idLRF = getLRFIdFromParams(s);
            if ( idLRF != null ) { // null means no study param for LRF found
                lrfList = logregrfieldController.findLogregrFieldByLogregrId(idLRF);
            } else {
                lrList = logregrController.findLogregrByStudyId(s.getIdStudy());
                if ( lrList.isEmpty() ) return null; // most likely method that does not require LRF
                // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
                lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(lrList.size()-1).getIdLogregr());
            }
            for (int i=0; i<lrfList.size(); i++) {
                JSONObject rec = new JSONObject();
                String fieldName = lrfList.get(i).getName();
                rec.put("name", fieldName);
                List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
                if ( lrdList.size() > 0 ) {
                    rec.put("estimate", lrdList.get(0).getValue());      
                } else {
                    rec.put("estimate", "error");    
                }    
                values.put(i,rec);
            }
            model.put("type",type);
            model.put("fieldClass",fieldClass);
            model.put("inputText",inputText);
            model.put("required",required);
            model.put("values",values);
            return model;
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving LRF for study with ID: {0} {1}", new Object[]{s.getIdStudy(), ex});
        }
        return null;
    }
    
   public String getLRFforModel(String modelId) {
        String result;
        
        List<Logregr> lrList = logregrController.findLogregrByModelId(Integer.valueOf(modelId));
        if ( lrList.size() < 1 ) {
            result = "{\"model\":\"\"}";
        } else {
            Logregr lr = lrList.get(lrList.size()-1); // get the last one 
            result = formatAllLRFModelString(lrList);
        }
        return result;
    }
    
    public String getLRFforStudy(String studyId) {
        String result;
        
        List<Logregr> lrList = logregrController.findLogregrByStudyId(Integer.valueOf(studyId));
        if ( lrList.size() < 1 ) {
            result = "{\"model\":\"\"}";
        } else {
            Logregr lr = lrList.get(lrList.size()-1); // get the last one 
            result = formatLastLRFModelString(lrList);
        }
        return result;
    }
 
    // this logic is used for calculating training windows and start of LR Step 2
    private String calcNewStartDate(String type, String year, String month, String day) {
        String newDateString = "";
        int intervalYearStart = Integer.parseInt(year);    
        int intervalMonthStart = Integer.parseInt(month);                
        switch ( type ) {
            case "year":
                // add one year
                intervalYearStart++;
                newDateString = Integer.toString(intervalYearStart) + "/01/01";   
                break;
            case "6months":
                int intervalHalfYear = ((intervalMonthStart-1) / 6) + 1;
                switch (intervalHalfYear) {
                    case 1:
                        intervalMonthStart = 7;
                        break;
                    case 2:
                        intervalMonthStart = 1;
                        intervalYearStart++;
                        break;
                }
                newDateString = Integer.toString(intervalYearStart) + "/0" + Integer.toString(intervalMonthStart) + "/01";                       
                break;                
            case "quarter":  
                int intervalQuarter = ((intervalMonthStart-1) / 3) + 1;
                switch (intervalQuarter) {
                    case 1:
                        intervalMonthStart = 4;
                        break;
                    case 2:
                        intervalMonthStart = 7;
                        break;
                    case 3:
                        intervalMonthStart = 10;
                        break;
                    case 4:
                        intervalMonthStart = 1;
                        intervalYearStart++;
                        break;
                }
                if ( intervalMonthStart > 9 ) {
                    newDateString = Integer.toString(intervalYearStart) + "/" + Integer.toString(intervalMonthStart) + "/01";     
                } else {
                    newDateString = Integer.toString(intervalYearStart) + "/0" + Integer.toString(intervalMonthStart) + "/01";                       
                }
                break;
            case "month":
                // add one month
                intervalMonthStart += 1;
                if ( intervalMonthStart > 12 ) {
                    intervalMonthStart = intervalMonthStart % 12;
                    intervalYearStart++; 
                }                    
                if ( intervalMonthStart > 9 ) {
                    newDateString = Integer.toString(intervalYearStart) + "/" + Integer.toString(intervalMonthStart) + "/01";     
                } else {
                    newDateString = Integer.toString(intervalYearStart) + "/0" + Integer.toString(intervalMonthStart) + "/01";                       
                }                
                break;                     
        }
        return newDateString;           
    }
    
    private String getLRFforProcess(Integer processId) {
        String result;
        
        List<Logregr> lrList = logregrController.findLogregrByProcessId(processId);
        if ( lrList.size() < 1 ) {
            result = "{\"logregrs\":\"\"}";
        } else {        
            Logregr lr = lrList.get(0); // assume one
            result = formatLastLRFModelString(lrList);    
        }
        return result;
    }
    
    public String getLRFforProcess(String processId, Integer userId) {
        String result;
        
        List<Logregr> lrList = logregrController.findLogregrByProcessId(Integer.valueOf(processId));
        if ( lrList.size() < 1 ) {
            result = "{\"logregrs\":\"\"}";
        } else {        
            Logregr lr = lrList.get(lrList.size()-1); // get the last one (and the only one)
            String stringJSON = "{\"logregrs\":[{\"idLogregr\":" + lr.getIdLogregr().toString()
                    + ",\"name\":\""  + lr.getName() + "\"}]}";  
            result = getLRFString(stringJSON, userId);    
        }
        return result;
    }
 
    public String formatLastLRFModelString(List<Logregr> lrList) {
        String responseStringModel = "{\"model\": {\"type\": \"ArrayList<String>\",\"class\": \"lrValue\",\"inputText\": \"Model values\",\"description\": \"Select all the model values for the study from the list\",\"required\": true,\"values\" : [";    
        // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
        List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(lrList.size()-1).getIdLogregr());
        for (int i=0; i<lrfList.size(); i++) {
            if ( i > 0 ) {
                responseStringModel += ",";
            }
            String fieldName = lrfList.get(i).getName();
            responseStringModel += "{\"name\":\"" + fieldName + "\",\"estimate\":\"";
            List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
            responseStringModel += lrdList.get(0).getValue() + "\"}";
        }
        responseStringModel += "]}}";
        return responseStringModel;
    }
    
    public String formatLRFModelString(Integer logregrId) {
        String responseStringModel = "{\"model\": {\"type\": \"ArrayList<String>\",\"class\": \"lrValue\",\"inputText\": \"Model values\",\"description\": \"Select all the model values for the study from the list\",\"required\": true,\"values\" : [";    
        // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
        List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(logregrId);
        for (int i=0; i<lrfList.size(); i++) {
            if ( i > 0 ) {
                responseStringModel += ",";
            }
            String fieldName = lrfList.get(i).getName();
            responseStringModel += "{\"name\":\"" + fieldName + "\",\"estimate\":\"";
            List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
            responseStringModel += lrdList.get(0).getValue() + "\"}";
        }
        responseStringModel += "]}}";
        return responseStringModel;
    }    
    
    public String formatAllLRFModelString(List<Logregr> lrList) {
        String responseStringModel = "[";
        for (int j=0; j<lrList.size(); j++) {
            if ( j> 0 ) {
                responseStringModel += ",";
            }
            int id = lrList.get(j).getIdLogregr();
            //responseStringModel += "{\"name\":\"" + lrList.get(j).getName() + "\",\"idLogregr\":" + Integer.toString(id) + ",\"formula\":";
            responseStringModel += "{\"model\": {\"name\":\"" + Integer.toString(id) + "-" + lrList.get(j).getName() + "\",\"id\":" + Integer.toString(id) + ",\"type\": \"ArrayList<String>\",\"fieldClass\": \"lrValue\",\"inputText\": \"Model values\",\"description\": \"Select all the model values for the study from the list\",\"required\": true,\"values\" : [";    
            List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(j).getIdLogregr());
            for (int i=0; i<lrfList.size(); i++) {
                if ( i > 0 ) {
                    responseStringModel += ",";
                }
                String fieldName = lrfList.get(i).getName();
                responseStringModel += "{\"name\":\"" + fieldName + "\",\"estimate\":\"";
                List<LogregrDate> lrdList = logregrdateController.findLogregrDateByLogregrFieldId(lrfList.get(i).getIdLogregrField());
                if ( lrdList.size() == 0 ) {
                    responseStringModel += "\"}";
                } else {
                    responseStringModel += lrdList.get(0).getValue() + "\"}";
                }
            }
            responseStringModel += "]}}";
        }
        responseStringModel += "]";
        return responseStringModel;
    }
    
    public String getIntervalQueryStep2(Study s) {
        String responseString = "";
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());

        List<String> chunks = getCalendarChunks(s.getIsChunking(), s.getChunkingBy(), s.getStartTS(), s.getEndTS());
        for (int i=0; i< chunks.size(); i++) {
            responseString = mc.getName() + " >= '" + chunks.get(i) + "'";
            if ( i + 1 < chunks.size() ) {
                // it is not the last column
                responseString += " AND " + mc.getName() + " < '" + chunks.get(i+1) + "'";
            } 
            if ( (s.getIsChunking() == false) && (i == 1) ) {
                // if no chunking select everything past first interval
                return responseString;
            }
        }        
        return responseString;
    } 
    
    public Set<String> retrieveColumnsFromLRF(Study s) throws Exception {
        List<Logregr> lrList = logregrController.findLogregrByStudyId(s.getIdStudy());        
        // use last logregr at index size-1, this is from last execution of LRF Step1 for this study
        List<LogregrField> lrfList = logregrfieldController.findLogregrFieldByLogregrId(lrList.get(lrList.size()-1).getIdLogregr());        
        Set<String> columns = new TreeSet();
        try { 
            String fieldName;
            for (int i=0; i<lrfList.size(); i++) {
                fieldName = lrfList.get(i).getName();
                if ( !"intercept".equals(fieldName.toLowerCase()) ) {
                    columns.add(fieldName);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, "Exception when retrieving columns from LRF ", ex);
            throw ex;
        }
        return columns;
    }    
 
    public String prepareJSONResponse(String callId, String message, String result, int counter) {
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            if ("".equals(result)) {
                success.put("success", false);
                status.put("message", message);
            } else {
                success.put("success", true);
                status.put("message", message);
                success.put("totalCount", Integer.toString(counter));
                success.put(callId, new JSONArray(result));
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in DELTA WS method " + callId, ex);
            result = "{\"status\":{\"message\":\"JSON error in DELTA WS method " + callId + "\"},\"success\":false}";
        }
        return result;
    }

    private String parseLRFResults(Study s, ModelColumn mc, Logregr lr, Integer currentUserId) throws JSONException, Exception {
        DecimalFormat df4 = new DecimalFormat(".####");        
        String result = "Ok";
        JSONObject formulaJSON;
        JSONArray resultsArrayJSON;
        JSONObject resultsJSON = null;
        JSONArray dataArrayJSON = null;
        JSONArray theArray;

        logger.log(Level.INFO, "Retrieving logregr object {0}", Integer.toString(lr.getIdLogregr()));
        formulaJSON = new JSONObject(lr.getFormula());
        resultsArrayJSON = formulaJSON.getJSONArray("results");

        for (int j = 0; j < resultsArrayJSON.length(); j++) {
            resultsJSON = resultsArrayJSON.getJSONObject(j);  
            if ( "LogisticRegressionFormulaOutput".equals(resultsJSON.getString("name")) ) {  
                dataArrayJSON = resultsJSON.getJSONArray("data");
            }         
        }            
        if ( dataArrayJSON == null ) {
            return "Logistic Regression Formula not found in results received.";
        }
        // remove all regrfields and regrdates attached to this logregr object
        List<LogregrField> lrf = logregrfieldController.findLogregrFieldByLogregrId(lr.getIdLogregr());
        deleteLogregrFields(lrf);
        List<LogregrDate> lrd = logregrdateController.findLogregrDateByLogregrId(lr.getIdLogregr());
        deleteLogregrDates(lrd);

        for (int i = 0; i < dataArrayJSON.length(); i++) {
            theArray = dataArrayJSON.getJSONArray(i);
            LogregrField entF = new LogregrField();
            entF.setActive(Boolean.TRUE);
            String fieldName = (String) theArray.get(1);
            if ( "intercept".equals(fieldName.toLowerCase()) ) {
                entF.setModelColumnidRisk(0);
            } else {
                List<ModelColumn> mcList = modelColumnController.findModelColumnByModelAndName(lr.getIdModel(), fieldName);
                if (mcList.isEmpty()) {
                    result += " Field " + fieldName + " not found. ";
                    continue;
                }
                entF.setModelColumnidRisk(mcList.get(0).getIdModelColumn());
            }
            entF.setName(fieldName);
            entF.setValue("");
            entF.setIdLogregr(lr.getIdLogregr());
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            entF.setCreatedTS(currentTimestamp);
            entF.setUpdatedTS(currentTimestamp);
            entF.setCreatedBy(currentUserId);
            entF.setUpdatedBy(currentUserId);
            entF = logregrfieldController.edit(entF);
            AuditLogger.log(currentUserId, entF.getIdLogregrField(), "create", entF);

            LogregrDate entD = new LogregrDate();
            List<String> chunks = getCalendarChunks(s.getIsChunking(), s.getChunkingBy(),  s.getStartTS(), s.getEndTS());
            entD.setName("");     // temp hardcoded to default blank
            String sqlWhereClause = mc.getName() + " >= '" + chunks.get(0) + "'";
            if ( i + 1 < chunks.size() ) {
                // it is not the last column
                sqlWhereClause += " AND " + mc.getName() + " < '" + chunks.get(1) + "'";
            }         
            entD.setDescription(sqlWhereClause);                
            entD.setActive(Boolean.TRUE);
            entD.setIdLogregr(lr.getIdLogregr());
            entD.setIdLogregrField(entF.getIdLogregrField());
            entD.setValue(df4.format(theArray.getDouble(2)));
            entD.setModelColumnidSequencer(lr.getModelColumnidSequencer());
            entD.setCreatedTS(currentTimestamp);
            entD.setUpdatedTS(currentTimestamp);
            entD.setCreatedBy(currentUserId);
            entD.setUpdatedBy(currentUserId);
            entD.setName(chunks.get(1));
            entD = logregrdateController.edit(entD);
            AuditLogger.log(currentUserId, entD.getIdLogregrDate(), "create", entD);
        }
        return result;
    }
 
    public Integer getLRFIdFromParams(Study study) {
        try {
            String result;           
            JSONArray modelArray = new JSONArray(study.getMethodParams());
            JSONObject params, inputs, model = null;
            params = (JSONObject) modelArray.get(0);
            inputs = params.getJSONObject("inputs");
            model = inputs.getJSONObject("model");
            result = model.getString("id");
            return Integer.parseInt(result);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error retrieving LRF Id from Study {0} parameters: {1}", new Object[]{Integer.toString(study.getIdStudy()), ex.getMessage()});
            return null;
        }
    }
    
    private String getLogregrDateJSON(int idLogregr) {
        logger.log(Level.INFO, "Retrieving Dates for Formula {0}", Integer.toString(idLogregr));
        String dataString = "";
        int counter = 0;

        List<LogregrDate> columns = logregrdateController.findLogregrDateByLogregrId(idLogregr);

        if ((columns != null) && !columns.isEmpty()) {
            Collections.sort(columns, new LogregrDateComp()); // sort by dates which become header names in UI
            Collections.sort(columns, new LogregrDateFieldComp()); // sort by LogregrField ID           
            counter = columns.size();
            logger.log(Level.INFO, "Logistic Regression Formula {0} has fields. Retrieving {1} formula data.", new Object[]{Integer.toString(idLogregr), Integer.toString(counter)});

            for (int i = 0; i < counter; i++) {
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(columns.get(i)).toString();
            }
            dataString = "[" + dataString + "]";
            return dataString;
        }
        return dataString;
    }
    
    private LogregrField updateLogregrField(LogregrField ent, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} updating relationship between field {1} and logregr formula {2}",
                new Object[]{Integer.toString((currentUserId)),
                    Integer.toString((ent.getModelColumnidRisk())),
                    Integer.toString(ent.getIdLogregr())});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        ent = logregrfieldController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "update", ent);
        return ent;
    }
 
    private LogregrField deleteLogregrField(int idLogregrField, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship {1} between field and formula", new Object[]{Integer.toString((currentUserId)), Integer.toString((idLogregrField))});

        LogregrField ent = new LogregrField(idLogregrField);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);
        logregrfieldController.destroy(idLogregrField);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "delete", ent);
        return ent;
    }   
    
    private String getLogregrFieldJSON(int idLogregr) {
        logger.log(Level.INFO, "Retrieving Fileds for Formula {0}", Integer.toString(idLogregr));
        String dataString = "";
        int counter = 0;

        List<LogregrField> columns = logregrfieldController.findLogregrFieldByLogregrId(idLogregr);

        if ((columns != null) && !columns.isEmpty()) {
            counter = columns.size();
            logger.log(Level.INFO, "Logistic Regression Formula {0} has fields. Retrieving {1} formula data.", new Object[]{Integer.toString(idLogregr), Integer.toString(counter)});

            for (int i = 0; i < counter; i++) {
                if (i > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(columns.get(i)).toString();
            }
            dataString = "[" + dataString + "]";
            return dataString;
        }
        return dataString;
    }

    private LogregrDate createLogregrDate(LogregrDate ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.INFO, "User {0} creating relationship between date field {1} and logregr formula {2}",
                new Object[]{Integer.toString((currentUserId)),
                    Integer.toString(ent.getModelColumnidSequencer()),
                    Integer.toString(ent.getIdLogregr())});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        if ( "".equals(ent.getValue()) ) {
            ent.setValue("0.0");
            }
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);
        ent = logregrdateController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrDate(), "create", ent);
        return ent;
    }
    
    private LogregrField createLogregrField(LogregrField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.INFO, "User {0} creating relationship between risk field {1} and logregr formula {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString(ent.getModelColumnidRisk()), ent.getName()});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);
        ent = logregrfieldController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "create", ent);
        return ent;
    }

    private String createLogregrFieldString(LogregrField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        String dataString;
        String regrDateString;
        logger.log(Level.INFO, "User {0} creating relationship between risk field {1} and logregr formula {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString(ent.getModelColumnidRisk()), ent.getName()});

        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);
        ent = logregrfieldController.edit(ent);
        AuditLogger.log(currentUserId, ent.getIdLogregrField(), "create", ent);
        dataString = new JSONObject(ent).toString();
        regrDateString = duplicateFieldDates(ent, currentUserId);
        return new StringBuilder(dataString).insert(dataString.length() - 1, "," + regrDateString).toString();
    }

    private String duplicateFieldDates(LogregrField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        String dataString = "";
        logger.log(Level.INFO, "User {0} duplicating relationship between risk field {1} and date", new Object[]{Integer.toString((currentUserId)), Integer.toString(ent.getIdLogregrField()), ent.getName()});

        List<LogregrField> fieldList = logregrfieldController.findLogregrFieldByLogregrId(ent.getIdLogregr());
        if (fieldList.size() > 1) { // check if there are more LegregrFields beyond the one being processed
            LogregrField topEnt = fieldList.get(0); // use first one as a source of Date records
            int ii = 0;
            List<LogregrDate> dateList = logregrdateController.findLogregrDateByLogregrFieldId(topEnt.getIdLogregrField());
            // to make sure JSON passed to UI is sorted by dates which become header names in UI:
            Collections.sort(dateList, new LogregrDateComp());
            // the following is necessary to revers previous sort and mimic similar action in getLogregrDates()
            Collections.sort(dateList, new LogregrDateFieldComp()); // sort by LogregrField ID
            for (LogregrDate t : dateList) {
                logger.log(Level.INFO, "Duplicating Formula Date {0}", new Object[]{Integer.toString(t.getIdLogregrDate())});
                t.setIdLogregrField(ent.getIdLogregrField());
                t.setIdLogregr(ent.getIdLogregr());
                t.setValue("");
                t.setIdLogregrDate(0);
                t = createLogregrDate(t, currentUserId);
                if (ii++ > 0) {
                    dataString = dataString.concat(",");
                }
                dataString = dataString + new JSONObject(t).toString();
            }
        }
        return "\"logregrdates\":[" + dataString + "]";
    }

    private LogregrField deleteLogregrFields(List<LogregrField> list) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException {
        for (LogregrField row : list) {
            logregrfieldController.destroy(row.getIdLogregrField());
        }
        return null;
    }

    private LogregrField deleteLogregrDates(List<LogregrDate> list) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException {
        for (LogregrDate row : list) {
            logregrdateController.destroy(row.getIdLogregrDate());
        }
        return null;
    }

    private LogregrField getLogregrFieldById(List<LogregrField> list, Integer idx) {
        for (LogregrField row : list) {
            if (Objects.equals(row.getIdLogregrField(), idx)) {
                return row;
            }
        }
        return null;
    }

    /* delegete this work to common method in admin service */
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        if (adminService == null) {
            adminService = new AdminServiceImpl();
        }        
        return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
    }
    
      
}
