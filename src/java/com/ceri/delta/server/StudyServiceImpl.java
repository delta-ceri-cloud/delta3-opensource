/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server;

import com.ceri.delta.db.DBHelper;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.GroupHasEntityPK;
import com.ceri.delta.entity.GroupTable;
import com.ceri.delta.entity.Method;
import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.model.ModelColumn;
import com.ceri.delta.entity.process.Process;
import com.ceri.delta.entity.study.Filter;
import com.ceri.delta.entity.study.FilterField;
import com.ceri.delta.entity.study.Stat;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.entity.study.StudyHasCategory;
import com.ceri.delta.entity.study.StudyHasCategoryPK;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.GroupJpaController;
import com.ceri.delta.jpa.MethodJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.process.ProcessJpaController;
import com.ceri.delta.jpa.study.FilterFieldJpaController;
import com.ceri.delta.jpa.study.FilterJpaController;
import com.ceri.delta.jpa.study.StatJpaController;
import com.ceri.delta.jpa.study.StudyHasCategoryJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.server.exceptions.EntityExistsException;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import com.ceri.delta.util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class StudyServiceImpl {

    public static HashMap<String, String[]> methodMap = new HashMap<String, String[]>()
    {
        {
            put("propensityScoreAnalysis", new String[] {"P1", "New", "PA 1 of 1 In Progress", "PA Completed"});
            put("propensityAnalysisCreate", new String[] {"PC", "New", "PA 1 of 1 In Progress", "PA Completed"});
            put("manualPropensityAnalysis", new String[] {"PA", "New", "PA 1 of 1 In Progress", "PA Completed"});
            put("propensityScoreOutcomes", new String[] {"PO", "New", "PA 1 of 1 In Progress", "PA Completed"});
            put("logisticRegression", new String[] {"LR", "New", "LR 1 of 2 In Progress", "LR Stage 2 of 2 Ready", "LR 2 of 2 In Progress", "LR Completed"});
           
            put("manualLogisticRegression", new String[] {"LRA", "New", "LR 1 of 1 In Progress", "LR Completed"});
            put("logisticRegressionCreate", new String[] {"LRC", "New", "LR 1 of 1 In Progress", "LR Completed"});
            put("logisticRegressionCombined", new String[] {"LRCOMB", "New", "LR 1 of 1 In Progress", "LR Completed"});
            put("riskAdjustedSPRT", new String[] {"SPRT1", "New", "SPRT 1 of 1 In Progress", "SPRT Completed"});
            put("survivalAnalysisUnmatched", new String[] {"SRV", "New", "SRV 1 of 1 In Progress", "SRV Completed"});
            put("survivalAnalysisMatched", new String[] {"SRVM", "New", "SRV 1 of 1 In Progress", "SRV Completed"});
            put("logisticRegressionComplete", new String[] {"LR Complete", "New", "LR Complete 1 of 1 In Progress", "LRFULL Completed"});
            put("iptw", new String[] {"iptw", "New", "IPTW 1 of 1 In Progress", "IPTW Completed"});
           
            put("weightingByOdds", new String[] {"weightingByOdds", "New", "PS WBO 1 of 1 In Progress", "PS WBO Completed"});
            put("propensityStratification", new String[] {"propensityStratification", "New", "PS Stratification 1 of 1 In Progress", "PS Stratification Completed"});
            put("logisticRegressionAnalysis", new String[] {"logisticRegressionAnalysis", "New", "LR Analysis 1 of 1 In Progress", "LR Analysis Completed"});
       
        
        
        
        }
    };    
    private static final Logger logger = Logger.getLogger(StudyServiceImpl.class.getName());    
    private static FilterJpaController filterController;  
    private static StudyHasCategoryJpaController studyhascategoryController; 
    private static FilterFieldJpaController filterfieldController;        
    private static ModelColumnJpaController modelcolumnController;    
    private static StudyJpaController studyController;
    private static MethodJpaController methodController;    
    private static StatJpaController statController;        
    private static ModelJpaController modelController; 
    private static ProcessJpaController processController;
    private static GroupJpaController groupController;
    private static GroupHasEntityJpaController groupHasEntityController;
    private static AdminServiceImpl adminService;    
    private static StatServiceImpl statService;  

    public StudyServiceImpl() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }        
        if ( statService == null ) {
            statService = new StatServiceImpl();
        }          
        if ( methodController == null ) {
           methodController = new MethodJpaController();
        }    
        if ( studyController == null ) {
           studyController = new StudyJpaController();
        }       
        if ( modelController == null ) {
           modelController = new ModelJpaController();
        }      
        if ( statController == null ) {
           statController = new StatJpaController();
        }           
        if ( modelcolumnController == null ) {
           modelcolumnController = new ModelColumnJpaController();
        }               
        if ( studyhascategoryController == null ) {
           studyhascategoryController = new StudyHasCategoryJpaController();
        }   
        if ( filterController == null ) {
            filterController = new FilterJpaController();
        }    
        if ( processController == null ) {
            processController = new ProcessJpaController();
        }    
        if ( groupController == null ) {
            groupController = new GroupJpaController();
        }         
        if ( groupHasEntityController == null ) {
            groupHasEntityController = new GroupHasEntityJpaController();
        }           
        if ( filterfieldController == null ) {
           filterfieldController = new FilterFieldJpaController();
        }       
    }
    //-------------------------------------------------------------------------- Study
    public String getOrgStudies(int start, int limit, Integer currentOrgId, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all users
            subjects = controller.findObjectEntities(currentOrgId);
        } else {
            subjects = controller.findObjectEntities(limit, start, currentOrgId);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            // insert categories object
            String category = getStudyCategoryJSON(((Study)subjects.get(recordCounter)).getIdStudy());     
            if ( "".equals(category) ) {
                category = "[]";
            }
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
                dataString = new StringBuilder(dataString).insert(dataString.length()-2, ",\"categories\":" + category).toString();
            } else {
                dataString = new StringBuilder(dataString).insert(dataString.length()-1, ",\"categories\":" + category).toString();
            }
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
   
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(controller.getObjectCount(currentOrgId)));
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  

    public String getOrgStudiesByGroup(int start, int limit,  int idGroup, Integer currentOrgId, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) {
            if ( idGroup == 0 ) {
                subjects = controller.findObjectEntities(currentOrgId);
            } else {
                subjects = controller.findObjectEntitiesByOrgGroup(currentOrgId, idGroup);
            }
        } else {
            if ( idGroup == 0 ) {
                subjects = controller.findObjectEntities(limit, start, currentOrgId);
            } else {
                subjects = controller.findObjectEntitiesByOrgGroup(limit, start, currentOrgId, idGroup);
            }
        }
        int max = subjects.size();
        int hitCounter = 0;
        
        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            hitCounter++;
            // insert categories object
            String category = getStudyCategoryJSON(((Study)subjects.get(recordCounter)).getIdStudy());     
            if ( "".equals(category) ) {
                category = "[]";
            }
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
                dataString = new StringBuilder(dataString).insert(dataString.length()-2, ",\"categories\":" + category).toString();
            } else {
                dataString = new StringBuilder(dataString).insert(dataString.length()-1, ",\"categories\":" + category).toString();
            }
        }
        dataString = "[" + dataString + "]";
        int totalCount;
        if ( idGroup == 0 ) {
            totalCount = controller.getObjectCountByOrg(currentOrgId);
        } else {
            totalCount = controller.getObjectCountByOrgGroup(currentOrgId, idGroup);
        }
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(hitCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(totalCount));                 
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  
  
     public String getOrgStudiesExtendedGroup(int start, int limit,  int idGroup, String filter, String sort, Integer currentOrgId, PersistenceHelper controller, Object subject) {
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        System.out.println("This is at getOrgStudiesExtendedGroup"+start);
        System.out.println("This is at getOrgStudiesExtendedGroup"+idGroup);
        System.out.println("This is at getOrgStudiesExtendedGroup"+sort);
        System.out.println("This is at getOrgStudiesExtendedGroup"+currentOrgId);
        System.out.println("This is at getOrgStudiesExtendedGroup"+limit);
        System.out.println("This is at getOrgStudiesExtendedGroup"+filter);
        
        
        List<Object> subjects = null;
        int recordCounter = 0;
        String whereClause = adminService.constructWhereClause(filter, sort);
        
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{controller.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) {
            if ( idGroup == 0 ) {
                subjects = controller.findObjectEntitiesExtendedOrg(currentOrgId, whereClause);
                System.out.println("idGroup 0 Limit 0");
            } else {
                subjects = controller.findObjectEntitiesExtendedOrgGroup(currentOrgId, idGroup, whereClause);
                System.out.println("idGroup non0 Limit 0");
            }
            
            
            
        } else {
            if ( idGroup == 0 ) {
                subjects = controller.findObjectEntitiesExtendedOrg(limit, start, currentOrgId, whereClause);
                System.out.println("idGroup 0 Limit non0");
            } else {
                subjects = controller.findObjectEntitiesExtendedOrgGroup(limit, start, currentOrgId, idGroup, whereClause);
                System.out.println("idGroup non0 Limit non0");
            }
        }
        
        
        
        int max = subjects.size();
        int hitCounter = 0;
        
        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            hitCounter++;
            // insert categories object
            String category = getStudyCategoryJSON(((Study)subjects.get(recordCounter)).getIdStudy());     
            if ( "".equals(category) ) {
                category = "[]";
            }
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
                dataString = new StringBuilder(dataString).insert(dataString.length()-2, ",\"categories\":" + category).toString();
            } else {
                dataString = new StringBuilder(dataString).insert(dataString.length()-1, ",\"categories\":" + category).toString();
            }
        }
        dataString = "[" + dataString + "]";
        int totalCount;
        if ( idGroup == 0 ) {
            totalCount = controller.getObjectCountExtendedOrg(currentOrgId, whereClause);
        } else {
            totalCount = controller.getObjectCountExtendedOrgGroup(currentOrgId, idGroup, whereClause);
        }
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(hitCounter) + " " + controller.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(totalCount));                 
                success.put(controller.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", ex);  
            result = "JSON error in WS method [admin.getObjects]";
        }        
        return result;
    }  
    
    public String deleteStudies(String JSONString, Integer currentUserId) {
        String result = "Study deleted";
        Study study = null;
        String dataString = "";
      
        try {
            JSONArray studyArray = new JSONArray(JSONString);
            for (int i=0; i<studyArray.length(); i++) {
                JSONObject JSONContentRoot =  studyArray.getJSONObject(i);
                logger.log(Level.INFO, "Deleting Study {0}", JSONContentRoot.getInt("idStudy"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = JSONContentRoot.toString();
                if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                dataString = dataString + jString;
                study = gson.fromJson(jString, Study.class);   
                Integer studyId = JSONContentRoot.getInt("idStudy");    
                Integer groupId = JSONContentRoot.getInt("idGroup");
                List<StudyHasCategory> shcOld = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(studyId);
                for (StudyHasCategory t : shcOld) {
                    int id = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
                    StudyHasCategoryPK shcPK = new StudyHasCategoryPK(studyId,id); 
                    studyhascategoryController.destroy(shcPK);
                }                 
                List<GroupHasEntity> gheOld = groupHasEntityController.findGroupHasEntityEntitiesByIdAndType(studyId, "studies");
                for (GroupHasEntity g : gheOld) {
                    int id = g.getGroupHasEntityPK().getGroupidGroup();
                    GroupHasEntityPK ghePK = new GroupHasEntityPK(id,studyId);    
                    groupHasEntityController.destroy(ghePK);                    
                }
                studyController.deleteObject(study, currentUserId);                
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting study: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0001 Exception while deleting study: " + ex.getMessage();
        }   
        dataString = "[" + dataString + "]";        
        return wrapAPI("studies", dataString, "Studies deleted", result);
    }
    
    public String deleteStudiesByModel(Model m) {    
        String returnString = "";
        List<Study> sList;
        
        try {
            sList = studyController.findStudyEntitiesByModelId(m.getIdModel());
            if ( (sList != null) && !sList.isEmpty() ) {
                returnString = "Following Sudies were deleted: ";
                int i = 0;
                for (Study s: sList) {
                   if ( i++ > 0 ) {
                       returnString += ",";
                   }
                   returnString += s.getName();
                   studyController.destroy(s.getIdStudy());
               }
            }
            return returnString;            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception when searching for Study to be deleted that uses: " + m.getIdModel().toString(), ex.getMessage());
            returnString =  "ST0019 Exception when searching for studies to be deleted: " + ex.getMessage();            
        }    
        return returnString;
    }   
    
    public String cloneStudy(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Study subject) {
        
        String resultString = "Study successfully cloned";       
        Integer oldStudyId;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            //Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateSerializer()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            if ( !Objects.equals(((Study)subject).getIdOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "ST0080 Cloning failed. Not authorized.");
                return "ST0080 Cloning failed. Not authorized."; 
            }               
            oldStudyId = ((Study)subject).getIdStudy();
            ((Study)subject).setIdStudy(0);
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
            String dateTime =  String.format("%1$TY%1$Tm%1$Td-%1$TH%1$TM%1$TS", currentTimestamp);  
            ((Study)subject).setName("Cloned " + dateTime);
            dateTime = null;
            ((Study)subject).setOriginalStudyId(oldStudyId);
            ((Study)subject).setStatus("New");
            ((Study)subject).setProcessedTS(null);
            // cloning assumes coping ids for Outcomes, Treatments, Method and Model and the rest of params
            subject = (Study) controller.createObjects(subject, currentOrgId, currentUserId);
            // now clone study_has_category relationships
            List<StudyHasCategory> shcOld = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(oldStudyId);
            for (StudyHasCategory t : shcOld) {
                int id = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
                StudyHasCategoryPK shcPK = new StudyHasCategoryPK(subject.getIdStudy(),id); 
                StudyHasCategory shc = new StudyHasCategory(shcPK);
                shc.setCreatedTS(currentTimestamp);
                shc.setUpdatedTS(currentTimestamp);
                shc.setCreatedBy(currentUserId);
                shc.setUpdatedBy(currentUserId);    
                shc = studyhascategoryController.edit(shc);
            }     
            // replicate all groupings original study might be subject of
            List<GroupHasEntity> gheOld = groupHasEntityController.findGroupHasEntityEntitiesByIdAndType(oldStudyId, "studies");
            for (GroupHasEntity g : gheOld) {
                int id = g.getGroupHasEntityPK().getGroupidGroup();
                adminService.createGroupEntities(id, subject.getIdStudy(), "studies", currentUserId);
            }    
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "ST0222 SQL exception while cloning study {0} {1}", new Object[]{subject.getName(), ex});
            return "ST0222 SQL exception while processing source fields.\n" + DBHelper.printSQLException(ex);  
        } catch (PersistenceException ex) {
            logger.log(Level.SEVERE, "ST0202 Persistence exception while cloning study {0} {1}", new Object[]{subject.getName(), ex});
            return "ST0202 Persistence exception while cloning study.\n" + DBHelper.printPersistenceException(ex);             
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "ST0002 Exception while cloning study {0}: {1}", new Object[]{subject.getName(), ex});
            return "ST0002 Exception while cloning study: "  + subject.getName();
        }
   
        return resultString;
    }
   
    public String splitStudy(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Study subject) {
        
        String resultString = "Study successfully split";       
        Integer oldStudyId;

        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            //Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            if ( !Objects.equals(((Study)subject).getIdOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "ST0081 Splitting study failed. Not authorized.");
                return "ST0081 Splitting study failed. Not authorized."; 
            }             
            oldStudyId = ((Study)subject).getIdStudy();
            ((Study)subject).setName(((Study)subject).getName() + " Clone");            
            ((Study)subject).setOriginalStudyId(oldStudyId);            
            // cycle through study_has_category relationships
            List<StudyHasCategory> shcOld = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(oldStudyId);
            for (StudyHasCategory t : shcOld) {
                // create new study
                ((Study)subject).setIdStudy(0);              
                ((Study)subject).setDescription("Cloned by Catagory Field ID: " + Integer.toString(t.getStudyHasCategoryPK().getModelColumnidModelColumn()));
                subject = (Study) controller.createObjects(subject, currentOrgId, currentUserId);         
                // create study_has_category
                int id = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
                StudyHasCategoryPK shcPK = new StudyHasCategoryPK(subject.getIdStudy(),id); 
                StudyHasCategory shc = new StudyHasCategory(shcPK);
                Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
                shc.setCreatedTS(currentTimestamp);
                shc.setUpdatedTS(currentTimestamp);
                shc.setCreatedBy(currentUserId);
                shc.setUpdatedBy(currentUserId);    
                shc = studyhascategoryController.edit(shc);
            }            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while cloning study {0}: {1}", new Object[]{subject.getName(), ex});
            return "ST0003 Exception while cloning new study: "  + subject.getName();
        }
   
        return resultString;
    }

    public String initStudyMethods(String postContent, Integer orgId, Integer userId) {
        
        try {
            String statConfig = null;
            JSONObject JSONConfig = new JSONObject(postContent);
            if ( "remote".equals(JSONConfig.getString("type")) ) {
                statConfig = statService.getStatConfigRemote(JSONConfig.getString("packageName"));
            } else {            
                statConfig = statService.getStatConfig(JSONConfig.getString("packageName"));
            }
            if ( statConfig != null ) {
                saveStudyMethods(statConfig, orgId, userId, Integer.valueOf(JSONConfig.getString("id")));
                migrateStudyMethodParameters(statConfig, orgId, userId, Integer.valueOf(JSONConfig.getString("id")));
            } else {
                logger.log(Level.SEVERE, "Not initialized stat methods for org {0}", orgId.toString());
            }
            return "Study Methods configuration updated.";
        } catch (JSONException ex) {
            Logger.getLogger(StudyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Study Methods configuration not updated.";
    }    
 
    // this method is used by initStudyMethods() triggered by WS
    // and from call back used to communicate with exteral stat package
    public String saveStudyMethods(String statConfig, Integer orgId, Integer userId, Integer statId) {
        try {
            JSONObject content = new JSONObject(statConfig);
            JSONObject conf = content.getJSONObject("statConfig");
            JSONArray methods = conf.getJSONArray("methods");
            Stat stat = (Stat)statController.findObjectEntityById(statId);    
            stat.setConfiguration(content.getString("statConfig"));
            stat.setConfigVersion(conf.getString("version"));    
            stat = statController.edit(stat);
            List<Object> methodRecords = methodController.findOrgMethodEntities(orgId);           
            // record that match by name needs to be kept, only params changed
            // deletes are not allowed
            for (int i=0; i<methods.length(); i++) {
                Object orig;
                JSONObject m = methods.getJSONObject(i);
                Method method = new Method();    
                method.setActive(Boolean.TRUE);
                method.setIdOrganization(orgId);
                method.setIdStat(statId);
                method.setShortName(m.getString("shortName"));
                method.setName(m.getString("name"));
                method.setDescription(m.getString("description"));
                method.setActive(m.getBoolean("enabled"));
                method.setMethodParams(m.getJSONArray("steps").toString());  
                method.setSequenceNumber(10*i);
                if ( (orig = isMethodNew(methodRecords, m.getString("name"), statId)) == null ) {
                    method.setIdMethod(0);
                    logger.log(Level.SEVERE, "Inserting statistical method: {0}", m.getString("name"));
                    methodController.createObjects(method, orgId, userId);
                } else {
                    if ( ((Method)orig).getIdStat() == statId ) {
                        method.setIdMethod(((Method)orig).getIdMethod());
                        method.setCreatedBy(((Method)orig).getCreatedBy());
                        method.setCreatedTS(((Method)orig).getCreatedTS());
                        method.setDescription(((Method)orig).getDescription());
                        logger.log(Level.SEVERE, "Updating statistical method: {0}", m.getString("name"));
                        methodController.updateObjects(method, orgId, userId);
                    }
                }
            }
            return null;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while initializing stat methods for org {0}: {1}", new Object[]{orgId.toString(), ex.getMessage()});
        } 
        return null;
    }

    private Object isMethodNew (List<Object> mr, String name, Integer statId) {
        for (Object m : mr) {
            int idStat = ((Method)m).getIdStat();
            if ( (idStat == statId) 
                    && (((Method)m).getName() == null ? name == null : ((Method)m).getName().equals(name))  ) {
                return m; 
            }
        }        
        return null;
    } 
 
   public String migrateStudyMethodParameters(String statConfig, Integer orgId, Integer userId, Integer statId) {
        try {
            JSONObject content = new JSONObject(statConfig);
            JSONObject conf = content.getJSONObject("statConfig");
            String version = conf.getString("version");            
            JSONArray methods = conf.getJSONArray("methods");
            List<Object> ls = studyController.findOrgStudyEntities(orgId);
                
            // Stat Config JSON hierarchy: methods->steps->inputs->params->values
            for (int i=0; i<methods.length(); i++) { // go through all methods in JSON Stat Config
                JSONObject configMethod = methods.getJSONObject(i);
                String configMethodName = configMethod.getString("name");
                JSONArray configNew = configMethod.getJSONArray("steps");  
                logger.log(Level.INFO, "Updating all studies for organization {0} that have method {1} parameters", new Object[]{orgId.toString(),configMethodName});                         
                for ( int j=0; j<ls.size(); j++) { // go through all studies for this org
                    Study s = (Study)ls.get(j);
                    Method studyMethod = (Method) methodController.findObjectEntityById(s.getIdMethod());
                    if ( studyMethod != null && configMethodName != null && configMethodName.equals(studyMethod.getName()) && studyMethod.getIdStat() == statId ) {                      
                        try {
                            String updatedStudyConfig = "";                            
                            String paramStr = s.getMethodParams();
                            if ( paramStr == null || "".equals(paramStr) ) {
                                // parameters are empty, insert the new ones based on current method selection
                                logger.log(Level.SEVERE, "There are no parameters for Org {0} Study {1} Method {2}", new Object[]{orgId.toString(), s.getIdStudy().toString(), configMethodName});                            
                                s.setMethodParams(configNew.toString());
                                s.setStatus("New");
                                studyController.updateObjects(s,orgId,userId); 
                                continue; // skip processing if params were empty
                            } 
                            JSONArray configCurrent = new JSONArray(paramStr);
                            for (int z=0; z<configNew.length(); z++) { // go through all steps in this method
                                JSONObject stepsNew = configNew.getJSONObject(z);
                                JSONObject stepsCurrent = configCurrent.getJSONObject(z);       
                                JSONObject inputsNew = stepsNew.getJSONObject("inputs");
                                JSONObject inputsCurrent = stepsCurrent.getJSONObject("inputs");    
                                Iterator keysNew = inputsNew.keys();
                                while (keysNew.hasNext()) { // iterate through all new params
                                    String key = (String)keysNew.next();
                                    JSONObject param = inputsCurrent.optJSONObject(key);
                                    if ( param == null ) { // new parameter -> add to current set
                                        inputsCurrent.put(key, inputsNew.optJSONObject(key));
                                    } else { // existing parameter -> update but keep values
                                        JSONArray valuesOld = param.getJSONArray("values");
                                        JSONObject paramNew = inputsNew.optJSONObject(key);
                                        paramNew.put("values", valuesOld);
                                        inputsCurrent.put(key, paramNew);
                                    }
                                }
                                Iterator keysCurrent = inputsCurrent.keys();                            
                                while (keysCurrent.hasNext()) { // iterate through all old/existing params
                                    String key = (String)keysCurrent.next();
                                    JSONObject param = inputsNew.optJSONObject(key);
                                    if ( param == null ) { // parameter does not exist in new set -> remove it
                                        //inputsCurrent.remove(key);
                                        keysCurrent.remove();
                                    }
                                }                            
                                stepsCurrent.put("inputs",inputsCurrent);
                                configCurrent.put(z,stepsCurrent);
                            }
                            updatedStudyConfig = configCurrent.toString();
                            s.setMethodParams(updatedStudyConfig);
                            s.setStatus("New");
                            s.setStatPackageVer(version);
                            studyController.updateObjects(s,orgId,userId);
                        } catch (Exception ex) {
                            logger.log(Level.SEVERE, "Exception while re-creating stat methods for Org {0} Study {1} Method {2}: {3}", new Object[]{orgId.toString(), s.getIdStudy().toString(), configMethodName, ex.getMessage()});                            
                            // existing config has problems, overwrite with a new one
                            s.setMethodParams(configNew.toString());
                            s.setStatus("New");
                            s.setStatPackageVer(version);                           
                            studyController.updateObjects(s,orgId,userId);                       
                        }
                    }
                }
            }
            return null;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while initializing stat methods for Org {0}: {1}", new Object[]{orgId.toString(), ex.getMessage()});
        } 
        return null;
    }    
    
    public String executeStudy(String JSONString, Integer currentOrgId, Integer currentUserId, Study subject, boolean manualMode) {
        
        String resultString = "ST0104 General study execution error";      
         
        try {
            logger.log(Level.SEVERE,"This is st execute study");
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            if ( !Objects.equals(((Study)subject).getIdOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "ST0082 Study execution failed. Not authorized.");
                return "ST0082 Study execution failed. Not authorized."; 
            }             
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            resultString = executeStudy(subject, currentOrgId, currentUserId, manualMode);    
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while executing study {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0105 Error while parsing study configuration for execution " + subject.getName() + ": " + ex.getMessage();            
        } 
        return resultString;
    }
    
    public String resetAndExecuteStudy(String JSONString, Integer currentOrgId, Integer currentUserId, Study subject, boolean manualMode) {
        
        String resultString = "ST0104 General study execution error";      
         
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            if ( !Objects.equals(((Study)subject).getIdOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "ST0082 Study execution failed. Not authorized.");
                return "ST0082 Study execution failed. Not authorized."; 
            }             
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            subject.setStatus("New");
            setStudyStatus(subject.getIdStudy(), "New", currentUserId);
            resultString = executeStudy(subject, currentOrgId, currentUserId, manualMode);    
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while executing study {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0105 Error while parsing study configuration for execution " + subject.getName() + ": " + ex.getMessage();            
        } 
        return resultString;
    }  
    
    public String executeStudy(Study study, Integer currentOrgId, Integer currentUserId, boolean manualMode) {
        
        String resultString = "ST0112 Problm with study state or general configuration";      
         
        try {
            // find method name to be a key to config
            logger.log(Level.SEVERE,"This is st execute study");
            Method method = (Method) methodController.findMethod(study.getIdMethod());
            if ( method == null ) {
                logger.log(Level.SEVERE, "No Method specified for study {0} execution", new Object[]{study.getName()});    
                return "ST0143 Error while executing study " + study.getName() + ". Please specify statistical method.";                           
            }
            if ( method.getName() == null || "".equals(method.getName()) ) {
                logger.log(Level.SEVERE, "No Method name specified for study {0} execution", new Object[]{study.getName()});    
                return "ST0144 Error while executing study " + study.getName() + ". Statistical method name missing.";                     
            }
            ModelColumn sequenceField = modelcolumnController.findModelColumn(study.getIdDate());      
            if ( sequenceField == null ) {
                logger.log(Level.SEVERE, "No Sequence Field specified for study {0} execution", new Object[]{study.getName()});    
                return "ST0145 Error while executing study " + study.getName() + ". Please specify sequence field.";                           
            }            
            switch ( method.getName() ) {
                case "logisticRegression":
                    switch ( study.getStatus() ) {
                        case "New": // needs to be matched with methodStates array
                            resultString = statService.submitStatJob(study, 
                                    "LR1", 0,
                                    getSqlFromFilterFormulas(study.getIdFilter()), 
                                    sequenceField.getName(), 
                                    currentOrgId, 
                                    currentUserId,
                                    manualMode);
                            break;
                        case "LR Stage 2 of 2 Ready": // needs to be matched with methodStates array
                            resultString = statService.submitStatJob(study, 
                                    "LR2", 1,
                                    getSqlFromFilterFormulas(study.getIdFilter()), 
                                    sequenceField.getName(), 
                                    currentOrgId, 
                                    currentUserId, 
                                    manualMode);       
                            break;
                    }                   
                    break;
                default:
                    switch ( study.getStatus() ) {
                        case "New": // needs to be matched with methodStates array
                            resultString = statService.submitStatJob(study, 
                                    methodMap.get(method.getName())[0], 0,
                                    getSqlFromFilterFormulas(study.getIdFilter()), 
                                    sequenceField.getName(), 
                                    currentOrgId, 
                                    currentUserId,
                                    manualMode);
                            break;
                    }        
                    break;                      
            }  
            resultString = advanceStatus(resultString, study, currentUserId);      
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while executing study {0}: {1}", new Object[]{study.getName(), ex});    
            return "ST0004 Error while executing study " + study.getName() + ": " + ex.getMessage();            
        } 
        return resultString;
    }
       
    public String rollbackStudyStatus(String JSONString, Integer currentUserId) {
        
        String resultString = "ST0117 Error Study status not changed";      
        Study subject = new Study();
         
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            Method m = (Method) methodController.findMethod(subject.getIdMethod());            
            resultString = rollbackStatusBypassInProgress(subject, currentUserId);   
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "JSON exception while executing study {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0005 Error while rolling back study status " + subject.getName() + ". Problem with study configuration: " + ex.getMessage();            
        } 
        return resultString;
    }

    public String advanceStudyStatus(String JSONString, Integer currentUserId) {    
        String resultString = "ST0116 Error Study status not changed.";      
    Study subject = new Study();
         
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            Gson gson=  new GsonBuilder().registerTypeAdapter(Date.class, new DateDeSerializer()).create();    
            String jString = objectArray.getJSONObject(0).toString();
            subject = gson.fromJson(jString, subject.getClass());    
            subject = (Study) studyController.findStudy(subject.getIdStudy());
            Method m = (Method) methodController.findMethod(subject.getIdMethod());
            resultString = advanceStatusBypassInProgress("OK", subject, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "JSON exception while advancing study status {0}: {1}", new Object[]{subject.getName(), ex});    
            return "ST0006 Error while advancing study status " + subject.getName() + ": " + ex.getMessage();            
        } 
        return resultString;        
    }
    
    public String advanceStatus(String resultString, Study s, Integer currentUserId) {       
        Method method = (Method) methodController.findMethod(s.getIdMethod());        
        String status;
        if ( "OK".equals(resultString) ) {
            status = getNextMethodStatus(method.getName(),s.getStatus());             
        } else {
            status = resultString;
        }
        resultString = status;
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while advancing status for study " + s.getName(), ex);
            resultString = "ST0110 Error while advancing status for study " + s.getName() + ": " + ex.getMessage();    
        }
        return resultString;
    }

    public String advanceStatusBypassInProgress(String resultString, Study s, Integer currentUserId) {       
        Method method = (Method) methodController.findMethod(s.getIdMethod());        
        String status;
        if ( "OK".equals(resultString) ) {
            status = getNextMethodStatus(method.getName(),s.getStatus());         
            if ( status.endsWith("In Progress") ) {
                status = getNextMethodStatus(method.getName(),status);          
            }
        } else {
            status = resultString;
        }
        resultString = status;
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while advancing status for study " + s.getName(), ex);
            resultString = "ST0111 Error while advancing status for study " + s.getName() + ": " + ex.getMessage();    
        }
        return resultString;
    }
    
    public String rollbackStatus(Study s, Integer currentUserId) {
        Method method = (Method) methodController.findMethod(s.getIdMethod());           
        String status = getPreviousMethodStatus(method.getName(),s.getStatus());                               
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while rolling back status for study " + s.getName(), ex);
            status = "Error";
        }
        return status;
    }

    public String rollbackStatusBypassInProgress(Study s, Integer currentUserId) {
        Method method = (Method) methodController.findMethod(s.getIdMethod());           
        String status = getPreviousMethodStatus(method.getName(),s.getStatus());     
        if ( status.endsWith("In Progress") ) {
            status = getPreviousMethodStatus(method.getName(),status);          
        }        
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while rolling back status for study " + s.getName(), ex);
            status = "Error";
        }
        return status;
    }
    
    public String setStudyStatus(Integer studyId, String status, Integer currentUserId) {
        Study s = (Study) studyController.findStudy(studyId);
        s.setStatus(status);
        try {
            studyController.updateObjects(s, currentUserId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while rolling back status on study " + s.getName(), ex);
            status = "Error";
        }
        return status;
    }

    //-------------------------------------------------------------------------- Study Details
    public String getStudyDetails(String studyId, Integer organizationId, Integer userId) {
        String result = "Study details not retrieved";
        String dataString = "";
        Study study = null;
        try {
            Integer stdId = Integer.parseInt(studyId);
            study = (Study)studyController.findStudy(stdId);
            if (study == null) {
               result = "ST0113 Study not found"; 
            } else {
                if ( (study.getIdOrganization() != organizationId) && (study.getIdOrganization() > 0) ) {
                    result = "ST0114 User not authorized";
                } else {
                    JSONObject JSONStudy = new JSONObject(study);
                    ModelColumn outcome;
                    outcome = modelcolumnController.findModelColumn(JSONStudy.getInt("idOutcome"));
                    if ( outcome != null ) {
                        JSONObject JSONOutcome = new JSONObject(outcome);
                        JSONStudy.put("outcome", JSONOutcome);
                    } else {
                        JSONStudy.put("outcome", "");
                    }
                    dataString = "[" + JSONStudy.toString() + "]";
                }
            }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while retrieving Study details: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0115 Exception while retrieving study details: " + ex.getMessage(); 
        }
        return wrapAPI("studyDetails", dataString, "Study details retrieved", result);
    }
    
    //-------------------------------------------------------------------------- Study Categories
    public List<ModelColumn> getStudyCategories(int studyId) {
        List<ModelColumn> columns = new ArrayList();
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK();       
        shcPK.setStudyidStudy(studyId);
        List<StudyHasCategory> shc = studyhascategoryController.findStudyHasCategoryByStudyIdEntities(studyId);
        if ( (shc.isEmpty()) ) {
            //logger.log(Level.INFO, "StudyCategory does not exist");
            return null;
        }
        for (StudyHasCategory t : shc) {
            int i = t.getStudyHasCategoryPK().getModelColumnidModelColumn();
            ModelColumn r = modelcolumnController.findModelColumn(i);
            if ( r != null ) {
                columns.add(r);
            }
        }
        return columns;
    }
    
    public boolean checkStudyHasCategoryExists(int studyId, int categoryId) {
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK(studyId,categoryId);          
        StudyHasCategory shc = studyhascategoryController.findStudyHasCategory(shcPK);
        if ( (shc == null) ) {
            //logger.log(Level.INFO, "StudyCategory does not exist");
            return false;
        } else {    
            return true;
        }
    }
    
    private StudyHasCategory createStudyCategories(Study study, int id, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} creating relationship between category {1} and study {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), study.getName()});        
 
        if ( checkStudyHasCategoryExists(study.getIdStudy(),id) == true ) {
            throw new EntityExistsException("The category for study " + study.getName() + " already exists.");
        }        
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK(study.getIdStudy(),id); 
        StudyHasCategory shc = new StudyHasCategory(shcPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        shc.setCreatedTS(currentTimestamp);
        shc.setUpdatedTS(currentTimestamp);
        shc.setCreatedBy(currentUserId);
        shc.setUpdatedBy(currentUserId);    
        shc = studyhascategoryController.edit(shc);
        return shc;
    }
     
    public String createStudyCategories(String JSONString, Integer currentUserId) {
        String result = "Study Categories not created";
        Study study = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray anArray = JSONContentRoot.getJSONArray("studies");
            for (int ii=0; ii<anArray.length(); ii++) {
                logger.log(Level.INFO, "Creating Categories for Study {0}", anArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                study = gson.fromJson(jString, Study.class);   
                JSONArray categoryArray = JSONContentRoot.getJSONArray("categories");                
                for (int i=0; i<categoryArray.length(); i++) {
                    StudyHasCategory shc = createStudyCategories(study, categoryArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(shc).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Study Category: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0008 Exception while creating new Category: " + ex.getMessage();  
        }   
        return wrapAPI("studyCategories", dataString, "Study Categories created", result);
    }

    private StudyHasCategory deleteStudyCategories(Study study, int id, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship between category {1} and study {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), study.getName()});
             
        StudyHasCategoryPK shcPK = new StudyHasCategoryPK(study.getIdStudy(),id); 
        StudyHasCategory shc = new StudyHasCategory(shcPK);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        shc.setUpdatedTS(currentTimestamp);
        shc.setUpdatedBy(currentUserId);    
        studyhascategoryController.destroy(shcPK);
        return shc;
    }
     
    public String deleteStudyCategories(String JSONString, Integer currentUserId) {
        String result = "StudyCategories not deleted";
        Study study = null;
        String dataString = "";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray anArray = JSONContentRoot.getJSONArray("studies");
            for (int ii=0; ii<anArray.length(); ii++) {
                logger.log(Level.INFO, "Deleting Categories for Study {0}", anArray.getJSONObject(ii).getString("name"));
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
                String jString = anArray.getJSONObject(ii).toString();
                study = gson.fromJson(jString, Study.class);   
                JSONArray categoryArray = JSONContentRoot.getJSONArray("categories");                
                for (int i=0; i<categoryArray.length(); i++) {
                    StudyHasCategory uhr = deleteStudyCategories(study, categoryArray.getInt(i), currentUserId);
                    if ( i > 0 ) {
                        dataString = dataString.concat(",");
                    } 
                    dataString = dataString + new JSONObject(uhr).toString();
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for Study: {0} {1}", new Object[]{study.getName(), ex});
            result = "ST0009 Exception while deleting category releationship for study: " + ex.getMessage();
        }   
        return wrapAPI("studyCategories", dataString, "Study Categories deleted", result);
    }
    
    public String getStudyCategoryInfo(int idStudy) {
            
        String dataString = "";
        String result = "No categories found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONObject JSONCategories = new JSONObject();  
        JSONArray JSONArrayCategories = null;

        //logger.log(Level.INFO, "Retrieving Categories for Study {0}.", Integer.toString(idStudy));         
        try {
            dataString = getStudyCategoryJSON(idStudy);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while retrieving Categories for Study {0}.", Integer.toString(idStudy));  
            result = "DA0010 Exception parsing Study into JSON";
        }   
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                JSONArrayCategories = new JSONArray(dataString);
                success.put("success", true);   
                status.put("message", Integer.valueOf(JSONArrayCategories.length()) + " study categories retrieved");   
                success.put("totalCount", Integer.valueOf(JSONArrayCategories.length()));
                JSONCategories.put("categories", JSONArrayCategories);
                success.put("studyCategories", JSONCategories);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [study.getStudyCategory]: ", ex);  
            result = "JSON error in WS method [study.getStudyCategory]";
        }         
        return result;
    }
  
    //-------------------------------------------------------------------------- Filter Formulas
    public List<FilterField> getFilterFormulas(int filterId) {

        List<FilterField> ent = filterfieldController.findFilterFieldByFilterId(filterId);
        if ( (ent.isEmpty()) ) {
            logger.log(Level.INFO, ""
                    + "FilterField does not exist");
            return null;
        }
        return ent;
    }
        
    /*public boolean checkFilterHasFormulaExists(int filterId, int formulaId) {

        FilterHasFieldPK entPK = new FilterHasFieldPK(filterId,formulaId);          
        FilterHasField ent = filterhasfieldController.findFilterHasField(entPK);
        if ( (ent == null) ) {
            logger.log(Level.INFO, "Filter Field does not exist");
            return false;
        } else {
            logger.log(Level.INFO, "Filter Field does not exist");            
            return true;
        }
    }*/

   public String getSqlFromFilterFormulas(int filterId) {

        if ( filterId == 0 ) return null;
        
        List<ModelColumn> columns = new ArrayList();
        List<FilterField> ffList = filterfieldController.findFilterFieldByFilterId(filterId);
        if ( (ffList.isEmpty()) ) {
            logger.log(Level.INFO, ""
                    + "FilterField does not exist");
            return null;
        }
        for (FilterField t : ffList) {
            int i = t.getModelColumnidModelColumn();
            ModelColumn r = modelcolumnController.findModelColumn(i);
            if ( r != null ) {
                columns.add(r);
            } else {
                return null; // do not apply filter if any of columns is missing
            }
        }
        //=================== this should stay in synch with JavaScript in FilterFormulaGrid.js
        int orFlag = 0;
        String sqlOR = "";
        String sql = "";
        // build ORs first
        for (int i=0; i<ffList.size(); i++) {
            FilterField r = ffList.get(i);   
            if ( "Include".equals(r.getType()) ) {
                String name = "";
                for (int j=0; j<columns.size(); j++) {
                    if ( columns.get(j).getIdModelColumn() == r.getModelColumnidModelColumn() ) {
                        name = columns.get(j).getName();
                        break;
                    }
                }
                if ( orFlag > 0 ) {
                    sql += " OR ";
                }
                orFlag++;
                sql += "(" + name + " " + r.getFormula() + ")";        
            }
        }
        if ( orFlag > 1 ) {
            sqlOR = "(" + sql + ")";
        } else {
            sqlOR = sql;
        }
        // now built negative ANDs
        int andFlag = 0;
        sql = "";
        for (int i=0; i<ffList.size(); i++) {
            FilterField r = ffList.get(i);        
            if ( "Exclude".equals(r.getType()) ) {
                String name = "";
                for (int j=0; j<columns.size(); j++) {
                    if ( columns.get(j).getIdModelColumn() == r.getModelColumnidModelColumn() ) {
                        name = columns.get(j).getName();
                        break;
                    }
                }
                if ( (orFlag > 0) || (andFlag > 0) ) {
                    sql += " AND ";
                }
                andFlag++;
                sql += "NOT (" + name + " " + r.getFormula() + ")";      
            }
        }                    
        //===================
        return sqlOR+sql;
    }          
    
    private FilterField createFilterFormulas(FilterField ent, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        //logger.log(Level.SEVERE, "User {0} creating relationship between formula {1} and filter {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString((id)), filter.getName()});        
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);    
        ent = filterfieldController.edit(ent);
        return ent;
    }
     
    public String createFilterFormulas(String JSONString, Integer currentUserId) {
        String result = "Filter formula not created";
        Filter filter = null;
        String dataString = "";
        
        try {
            //JSONObject JSONContentRoot =  new JSONObject(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
            //String jString = anArray.getJSONObject(ii).toString();
            FilterField filterField = gson.fromJson(JSONString, FilterField.class);   
            logger.log(Level.INFO, "Creating Formula for Filter ID {0}", filterField.getFilteridFilter());            
            FilterField ent = createFilterFormulas(filterField, currentUserId);
            dataString = dataString + new JSONObject(ent).toString();
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Filter Formula: {0} {1}", new Object[]{filter.getName(), ex.getMessage()});
            result = "ST0011 Exception while creating new Formula: " + ex.getMessage();  
        }   
        return wrapAPI("filterFormulas", dataString, "Filter Formulas created", result);
    }
 
    private FilterField updateFilterFormula(FilterField ent, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} updating relationship between field {1} and filter {2}", 
                new Object[]{Integer.toString((currentUserId)), 
                    Integer.toString((ent.getModelColumnidModelColumn())), 
                    Integer.toString(ent.getFilteridFilter())});            
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);    
        ent = filterfieldController.edit(ent);
        return ent;
    }
    
    public String updateFilterFormula(String JSONString, Integer currentUserId) {
        String result = "Filter Formula not updated";
        String dataString = "";
        FilterField entity;
        int idFilter = 0, idModelColumn = 0;
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
            String jString = JSONContentRoot.toString();
            entity = gson.fromJson(jString, FilterField.class);             
            idModelColumn = entity.getModelColumnidModelColumn();
            idFilter = entity.getFilteridFilter();
            logger.log(Level.INFO, "Updating Formula Field {0} for Filter {1}", new Object[]{Integer.toString(idModelColumn),Integer.toString(idFilter)});
            entity = updateFilterFormula(entity, currentUserId);
            dataString = dataString + new JSONObject(entity).toString();
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while updating category releationship for Filter: {0} {1}", new Object[]{Integer.toString(idFilter), ex});
            result = "ST0012 Exception while updating formula releationship for filter: " + ex.getMessage();
        }   
        return wrapAPI("filterFormulas", dataString, "Filter Formula updated", result);
    }
    
    private FilterField deleteFilterFormulas(int idFilterField, int currentUserId) throws NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} deleting relationship {1} between field and filter", new Object[]{Integer.toString((currentUserId)), Integer.toString((idFilterField))});             
        FilterField ent = new FilterField(idFilterField);
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ent.setUpdatedTS(currentTimestamp);
        ent.setUpdatedBy(currentUserId);    
        filterfieldController.destroy(idFilterField);
        return ent;
    }

    public String deleteFilterFormula(String JSONString, Integer currentUserId) {
        String result = "Filter Formulas not deleted";
        String dataString = "";
        
        int idFilterField = 0;
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i=0; i<objectArray.length(); i++) {                        
                JSONObject JSONContentRoot =  objectArray.getJSONObject(i);
                idFilterField = JSONContentRoot.getInt("idFilterField");
                logger.log(Level.INFO, "Deleting Formula Field {0}", new Object[]{Integer.toString(idFilterField)});
                FilterField ent = deleteFilterFormulas(idFilterField, currentUserId);
                if ( sepCounter++ > 0 ) {
                    dataString = dataString.concat(",");
                }                     
                dataString = dataString + new JSONObject(ent).toString();            
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for Filter: {0} {1}", new Object[]{Integer.toString(idFilterField), ex});
            result = "ST0013 Exception while deleting formula releationship for filter: " + ex.getMessage();
        }   
        return wrapAPI("filterFormulas.formulas", dataString, "Filter Formulas deleted", result);
    }

    public String deleteFilterFormulas(String JSONString, Integer currentUserId) {
        String result = "Filter Formulas not deleted";
        String dataString = "";
        
        int idFilter = 0;
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            int sepCounter = 0;
            for (int i=0; i<objectArray.length(); i++) {                        
                JSONObject JSONContentRoot =  objectArray.getJSONObject(i);
                idFilter = JSONContentRoot.getInt("idFilter");
                List<FilterField> ffList = filterfieldController.findFilterFieldByFilterId(idFilter);
                for (FilterField t : ffList) {
                    logger.log(Level.INFO, "Deleting Formula Field {0}", new Object[]{Integer.toString(t.getIdFilterField())});
                    FilterField ent = deleteFilterFormulas(t.getIdFilterField(), currentUserId);
                    if ( sepCounter++ > 0 ) {
                        dataString = dataString.concat(",");
                    }                     
                    dataString = dataString + new JSONObject(ent).toString();
                }                
            }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting category releationship for Filter: {0} {1}", new Object[]{Integer.toString(idFilter), ex});
            result = "ST0014 Exception while deleting formula releationship for filter: " + ex.getMessage();
        }   
        return wrapAPI("filterFormulas.formulas", dataString, "Filter Formulas deleted", result);
    }

    public String deleteFilterAndFormulas(String JSONString, Integer currentUserId) {
        String result = "ok";
        Filter filter = null;
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
            String jString = JSONContentRoot.toString();
            filter = gson.fromJson(jString, Filter.class);   
            logger.log(Level.INFO, "Deleting Formulas for Filter {0}", filter.getName());         
            filterController.deleteObject(filter, currentUserId);
            List<FilterField> filterFields = filterfieldController.findFilterFieldByFilterId(filter.getIdFilter());             
            for (int i=0; i<filterFields.size(); i++) {
                filterfieldController.destroy(filterFields.get(i).getIdFilterField());
                }     
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while deleting Filter and Formulas: {0}", new Object[]{ex.getMessage()});
            result = "ST0119 Exception while deleting Filter and Formulas: " + ex.getMessage();  
        }   
        return result;
    }    
    
    public String cloneFilter(String JSONString, Integer currentUserId) {
        String result = "ok";
        Filter filter = null;
        
        try {
            int oldId;
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
            String jString = JSONContentRoot.toString();
            filter = gson.fromJson(jString, Filter.class);   
            oldId = filter.getIdFilter();
            logger.log(Level.INFO, "Cloning Formulas for Filter {0}", filter.getName());         
            filter.setIdFilter(0);
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
            String dateTime =  String.format("%1$TY%1$Tm%1$Td-%1$TH%1$TM%1$TS", currentTimestamp);  
            filter.setName("Cloned " + dateTime);                
            filter.setCreatedTS(currentTimestamp);
            filter.setUpdatedTS(currentTimestamp);
            filter.setCreatedBy(currentUserId);
            filter.setUpdatedBy(currentUserId); 
            filter = filterController.edit(filter);
            List<FilterField> filterFields = filterfieldController.findFilterFieldByFilterId(oldId);             
            for (int i=0; i<filterFields.size(); i++) {
                cloneFilterFormulas(filter, filterFields.get(i), currentUserId);
                }
            filterFields = null;
            filter = null;            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while cloning new Formula for Filter: {0}", new Object[]{ex.getMessage()});
            result = "ST0118 Exception while cloning new Formula for Filter: " + ex.getMessage();  
        }   
        return result;
    }
    
    private FilterField cloneFilterFormulas(Filter filter, FilterField ff, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
        logger.log(Level.SEVERE, "User {0} cloning filter field {1} for filter {2}", new Object[]{Integer.toString((currentUserId)), Integer.toString(ff.getIdFilterField()), filter.getName()});        
        FilterField ent = new FilterField(); 
        ent.setIdFilterField(0);
        ent.setFilteridFilter(filter.getIdFilter());
        ent.setModelColumnidModelColumn(ff.getModelColumnidModelColumn());
        ent.setFormula(ff.getFormula());
        ent.setNegator(ff.getNegator());
        ent.setOperator(ff.getOperator());
        ent.setType(ff.getType());
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ent.setCreatedTS(currentTimestamp);
        ent.setUpdatedTS(currentTimestamp);
        ent.setCreatedBy(currentUserId);
        ent.setUpdatedBy(currentUserId);    
        ent = filterfieldController.edit(ent);
        return ent;
    }
    
    public String getFilterFormulaInfo(int idFilter) {
            
        String dataString = "";
        String result = "No formulas found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONObject JSONCategories = new JSONObject();  
        JSONArray JSONArrayCategories = null;

        //logger.log(Level.INFO, "Retrieving Formulas for Filter {0}.", Integer.toString(idFilter));         
        try {
            dataString = getFilterFormulaJSON(idFilter);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception parsing Filter into JSON", ex.getMessage());
            result = "ST0015 Exception parsing Filter into JSON";
        }   
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", true);       // change from false not to alarm user, possible todo
                status.put("message", result);   
            } else {
                JSONArrayCategories = new JSONArray(dataString);
                success.put("success", true);   
                status.put("message", JSONArrayCategories.length() + " filter formulas retrieved");   
                success.put("totalCount", Integer.valueOf(JSONArrayCategories.length()));
                //JSONCategories.put("formulas", JSONArrayCategories);
                success.put("filterFormulas", JSONArrayCategories);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [study.getFilterFormulaInfo]: ", ex);  
            result = "JSON error in WS method [study.getFilterFormulaInfo]";
        }         
        return result;
    }
      
    public String getFilters(int start, int limit, Integer userId, Integer organizationId) {
   
        // this method gets filter by OrganizationId
        // the one implemnted through adminService.getOrgObjects() gets them by ModelId
        String logId = "DELTA3 WS method [study.getFilters]";
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                      
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Object> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[]{filterController.getObjectType(), Integer.toString(start)});
        if ( limit == 0 ) { // get all
            subjects = filterController.findOrgFilterEntities(organizationId);
        } else {
            subjects = filterController.findOrgFilterEntities(limit, start, organizationId);
        }
        int max = subjects.size();

        for ( ; recordCounter < max; recordCounter++) {         
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString(); 
            String fields = getFilterFormulaJSON(((Filter)subjects.get(recordCounter)).getIdFilter());      
            if ( "".equals(fields) ) {
                fields = "[]";
            }
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
                dataString = new StringBuilder(dataString).insert(dataString.length()-2, ",\"fields\":" + fields).toString();
            } else {
                dataString = new StringBuilder(dataString).insert(dataString.length()-1, ",\"fields\":" + fields).toString();
            }            
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", Integer.valueOf(recordCounter) + " " + filterController.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(filterController.getOrgFilterCount(organizationId)));
                success.put(filterController.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in " + logId + ": ", ex);  
            result = "JSON error in " + logId;
        }        
        return result;        
    }    

    public String getFilterUsage(Integer filterId, Integer userId, Integer organizationId) {

        String logId = "DELTA3 WS method [study.getFilterUsage]";
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());               
        
        String dataString = "";
        String result = "No records found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  

        List<Study> subjects = null;
        int recordCounter = 0;
        logger.log(Level.INFO, "Getting list of studies dependent on filter {0}", new Object[]{Integer.toString(filterId)});
        subjects = studyController.findStudyEntitiesByFilterId(filterId);
        int max = subjects.size();
        for ( ; recordCounter < max; recordCounter++) {
            dataString = dataString + new JSONObject(subjects.get(recordCounter)).toString();
            if ( recordCounter != max-1 ) {
                dataString = dataString.concat(",");
            } 
        }
        dataString = "[" + dataString + "]";
 
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                success.put("success", true);   
                status.put("message", recordCounter + " " + studyController.getObjectType() + " retrieved");   
                success.put("totalCount", Integer.valueOf(recordCounter));
                success.put(studyController.getObjectType(), new JSONArray(dataString));
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in " + logId + ": ", ex);  
            result = "JSON error in " + logId;
        }        
        return result;        
    }  
    
    public String getFilterWhereClause(Integer filterId) {
        String result = "[]";
        String dataString = "SQL formatted";
        
        try {
            String backSql = getSqlFromFilterFormulas(filterId);
            logger.log(Level.INFO, "Obtaining Filter Where Clause using Filter ID {0} result: {1}", new Object[]{filterId,backSql});
            result = "[\"" + backSql + "\"]";
            logger.log(Level.SEVERE, dataString + " : " + result);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while obtaining Filter Where Clause: {0} {1}", new Object[]{filterId, ex});
            dataString = "ST0016 Exception while obtaining Filter Where Clause: " + ex.getMessage();  
        }   
        return wrapAPI("filterWhereClause", result, dataString, dataString);
    }   
    
    public String validateFilter(String JSONString, Integer userId) {
        String result = "[]";
        Filter filter = null;
        String dataString = "";
        String dataString2 = "0";
        
        try {
            JSONObject JSONContentRoot =  new JSONObject(JSONString);
            JSONArray filterFormulaArray = JSONContentRoot.getJSONArray("filterFormulas");
            dataString = "Filter(s) validated: ";
            dataString2 = "<br/>Number of records: ";            
            for (int ii=0; ii<filterFormulaArray.length(); ii++) {
                // for now response formatting is only for a single filter 
                JSONObject filterFormula = filterFormulaArray.getJSONObject(ii);  
                //String frontSql = filterFormula.getString("condition");
                JSONObject filterObject = filterFormula.getJSONObject("filter");
                String filterName = filterObject.getString("name");   
                int filterId = filterObject.getInt("idFilter");
                String backSql = getSqlFromFilterFormulas(filterId);
                logger.log(Level.INFO, "Verifying Formulas for Filter {0}", filterName);
//                if ( !frontSql.equals(backSql) ) {
//                    dataString = "SQL generation mismatch for filter " + filterName + 
//                            " Id " + Integer.toString(filterId) + ". Contact your System Administrator";
//                    result = "";
//                    break;
//                    }
                filter = filterController.findFilter(filterId);
                int modelId = filter.getModelidModel();
          
                Model model = modelController.findModel(modelId);     
                String validationResult = verifyFilter(model.getOutputName() + ModelServiceImpl.flatTableAppendString, backSql, userId);
                if ( !"Ok".equals(validationResult) ) {
                    dataString = "Filter " + filterName + " was not validated: " + validationResult;
                    result = "";
                    break;
                    }
                dataString += filterName + " ";
                String numberOfRecords = getNumberOfFilterRecords(model.getOutputName() + ModelServiceImpl.flatTableAppendString, backSql, userId);
                dataString2 += numberOfRecords + " ";
                Integer resultCount = Integer.parseInt(numberOfRecords);
                filter.setResultCount(resultCount);
                filterController.updateObjects(filter, userId);
                }
            dataString += dataString2;
            logger.log(Level.SEVERE, dataString);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while veryfing Filter Formula: {0} {1}", new Object[]{JSONString, ex});
            dataString = "ST0016 Exception while verifying Filter Formula: " + ex.getMessage();  
        }   
        return wrapAPI("filterFormulas", result, dataString, dataString);
    }    

    public String findStudiesByModel(Model m) {    
        String returnString = "";
        List<Study> sList;
        
        try {
            sList = studyController.findStudyEntitiesByModelId(m.getIdModel());
            if ( (sList != null) && !sList.isEmpty() ) {
                returnString = "Model is used by Study: ";
                int i = 0;
                for (Study s: sList) {
                   if ( i++ > 0 ) {
                       returnString += ",";
                   }
                   returnString += s.getName();
               }
            }
            return returnString;            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception when searching for Study that uses: " + m.getIdModel().toString(), ex.getMessage());
            returnString =  "ST0017 Exception when searching for study: " + ex.getMessage();            
        }    
        return returnString;
    } 
 
    public String getResultsforStudy(String studyId, Integer userId) {      
        String dataString = "";
        List<Process> processList = processController.findProcessEntitiesByStudyId(Integer.valueOf(studyId));
        if ( processList.size() > 0 ) {
            Process process = processList.get(processList.size()-1); // get the last one (and the only one)
            dataString = "[" + new JSONObject(process).toString() + "]";   
        }             
        return wrapAPI("process", dataString, "Results retrieved", "ST0018 Study not processed by statistic engine");
    }    

    public String exportDoubleArrayCSV(String studyId, Integer currentOrgId, Integer currentUserId) {
        Study study = null;       
        try {
            logger.log(Level.INFO,"GET Study Integer ID: "+studyId+" org id :"+currentOrgId+" userid:"+currentUserId);
            Integer stdId = Integer.parseInt(studyId);
            logger.log(Level.INFO,"GET Study Integer ID:",stdId);
            
            study = (Study)studyController.findStudy(stdId);
            if ( study.getIdOrganization() != currentOrgId ) {
                return "ST0113 User not authorized";
            } else {    
                
                //System.out.println("ReadDoubleArray function :"+Util.dbReadDoubleArrayFromCSV(studyId, logger));
                return Util.dbReadDoubleArrayFromCSV(studyId, logger);         
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "File not found while getting Double Array for Study: {0} {1}", new Object[]{study.getName(), ex});
            return "ST0140 File not found for Study "  + study.getName();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Double Array for Model: {0} {1}", new Object[]{study.getName(), ex});
            return "ST0141 Exception while getting Double Array for Model "  + study.getName();
        }
    }
 
   public String exportStudy(String JSONString, Integer currentOrgId, Integer currentUserId, Object subject) {
        
        String resultString = "Study not exported";   
        Integer studyId;
        Study study = null;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            studyId = objectArray.getJSONObject(0).getInt("idStudy"); 
            study = (Study) studyController.findStudy(studyId);     
            if ( !Objects.equals(study.getIdOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "ST0083 Study export failed. Not authorized.");
                return "ST0083 Study export failed. Not authorized."; 
            }             
            JSONObject theStudy = new JSONObject(study);
            String serializedModel = theStudy.toString();
            resultString = "{\"objectType\":\"Study\",\"version\":\"300\",\"study\":" + serializedModel + "}";            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while exporting Study: {0} {1}", new Object[]{study.getName(), ex});
            return "ST0040 Exception while exporting Study "  + study.getName();
        }
        return resultString;
    }

   public String exportStudyInfo(String JSONString, Integer currentOrgId, Integer currentUserId, String webRootPath) {
    
        String templStr = "";
        Integer studyId;
        Study study = null;
        ModelColumn field;
        Integer id;
        
        try {           
            templStr = Util.readFileFromDisk("studyInfo.html", webRootPath + "/resources/templates/");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception while exporting Study Info: {0}", e.getMessage());
            return "ST0056 Exception while exporting Study Info. Cannot find template.";            
        }
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            studyId = objectArray.getJSONObject(0).getInt("idStudy");    
            study = (Study) studyController.findStudy(studyId);     
            if ( !Objects.equals(study.getIdOrganization(), currentOrgId) ) {
                logger.log(Level.SEVERE, "ST0084 Export study information failed. Not authorized.");
                return "ST0084 Export study information failed. Not authorized."; 
            }             
            templStr = templStr.replaceAll("\\{study.id\\}",Integer.toString(studyId));
            if ( study.getProcessedTS() != null ) {
                templStr = templStr.replaceAll("\\{study.processedTS\\}", new SimpleDateFormat("MM/dd/yyyy").format(study.getProcessedTS()));          
            } else {
                templStr = templStr.replaceAll("\\{study.processedTS\\}", "");                          
            }
            templStr = templStr.replaceAll("\\{study.name\\}",study.getName());
            templStr = templStr.replaceAll("\\{study.description\\}",study.getDescription());     
            templStr = templStr.replaceAll("\\{study.stage\\}",study.getStatus());     
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            templStr = templStr.replaceAll("\\{system.timestamp\\}",new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(currentTimestamp));
            if ( study.getIdGroup() > 0 ) {
                GroupTable group = groupController.findGroupTable(study.getIdGroup());
                if ( group != null ) {
                    templStr = templStr.replaceAll("\\{project.name\\}",group.getName());
                }
            } else {
                templStr = templStr.replaceAll("\\{project.name\\}","Not Assigned");
            }
            Model model = modelController.findModel(study.getIdModel());
            if ( model != null ) {
                templStr = templStr.replaceAll("\\{model.name\\}",model.getName());                
                templStr = templStr.replaceAll("\\{model.description\\}",model.getDescription());
                if ( model.getStatus() != null ) {
                    templStr = templStr.replaceAll("\\{model.status\\}",model.getStatus());
                } else {
                    templStr = templStr.replaceAll("\\{model.status\\}",""); 
                }
            } else {
                templStr = templStr.replaceAll("\\{model.name\\}","not defined");                
                templStr = templStr.replaceAll("\\{model.description\\}","");  
                templStr = templStr.replaceAll("\\{model.status\\}","");              
            }
            Method method = (Method) methodController.findMethod(study.getIdMethod());
            if ( method != null ) {
                templStr = templStr.replaceAll("\\{method.name\\}",method.getName());    
                if ( method.getDescription() != null ) {
                    templStr = templStr.replaceAll("\\{method.description\\}",method.getDescription());
                } else {
                    templStr = templStr.replaceAll("\\{method.description\\}","");
                }
                templStr = templStr.replaceAll("\\{method.updatedTS\\}",new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(method.getUpdatedTS()));
            } else {
                templStr = templStr.replaceAll("\\{method.name\\}","not defined");                
                templStr = templStr.replaceAll("\\{method.description\\}","");
                templStr = templStr.replaceAll("\\{method.updatedTS\\}","");            
            }            
            templStr = templStr.replaceAll("\\{study.startTS\\}", new SimpleDateFormat("MM/dd/yyyy").format(study.getStartTS()));  
            templStr = templStr.replaceAll("\\{study.endTS\\}", new SimpleDateFormat("MM/dd/yyyy").format(study.getEndTS()));     
            
            id = study.getIdOutcome();
            if ( id != null ) {            
                field = modelcolumnController.findModelColumn(study.getIdOutcome());
                if ( field != null ) {
                    templStr = templStr.replaceAll("\\{study.outcome.name\\}",field.getName());       
                    templStr = templStr.replaceAll("\\{study.outcome.description\\}",field.getDescription());      
                    templStr = templStr.replaceAll("\\{study.outcome.positive\\}",study.getOutcomePositive().toString());       
                } else {
                    templStr = templStr.replaceAll("\\{study.outcome.name\\}","not defined");   
                    templStr = templStr.replaceAll("\\{study.outcome.description\\}","");      
                    templStr = templStr.replaceAll("\\{study.outcome.positive\\}","");    
                }
            } else {
                templStr = templStr.replaceAll("\\{study.outcome.name\\}","not assigned");   
                templStr = templStr.replaceAll("\\{study.outcome.description\\}","");      
                templStr = templStr.replaceAll("\\{study.outcome.positive\\}","");                    
            }
            
            id = study.getIdKey();
            if ( id != null ) {
                field = modelcolumnController.findModelColumn(study.getIdKey());
                if ( field != null ) {
                    templStr = templStr.replaceAll("\\{study.key.name\\}",field.getName());              
                    templStr = templStr.replaceAll("\\{study.key.description\\}",field.getDescription());           
                } else {
                    templStr = templStr.replaceAll("\\{study.key.name\\}","not defined");   
                    templStr = templStr.replaceAll("\\{study.key.description\\}","");                        
                }
            } else {
                templStr = templStr.replaceAll("\\{study.key.name\\}","not assigned");   
                templStr = templStr.replaceAll("\\{study.key.description\\}","");                       
            }
            
            id = study.getIdDate();
            if ( id != null ) {            
                field = modelcolumnController.findModelColumn(study.getIdDate());
                if ( field != null ) {
                    templStr = templStr.replaceAll("\\{study.sequence.name\\}",field.getName());              
                    templStr = templStr.replaceAll("\\{study.sequence.description\\}",field.getDescription());           
                } else {
                    templStr = templStr.replaceAll("\\{study.sequence.name\\}","not defined");   
                    templStr = templStr.replaceAll("\\{study.sequence.description\\}","");                        
                }   
            } else {
                templStr = templStr.replaceAll("\\{study.sequence.name\\}","not assigned");   
                templStr = templStr.replaceAll("\\{study.sequence.description\\}","");                         
            }         
            
            id = study.getIdTreatment();
            if ( id != null ) {
                field = modelcolumnController.findModelColumn(study.getIdTreatment());
                if ( field != null ) {
                    templStr = templStr.replaceAll("\\{study.treatment.name\\}",field.getName());              
                    templStr = templStr.replaceAll("\\{study.treatment.description\\}",field.getDescription());           
                } else {
                    templStr = templStr.replaceAll("\\{study.treatment.name\\}","not defined");   
                    templStr = templStr.replaceAll("\\{study.treatment.description\\}","");                        
                }                  
            } else {
                templStr = templStr.replaceAll("\\{study.treatment.name\\}","not assigned");   
                templStr = templStr.replaceAll("\\{study.treatment.description\\}","");                        
            }    
            if ( study.getConfidenceInterval() != null ) {
                templStr = templStr.replaceAll("\\{study.CI\\}", study.getConfidenceInterval().toString());
            } else {
                templStr = templStr.replaceAll("\\{study.CI\\}", "");
            }
            if ( study.getConfidenceInterval2() != null ) {            
                templStr = templStr.replaceAll("\\{study.CI2\\}", study.getConfidenceInterval2().toString());
            } else {
                templStr = templStr.replaceAll("\\{study.CI2\\}", "");
            }
            if ( study.getChunkingBy() != null ) {
                templStr = templStr.replaceAll("\\{study.chunking\\}", study.getChunkingBy());
            } else {
                templStr = templStr.replaceAll("\\{study.chunking\\}", "");
            }
            if ( study.getIsMissingDataUsed() == true ) {
                templStr = templStr.replaceAll("\\{study.missingData\\}","yes"); 
            } else {
                templStr = templStr.replaceAll("\\{study.missingData\\}","no"); 
            }
            Filter filter = filterController.findFilter(study.getIdFilter());
            if ( (filter != null) && (filter.getActive() == true) ) {
                templStr = templStr.replaceAll("\\{study.filter.name\\}",filter.getName());       
                templStr = templStr.replaceAll("\\{study.filter.description\\}",filter.getDescription());      
                templStr = templStr.replaceAll("\\{study.filter.sql\\}",getSqlFromFilterFormulas(study.getIdFilter()));                
                templStr = templStr.replaceAll("\\{study.filter.recordCount\\}",filter.getResultCount().toString());     
            } else {
                templStr = templStr.replaceAll("\\{study.filter.name\\}","not defined");   
                templStr = templStr.replaceAll("\\{study.filter.description\\}","");  
                templStr = templStr.replaceAll("\\{study.filter.sql\\}",""); 
                templStr = templStr.replaceAll("\\{study.filter.recordCount\\}","");  
                
            }    
            String paramHtml = "";
            if ( (study.getMethodParams() == null) || ("".equals(study.getMethodParams())) ) {
                paramHtml = "<tr><td>Study parameters are not initialized</td></tr>";
            } else {             
                
                JSONArray params = new JSONArray(study.getMethodParams());
                //System.out.println("This is JSONArray params"+params);
                
                for ( int i=0; i < params.length(); i++ ) {
                    JSONArray paramNames = params.getJSONObject(i).getJSONObject("inputs").names();
                    
                    JSONArray paramNamesOrdered = new JSONArray();
                    
                    //Designed hash map for ordered JSON
                    HashMap<Integer, String> ordered = new HashMap<Integer, String>();
                    //System.out.println("this is paramNames in JSONArray"+paramNames+" index "+i);
                   
                     //Add sequence position hashmap
                    for ( int k=0; k < paramNames.length(); k++ ) {
                            if (paramNames.getString(k).equals("studyUniqueId")){ ordered.put(1, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("sequencingVariableSelection")){ordered.put(2, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("model")){ordered.put(3, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("outcomeVariableSelection")){ ordered.put(4, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("exposureVariableSelection")){ ordered.put(5, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("dependentVariableSelection")){ordered.put(6, paramNames.getString(k));}        
                            if (paramNames.getString(k).equals("independentVariableSelection")){ordered.put(7, paramNames.getString(k));}       
                            if (paramNames.getString(k).equals("studyStartDate")){ordered.put(8, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("studyEndDate")){ordered.put(9, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("followupDateSelection")){ordered.put(10, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("exposureDateSelection")){ordered.put(11, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("outcomeDateSelection")){ordered.put(12, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("durationType")){ordered.put(13, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("caliper")){ordered.put(14, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("caliperType")){ordered.put(15, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("matchNumber")){ordered.put(16, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("matchType")){ordered.put(17, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("matchOrder")){ordered.put(18, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("replaceMatches")){ordered.put(19, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("matchSet")){ordered.put(20, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("reportingPeriod")){ordered.put(21, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("estimationPscore")){ordered.put(22, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("dataSplitOption")){ordered.put(23, paramNames.getString(k));}                         
                            if (paramNames.getString(k).equals("alphaError")){ordered.put(24, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("betaError")){ordered.put(25, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("oddsRatio")){ordered.put(26, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("numberPeriods")){ordered.put(27, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("alphaSpending")){ordered.put(28, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("commonSupport")){ordered.put(29, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("duplicatePscores")){ordered.put(30, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("automaticVariableSelection")){ordered.put(31, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("chunkingPeriod")){ordered.put(32, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("upper_trim")){ordered.put(33, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("lower_trim")){ordered.put(34, paramNames.getString(k));}
                            if (paramNames.getString(k).equals("rollingWindowUse")){ordered.put(35, paramNames.getString(k));}
                           
                    }
                    
                    //System.out.println("Ordered Hash Map : "+ordered);
                    //System.out.println("Ordered Hash Map keys : "+ordered.keySet().toArray().toString());
                    //System.out.println("Ordered sorted Hash Map keys : =========");
                    Object[] mykeys = ordered.keySet().toArray();
                    Arrays.sort(mykeys);
                        for(Object sortedkey : mykeys) {
                            //System.out.println(sortedkey.toString());
                            //System.out.println(ordered.get(sortedkey));
                            paramNamesOrdered.put(ordered.get(sortedkey));
                    }
                    /*-----------------------------------------------------------------------
                        System.out.println("withonut Sorting--------------------------------------- ");
                        for(Integer key : ordered.keySet()){
                            System.out.println(key);
                        }
                        
                        System.out.println("Ordered Hash Map values : "+ordered.values().toArray().toString());
                        for(String values : ordered.values()){
                            paramNamesOrdered.put(values);
                            System.out.println(values);
                        }
                    -----------------------------------------------------------------------*/    
                    //System.out.println("New ordered array by processing sorted hashmap-------------------");
                    //System.out.println("paramNamesOrdered final" + paramNamesOrdered);
                        
                    for (int j=0; j<paramNamesOrdered.length(); j++) {
                        paramHtml += "<tr>";
                        String inputs = paramNamesOrdered.getString(j);   
                        //System.out.println("\n");
                        //System.out.println("String inputs paramNames : "+inputs+" j : "+j);           
                        if ( j == 0 ) {
                            //for first row
                            paramHtml += "<td rowspan=\"" + Integer.toString(paramNamesOrdered.length()) + "\">Step " 
                            + Integer.toString(i+1) + "<br>" + params.getJSONObject(i).getString("name") + "</td><br>";    
                            //System.out.println("This is paramHtml first row : " +paramHtml);
                        }
                    
                    
                    
                    
                        /*
                        for (int j=0; j<paramNames.length(); j++) {
                        paramHtml += "<tr>";
                        String inputs = paramNames.getString(j);                      
                        if ( j == 0 ) {
                            paramHtml += "<td rowspan=\"" + Integer.toString(paramNames.length()) + "\">Step " 
                                    + Integer.toString(i+1) + "<br>" + params.getJSONObject(i).getString("name") + "</td>";                      
                        } */
                        
                        
                    
                        //input text
                        JSONObject subParams = params.getJSONObject(i).getJSONObject("inputs").getJSONObject(inputs);
                        try {
                            String inputTextString = "";
                            if ( "model".equals(inputs) ) {
                                inputTextString = inputs;
                            } else {
                                inputTextString = subParams.getString("inputText");
                            }
                            paramHtml += "<td>" + inputTextString + "</td>";
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "ST0042 Exception parsing Study parameters: {0} {1}", new Object[]{study.getName(), e.getMessage()});                            
                            paramHtml += "<td>ST0042 Error</td>";
                        }
                        
                        //input value
                        try {
                            String valuesString = "";
                            if ( "model".equals(inputs) ) {
                                valuesString = "[" + subParams.getString("name") + "]";
                            } else {                            
                                valuesString = subParams.getJSONArray("values").toString();
                            }
                            paramHtml += "<td width=\"46%\">" + valuesString + "</td></tr>";
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "ST0043 Exception parsing Study parameters: {0} {1}", new Object[]{study.getName(), e.getMessage()});                            
                            paramHtml += "<td width=\"46%\">ST0043 Error</td></tr>";
                        }
                        
                        
                        
                    }
                }
                
                
                
            }
            templStr = templStr.replaceAll("\\{study.param\\}",paramHtml);                        
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "ST0041 Exception while exporting Study: {0} {1}", new Object[]{study.getName(), ex});
            return "ST0041 Exception while exporting Study " + study.getName() + ". Exception: " + ex.getMessage();   
        }
        return templStr;
    }
   
    public String importStudy(JSONObject impObject, Integer currentOrgId, Integer currentUserId) throws Exception {
        String resultString = "Study not imported.";  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        Study study = new Study();
        JSONObject newStudy;           
        Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();       
        
        try {
            JSONObject studyObject = impObject.getJSONObject("study");  
            Integer modelId = studyObject.getInt("idModel");
            String jString = studyObject.toString();
            study = gson.fromJson(jString, study.getClass());  
            // to allow cros-org import following section is commented out
            //if ( !Objects.equals(study.getIdOrganization(), currentOrgId) ) {
            //    logger.log(Level.SEVERE, "ST0085 Study import failed. Not authorized.");
            //    return "ST0085 Study import failed. Not authorized."; 
            //}             
            // re-use existing object to persist into new db record
            study.setIdStudy(0);
            // pull column ids from imported JSON if they belong to the model in db
            // otherwise 0 will be stored
            study.setIdDate(checkColumnUsedInModel(modelId,studyObject.getInt("idDate")));
            study.setIdTreatment(checkColumnUsedInModel(modelId,studyObject.getInt("idTreatment")));
            study.setIdOutcome(checkColumnUsedInModel(modelId,studyObject.getInt("idOutcome")));
            study.setIdKey(checkColumnUsedInModel(modelId,studyObject.getInt("idKey")));
            study.setStatus("New");           
            study.setIdFilter(0);            
            study.setIdOrganization(currentOrgId);          
            study.setCreatedTS(currentTimestamp);
            study.setCreatedBy(currentUserId);        
            study.setUpdatedTS(currentTimestamp);
            study.setUpdatedBy(currentUserId);                  
            study = studyController.edit(study);     
            newStudy = new JSONObject(study);
            List<GroupHasEntity> gheOld = groupHasEntityController.findGroupHasEntityEntitiesByIdAndType(studyObject.getInt("idStudy"), "studies");
            for (GroupHasEntity g : gheOld) {
                int id = g.getGroupHasEntityPK().getGroupidGroup();
                adminService.createGroupEntities(id, study.getIdStudy(), "studies", currentUserId);
            }              
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing Model: {0} {1}", new Object[]{study.getName(), ex});
            throw ex;
        }
        resultString = newStudy.toString();     
        study = null; 
        newStudy = null;
        gson = null;             
        return resultString;        
    }
    
    public String importStudyWithOverwrite(JSONObject impObject, Integer currentOrgId, Integer currentUserId) throws Exception {
        
        String resultString = "ST0055 Study import failed";  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        Study study = null;      
        List<Study> origStudy;       

        try {
            JSONObject studyObject = impObject.getJSONObject("study");                     
            origStudy = studyController.findStudyByName(studyObject.getString("name"));      
            Integer idModel = studyObject.getInt("idModel");            
            if ( origStudy.isEmpty() ) {
                if ( modelController.findModel(idModel) != null ) {
                    Integer idMethod = studyObject.getInt("idMethod");
                    if ( methodController.findMethod(idMethod) != null ) {
                        // create brand new study
                       return importStudy(impObject, currentOrgId, currentUserId); 
                    } else {
                        throw new Exception("Cannot import Study. Method with ID " 
                                + idMethod.toString()
                                + " does not exist");                        
                    }
                } else {
                    throw new Exception("Cannot import Study. Model with ID " 
                           + idModel.toString()
                           + " does not exist");
                }
            }
            study = origStudy.get(0);
            // to allow cros-org import following section is commented out
            //if ( !Objects.equals(study.getIdOrganization(), currentOrgId) ) {
            //    logger.log(Level.SEVERE, "Study import failed. Not authorized.");
            //    throw new Exception("Study import failed. Not authorized."); 
            //}             
            if ( study.getIdModel() != idModel ) {
                throw new Exception("Cannot import Study. Model IDs do not match");
            }
            study.setName(study.getName() + " imported");
            study.setMethodParams(studyObject.getString("methodParams"));
            study.setUpdatedTS(currentTimestamp);
            study.setUpdatedBy(currentUserId);                 
            study = studyController.edit(study);   
            AuditLogger.log(currentUserId, study.getIdStudy(), "import", study);
            JSONObject newStudy = new JSONObject(study);
            resultString = newStudy.toString();
        } catch (Exception ex) {
            String importSting = impObject.toString();
            logger.log(Level.SEVERE, "Exception while importing Study: {0} {1}", new Object[]{ex.getMessage(), importSting});
            throw ex;
        }     
        return resultString;
    }  
 
    private Integer checkColumnUsedInModel(Integer modelId, Integer modelColumnId) {
        ModelColumn mc = modelcolumnController.findModelColumn(modelColumnId);
        if ( (mc != null) && Objects.equals(mc.getIdModel(), modelId) ) {
            return mc.getIdModelColumn();
        }
        return 0;
    } 
    
    private String verifyFilter(String tableName, String condition, Integer currentUserId) {      
        String sqlTestStmt = null;
        try {
            sqlTestStmt = "SELECT * FROM " + tableName + " WHERE " + condition + " AND (1 = 0)";
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
            AuditLogger.log(currentUserId, 0, "verify filter", sqlTestStmt);            
            Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);
            List<Object[]> results = queryResult.getResultList();
            return "Ok";            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "ST0053 Error while evaluating filter formula: " + sqlTestStmt + " " + ex.getMessage());
            return "ST0053 Error while evaluating filter.\n" + ex.getCause().getCause().toString();            
        }    
    } 
 
    
    private String getNumberOfFilterRecords(String tableName, String condition, Integer currentUserId) {      
        String sqlTestStmt = null;
        try {
            sqlTestStmt = "SELECT COUNT(*) FROM " + tableName + " WHERE " + condition;
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlTestStmt});
            AuditLogger.log(currentUserId, 0, "count filter records", sqlTestStmt);            
            Query queryResult = DBHelper.executeSqlQuery("DeltaData",sqlTestStmt,logger);
            List<Object[]> resultCounts = queryResult.getResultList(); 
            return String.valueOf(resultCounts.get(0));                                   
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "ST0054 Error while calculating number of filter records: " + sqlTestStmt + " " + ex.getMessage());
            return "ST0054 Error while calculating number of filter records.\n" + ex.getCause().toString() + "\n" + ex.getCause().getCause().toString();            
        }    
    } 
    
    private String getStudyCategoryJSON(int idStudy) {
        //logger.log(Level.INFO, "Retrieving Categories for Study {0}", Integer.toString(idStudy));       
        String dataString = "";
        int categoryCounter = 0;
        List<ModelColumn> columns = getStudyCategories(idStudy);

        if ( (columns != null) && !columns.isEmpty() ) { 
            categoryCounter = columns.size();
            //logger.log(Level.INFO, "Study {0} has categories. Retrieving {1} category data.", new Object[]{Integer.toString(idStudy), Integer.toString(categoryCounter)});        

            for (int i=0; i<categoryCounter; i++) {
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(columns.get(i)).toString();
                }
            dataString = "[" + dataString + "]";
            return dataString;
        }       
        return dataString;
    } 

    private String getFilterFormulaJSON(int idFilter) {
        //logger.log(Level.INFO, "Retrieving Formulas for Filter {0}", Integer.toString(idFilter));       
        String dataString = "";
        int counter = 0;
        //List<ModelColumn> columns = getFilterFormulas(idFilter);
        List<FilterField> columns = getFilterFormulas(idFilter);

        if ( (columns != null) && !columns.isEmpty() ) { 
            counter = columns.size();
            //logger.log(Level.INFO, "Filter {0} has formulas. Retrieving {1} formula data.", new Object[]{Integer.toString(idFilter), Integer.toString(counter)});        
            for (int i=0; i<counter; i++) {
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(columns.get(i)).toString();
                }
            dataString = "[" + dataString + "]";
            return dataString;
        }
        return dataString;
    } 
 
    private String getNextMethodStatus(String methodName, String statusName) {
        int len = methodMap.get(methodName).length;
        String [] mStates = methodMap.get(methodName);
        int i=0;
        for ( ; i< len; i++) {
            if ( statusName.equals(mStates[i])) {
                if ( i < len - 1 ) {
                    return mStates[++i];
                } else {
                    break; // this was is last allowed state
                }
            }
        }
        return mStates[1];    
    }
    
    private String getPreviousMethodStatus(String methodName, String statusName) {
        int len = methodMap.get(methodName).length;
        String [] mStates = methodMap.get(methodName);        
        int i=0;
        if ( statusName.equals(mStates[0]) ) {
            return mStates[1]; // special case: error -> first State
        }
        for ( ; i< len; i++) {
            if ( statusName.equals(mStates[i])) {
                if ( i > 2 ) {
                    return mStates[--i];
                } else {
                    break; // this is the first allowed state
                }
            }
        }
        return mStates[1];    
    }   
        
    /* delegete this work to common method in admin service */  
    private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
      return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure); 
    }    
  
    //-------------------------------------------------------------------------- following is custom handling of gson dates
    private static final String[] DATE_FORMATS = new String[] {
            "yyyy-MM-dd'T'HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss.S"
    };

    private class DateDeSerializer implements JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement jsonElement, Type typeOF, JsonDeserializationContext context) throws JsonParseException {
            for (String format : DATE_FORMATS) {
                try {
                    return new SimpleDateFormat(format, Locale.US).parse(jsonElement.getAsString());
                } catch (ParseException e) {        
                    int i = 0;
                }
            }
            throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
                    + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));                
        }
    }  
}
