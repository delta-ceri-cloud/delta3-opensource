/*
 * This class is used to start asynch processing of Atomic Fields
 */

package com.ceri.delta.server.processor;

import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.server.StudyServiceImpl;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;

/**
 *
 * @author Lahey Clinic
 */
public class ProcessStudyBatch implements Runnable {
    private AsyncContext asyncContext;
    private String data;
    private Integer studyId, userId;
    private StudyServiceImpl studyService = null;
    private StudyJpaController studyController = null;
    private static final Logger logger = Logger.getLogger(ProcessStudyBatch.class.getName());        

    public ProcessStudyBatch(AsyncContext asyncContext, String load, String timeStamp, Integer id) {
        this.asyncContext = asyncContext;
        data = load;
        userId = id;
        studyService = new StudyServiceImpl();
        studyController = new StudyJpaController();
        logger.log(Level.SEVERE, "ProcessStudyBatch initialized. Studies to be processed: {0}", data);
    }

    public void run() {
        logger.log(Level.SEVERE,"ProcessStudyBatch starting execution.");
        
        if ( asyncContext != null ) {
            asyncContext.complete();   
            
        }       
        try {
            StringTokenizer st = new StringTokenizer(data, ",");
            logger.log(Level.SEVERE,"ProcessStudyBatch at try method");
            
            
            while (st.hasMoreElements()) {
                studyId = Integer.parseInt((String)st.nextElement());
                Study study = (Study) studyController.findStudy(studyId); 
                if ( study != null ) {
                    study.setStatus("New");                
                    logger.log(Level.SEVERE,"This is find study");
                    studyService.executeStudy(study, study.getIdOrganization(), userId, false);
                } else {
                    logger.log(Level.SEVERE, "ProcessStudyBatch could not find study {0}. Skipping.", studyId.toString());
                }
            }   
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "ProcessStudyBatch exception {0}", ex.getMessage());
        }
        
        studyService = null; 
        studyId = null; userId = null; 
        studyController = null;   
        data = null; 
        asyncContext = null;
        logger.log(Level.SEVERE,"ProcessStudyBatch finished.");
    }     
}
