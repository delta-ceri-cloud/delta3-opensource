/*
 * This method generates executes study w/o generating Flat Table.
 * It is initiated by Scheduler
 */

package com.ceri.delta.server.processor;

import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.server.StudyServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CERI Lahey
 */
public class ProcessStudy implements Runnable {
    private Integer studyId, userId;
    private StudyServiceImpl studyService = null;
    private StudyJpaController studyController = null;
    private static final Logger logger = Logger.getLogger(ProcessVirtual0.class.getName());        
    
    public ProcessStudy() {
    }
    
    public void init(int objectId, int idUser) {
        studyId = objectId;
        userId = idUser;
        studyService = new StudyServiceImpl();
        studyController = new StudyJpaController();
        logger.log(Level.SEVERE,"ProcessStudyAll initialized.");        
    }  
    
    public void run() { 
        Study study = (Study) studyController.findStudy(studyId);    
        logger.log(Level.SEVERE,"ProcessStudyAll starting Study execution.");
        study.setStatus("New");
        studyService.executeStudy(study, userId, userId, false);
        studyService = null; 
        studyId = null; userId = null; 
        studyController = null;        
        logger.log(Level.SEVERE,"ProcessStudyAll finished.");
    }
}
