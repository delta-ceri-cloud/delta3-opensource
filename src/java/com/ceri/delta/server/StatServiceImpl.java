/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server;

import com.ceri.delta.db.Continuous;
import com.ceri.delta.db.DBHelper;
import com.ceri.delta.db.Dichotomous;
import com.ceri.delta.db.Enumerated;
import com.ceri.delta.entity.User;
import com.ceri.delta.entity.events.Eventtemplate;
import com.ceri.delta.entity.events.VisualAlert;
import com.ceri.delta.entity.logregr.LogregrDate;
import com.ceri.delta.entity.matchset.MatchSet;
import com.ceri.delta.entity.model.D3DescStats;
import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.model.ModelColumn;
import com.ceri.delta.entity.model.ModelTable;
import com.ceri.delta.entity.process.Process;
import com.ceri.delta.entity.study.Stat;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.UserJpaController;
import com.ceri.delta.jpa.logregr.LogregrJpaController;
import com.ceri.delta.jpa.matchset.MatchSetJpaController;
import com.ceri.delta.jpa.model.D3DescStatsJpaController;
import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.process.ProcessJpaController;
import com.ceri.delta.jpa.study.StatJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.property.Property;
import com.ceri.delta.server.exceptions.StudyParameterException;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import com.ceri.delta.util.Util;
import com.ceri.proxy.service.ProxyBridge;
import com.ceri.proxy.service.StatPackageStatus;
import com.copsys.statproxy.Proxy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class StatServiceImpl {
    
    private static final Logger logger = Logger.getLogger(StatServiceImpl.class.getName());
    private static ProxyBridge pb;
    static Proxy proxy;
    static String proxyVersion;
    private static ModelServiceImpl modelService;
    private static EventServiceImpl eventService;
    private static StudyServiceImpl studyService;    
    private static LogregrServiceImpl logregrService;    
    private static ModelJpaController modelController;    
    private static ModelColumnJpaController modelColumnController;
    private static ProcessJpaController processController;
    private static StudyJpaController studyController;
    private static UserJpaController userController;
    private static LogregrJpaController logregrController;
    private static StatJpaController statController;
    private static MatchSetJpaController matchsetController;
    private static D3DescStatsJpaController descStatsController;

    private static final int RESULT_PROPDIFF_ROWID = 0;
    private static final int RESULT_PROPDIFF_PERIOD = 1;
    private static final int RESULT_PROPDIFF_LOW = 4;
    private static final int RESULT_PROPDIFF_MED = 3;
    private static final int RESULT_PROPDIFF_HIGH = 2;
    
    private static final int RESULT_OBSEXP_ROWID = 0;
    private static final int RESULT_OBSEXP_PERIOD = 1;    
    private static final int RESULT_OBS_COUNT = 2;
    private static final int RESULT_OBS_EVENT = 3;
    private static final int RESULT_OBS_HIGH = 4;
    private static final int RESULT_OBS_LOW = 5;
    private static final int RESULT_OBS_HIGH2 = 6;
    private static final int RESULT_OBS_LOW2 = 7;
    private static final int RESULT_OBS_PROP = 8;
    private static final int RESULT_EXP_COUNT = 9;
    private static final int RESULT_EXP_EVENT = 10;
    private static final int RESULT_EXP_HIGH = 11;
    private static final int RESULT_EXP_LOW = 12;
    private static final int RESULT_EXP_HIGH2 = 13;
    private static final int RESULT_EXP_LOW2 = 14;
    private static final int RESULT_EXP_PROP = 15;   
    
    public StatServiceImpl() {  
        if ( modelService == null ) {
            modelService = new ModelServiceImpl();
        }                
        if ( eventService == null ) {
            eventService = new EventServiceImpl();
        }     
        if ( logregrService == null ) {
            logregrService = new LogregrServiceImpl();
        }          
        if ( logregrController == null ) {
            logregrController = new LogregrJpaController();
        }        
        if ( processController == null ) {
            processController = new ProcessJpaController();
        }  
        if ( studyController == null ) {
            studyController = new StudyJpaController();
        }        
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }          
        if ( modelColumnController == null ) {
            modelColumnController = new ModelColumnJpaController();
        }        
        if ( userController == null ) {
            userController = new UserJpaController();
        }  
        if ( statController == null ) {
            statController = new StatJpaController();            
        }
        if ( matchsetController == null ) {
            matchsetController = new MatchSetJpaController();            
        }        
        initializeProxy();
    }

    public void setProxyVersion(String version) {
        proxyVersion = version;
    }
    
    public String getProxyVersion() {
        return proxyVersion;
    }
    //---------------------------------------------------------------------------------------------------------------
    // Following method is a main entry point for Descriptive Statistics
    // getXYZ() methods are first level preocessors, which call calculateXYZ() methods
    // calculate methods throw exceptions into get methods, which should prepare error messages and wrap them in JSON
    public String getDStatsPreprocessor(String load, boolean isFlatTable, Integer currentUserId) {
        String result = "DS0010 Descriptive Statistics not supported.";
       
        try {
            JSONObject JSONContent = new JSONObject(load);
            JSONArray dStatsArray = JSONContent.getJSONArray("dStats");
            JSONObject dStatsObject = dStatsArray.getJSONObject(0);
            Model model = ModelServiceImpl.parseModelFromJSON(dStatsArray.getJSONObject(0).toString());
            JSONArray fieldArray = dStatsObject.getJSONArray("fields");
            JSONArray filterArray = dStatsObject.getJSONArray("filters");
            String tableName = model.getOutputName();
            if ( isFlatTable == true ) {
                tableName += "_final";
            } 
            if (fieldArray.length() == 0) {
                // special case: no fields, no filters - get all Descriptive Stats
                // read from db if present, or get new
                String dataString = "";
                if ( descStatsController == null ) {
                    descStatsController = new D3DescStatsJpaController();
                }                
                D3DescStats ds = new D3DescStats();
                List<D3DescStats> dsList = descStatsController.findDescStatsByModelId(model.getIdModel(), isFlatTable);
                if ( dsList.size() >  0 ) {                   
                    dataString = "[" + dStatsObject.toString() + "," + dsList.get(dsList.size()-1).getStats() + "]";                    
                } else {       
                    String statString = getAllDS(model, tableName, isFlatTable);
                    dataString = "[" + dStatsObject.toString() + "," + statString + "]";
                    saveDescStats(statString, currentUserId, model, isFlatTable);                    
                }
                return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, 1);                
            }
            
            ModelColumn mc = modelColumnController.findModelColumn((int) fieldArray.getLong(0));
            if ( mc.getFieldClass() == null ) {
                return prepareJSONResponse("dStats", "DS0031 Cannot obtain Descriptive Statistics. No Class assigned to the field.", "", 0);                
            }
            switch (mc.getFieldClass()) {
                case "Treatment":
                case "Risk Factor":
                case "Outcome":
                    switch (mc.getFieldKind()) {
                        case "Dichotomous":
                            result = getDichotomousStats(model, dStatsObject, filterArray, mc, tableName);
                            break;
                        case "Continuous":
                            result = getContinuousStats(model, dStatsObject, filterArray, mc, tableName);
                            break;
                    }
                    break;
                case "Category":
                    result = getCategoryStats(model, dStatsObject, filterArray, mc, tableName);
                    break;
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex.toString()});
            return prepareJSONResponse("dStats", "DS0011 Exception while parsing JSON statistics description.", "", 0);
        }
        return result;
    }    

    public int saveDescStats(String statString, Integer currentUserId, Model m, boolean flatTable) throws Exception {
        if ( descStatsController == null ) {
            descStatsController = new D3DescStatsJpaController();
        }
        D3DescStats ds = new D3DescStats();

        List<D3DescStats> dsList = descStatsController.findDescStatsByModelId(m.getIdModel(), flatTable);
        int listSize = dsList.size();
        if ( listSize >  0 ) {
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());   
            dsList.get(listSize-1).setUpdatedTS(currentTimestamp);
            dsList.get(listSize-1).setUpdatedBy(currentUserId);                     
            dsList.get(listSize-1).setActive(Boolean.TRUE);
            dsList.get(listSize-1).setFlat(flatTable);
            dsList.get(listSize-1).setStats(statString);
            descStatsController.edit(dsList.get(listSize-1));
        } else {
            ds.setIdOrganization(m.getOrganization());
            ds.setIdModel(m.getIdModel());
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
            ds.setCreatedTS(currentTimestamp);
            ds.setCreatedBy(currentUserId);        
            ds.setUpdatedTS(currentTimestamp);
            ds.setUpdatedBy(currentUserId);          
            ds.setActive(Boolean.TRUE);
            ds.setFlat(flatTable);
            ds.setStats(statString);              
            descStatsController.create(ds);
        }
        return 0;
    }

    
    //----------------------------------new method added for v3/67 API
    public String getLatestProcess(Integer studyId, Integer organizationId) throws InterruptedException {       
        String pro = (String) processController.findLatestProcessByStudyId(studyId); 
        return pro;
    }
    
    
    //---------------------------------------------------------------------------------------------------------------
    // Following method is a main entry point for Correlation Statistics
    // calculate methods throw exceptions into get methods, which should prepare error messages and wrap them in JSON
    public String getCorrelationProcessor(String load, boolean isFlatTable) {      
        try {
            JSONObject JSONContent = new JSONObject(load);
            JSONArray dStatsArray = JSONContent.getJSONArray("dStats");
            JSONObject dStatsObject = dStatsArray.getJSONObject(0);
            Model model = ModelServiceImpl.parseModelFromJSON(dStatsArray.getJSONObject(0).toString());
            JSONArray filterArray = dStatsObject.getJSONArray("filters");
            String tableName = model.getOutputName();
            if ( isFlatTable == true ) {
                tableName += "_final";
            } 
            //return getCorrelationDS(model, dStatsObject, tableName, isFlatTable);
            String dataString;
            String dstats = "{\"type\":\"COR\",\"statistics\": [";

            List<ModelTable> mt = modelService.getModelTables(model.getIdModel());
            List<ModelColumn> modelColumn = new ArrayList();
            // if filter values present load only those columns
            for (int filterCount = 0; filterCount < filterArray.length(); filterCount++) {
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(filterCount));
                modelColumn.add(mcf);
            }   
            if ( filterArray.length() == 0 ) {
                modelColumn = modelService.getModelColumns(mt); // all columns, square
            }
            ModelColumn mc_x = null;
            ModelColumn mc_y = null;        
            int i = 0;
            for (; i < modelColumn.size(); i++) {
                try {
                    mc_x = modelColumn.get(i);
                    if (((isFlatTable == false) && (mc_x.getAtomic() == true)) 
                            || ((isFlatTable == true) && (mc_x.getInsertable() == true))) {
                        if (mc_x.getType().startsWith("INT") 
                                || mc_x.getType().startsWith("DOUBLE") 
                                || mc_x.getType().startsWith("SMALLINT") 
                                || mc_x.getType().startsWith("BIGINT") 
                                || mc_x.getType().startsWith("FLOAT") 
                                || mc_x.getType().startsWith("BIT") 
                                || mc_x.getType().startsWith("TINYINT")) {
                            int index = 0;
                            for (int j=0; j < modelColumn.size(); j++) {   
                                mc_y = modelColumn.get(j);
                                if (((isFlatTable == false) && (mc_y.getAtomic() == true)) 
                                        || ((isFlatTable == true) && (mc_y.getInsertable() == true))) {
                                    if (mc_y.getType().startsWith("INT") 
                                            || mc_y.getType().startsWith("DOUBLE") 
                                            || mc_y.getType().startsWith("SMALLINT") 
                                            || mc_y.getType().startsWith("BIGINT") 
                                            || mc_y.getType().startsWith("FLOAT") 
                                            || mc_y.getType().startsWith("BIT") 
                                            || mc_y.getType().startsWith("TINYINT")) {          
                                        dstats += calculateCorrelation(tableName, mc_y, mc_x, index++);
                                        dstats += ",";     
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception while getting Correlation Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
                    if ( mc_x != null && mc_y != null ) {
                        dstats += "{\"column_x\":\"" + mc_x.getName() + "\",\"column_y\":\"" + mc_y.getName() + "\",\"value\":\"NaN\"},";                
                    }
                }
            }
            if ( dstats.charAt(dstats.length()-1) == ',' ) {
                dstats = Util.removeLastChar(dstats);
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
            return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, i);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while parsing JSON model description: {0} {1}", new Object[]{load, ex.toString()});
            return prepareJSONResponse("dStats", "DS0111 Exception while parsing JSON statistics description.", "", 0);
        }
    }    
    
    public String getStatConfigRemote(String packageName) {     
        if ( packageName == "OCEANS" ) {
            proxy.getStatConfig();
        }
        return null;
    }

    public String getStatConfig(String packageName) {

        String resultString = null;
        
        // following reads from WEB-INF/classes directory
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("studydefinition" + packageName + ".json");
        try {
            resultString = Util.StringfromStream(inputStream);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Study configuration file not found.", ex.getMessage());
        }
        return resultString;
    }

    public String getStatResults(String load) {
        proxy.getResults(load);
        return null;
    }

    public String getStatResult(String processId, String guid, String alias) {
        String load = "{\"auth\":" + getAuthString(processId, alias) + "," + getProcessString(processId, guid) + "}";    
        proxy.getResults(load);
        return null;
    }

    public String getMatchSets(Integer studyId, Integer organizationId) {
            
        String dataString = "";
        String result = "No Match Sets found";
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();  
        JSONArray JSONArrayEntitlements;

        logger.log(Level.INFO, "Retrieving Match Sets for Study {0}.", Integer.toString(studyId));         
        try {
            dataString = getMatchSetsJSON(studyId, organizationId);
        } catch (Exception ex) {
            Logger.getLogger(AdminServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            result = "ST0027 Exception parsing Match Sets into JSON";
        }   
        try {
            if ( "".equals(dataString) ) {        
                success.put("success", false);       
                status.put("message", result);   
            } else {
                JSONArrayEntitlements = new JSONArray(dataString);
                success.put("success", true);   
                status.put("message", JSONArrayEntitlements.length() + " match sets retrieved");   
                success.put("totalCount", Integer.valueOf(JSONArrayEntitlements.length()));
                success.put("matchSets", JSONArrayEntitlements);
            }                
            success.put("status", status); 
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in WS method [admin.getMatchSets]: ", ex);  
            result = "JSON error in WS method [admin.getMatchSets]";
        }         
        return result;
    }
    
    public String submitStatJob(Study study, String step, int stepNumber, String filterWhereClause, String orderField,  Integer currentOrgId, Integer currentUserId, boolean manualMode) {
        String submitString = "empty";
        JSONObject modelInfo = null;
        String intervalQuery = null;
        List<LogregrDate> lrdList;       
        String processName = study.getName() + " step " + step;
        String dataUrl =null;
        Process process = new Process();
        double[] data = null;
        logger.log(Level.INFO, "Submitting stat job for study {0}", new Object[]{study.getName()});       
      
        try {
            if ( study.getOverwriteResults() == true ) {
                // search by name and studyId to separate steps
                List<Object> pList = processController.findProcessByNameAndStudyId(processName, study.getIdStudy());                    
                if ( pList.size() > 0 ) {
                    process = (Process)pList.get(pList.size()-1);
                    process.setUpdatedBy(currentUserId);
                    process.setData("");
                    process.setMessage("");
                    Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
                    process.setSubmittedTS(currentTimestamp);                        
                }
                
            }
            
            
            process.setIdModel(study.getIdModel());
            process.setIdStudy(study.getIdStudy());
            process.setIdGroup(study.getIdGroup());
            process.setProgress(0);
            process.setManualExecution(manualMode);
            process.setActive(Boolean.TRUE);
            process.setName(processName);
            process.setIdFilter(study.getIdFilter());
            process.setData("");
            //process.setFilterClause(filterWhereClause);                   
            process = (Process) processController.createObjects(process, currentOrgId, currentUserId);
            modelInfo = logregrService.formatLRFParams(study);            
            logger.log(Level.INFO, "Executing step {0} for study {1}", new Object[]{step, study.getName()});    
            
            switch ( step ) {
                case "LR1":
                    lrdList = logregrService.calculateLRFTrainingPeriod(study, process, currentUserId);         
                    if ( lrdList.isEmpty() ) return "DS0011 LR Configuration problem: cannot create training period where clause.";
                    intervalQuery = lrdList.get(0).getDescription();
                    process.setStepNumber(0);
                    break;
                case "LR2":
                    intervalQuery = logregrService.getIntervalQueryStep2(study);                    
                    process.setStepNumber(1);                    
                    break;    
                
                case "SRVM":
                     //v3.64 added new code for matched survival with follow up date for item https://ceri-lahey.atlassian.net/browse/DELQA-787
                    intervalQuery = getIntervalQuerySRVM(study,stepNumber); 
                    process.setStepNumber(0);
                    break;
                
                default:
                    intervalQuery = getIntervalQuery(study); 
                    process.setStepNumber(0);
                    break;
            }            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating Process {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0021 Error submitting job: " + ex.getMessage();
        }
        
        if ( "".equals(study.getMethodParams()) || study.getMethodParams() == null ) return "DS0214 Study parameters are missing.";
       
         //------------preparing interval query
        if ( filterWhereClause == null ) {
            filterWhereClause = "DROP_RECORD=0 AND " + intervalQuery;
        }  else {
            filterWhereClause += " AND DROP_RECORD=0 AND " + intervalQuery;
        }
        
        try {     
            JSONArray params = new JSONArray(study.getMethodParams());  
            JSONObject jobObject = null;
            JSONObject stepConf = params.getJSONObject(stepNumber);   
            
            if ( modelInfo != null ) {
                if ( stepNumber > 0 ) {
                    // following allows changing LRF between 2 steps
                    stepConf.getJSONObject("inputs").put("model", modelInfo);
                }
            } else {
                if ( "PA".equals(step) ) {
                    logger.log(Level.SEVERE, "DS0024 Error LRF for this study is not specified {0} ID {1}", new Object[]{process.getName(), process.getIdProcess()});
                    return "DS0024 Error LRF for this study is not specified";         
                }
            }          
            logger.log(Level.INFO, "Preparing metadata for study {0} before submitting to stat package.", new Object[]{study.getName()});         
            if ( study.getSendAllColumns() == false ) {
                // if step is > 0 then pick up and merge also columns from previous (other) param sets
                Set<String> columns = new TreeSet();
                Set<String> lastStepColumns = retrieveColumnsFromParams(study, stepConf.getJSONObject("inputs"));  
                columns = Util.mergeSets(columns, lastStepColumns);
                
                for ( int i=0; i<stepNumber; i++ ) {
                    JSONObject otherStepConf = params.getJSONObject(i);   
                    Set<String> otherColumns = retrieveColumnsFromParams(study, otherStepConf.getJSONObject("inputs")); 
                    columns = Util.mergeSets(columns, otherColumns);
                }
                //Set<String> modelColumns = logregrService.retrieveColumnsFromLRF(study);
                Set<String> modelColumns = retrieveModelColumnsFromParams(study, stepConf.getJSONObject("inputs"));
                columns = Util.mergeSets(columns, modelColumns);
                
                  
              //  System.out.println("stepConf "+stepConf);
              //  System.out.println("stepConf.getJSONObject string"+stepConf.getJSONObject("inputs").toString());
              //  System.out.println("--------Follow up date selection----------"+ stepConf.getJSONObject("inputs").getJSONObject("followupDateSelection").get("values"));
              //  System.out.println("columns "+columns);
              //  System.out.println("modelColumns "+modelColumns);
              //   System.out.println("Before Calling model service,................");
              //  System.out.println("process"+process+"filterWhereClause"+filterWhereClause);
              //  System.out.println("filterWhereClause"+filterWhereClause+"stepConf.getString"+stepConf.getString("name"));
              //  System.out.println("orderField"+orderField+"columns"+columns);
              //  System.out.println("proxy"+proxy+"study.getDumpData()"+study.getDumpData());
                
            
              
              
              
              jobObject = modelService.submitFlatTabletoProxy(process, filterWhereClause, stepConf.getString("name"), orderField,  columns, proxy, study.getDumpData(), study);
              logger.log(Level.INFO, "Jobobject before putting",jobObject.toString());    
              dataUrl="{\"url\":\""+process.getfileUrl()+"\"}";
              jobObject.put("data", new JSONObject(dataUrl));
             logger.log(Level.INFO, "Jobobject after putting at SEND ALL COLUMN False",jobObject.toString());
                
            } else {
                
                jobObject = modelService.submitFlatTabletoProxy(process, filterWhereClause, stepConf.getString("name"), orderField,  null, proxy, study.getDumpData(), study);
                dataUrl="{\"url\":\""+process.getfileUrl()+"\"}";
                jobObject.put("data", new JSONObject(dataUrl));
                logger.log(Level.INFO, "Jobobject after putting at SEND ALL COLUMN True",jobObject.toString());
            
            }
            if (jobObject == null) {
                return "DS0022 Creation of new Process failed. Possibly Model or Flat Table does not exist.";
            }          

            logger.log(Level.INFO, "Creating Job JSON Object for study {0}, stepConf {1}", new Object[]{study.getName(), stepConf.toString()});                     
            jobObject.put("inputs", stepConf.getJSONObject("inputs"));

            if ( currentUserId > 0 ) {
                User user = userController.findUser(currentUserId);               
                if ( user != null ) {
                    logger.log(Level.INFO, "Job auth infor for {0}", new Object[]{user.getAlias()});                                         
                    jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), user.getAlias())));
                } else {
                    logger.log(Level.INFO, "Job auth infor for null user - system");                        
                    jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), "system")));    
                }
            } else {
                logger.log(Level.INFO, "Job auth infor for user system");  
                jobObject.put("auth", new JSONObject(getAuthString(process.getIdProcess().toString(), "system")));
            }
            
            submitString = jobObject.toString();
            logger.log(Level.INFO, "Submit string to DELTAlytics {0}", submitString);          
            process.setInput(submitString);
            Stat st = (Stat)statController.findObjectEntityById(study.getIdStat());
            
            if ( st != null ) {
                logger.log(Level.INFO, "Retrieved stat package data for ID {0}", new Object[]{study.getIdStat().toString()});                      
                
                if ( st.getUseProxy() == false ) {
                    process.setLocalStatPackage(true);
                    process.setRemoteStatPackage(false);                
                } else {
                    process.setLocalStatPackage(false);                
                    process.setRemoteStatPackage(true);
                } 
            } else {
                logger.log(Level.SEVERE, "Could not find stat package for ID {0}", new Object[]{study.getIdStat().toString()});                  
            }
            process.setStatus("submitted");    
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
            process.setSubmittedTS(currentTimestamp);           
            
            processController.edit(process);            
            if ( study.getRemoteStatPackage() == true ) {
                //proxy.sumbitStatJob(submitString);
                logger.log(Level.INFO, "Remote Job submitted for study {0}. now logging Event", new Object[]{study.getName()});       
            } else {
                logger.log(Level.INFO, "Local Job submitted for study {0}. now logging Event", new Object[]{study.getName()});                 
            }             
            
            eventService.logEvent(currentOrgId, currentUserId, Eventtemplate.SUBMIT_EVENT, 
                    process.getName() + 
                    ", Result ID: " + process.getIdProcess().toString() +
                    " Study ID: " + process.getIdStudy().toString(),
                    process.getIdStudy(), "studies. Submission remote: " + st.getShortName()
                    );
            
              //API to to run deltalytics
            try {
               
                logger.log(Level.INFO, "Try block runDeltalytics"); 
                /*Crosscheck if file is created or not
                File f = new File(dataUrl);
                if(f.exists() && !f.isDirectory()) { 
                    // do something
                    System.out.println("This is at try block runDeltalytics");
                }*/
                
                runDeltalytics(submitString);
                
            } catch (IOException ex) {
                Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (StudyParameterException ex) {
            logger.log(Level.SEVERE, "Exception while submitting Job {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return ex.getMessage();               
        } catch (MySQLSyntaxErrorException ex) {
            logger.log(Level.SEVERE, "Exception while submitting Job {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), DBHelper.printSQLException(ex)});
            return "DS0123 Error submitting job: " + DBHelper.printSQLException(ex);            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while submitting Job {0} ID {1}, {2}", new Object[]{process.getName(), process.getIdProcess(), ex});
            return "DS0023 Error submitting job: " + ex.getMessage();
        }
        return "OK";
    }    
    
    public String getJobStatus(String processId, String guid, String alias) {
        String load = "{\"auth\":" + getAuthString(processId, alias) + "," + getProcessString(processId, guid) + "}";      
        proxy.getProcessStatus(load);
        return null;
    }  
    
    public String periodicJobStatusCheck(String alias, long pollingPeriod) {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        long ctLong = currentTimestamp.getTime();
        Timestamp cutOffTimestamp = new Timestamp(ctLong - pollingPeriod);       
        List<Object>  processList = new ArrayList();
        processList = processController.findActiveProcessEntities(cutOffTimestamp.toString());
        for ( Object activeProcess : processList ) {
            String load = "{\"auth\":" + getAuthString(((Process)activeProcess).getIdProcess().toString(), alias) 
                    + "," + getProcessString(((Process)activeProcess).getIdProcess().toString(), ((Process)activeProcess).getGuid()) + "}";
            if ( ((Process)activeProcess).getRemoteStatPackage() == true ) { // check only processes that were submitted remotely
                //logger.log(Level.INFO, "Checking status of " + load);
                proxy.getProcessStatus(load);
            }
        }   
        return null;
    }

    public String callLocalStatPackage(String command, String data) {
        URL servlet = null;
        URLConnection con = null;
        try { 
            if ( data != null ) {
                servlet = new URL( new Property("localStatPackageHostPort").getParamValue() + "/DAM/"+ command + data); 
                 //servlet = new URL( new Property("localStatPackageHostPort").getParamValue() + command + data); 
            } 
            
            else {
                servlet = new URL( new Property("localStatPackageHostPort").getParamValue() + "/DAM/" + command); 
                //servlet = new URL( new Property("localStatPackageHostPort").getParamValue() + command + data);              
            }

            con = servlet.openConnection();
            con.setConnectTimeout(Integer.parseInt(new Property("localStatPackageConnectionTimeout").getParamValue()));
            con.setReadTimeout(Integer.parseInt(new Property("localStatPackageReadTimeout").getParamValue()));
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())); 
            String line = Util.StringfromBufferedReader(in); 
            //logger.log(Level.INFO, "Local statistical package response: {0}", line);	                        
            return line;
        } catch ( FileNotFoundException e) {
            logger.log(Level.SEVERE, "FileNotFoundException while initializing local stat package: {0}", e.getMessage());    
            return "DS0124 Local statistical package not found";
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception while initializing local stat package: {0}", e.getMessage());
            return ("DS0125 Error while initializing local statistical package: " + e.getMessage());
        }        
    }
    
    public String initializeExternalStatPackage(String alias) {     
        URL servlet = null;
        URLConnection con = null;
        logger.log(Level.INFO, "User {0} checking status of statistical package.", alias);
        StatPackageStatus st = new StatPackageStatus();
        if ( !st.getStatus().equals("OK") ) {
            logger.log(Level.INFO, "User {0} initializing statistical package.", alias);
            try { 
                servlet = new URL(new Property("externalStatPackageHostPort").getParamValue() + "/AnalyticsBrokerProxy/ProxyAccessor" ); 
                con = servlet.openConnection();
                con.setConnectTimeout(Integer.parseInt(new Property("externalStatPackageConnectionTimeout").getParamValue()));
                con.setReadTimeout(Integer.parseInt(new Property("externalStatPackageReadTimeout").getParamValue()));
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())); 
                String line = Util.StringfromBufferedReader(in); 
                logger.log(Level.INFO, "OCEANS initialization response: {0}", line);	                
                if ( line.contains("Initiated") ) {
                    st.setStatus("OK");
                }            
            } catch ( FileNotFoundException e) {
                st.setStatus("Not Installed");
                logger.log(Level.SEVERE, "FileNotFoundException while initializing external stat package: {0}", e.getMessage());                
            } catch (Exception e) {
                st.setStatus("Error");
                logger.log(Level.SEVERE, "Exception while initializing external stat package: {0}", e.getMessage());
            }        
        }
        return st.getStatus();
    }
    
    public String pingStatPackage() {     
        //logger.log(Level.INFO, "User {0} pinging statistical package.", alias);
        proxy.getVersion();   
        String version = getProxyVersion();
        // this value will be ovewritten when message comes back from ext package
        setProxyVersion("?");
        return version;
    }

    public String saveResults(String JSONString, Integer currOrgId, Integer currUserId) throws Exception {
        logger.log(Level.INFO, "DELTA saving Process: " + JSONString);    

        try {        
            JSONObject responseObject =  new JSONObject(JSONString);
            JSONArray processArray = responseObject.getJSONArray("processes");
            for (int i=0; i<processArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = processArray.getJSONObject(i).toString();
                Object subject = gson.fromJson(jString, Process.class);   
                subject = processController.createObjects(subject, currOrgId, currUserId);                 
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing new Process/Results Object: {0}", new Object[]{ex});         
            return "ST0015 Exception while importing new results object: " + ex.getMessage();
        }                                
        return "Results saved";
    }
    
   
    public int receiveSubmit(String JSONString) throws Exception {
        logger.log(Level.SEVERE, "DELTA updating Process: " + JSONString);    
        Integer userId = new Integer(0); // means "system"
        Process originalProcess = null;
  
        try {        

            JSONObject responseObject =  new JSONObject(JSONString);
            JSONArray processArray = responseObject.getJSONArray("processes");
            for (int i=0; i<processArray.length(); i++) {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = processArray.getJSONObject(i).toString();
                Object subject = gson.fromJson(jString, Process.class);   
                originalProcess = ((ProcessJpaController)processController).findProcess(((Process)subject).getIdProcess());              
                if ( originalProcess != null ) {
                    originalProcess.setProgress(((Process)subject).getProgress());
                    originalProcess.setStatus(((Process)subject).getStatus());     
                    originalProcess.setMessage(((Process)subject).getMessage());           
                    subject = processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                } else {
                    logger.log(Level.SEVERE, "Received Process ID that does not match local database: {0}", new Object[]{((Process)subject).getIdProcess()});  
                    if ( originalProcess != null ) {
                        originalProcess.setProgress(0);
                        originalProcess.setStatus("Error");     
                        originalProcess.setMessage("Received Process ID that does not match DELTA database.");   
                        processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                        return -2;
                        }
                    }
                }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Process Object: {0}", new Object[]{ex});
            if ( originalProcess != null ) {
                originalProcess.setProgress(100);
                originalProcess.setStatus("Error");     
                originalProcess.setMessage("Invalid results: " + ex.getMessage());   
                processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                }            
            return -1;
        }                                
        return 0;
    }  
    
    // main method responsible for parsing, interpreting, storing results and triggering next step if needed
    public String receiveResults(String JSONString) throws Exception {
        logger.log(Level.SEVERE, "DELTA receiveResults, updating Process: " + JSONString);    
        //System.out.println("ReceiveResult Json string "+ JSONString);
        Integer userId = new Integer(0); // means "system"
        Process theProcess = null;
        Study s = null;
        
        if ( studyService == null ) {
            studyService = new StudyServiceImpl();
        }           

        try {        
            JSONObject responseObject =  new JSONObject(JSONString);
            //System.out.println("responseObject: "+responseObject.toString());
            JSONArray processArray = responseObject.getJSONArray("processes");
            //System.out.println("processArray: "+processArray.toString());
            
            for (int i=0; i<processArray.length(); i++)
            {
                Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                String jString = processArray.getJSONObject(i).toString();
                JSONObject processObject = new JSONObject(jString);
                Object subject = gson.fromJson(jString, Process.class);  
                
                //System.out.println("This is subject"+subject.toString());
                theProcess = ((ProcessJpaController)processController).findProcess(((Process)subject).getIdProcess());   
                //System.out.println("This is theProcess find: "+theProcess.toString());
                
                
                if ( theProcess != null ) {
                    theProcess.setProgress(((Process)subject).getProgress());
                    //System.out.println("This is setting progress: "+((Process)subject).getProgress());
                    theProcess.setStatus(((Process)subject).getStatus());   
                    //System.out.println("This is setting status: "+((Process)subject).getStatus());
                    
                    theProcess.setMessage(((Process)subject).getMessage());            
                    //System.out.println("This is setting message: "+((Process)subject).getMessage());
                    
                    JSONArray results = processObject.getJSONArray("results"); // this key is ignored by gson, which allows processing           
                    theProcess.setData("{\"results\":" + results.toString() + "}");
                    //System.out.println("This is setting data: "+"{\"results\":" + results.toString() + "}");
                    //theProcess.setData(jString);
                    
                    subject = processController.updateObjects(theProcess, theProcess.getIdOrganization(), userId);  
                    JSONObject subResults = null;    
                    s = (Study) studyController.findStudy(((Process)subject).getIdStudy());      
                    Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());     
                    s.setIdLastProcess(theProcess.getIdProcess());
                    s.setProcessedTS(currentTimestamp);
                    
                    //s.setStatus(((Process)subject).getStatus());   ???
                    
                    if ( "Error".equals(((Process)subject).getStatus()) || "Stat Error".equals(((Process)subject).getStatus()) ) 
                    {
                        
                        s.setStatus("DS0032 Stat Package " + ((Process)subject).getMessage()); 
                        s = (Study) studyController.edit(s);                       
                        
                    
                    } else 
                    {
                        // no error - store results and continue with next step configured
                        // loop through and branch on result.name         
                        for (int j = 0; j < results.length(); j++) {
                            subResults = results.getJSONObject(j);  
                            if ( "ObservedExpectedGraphOutput".equals(subResults.getString("name")) ) {     
                                results = processObservedExpectedGraph((Process)subject, s, j, results);
                                theProcess.setData("{\"results\":" + results.toString() + "}");
                                subject = processController.updateObjects(theProcess, theProcess.getIdOrganization(), userId);                              
                            }         
                            if ( "ProportionalDifferenceGraphOutput".equals(subResults.getString("name")) ) {
                                results = processProportionalDifferenceGraph((Process)subject, s, j, results);
                                theProcess.setData("{\"results\":" + results.toString() + "}");
                                subject = processController.updateObjects(theProcess, theProcess.getIdOrganization(), userId);                   
                            }    
                            if ( "MatchSetOutput".equals(subResults.getString("name")) ) 
                            
                            {
                                System.out.println("This is matchset output");
                                processMatchSet((Process)subject, s, j, results, userId);                 
                            }                              
                            if ( "LogisticRegressionFormulaOutput".equals(subResults.getString("name")) ) {                     
                                logregrService.saveLRFResults((Process)subject, s, "{\"results\":[" + subResults.toString() + "]}");                        
                            } else {  
                                // persist to database table, first delete old data if exist
                                String insertStm = buildInsertStm(s.getIdStudy().toString(), subResults.getString("name"), subResults);
                                if ( insertStm != null ) {
                                    DBHelper.executeSqlStatement("DeltaData", buildDropStm(s.getIdStudy().toString(), subResults.getString("name")), logger);
                                    DBHelper.executeSqlStatement("DeltaData", buildCreateStm(s.getIdStudy().toString(), subResults.getString("name"), subResults), logger);                                
                                    DBHelper.executeSqlStatement("DeltaData", insertStm, logger);
                                } else {
                                    logger.log(Level.SEVERE, "Result set is empty - nothing persisted to db, study {0}, {1} ", new Object[]{s.getIdStudy().toString(),subResults.getString("name")});   
                                }
                            }
                        }                     
                        
                        
                        
                        if ( !"InProgress".equals(((Process)subject).getStatus()) ) 
                        
                        {
                            //System.out.println("This is at In progress"+((Process)subject).getStatus());
                            
                            // advance Study status only if processing completed                      
                            studyService.advanceStatus("OK", s, 0);   
                            
                            s = (Study) studyController.edit(s);   
                            
                            if ( !s.getStatus().toLowerCase().contains("complete") && s.getAutoExecute() ) 
                            {
                                //System.out.println("This is inside loop In"+s.getStatus().toLowerCase().contains("complete"));
                                //System.out.println("This is inside loop In"+s.getAutoExecute());
                                
                                // call next step if not finished and in auto mode
                                //studyService.executeStudy(s, s.getIdOrganization(), 0, false);
                            }
                        }
                    }
                } else {
                    logger.log(Level.SEVERE, "DS0033 Received Process ID that does not match local database: {0}", new Object[]{((Process)subject).getIdProcess()});  
                    return "DS0033 Received Process ID that does not match DELTA database.";
                    }
                }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "DS0035 JSON exception processing results: {0}", new Object[]{JSONString});
            if ( theProcess != null ) {
                theProcess.setProgress(100);
                theProcess.setStatus("Error");     
                theProcess.setMessage("DS0034 Exception processing results: " + JSONString);   
                processController.updateObjects(theProcess, theProcess.getIdOrganization(), userId);   
                if ( s != null ) {
                    studyService.advanceStatus("OK", s, 0);     
                    s = (Study) studyController.edit(s);   
                }
            }         
            return "DS0035 JSON exception processing results: " + JSONString;            
        } catch (Exception ex) 
        
        {            
            logger.log(Level.SEVERE, "DS0034 Exception processing results: {0}", new Object[]{ex});
            if ( theProcess != null ) {
                theProcess.setProgress(100);
                theProcess.setStatus("Error");     
                theProcess.setMessage("DS0034 Exception processing results: " + ex.getMessage());   
                processController.updateObjects(theProcess, theProcess.getIdOrganization(), userId);   
                if ( s != null ) {
                    studyService.advanceStatus("OK", s, 0);     
                    s = (Study) studyController.edit(s);   
                }
            }            
            return "DS0034 Exception processing results: " + ex.getMessage();
        }                                
        return "OK";
    }
    
    
    
    
    
    // method used by UI to trigger alert procesessing
    public String processResults(String JSONString) throws Exception {
        logger.log(Level.SEVERE, "DELTA updating results of Process: {0}", JSONString);    
        Integer userId = 0; // means "system"
        Process originalProcess = null;
  
        try {        
            JSONObject processObject =  new JSONObject(JSONString);
            originalProcess = ((ProcessJpaController)processController).findProcess(processObject.getInt("idProcess"));              
            if ( originalProcess != null ) {
                JSONObject data = new JSONObject(originalProcess.getData());
                JSONArray results = data.getJSONArray("results");
                JSONObject subResults = null;    
                Study s = (Study) studyController.findStudy(originalProcess.getIdStudy());      
                // loop through and branch on result.name         
                for (int j = 0; j < results.length(); j++) {
                    subResults = results.getJSONObject(j);  
                    if ( "LogisticRegressionFormulaOutput".equals(subResults.getString("name")) ) {                     
                        logregrService.saveLRFResults(originalProcess, s, "{\"results\":[" + subResults.toString() + "]}");                        
                    }         
                    if ( "RaSprtGraphOutput".equals(subResults.getString("name")) ) {            
                    }
                    if ( "ObservedExpectedGraphOutput".equals(subResults.getString("name")) ) {     
                        results = processObservedExpectedGraph(originalProcess, s, j, results);
                        originalProcess.setData("{\"results\":" + results.toString() + "}");
                        originalProcess = (Process)processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);                              
                    }         
                    if ( "ProportionalDifferenceGraphOutput".equals(subResults.getString("name")) ) {
                        results = processProportionalDifferenceGraph(originalProcess, s, j, results);
                        originalProcess.setData("{\"results\":" + results.toString() + "}");
                        originalProcess = (Process)processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);                   
                    }     

                }
                s = (Study) studyController.edit(s);   // do not set processedOn field              
            } else {
                logger.log(Level.SEVERE, "Received Process ID that does not match local database: {0}", new Object[]{originalProcess.getIdProcess()});  
                if ( originalProcess != null ) {
                    originalProcess.setProgress(0);
                    originalProcess.setStatus("Error");     
                    originalProcess.setMessage("Received Process ID that does not match DELTA database.");   
                    processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                    return originalProcess.getMessage();
                    }
                }  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while creating new Process Object: {0}", new Object[]{ex});
            if ( originalProcess != null ) {
                originalProcess.setProgress(100);
                originalProcess.setStatus("Error");     
                originalProcess.setMessage("Invalid results: " + ex.getMessage());   
                processController.updateObjects(originalProcess, originalProcess.getIdOrganization(), userId);   
                }            
            return originalProcess.getMessage();
        }         
        return "Study results processed.";
    }
    
    private JSONArray processObservedExpectedGraph(Process p, Study s, int index, JSONArray results) {
        String alertString = "[";
        JSONArray alertArray = new JSONArray();
        try {
            JSONObject subResults = results.getJSONObject(index);
            JSONArray dataArray = subResults.getJSONArray("data");
            Double last1 = null, last2 = null;
            boolean trendingUpTriggered = false, trendingDownTriggered = false;
            for (int i=0, j=0; i<dataArray.length(); i++) {
                JSONArray data = dataArray.getJSONArray(i);
                String rowId = data.getString(RESULT_OBSEXP_ROWID);
                String periodName = data.getString(RESULT_OBSEXP_PERIOD);
                Double observedCount = Double.parseDouble(data.getString(RESULT_OBS_COUNT));
                Double observedEvent = Double.parseDouble(data.getString(RESULT_OBS_EVENT));
                Double observedUpperConfidenceInterval;
                try { observedUpperConfidenceInterval = Double.parseDouble(data.getString(RESULT_OBS_HIGH));
                } catch (Exception e) { observedUpperConfidenceInterval = null; }                
                Double observedLowerConfidenceInterval;
                try { observedLowerConfidenceInterval = Double.parseDouble(data.getString(RESULT_OBS_LOW));
                } catch (Exception e) { observedLowerConfidenceInterval = null; }                      
                Double observedProportion;
                try { observedProportion = Double.parseDouble(data.getString(RESULT_OBS_PROP));
                } catch (Exception e) { observedProportion = null; }                      
                Double expectedCount;
                try { expectedCount = Double.parseDouble(data.getString(RESULT_EXP_COUNT));
                } catch (Exception e) { expectedCount = null; }                      
                Double expectedEvent;
                 try { expectedEvent = Double.parseDouble(data.getString(RESULT_EXP_EVENT));
                } catch (Exception e) { expectedEvent = null; }                     
                Double expectedUpperConfidenceInterval;
                try { expectedUpperConfidenceInterval = Double.parseDouble(data.getString(RESULT_EXP_HIGH));
                } catch (Exception e) { expectedUpperConfidenceInterval = null; }                      
                Double expectedLowerConfidenceInterval;
                try { expectedLowerConfidenceInterval = Double.parseDouble(data.getString(RESULT_EXP_LOW));
                } catch (Exception e) { expectedLowerConfidenceInterval = null; }                      
                Double expectedProportion;
                try { expectedProportion = Double.parseDouble(data.getString(RESULT_EXP_PROP));
                } catch (Exception e) { expectedProportion = null; }                      
                Double observedSecondaryUpperConfidenceInterval;
                try { observedSecondaryUpperConfidenceInterval = Double.parseDouble(data.getString(RESULT_OBS_HIGH2));
                } catch (Exception e) { observedSecondaryUpperConfidenceInterval = null; }                 
                Double observedSecondaryLowerConfidenceInterval;   
                try { observedSecondaryLowerConfidenceInterval = Double.parseDouble(data.getString(RESULT_OBS_LOW2));
                } catch (Exception e) { observedSecondaryLowerConfidenceInterval = null; }                 
                Double expectedSecondaryUpperConfidenceInterval;
                try { expectedSecondaryUpperConfidenceInterval = Double.parseDouble(data.getString(RESULT_EXP_HIGH2));
                } catch (Exception e) { expectedSecondaryUpperConfidenceInterval = null; }                 
                Double expectedSecondaryLowerConfidenceInterval;   
                try { expectedSecondaryLowerConfidenceInterval = Double.parseDouble(data.getString(RESULT_EXP_LOW2));
                } catch (Exception e) { expectedSecondaryLowerConfidenceInterval = null; }                 
                // sequence of following if statments is not important in establishing hierarchy of notification
                // alerts with higher "type" will overwrite other in the UI
                if ( observedProportion != null && expectedSecondaryUpperConfidenceInterval != null ) {
                    if ( observedProportion > expectedSecondaryUpperConfidenceInterval ) {
                        if ( j > 0 ) {
                            alertString += ",";
                        }
                        j++;
                        VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                        if ( s.getOutcomePositive() == false ) {
                            va.setType(VisualAlert.red);
                        } else {
                            va.setType(VisualAlert.blue);
                        }
                        va.setDescription(VisualAlert.alertDescription[VisualAlert.aboveSecondaryConfidenceInterval]);
                        JSONObject vaObject = new JSONObject(va);
                        alertArray.put(vaObject);
                        alertString += new JSONObject(va).toString();
                        eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_ABOVE_SECONDARY_CL,
                                p.getName() +
                                        " Study ID: " + p.getIdStudy().toString() +
                                        ", Period " + data.getString(RESULT_OBSEXP_ROWID) +
                                        ", Observed value = " + observedProportion.toString() +
                                        ", Expected Secondary Upper CI value = " + expectedSecondaryUpperConfidenceInterval.toString(),
                                p.getIdProcess(), "results"
                        );
                    }                  
                }
                if ( observedProportion != null && expectedUpperConfidenceInterval != null ) {                
                    if ( observedProportion > expectedUpperConfidenceInterval ) {
                        if ( j > 0 ) {
                            alertString += ",";
                        }     
                        j++;
                        VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                        if (s.getOutcomePositive() == false) {
                            va.setType(VisualAlert.yellow);
                        } else {
                            va.setType(VisualAlert.gray);
                        }
                        va.setDescription(VisualAlert.alertDescription[VisualAlert.abovePrimaryConfidenceInterval]);
                        JSONObject vaObject = new JSONObject(va);
                        alertArray.put(vaObject);
                        alertString += new JSONObject(va).toString();
                        eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_ABOVE_PRIMARY_CL, 
                            p.getName() + 
                            " Study ID: " + p.getIdStudy().toString() +
                            ", Period " + data.getString(RESULT_OBSEXP_ROWID) +
                            ", Observed value = " + observedProportion.toString() +
                            ", Expected Primary Upper CI value = " + expectedUpperConfidenceInterval.toString(),                        
                            p.getIdProcess(), "results"
                            );
                    }   
                }
                if ( observedProportion != null && expectedSecondaryLowerConfidenceInterval != null ) {
                    if ( observedProportion < expectedSecondaryLowerConfidenceInterval ) {
                        if ( j > 0 ) {
                            alertString += ",";
                        }     
                        j++;
                        VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                        if (s.getOutcomePositive() == true) {
                            va.setType(VisualAlert.red);
                        } else {
                            va.setType(VisualAlert.blue);                          
                        }
                        va.setDescription(VisualAlert.alertDescription[VisualAlert.belowSecondaryConfidenceInterval]);    
                        JSONObject vaObject = new JSONObject(va);
                        alertArray.put(vaObject);                    
                        alertString += new JSONObject(va).toString();
                        eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_BELOW_SECONDARY_CL, 
                            p.getName() + 
                            " Study ID: " + p.getIdStudy().toString() +
                            ", Period " + data.getString(RESULT_OBSEXP_ROWID) +
                            ", Observed value = " + observedProportion.toString() +
                            ", Expected Secondary Lower CI value = " + expectedSecondaryLowerConfidenceInterval.toString(), 
                            p.getIdProcess(), "results"
                            );                    
                    }      
                }
                if ( observedProportion != null && expectedLowerConfidenceInterval != null ) {                
                    if ( observedProportion < expectedLowerConfidenceInterval ) {
                        if ( j > 0 ) {
                            alertString += ",";
                        }     
                        j++;
                        VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                        if (s.getOutcomePositive() == true) {
                            va.setType(VisualAlert.yellow);
                        } else {
                            va.setType(VisualAlert.gray);                          
                        }
                        va.setDescription(VisualAlert.alertDescription[VisualAlert.belowPrimaryConfidenceInterval]);                        
                        JSONObject vaObject = new JSONObject(va);
                        alertArray.put(vaObject);                    
                        alertString += new JSONObject(va).toString();
                        eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_BELOW_PRIMARY_CL, 
                            p.getName() + 
                            " Study ID: " + p.getIdStudy().toString() +
                            ", Period " + data.getString(RESULT_OBSEXP_ROWID) +
                            ", Observed value = " + observedProportion.toString() +
                            ", Expected Primary Lower CI value = " + expectedLowerConfidenceInterval.toString(), 
                            p.getIdProcess(), "results"
                            );                    
                    }           
                }
                if ( trendingUpTriggered == false && observedProportion != null && last1 != null && last2 != null ) {                
                    if ( observedProportion > last2 && last2 > last1) {
                        if ( j > 0 ) {
                            alertString += ",";
                        }     
                        j++;
                        VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                        if (s.getOutcomePositive() == true) {
                            va.setType(VisualAlert.gray);
                        } else {
                            va.setType(VisualAlert.red);                          
                        }
                        va.setDescription(VisualAlert.alertDescription[VisualAlert.trending3up]);                        
                        JSONObject vaObject = new JSONObject(va);
                        alertArray.put(vaObject);                    
                        alertString += new JSONObject(va).toString();
                        eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_TRENDING_UP, 
                            p.getName() + 
                            " Study ID: " + p.getIdStudy().toString() +
                            ", Period " + data.getString(RESULT_OBSEXP_ROWID) +
                            ", Third consecutive value up: " + last1.toString() + " " + last2.toString() + " " + observedProportion.toString(),
                            p.getIdProcess(), "results"
                            );   
                        trendingUpTriggered = true;
                    }       
                } 
                if ( trendingDownTriggered == false && observedProportion != null && last1 != null && last2 != null ) {                
                    if ( observedProportion < last2 && last2 < last1) {
                        if ( j > 0 ) {
                            alertString += ",";
                        }     
                        j++;
                        VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                        if (s.getOutcomePositive() == true) {
                            va.setType(VisualAlert.red);
                        } else {
                            va.setType(VisualAlert.gray);                          
                        }
                        va.setDescription(VisualAlert.alertDescription[VisualAlert.trending3down]);                        
                        JSONObject vaObject = new JSONObject(va);
                        alertArray.put(vaObject);                    
                        alertString += new JSONObject(va).toString();
                        eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_TRENDING_DOWN, 
                            p.getName() + 
                            " Study ID: " + p.getIdStudy().toString() +
                            ", Period " + data.getString(RESULT_OBSEXP_ROWID) +
                            ", Third consecutive value down: " + last1.toString() + " " + last2.toString() + " " + observedProportion.toString(),
                            p.getIdProcess(), "results"
                            );   
                        trendingDownTriggered = true;
                    }       
                }                 
                last1 = last2;
                last2 = observedProportion;               
            }            
            subResults.put("alerts", alertArray);
            results.put(index, subResults);
        } catch (JSONException ex) {
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return results;
    }
    
    private JSONArray processProportionalDifferenceGraph(Process p, Study s, int index, JSONArray results) {
        String alertString = "[";
        JSONArray alertArray = new JSONArray();
        try {
            JSONObject subResults = results.getJSONObject(index);
            JSONArray dataArray = subResults.getJSONArray("data");
            for (int i=0, j=0; i<dataArray.length(); i++) {
                JSONArray data = dataArray.getJSONArray(i);
                String rowId = data.getString(0);
                Double low, medium, high;
                try { low = Double.parseDouble(data.getString(RESULT_PROPDIFF_LOW));
                } catch (Exception e) { low = 0.0; }
                try { medium = Double.parseDouble(data.getString(RESULT_PROPDIFF_MED));
                } catch (Exception e) {medium = 0.0;}
                try { high = Double.parseDouble(data.getString(RESULT_PROPDIFF_HIGH));
                } catch (Exception e) { high = 0.0; }               
                if ( (low < 0.0) && (s.getOutcomePositive() == true) ) {
                    if ( j > 0 ) {
                        alertString += ",";
                    }     
                    j++;
                    VisualAlert va = new VisualAlert(Integer.parseInt(rowId)-1);
                    va.setType(VisualAlert.belowZero);
                    va.setDescription(VisualAlert.alertDescription[VisualAlert.belowZero]);
                    JSONObject vaObject = new JSONObject(va);
                    alertArray.put(vaObject);
                    eventService.logEvent(s.getIdOrganization(), 0, Eventtemplate.RESULT_BELOW_ZERO, 
                        p.getName() + 
                        " Study ID: " + p.getIdStudy().toString() +
                        ", Period " + data.getString(RESULT_PROPDIFF_ROWID) +
                        ", Observed value = " + low.toString(),
                        p.getIdProcess(), "results"
                        );                    
                    alertString += new JSONObject(va).toString();
                }
            }
            subResults.put("alerts", alertArray);
            //subResults.put("alerts", alertString+"]");
            results.put(index, subResults);
        } catch (JSONException ex) {
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return results;
    }
    
    private void processMatchSet(Process p, Study s, int index, JSONArray results, Integer userId) {

        try {
            JSONObject subResults = results.getJSONObject(index);
            String name = subResults.getString("id");   
            String description = subResults.getString("description");  
            MatchSet ms = new MatchSet();
            ms.setActive(Boolean.TRUE);
            ms.setIdStudy(s.getIdStudy());
            ms.setIdGroup(s.getIdGroup());
            ms.setIdOrganization(s.getIdOrganization());
            ms.setIdModel(s.getIdModel());
            ms.setLocalStatPackage(s.getLocalStatPackage());
            ms.setRemoteStatPackage(s.getRemoteStatPackage());
            ms.setIdProcess(p.getIdProcess());
            ms.setName(name);
            ms.setDescription(description);
            try {
                if ( s.getOverwriteResults() == true ) {
                    List<MatchSet> msList = matchsetController.findMatchsetByUniqueId(s.getIdStudy(), s.getIdOrganization(), s.getIdGroup(), p.getIdProcess(), s.getIdModel());
                    if ( msList.size() > 0 ) {
                        ms = msList.get(0);
                        matchsetController.updateObjects(ms, userId);
                    } else {
                        matchsetController.createObjects(ms, userId);  
                    }
                } else {
                    matchsetController.createObjects(ms, userId);                    
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Exception while storing Match Set: " + ex.getMessage());
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception while parsing Match Set: " + e.getMessage());            
        }
    }
    

    private String getMatchSetsJSON(Integer studyId, Integer organizationId) {
        String dataString = "";
        int counter = 0;
        Study study = (Study) studyController.findObjectEntityById(studyId);
        if ( study != null ) {
            if ( study.getIdOrganization() != organizationId ) {
                logger.log(Level.SEVERE, "An attempt to retrieve Match Sets for wrong organization {0}", organizationId);   
                return dataString;
            }
            logger.log(Level.INFO, "Retrieving match sets.");       
            ArrayList<MatchSet> msl = (ArrayList<MatchSet>) matchsetController.findMatchSetEntitiesForModelAndProject(study.getIdModel(), study.getIdGroup());
            counter = msl.size();
            for (int i=0; i<counter; i++) {
                if ( i > 0 ) {
                    dataString = dataString.concat(",");
                } 
                dataString = dataString + new JSONObject(msl.get(i)).toString();
            }
        }
        dataString = "[" + dataString + "]";
        return dataString;
    }
        
    
    private String prepareJSONResponse(String callId, String message, String result, int counter) {
        JSONObject success = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            if ("".equals(result)) {
                success.put("success", false);
                status.put("message", message);
            } else {
                success.put("success", true);
                status.put("message", message);
                success.put("totalCount", Integer.toString(counter));
                success.put(callId, new JSONArray(result));
            }
            success.put("status", status);
            return success.toString();
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "JSON error in DELTA WS method " + callId, ex);
            result = "{\"status\":{\"message\":\"JSON error in DELTA WS method " + callId + "\"},\"success\":false}";
        }
        return result;
    }
    
//    private String getAllDS_old(Model model, JSONObject dStatsObject, String tableName, boolean isFlatTable) {
//        String dataString;
//        String dstats = "{\"type\":\"ALL\",\"statistics\": [";
//        List<ModelTable> mt = modelService.getModelTables(model.getIdModel());
//        List<ModelColumn> modelColumns = modelService.getModelColumns(mt);
//        if ( isFlatTable == true ) {
//           modelColumns = modelService.filterInsertableModelColumns(modelColumns);        
//        } else {
//           modelColumns = modelService.filterInsertableAtomicModelColumns(modelColumns);                 
//        }
//
//        try {
//            String sqlStmt = "select count(*) from " + tableName;
//            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
//            Query queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
//            List<Object[]> resultCounts = queryResult.getResultList();
//            String tmp = String.valueOf(resultCounts.get(0));
//            long totalCount = Long.parseLong(tmp);            
//            ModelColumn mc = null;
//            int i = 0;
//            for (; i < modelColumns.size(); i++) {
//                try {
//                    mc = modelColumns.get(i);
//                    if (((isFlatTable == false) && (mc.getAtomic() == true)) 
//                            || ((isFlatTable == true) && (mc.getInsertable() == true))) {
//                        if ( "Continuous".equals(mc.getFieldKind()) ) {
//                            Continuous cField = new Continuous(tableName, mc.getName(), "");
//                            dstats += calculateStatsCandD(cField, mc);
//                        } else {
//                            if ( "Dichotomous".equals(mc.getFieldKind()) ) {
//                                Dichotomous dField = new Dichotomous(tableName, mc.getName());
//                                dstats += calculateStatsDandD(dField, mc);
//                            } else {                               
//                                dstats += calculateTotalAndPercentNullStats(tableName, mc, totalCount);
//                            }
//                        }
//                        dstats += ",";
//                    }
//                    if ( model.getIsMissingFinished() == true && mc.getSub() == true && isFlatTable == true) {  // inject raw_ column for DS calculations, if exists
//                        ModelColumn raw_mc = mc;
//                        raw_mc.setName(mc.getName() + "_raw");
//                        raw_mc.setFormula("");
//                        if (((isFlatTable == false) && (raw_mc.getAtomic() == true)) 
//                                || ((isFlatTable == true) && (raw_mc.getInsertable() == true))) {
//                            if ( "Continuous".equals(mc.getFieldKind()) ) {
//                                Continuous cField = new Continuous(tableName, raw_mc.getName(), "");
//                                dstats += calculateStatsCandD(cField, raw_mc);
//                                cField = null;
//                            } else {
//                                if ( "Dichotomous".equals(mc.getFieldKind())) {
//                                    Dichotomous dField = new Dichotomous(tableName, raw_mc.getName());
//                                    dstats += calculateStatsDandD(dField, raw_mc);
//                                    dField = null;
//                                } else {                               
//                                    dstats += calculateTotalAndPercentNullStats(tableName, raw_mc, totalCount);
//                                }
//                            }
//                            dstats += ",";
//                        }                        
//                        raw_mc = null;
//                    }
//                } catch (Exception ex) {
//                    logger.log(Level.SEVERE, "Error DS0126. Exception while getting column Descriptive Statistics for Model.Column: {0}.{1} {2} {3} {4}", new Object[]{model.getName(), mc.getName(), ex.getMessage(), ex.getCause(), ex});
//                    dstats += "{\"filter\":\"\""
//                        + ",\"field\":\"Error DS0126: " + mc.getName() + " " + ex.getMessage() + "\",\"source\":\"\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
//                        + ",\"count\":\"\",\"nullCount\":\"\",\"zeroCount\":\"\",\"notNullCount\":\"\",\"notZeroCount\":\"\",\"nullPercent\":\"\",\"zeroPercent\":\"\",\"notNullPercent\":\"\",\"notZeroPercent\":\"\"},";              
//                }
//            }
//        } catch (Exception ex) {
//            logger.log(Level.SEVERE, "Error DS0127. Exception while getting All Descriptive Statistics for Model: {0} {1} {3}", new Object[]{model.getName(), ex.getMessage(), ex});
//            dstats += "{\"filter\":\"\""
//                + ",\"field\":\"Error DS0127: "  + ex.getMessage() + "\",\"source\":\"\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
//                + ",\"count\":\"\",\"nullCount\":\"\",\"zeroCount\":\"\",\"notNullCount\":\"\",\"notZeroCount\":\"\",\"nullPercent\":\"\",\"zeroPercent\":\"\",\"notNullPercent\":\"\",\"notZeroPercent\":\"\"},";
//        }
//        dstats = Util.removeLastChar(dstats);
//        dstats += "]}";
//        dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
//        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, 1);
//    }

    public String getAllDS(Model model, String tableName, boolean isFlatTable) {
        String dstats = "{\"type\":\"ALL\",\"statistics\": [";
        List<ModelTable> mt = modelService.getModelTables(model.getIdModel());
        List<ModelColumn> modelColumns = modelService.getModelColumns(mt);
        if ( isFlatTable == true ) {
           modelColumns = modelService.filterInsertableModelColumns(modelColumns);        
        } else {
           modelColumns = modelService.filterInsertableAtomicModelColumns(modelColumns);                 
        }

        try {
            String sqlStmt = "select count(*) from " + tableName;
            logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});            
            Query queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
            List<Object[]> resultCounts = queryResult.getResultList();
            String tmp = String.valueOf(resultCounts.get(0));
            long totalCount = Long.parseLong(tmp);            
            ModelColumn mc = null;
            int i = 0;
            for (; i < modelColumns.size(); i++) {
                try {
                    mc = modelColumns.get(i);
                    if (((isFlatTable == false) && (mc.getAtomic() == true)) 
                            || ((isFlatTable == true) && (mc.getInsertable() == true))) {
                        if ( "Continuous".equals(mc.getFieldKind()) ) {
                            Continuous cField = new Continuous(tableName, mc.getName(), "");
                            dstats += calculateStatsCandD(cField, mc);
                        } else {
                            if ( "Dichotomous".equals(mc.getFieldKind()) ) {
                                Dichotomous dField = new Dichotomous(tableName, mc.getName());
                                dstats += calculateStatsDandD(dField, mc);
                            } else {                               
                                dstats += calculateTotalAndPercentNullStats(tableName, mc, totalCount);
                            }
                        }
                        dstats += ",";
                    }
                    if ( model.getIsMissingFinished() == true && mc.getSub() == true && isFlatTable == true) {  // inject raw_ column for DS calculations, if exists
                        ModelColumn raw_mc = mc;
                        raw_mc.setName(mc.getName() + "_raw");
                        raw_mc.setFormula("");
                        if (((isFlatTable == false) && (raw_mc.getAtomic() == true)) 
                                || ((isFlatTable == true) && (raw_mc.getInsertable() == true))) {
                            if ( "Continuous".equals(mc.getFieldKind()) ) {
                                Continuous cField = new Continuous(tableName, raw_mc.getName(), "");
                                dstats += calculateStatsCandD(cField, raw_mc);
                                cField = null;
                            } else {
                                if ( "Dichotomous".equals(mc.getFieldKind())) {
                                    Dichotomous dField = new Dichotomous(tableName, raw_mc.getName());
                                    dstats += calculateStatsDandD(dField, raw_mc);
                                    dField = null;
                                } else {                               
                                    dstats += calculateTotalAndPercentNullStats(tableName, raw_mc, totalCount);
                                }
                            }
                            dstats += ",";
                        }                        
                        raw_mc = null;
                    }
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Error DS0126. Exception while getting column Descriptive Statistics for Model.Column: {0}.{1} {2} {3} {4}", new Object[]{model.getName(), mc.getName(), ex.getMessage(), ex.getCause(), ex});
                    dstats += "{\"filter\":\"\""
                        + ",\"field\":\"Error DS0126: " + mc.getName() + " " + ex.getMessage() + "\",\"source\":\"\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
                        + ",\"count\":\"\",\"nullCount\":\"\",\"zeroCount\":\"\",\"notNullCount\":\"\",\"notZeroCount\":\"\",\"nullPercent\":\"\",\"zeroPercent\":\"\",\"notNullPercent\":\"\",\"notZeroPercent\":\"\"},";              
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error DS0127. Exception while getting All Descriptive Statistics for Model: {0} {1} {3}", new Object[]{model.getName(), ex.getMessage(), ex});
            dstats += "{\"filter\":\"\""
                + ",\"field\":\"Error DS0127: "  + ex.getMessage() + "\",\"source\":\"\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
                + ",\"count\":\"\",\"nullCount\":\"\",\"zeroCount\":\"\",\"notNullCount\":\"\",\"notZeroCount\":\"\",\"nullPercent\":\"\",\"zeroPercent\":\"\",\"notNullPercent\":\"\",\"notZeroPercent\":\"\"},";
        }
        dstats = Util.removeLastChar(dstats);
        dstats += "]}";
        return dstats;
    }    
    
    private String getDichotomousStats(Model model, JSONObject dStatsObject, JSONArray filterArray, ModelColumn mc, String tableName) {
        String dataString = "";
        int filterCount = 0;

        try {
            String dstats = "{\"type\":\"DD\",\"statistics\": [";
            Dichotomous dMain = null;

            dMain = new Dichotomous(tableName, mc.getName());
            logger.log(Level.INFO, "Dichotomous field {0} processed: {1}, {2}, {3}, {4}, {5}, {6}",
                    new Object[]{mc.getName(), dMain.getTrueValue(), dMain.getFalseValue(), dMain.getNullValue(),
                        Integer.toString(dMain.getTrueCount()), Integer.toString(dMain.getFalseCount()), Integer.toString(dMain.getNullCount())});
            dstats += "{\"TRUE\":\"" + dMain.getTrueValue() + "\""
                    + ",\"FALSE\":\"" + dMain.getFalseValue() + "\""
                    + ",\"NULL\":" + dMain.getNullValue() + "}";
            dstats += ",{\"TRUE\":" + Integer.toString(dMain.getTrueCount())
                    + ",\"FALSE\":" + Integer.toString(dMain.getFalseCount())
                    + ",\"NULL\":" + Integer.toString(dMain.getNullCount()) + "}";

            for (filterCount = 0; filterCount < filterArray.length(); filterCount++) {
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(filterCount));
                Dichotomous dFilter = new Dichotomous(tableName, mcf.getName());
                logger.log(Level.INFO, "Dichotomous filter {0} processed: true={1}, false={2}, null={3}, trueCount={4}, falseCount={5}, nullCount={6}",
                        new Object[]{mcf.getName(), dFilter.getTrueValue(), dFilter.getFalseValue(), dFilter.getNullValue(),
                            Integer.toString(dFilter.getTrueCount()), Integer.toString(dFilter.getFalseCount()), Integer.toString(dFilter.getNullCount())});
                dstats += "," + calculateStatsDandD(dMain, dFilter, dFilter.getTrueValueFromDb());
                dstats += "," + calculateStatsDandD(dMain, dFilter, dFilter.getFalseValueFromDb());
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0012 Exception while getting Descriptive Statistics. Model not found.", "", 0);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0013 Database operation failed while getting Descriptive Statistics from table: " + tableName + ", " + DBHelper.printSQLException(ex), "", 0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
            return prepareJSONResponse("dStats", "DS0014 Database connection failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
        }
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, filterCount + 1);
    }

    private String getContinuousStats(Model model, JSONObject dStatsObject, JSONArray filterArray, ModelColumn mc, String tableName) {
        String dataString = "";
        int filterCount = 0;

        try {
            String dstats = "{\"type\":\"CD\",\"statistics\": [";
            Continuous field = null;

            field = new Continuous(tableName, mc.getName(), "");
            logger.log(Level.INFO, "Continuous field {0} processed: min={1}, mean={2}, max={3}, total={4}, notNullPercent={5}",
                    new Object[]{mc.getName(), field.getMinValue(), field.getMeanValue(), field.getMaxValue(),
                        Integer.toString(field.getTotalCount()), field.getNotNullPercent()});
            dstats += calculateStatsCandD(field, mc);

            for (filterCount = 0; filterCount < filterArray.length(); filterCount++) {
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(filterCount));
                if ("Dichotomous".equals(mcf.getFieldKind())) {
                    Dichotomous dFilter = new Dichotomous(tableName, mcf.getName());
                    logger.log(Level.INFO, "Dichotomous filter {0} processed: true={1}, false={2}, null={3}, trueCount={4}, falseCount={5}, nullCount={6}",
                            new Object[]{mcf.getName(), dFilter.getTrueValue(), dFilter.getFalseValue(), dFilter.getNullValue(),
                                Integer.toString(dFilter.getTrueCount()), Integer.toString(dFilter.getFalseCount()), Integer.toString(dFilter.getNullCount())});
                    dstats += ",";
                    Continuous fieldTrue = new Continuous(tableName, mc.getName(), dFilter.getName() + " = '" + dFilter.getTrueValueFromDb() + "'");
                    dstats += calculateStatsCandD(fieldTrue, mcf) + ",";
                    Continuous fieldFalse = new Continuous(tableName, mc.getName(), dFilter.getName() + " = '" + dFilter.getFalseValueFromDb() + "'");
                    dstats += calculateStatsCandD(fieldFalse, mcf);
                }
                if ("Enumerated".equals(mcf.getFieldKind())) {
                    Enumerated eFilter = new Enumerated(tableName, mcf.getName());
                    logger.log(Level.INFO, "Enumerated filter {0} processed: values={1}, null={2}, nullCount={3}",
                            new Object[]{mcf.getName(), eFilter.getValue().toString(), eFilter.getNullValue(), Integer.toString(eFilter.getNullCount())});
                    for (int ii = 0; ii < eFilter.getSize(); ii++) {
                        dstats += ",";
                        Continuous filteredField = new Continuous(tableName, mc.getName(), eFilter.getName() + " = '" + eFilter.getValue()[ii] + "'");
                        dstats += calculateStatsCandD(filteredField, mcf);
                    }
                }
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0015 Exception while getting Descriptive Statistics. Model not found.", "", 0);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0016 Database operation failed while getting Descriptive Statistics from table: " + tableName + ", " + DBHelper.printSQLException(ex), "", 0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
            return prepareJSONResponse("dStats", "DS0017 Database operation failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
        }
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, filterCount + 1);
    }

    private String getCategoryStats(Model model, JSONObject dStatsObject, JSONArray filterArray, ModelColumn mc, String tableName) {

        String dataString = "";
        int count = 0;

        try {
            String dstats = "{\"type\":\"CAT\",\"statistics\": [";
            dstats += calculateCategoryStatsD(tableName, mc);

            for (int j = 0; j < filterArray.length(); j++) {
                Dichotomous dFilter = null;
                ModelColumn mcf = modelColumnController.findModelColumn((int) filterArray.getLong(j));
                if ("Dichotomous".equals(mcf.getFieldKind())) {
                    dFilter = new Dichotomous(tableName, mcf.getName());
                    dstats += "," + calculateCategoryStatsDWithFilter(tableName, mc, dFilter.getName(), dFilter.getTrueValueFromDb());
                    dstats += "," + calculateCategoryStatsDWithFilter(tableName, mc, dFilter.getName(), dFilter.getFalseValueFromDb());
                }
            }
            dstats += "]}";
            dataString = "[" + dStatsObject.toString() + "," + dstats + "]";
        } catch (EntityNotFoundException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0018 Exception while getting Descriptive Statistics. Model not found.", "", 0);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getCause().toString()});
            return prepareJSONResponse("dStats", "DS0019 Database operation failed while getting Descriptive Statistics from table: " + tableName + ", " + DBHelper.printSQLException(ex), "", 0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while getting Descriptive Statistics for Model: {0} {1}", new Object[]{model.getName(), ex.getMessage()});
            return prepareJSONResponse("dStats", "DS0020 Database connection failed while getting Descriptive Statistics. " + ex.getMessage(), "", 0);
        }
        return prepareJSONResponse("dStats", "Descriptive Statistics retrieved", dataString, count);
    }

    private String calculateTotalAndPercentNullStats(String tableName, ModelColumn mc, long totalCount) throws Exception {
        Query queryResult;
        DecimalFormat df = new DecimalFormat("#.##");
        String notNullPercent, nullPercent;
        String sqlStmt = "select count(*) from "
                + tableName + " where "
                + mc.getName() + " is null";
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        List<Object[]> resultCounts = queryResult.getResultList();
        String tmp = String.valueOf(resultCounts.get(0));
        long nullCount = Long.parseLong(tmp);
        long notNullCount = totalCount - nullCount;

        if (totalCount != 0) {
            notNullPercent = df.format(100 * (totalCount - nullCount) / totalCount);
            nullPercent = df.format(100 * (nullCount) / totalCount);
        } else {
            notNullPercent = df.format(0);
            nullPercent = df.format(0);
        }
        String dstats = "{\"filter\":\"\""
                + ",\"field\":\"" + mc.getName() + "\""   
                + ",\"source\":\"" + getFieldSource(mc) + "\""                      
                + ",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0"
                + ",\"count\":" + Long.toString(totalCount)
                + ",\"nullCount\":" + Long.toString(nullCount)
                + ",\"nullPercent\":" + nullPercent
                + ",\"notNullCount\":" + Long.toString(notNullCount)
                + ",\"notNullPercent\":" + notNullPercent                
                + ",\"class\":\"" + mc.getFieldClass() + "\""
                + ",\"kind\":\"" + mc.getFieldKind() + "\""
                + ",\"formula\":\"" + mc.getFormula() + "\""
                + ",\"description\":\"" + mc.getDescription() + "\""                     
                + "}";
        return dstats;
    }

    /* this method returns descriptive statistcs for Dichotomous field based on Dichotomous filters */
    private String calculateStatsDandD(Dichotomous field, Dichotomous filter, String trueOrFalse) throws Exception {

        Query queryResult;
        String sqlStmt = "select count(" + field.getName() + ") from "
                + field.getTableName() + " where "
                + filter.getName() + " = '" + trueOrFalse + "' AND "
                + field.getName() + " = '" + field.getTrueValueFromDb() + "'";
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        List<Object[]> resultCounts = queryResult.getResultList();
        String tmp = String.valueOf(resultCounts.get(0));
        int trueCount = Integer.parseInt(tmp);

        sqlStmt = "select count(" + field.getName() + ") from "
                + field.getTableName() + " where "
                + filter.getName() + " = '" + trueOrFalse + "' AND "
                + field.getName() + " = '" + field.getFalseValueFromDb() + "'";
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        resultCounts = queryResult.getResultList();
        tmp = String.valueOf(resultCounts.get(0));
        int falseCount = Integer.parseInt(tmp);

        String dstats = "{\"TRUE\":" + Integer.toString(trueCount)
                + ",\"FALSE\":" + Integer.toString(falseCount)
                + ",\"NULL\":\"\"}";
        return dstats;
    }

    /* this method is used for calculating dichotomous stats when "all" stats are requested */
    private String calculateStatsDandD(Dichotomous field, ModelColumn mc) throws Exception {

        //Query queryResult;
        DecimalFormat df = new DecimalFormat("#.##");
        String notNullPercent;

        if (field.getTotalCount() != 0) {
            notNullPercent = df.format(100 * (field.getTotalCount() - field.getNullCount()) / field.getTotalCount());
        } else {
            notNullPercent = df.format(0);
        }

        String dstats = "{\"filter\":\"\""
                + ",\"field\":\"" + field.getName() + "\""
                + ",\"source\":\"" + getFieldSource(mc) + "\""                
                + ",\"min\":\"" + field.getFalseValue() + "\""
                + ",\"mean\":\"" + field.getMeanValue() + "\""
                + ",\"max\":\"" + field.getTrueValue() + "\""
                + ",\"value25\":\"\""
                + ",\"value75\":\"\""
                + ",\"median\":\"\""
                + ",\"std\":\"" + field.getStdValue() + "\""
                + ",\"count\":" + Integer.toString(field.getTotalCount())
                + ",\"nullCount\":" + Integer.toString(field.getNullCount())
                + ",\"zeroCount\":" + Integer.toString(field.getZeroCount())  
                + ",\"notNullCount\":" + Integer.toString(field.getNotNullCount())                      
                + ",\"notZeroCount\":" + Integer.toString(field.getNotZeroCount())      
                + ",\"nullPercent\":" + field.getNullPercent()     
                + ",\"zeroPercent\":" + field.getZeroPercent()                      
                + ",\"notNullPercent\":" + field.getNotNullPercent()
                + ",\"notZeroPercent\":" + field.getNotZeroPercent()  
                + ",\"class\":\"" + mc.getFieldClass() + "\""
                + ",\"kind\":\"" + mc.getFieldKind() + "\""
                + ",\"formula\":\"" + mc.getFormula() + "\""
                + ",\"description\":\"" + mc.getDescription() + "\""          
                + "}";        
        return dstats;
    }
    
    /* this method returns descriptive statistcs for Continuous field based on Dichotomous filters */
    private String calculateStatsCandD(Continuous field, ModelColumn mc) {
        String dstats = "{\"filter\":\"" + field.getFilterString() + "\""
                + ",\"field\":\"" + field.getName() + "\""
                + ",\"source\":\"" + getFieldSource(mc) + "\""
                + ",\"min\":\"" + field.getMinValue() + "\""
                + ",\"mean\":\"" + field.getMeanValue() + "\""
                + ",\"max\":\"" + field.getMaxValue() + "\""
                + ",\"value25\":\"" + field.getValue25() + "\""
                + ",\"value75\":\"" + field.getValue75() + "\""
                + ",\"median\":\"" + field.getMedianValue() + "\""
                + ",\"std\":\"" + field.getStdValue() + "\""
                + ",\"count\":" + Integer.toString(field.getTotalCount())
                + ",\"nullCount\":" + Integer.toString(field.getNullCount())
                + ",\"zeroCount\":" + Integer.toString(field.getZeroCount())  
                + ",\"notNullCount\":" + Integer.toString(field.getNotNullCount())                      
                + ",\"notZeroCount\":" + Integer.toString(field.getNotZeroCount())      
                + ",\"nullPercent\":" + field.getNullPercent()     
                + ",\"zeroPercent\":" + field.getZeroPercent()                      
                + ",\"notNullPercent\":" + field.getNotNullPercent()
                + ",\"notZeroPercent\":" + field.getNotZeroPercent()                       
                + ",\"class\":\"" + mc.getFieldClass() + "\""
                + ",\"kind\":\"" + mc.getFieldKind() + "\""
                + ",\"formula\":\"" + mc.getFormula() + "\""
                + ",\"description\":\"" + mc.getDescription() + "\""                      
                + "}";
        return dstats;
    }

    /* this method returns descriptive statistcs for Category field */
    private String calculateCategoryStatsD(String modelTable, ModelColumn mc) throws Exception {

        int limit = 100;
        int count;
        String sqlStmt;
        Query queryResult;
        List<Object[]> results;
        String dstats = "";

        sqlStmt = "select " + mc.getName() + ", count(*) as count from " + modelTable
                + " group by " + mc.getName()
                + " limit " + String.valueOf(limit);

        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        results = queryResult.getResultList();
        count = results.size();

        boolean nullOccurenceFlag = false;
        for (int i = 0; i < count; i++) {
            if (i > 0) {
                dstats += ",";
            }
            String tmpValue, tmpCount;
            if (results.get(i)[0] == null) {
                tmpValue = "NULL";
                tmpCount = String.valueOf(results.get(i)[1].toString());
                nullOccurenceFlag = true;
            } else {
                if ((i == 0) && (nullOccurenceFlag == false)) {
                    // compensate for db not reporting count of records with nulls
                    dstats += formatCategoryStatsDNullJSON("", "") + ",";
                }
                tmpValue = results.get(i)[0].toString();
                tmpCount = String.valueOf(results.get(i)[1].toString());
            }
            dstats += "{\"filter\":\"\"";
            dstats += ",\"value\":\"" + tmpValue + "\"";
            dstats += ",\"count\":" + tmpCount + "}";
        }
        return dstats;
    }

    /* this method returns descriptive statistcs for Category field based on Dichotomous filters */
    private String calculateCategoryStatsDWithFilter(String modelTable, ModelColumn mc, String filterName, String filterValue) throws Exception {

        int limit = 100;
        int count, countWithFilter;
        String sqlStmt;
        Query queryResult;
        List<Object[]> results, resultsWithFilter;
        String dstats = "";

        // establish number of values without filter
        sqlStmt = "select " + mc.getName() + ", count(*) as count from " + modelTable
                + " group by " + mc.getName()
                + " limit " + String.valueOf(limit);
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        results = queryResult.getResultList();
        count = results.size();

        // apply filter and get count
        sqlStmt = "select " + mc.getName() + ", count(*) as count from " + modelTable
                + " where " + filterName + " = '" + filterValue + "'"
                + " group by " + mc.getName()
                + " limit " + String.valueOf(limit);
        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        resultsWithFilter = queryResult.getResultList();
        countWithFilter = resultsWithFilter.size();

        boolean nullOccurenceFlag = false;
        int j;
        for (int i = 0; i < count; i++) {
            if (i > 0) {
                dstats += ",";
            }
            String tmpValue, tmpCount;
            j = getMatchingFilteredRecord(countWithFilter, resultsWithFilter, results.get(i)[0]);
            if (j == countWithFilter) { // no match
                // compensate for db not reporting count of records with nulls
                dstats += formatCategoryStatsDNullJSON(filterName, filterValue);
                continue;
            }
            if (resultsWithFilter.get(j)[0] == null) {
                tmpValue = "NULL";
                tmpCount = String.valueOf(resultsWithFilter.get(j)[1].toString());
                nullOccurenceFlag = true;
            } else {
                if ((i == 0) && (nullOccurenceFlag == false)) {
                    // compensate for db not reporting count of records with nulls
                    dstats += formatCategoryStatsDNullJSON(filterName, filterValue) + ",";
                }
                tmpValue = resultsWithFilter.get(j)[0].toString();
                tmpCount = String.valueOf(resultsWithFilter.get(j)[1].toString());
            }
            dstats += "{\"filter\":\"" + filterName + " = '" + filterValue + "'\"";
            dstats += ",\"value\":\"" + tmpValue + "\"";
            dstats += ",\"count\":" + tmpCount + "}";
        }
        return dstats;
    }
    
    /* this method uses MySQL statistical functions to caculate correlation between values in 2 columns X and Y */
    private String calculateCorrelation(String modelTable, ModelColumn mc_x, ModelColumn mc_y, int index) throws Exception {

        int count;
        String sqlStmt;
        Query queryResult;
        List<Object[]> results;
        String dstats = "";
        
        sqlStmt = "select (count(*)*sum(" + mc_x.getName() + "*" + mc_y.getName() + ")-sum(" + mc_x.getName() + ")*sum(" + mc_y.getName() 
                + "))/(sqrt(Count(*)*Sum(" + mc_x.getName() + "*" + mc_x.getName() + ")-Sum(" + mc_x.getName() + ")*Sum(" + mc_x.getName() 
                + "))*sqrt(Count(*)*Sum(" + mc_y.getName() + "*" + mc_y.getName() + ")-Sum(" + mc_y.getName() + ")*Sum(" + mc_y.getName() + "))) AS TotalCorelation FROM " + modelTable;


        logger.log(Level.INFO, "Executing SQL statement: {0}", new Object[]{sqlStmt});
        queryResult = DBHelper.executeSqlQuery("DeltaData", sqlStmt, logger);
        results = queryResult.getResultList();
        //count = results.size();

        //Double tmpValue = (Double)results.get(0)[0];
        DecimalFormat df = new DecimalFormat("#.####");        
        String tmpValue = String.valueOf(results.get(0));
        if ( !"null".equals(tmpValue) ) {
            tmpValue = df.format(Double.parseDouble(tmpValue));       
        } else {
            tmpValue = "\"NaN\"";
        }
        dstats += "{\"column_x\":\"" + mc_x.getName() + "\"";
        dstats += ",\"column_y\":\"" + mc_y.getName() + "\"";       
        dstats += ",\"value\":" + tmpValue + "}";
        return dstats;
    }
    
    private String formatCategoryStatsDNullJSON(String filterName, String filterValue) {
        String dstats = "";

        if (!"".equals(filterName)) {
            dstats += "{\"filter\":\"" + filterName + " = '" + filterValue + "'\"";
        } else {
            dstats += "{\"filter\":\"\"";
        }
        dstats += ",\"value\":\"NULL\"";
        dstats += ",\"count\":0}";
        return dstats;
    }

    private String getIntervalQuery(Study s) {
        String responseString = "";
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());
        String endDate = String.format("%1$TY/%1$Tm/%1$Td", s.getEndTS());  
        String startDate =  String.format("%1$TY/%1$Tm/%1$Td", s.getStartTS());             
        if ( startDate != null && !"".equals(startDate) ) {
            responseString = mc.getName() + " >= '" + startDate + "'";
        }
        if ( endDate != null && !"".equals(endDate) ) {
            if ( !"".equals(responseString) ) {
                responseString += " AND ";
            }
            responseString += mc.getName() + " <= '" + endDate + "'";
        }        
        return responseString;
    } 
    
    
    //v3.64 added new code for matched survival with follow up date for item https://ceri-lahey.atlassian.net/browse/DELQA-787
    private String getIntervalQuerySRVM(Study s,int stepNo ) throws JSONException {
        String responseString = "";
        ModelColumn mc = modelColumnController.findModelColumn(s.getIdDate());
        //System.out.println("getstart date"+s.getStartTS()+"type"+s.getStartTS().getClass().getName());
        //System.out.println("getend date"+s.getEndTS()+"type"+s.getEndTS().getClass().getName());
             
        String endDate = String.format("%1$TY/%1$Tm/%1$Td", s.getEndTS());  
        String startDate =  String.format("%1$TY/%1$Tm/%1$Td", s.getStartTS());  

        JSONArray params = new JSONArray(s.getMethodParams());  
        JSONObject jobObject = null;
        JSONObject stepConf = params.getJSONObject(stepNo);   
        
        //System.out.println("startDate "+ startDate);
        //System.out.println("endDate "+ endDate);
      
       String followupDateRaw = stepConf.getJSONObject("inputs").getJSONObject("followupDateSelection").get("values").toString();
       followupDateRaw = followupDateRaw.replaceAll("\\[","");
       followupDateRaw = followupDateRaw.replaceAll("\\]","");
       followupDateRaw = followupDateRaw.replaceAll("^\"|\"$","");
       followupDateRaw = followupDateRaw.replaceAll("-","/");
       
       //System.out.println("followupDateRaw"+followupDateRaw);
       //System.out.println("followupDateRaw type "+ followupDateRaw.getClass().getName());

        if ( startDate != null && !"".equals(startDate) ) {
            responseString = mc.getName() + " >= '" + startDate + "'";
        }
        
        if(followupDateRaw.equals(endDate)){
             //System.out.println("Both dates are equal");
             if ( endDate != null && !"".equals(endDate) ) {
                    if ( !"".equals(responseString) ) {
                        responseString += " AND ";
                    }
                 responseString += mc.getName() + " <= '" + endDate + "'";
             }
             
           }
        else{
            //System.out.println("Both dates are NOT equal");
                responseString +=" AND "+ mc.getName() + " <= '" + followupDateRaw + "'";
            }
        
       return responseString;
    } 
    
    
    
    
    
    
    private int getMatchingFilteredRecord(int max, List<Object[]> results, Object o) {
        int i = 0;
        for (; i < max; i++) {
            if (results.get(i)[0] == null) {
                if (o == null) {
                    break; // match on null
                } else {
                    continue;
                }
            }
            if (results.get(i)[0].equals(o)) {
                break;
            }
        }
        return i;
    }
    
    private String getAuthString(String process, String alias)
    {    
        String authString = "{\"idApp\": \"DELTA3.0\",\"idProject\": \"" 
            + process + "\",\"idUser\": \"" + alias + "\"}";
        return authString;
    }     
    
    private String getProcessString(String processId, String guid) {
        StringTokenizer stP = new StringTokenizer(processId, ",");
        StringTokenizer stG = new StringTokenizer(guid, ",");
        String processString = "\"processes\":[";        
        int i = 0;
        while (stP.hasMoreElements()) {
            if ( i++ > 0 ) {
                processString += ',';
            }
            processString += "{\"idProcess\": " + stP.nextElement() + ",\"guid\": \"" + stG.nextElement() + "\"}";         
        }      
        processString += "]";
        return processString;
    }
 
    private Set<String> retrieveColumnsFromParams(Study study, JSONObject params) throws StudyParameterException {
        Set<String> columns = new TreeSet();
        try { 
            String col = null;
            ModelColumn mc = modelColumnController.findModelColumn(study.getIdKey());
            columns.add(mc.getName());
            try { 
                col = params.getJSONObject("dependentVariableSelection").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: dependentVariable is undefined");
                columns.add(col); 
            } catch (JSONException ex) {}
            try {             
                col = params.getJSONObject("sequencingVariableSelection").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: sequencingVariable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}
            try {
                col = params.getJSONObject("exposureVariableSelection").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: exposureVariable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}  
            try {             
                col = params.getJSONObject("start").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col)) throw new StudyParameterException("DS0245 Study Parameter error: start variable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}
            try {             
                col = params.getJSONObject("stop").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: stop variable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}   
            try {             
                col = params.getJSONObject("exposureDateSelection").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: exposureDateSelection variable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}   
            try {             
                col = params.getJSONObject("outcomeDateSelection").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: outcomeDateSelection variable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}    
            try {             
                col = params.getJSONObject("lastSeenDateSelection").getJSONArray("values").getString(0);
                if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: lastSeenDateSelection variable is undefined");                
                columns.add(col);
            } catch (JSONException ex) {}   
            
            
            
            //int length = inputs.getJSONObject("inputs").getJSONObject("independentVariableSelection").getJSONArray("values").length();
            int length = 0;
            try {
                length = params.getJSONObject("independentVariableSelection").getJSONArray("values").length();            
                for (int i=0; i<length; i++) {
                    col = params.getJSONObject("independentVariableSelection").getJSONArray("values").getString(i);
                    if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: independent variable is undefined");                                    
                    columns.add(col);
                }
            } catch (JSONException ex) {}     
            try {
                length = params.getJSONObject("outcomeVariableSelection").getJSONArray("values").length();            
                for (int i=0; i<length; i++) {
                    col = params.getJSONObject("outcomeVariableSelection").getJSONArray("values").getString(i);
                    if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0245 Study Parameter error: outcome variable is undefined");                                    
                    columns.add(col);
                }
            } catch (JSONException ex) {}                
        } catch (StudyParameterException ex) {           
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, "StudyParameterException when retrieving columns from study parameters ", ex);         
            throw ex;
        } catch (Exception ex) {
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, "Exception when retrieving columns from study parameters ", ex);
        }
        return columns;
    }    
 
    private Set<String> retrieveModelColumnsFromParams(Study study, JSONObject params) throws StudyParameterException {
        Set<String> columns = new TreeSet();
        try { 
            String col = null;
            int length = 0;
            try {
                length = params.getJSONObject("model").getJSONArray("values").length();            
                for (int i=0; i<length; i++) {
                    col = params.getJSONObject("model").getJSONArray("values").getJSONObject(i).getString("name");
                    if ( col == null || "".equals(col) ) throw new StudyParameterException("DS0246 Study Parameter error: LRF model column is undefined");                                    
                    if ( !"intercept".equals(col.toLowerCase()) ) { 
                        columns.add(col);
                    }
                }
            } catch (JSONException ex) {}        
        } catch (StudyParameterException ex) {           
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, "StudyParameterException when retrieving columns from study parameters ", ex);         
            throw ex;
        } catch (Exception ex) {
            Logger.getLogger(StatServiceImpl.class.getName()).log(Level.SEVERE, "Exception when retrieving columns from study parameters ", ex);
        }
        return columns;
    }  
    
    private void initializeProxy() {
        if (pb == null) {
            logger.log(Level.SEVERE, "Creating new ProxyBridge");
            pb = new ProxyBridge();
        }
        if (proxy == null) {
            logger.log(Level.SEVERE, "Creating new Proxy");
            proxy = pb.getProxy(Proxy.NON_MULTICAST, new Property("hazelcastPartnerIp").getParamValue(), new Property("hazelcastInterface").getParamValue());
        }        
    }
    
    private String buildDropStm(String dbPrepend, String tag) {
        String sqlString = "DROP TABLE IF EXISTS `" + dbPrepend + "_" + tag + "`;";
        return sqlString;
    };

    private String buildCreateStm(String dbPrepend, String tag, JSONObject results) {
        int j;
        String sqlString = "CREATE TABLE `" + dbPrepend + "_" + tag + "` (";
        sqlString += formatSQLColumns(results," varchar(256) DEFAULT NULL");
        sqlString += ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        return sqlString;
    };

    private String buildInsertStm(String dbPrepend, String tag, JSONObject results) {
        String sqlString = null;
        try {
            JSONArray resultsArray = results.getJSONArray("data");
            String fields = formatSQLColumns(results,"");
            if ( resultsArray.length() > 0 ) {
                sqlString = "INSERT INTO `" + dbPrepend + "_" + tag + "` (" + fields + ") VALUES";            
                for (int i = 0; i < resultsArray.length(); i++) {
                    JSONArray dataArray = resultsArray.getJSONArray(i);
                    if (i == 0) {
                        sqlString += " (";
                    } else {
                        sqlString += ",(";
                    }
                    for (int j = 0; j < dataArray.length(); j++) {
                        String value = dataArray.getString(j);
                        if ( j != 0 ) {
                            sqlString += ",";
                        }
                        if ( "null".equals(value) ) {
                            value = "";
                        }
                        sqlString += "'" + value + "'";
                    }
                    sqlString += ")";                
                }
                sqlString += ';';
            }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while formatting SQL to insert table {0} {1}", new Object[]{dbPrepend + "_" + tag, ex});
        }
        return sqlString;
    }    
    
    private String formatSQLColumns(JSONObject results, String columnType) {
        String sqlString = "";
        try {
            JSONArray columnArray = results.getJSONArray("columns");
            for (int j = 0; j < columnArray.length(); j++) {
                JSONObject column = columnArray.getJSONObject(j);
                String columnName = "", columnName1 = "", columnName2 = "";
                columnName = column.getString("name");
                try {
                    JSONObject parentColumn1 = column.getJSONObject("parentcolumn");
                    columnName1 = "_" + parentColumn1.getString("name");
                    try {
                        JSONObject parentColumn2 = parentColumn1.getJSONObject("parentcolumn");
                        columnName2 = "_" + parentColumn2.getString("name");
                    } catch (Exception e) {                   
                    }                    
                } catch (Exception e) {                   
                }
                if ( j != 0 ) {
                    sqlString += ", ";
                }
                sqlString += "`" + columnName + columnName1 + columnName2 + "`" + columnType;
            }
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, "Exception while formatting SQL column string {0} {1}", new Object[]{sqlString, ex});            
        }
        return sqlString;        
    }
    
    
    //Replace Atomic name to source 
    private String getFieldSource(ModelColumn mc) {
        if ( mc.getSub() == true ) return "Missing";        
        if ( mc.getAtomic() == true ) return "Source";
        if ( mc.getVirtual() == true ) return "Virtual";
        return "Unknown";
    }
    
    public void runDeltalytics(String submitStudy) throws MalformedURLException, IOException, JSONException { 
        String response="This is POST test";
        logger.log(Level.INFO, "Running DELTAlytics POST API: {0}", submitStudy);
        JSONObject submitStudyjsonObject = new JSONObject(submitStudy);
        String APIKey = new Property("x-api-key").getParamValue();
        URL url = new URL( new Property("localStatPackageHostPort").getParamValue() + "/process_study"); 
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("POST");
        //System.out.println("API key read: "+APIKey);
        con.setRequestProperty("x-api-key", APIKey);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream (
            con.getOutputStream());
            wr.writeBytes(submitStudy);
            wr.close();
        logger.log(Level.INFO, "*****************This is response code DELTAlytics POST API: {0}", con.getResponseCode());
        logger.log(Level.INFO, "*****************This is response msg Running DELTAlytics POST API: {0}", con.getResponseMessage());
        //response for progress in msg
        //return response;
    }
}
