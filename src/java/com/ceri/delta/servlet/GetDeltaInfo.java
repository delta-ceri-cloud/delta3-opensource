/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.servlet;

import com.ceri.delta.security.Auth;
import com.ceri.delta.server.processor.ProcessStudyBatch;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import com.ceri.delta.jpa.process.ProcessJpaController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns="/GetDeltaInfo", asyncSupported=true)

public class GetDeltaInfo extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestType = "Unknown";
        try {
            String payload = req.getParameter("payload");
            requestType = req.getParameter("requestType");      
            System.out.println("++++++++++++++++++++++ at doPost file servlet :"+payload);
            
            Integer userId = Auth.getUserIdFromSession(req);
            System.out.println("++++++++++++++++++++++ at userId  file servlet :"+userId);
            
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            System.out.println("++++++++++++++++++++++ at decodedContent file servlet :"+decodedContent);
            response(resp, "Success file servlet");
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "Exception in POST method of " + requestType, ex);
        }
    }
    
    
    
    @Override
     public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
           /* String payload = req.getParameter("payload");
            
            Integer userId = Auth.getUserIdFromSession(req);
            if ( userId > 0 ) {
                String decodedContent = URLDecoder.decode(payload, "UTF-8");
                System.out.println("This is decodedContent"+decodedContent);
                System.out.println("This is payload"+payload);
                System.out.println("This is process field"+Process.class.getFields().toString());
                
                response(resp,"This is do get call request authenticated");
            } else {
                response(resp, "Requester not authenticated. Request ignored.");
            }*/
           
           
           String JsonString = "{"+ "\"DELTA\":\"v3.7\"" +"\"Message\":\"API Under Construction\""+"\"Data\":{}"+"}";
           response(resp,JsonString );        
           //response(resp, "API under construction");
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "Exception in GET method of ProcessStudyServlet  ", ex);
        }
    }
    
    
    private void response(HttpServletResponse resp, String msg) throws IOException {
            PrintWriter out = resp.getWriter();
            out.println(msg);
    }    
}
