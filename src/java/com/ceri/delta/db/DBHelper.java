package com.ceri.delta.db;
import com.opencsv.CSVWriter;
import com.ceri.delta.entity.Configuration;
import com.ceri.delta.jpa.ConfigurationJpaController;
import com.ceri.delta.security.DesEncrypter;
import com.ceri.delta.util.Util;
import com.ceri.delta.entity.process.Process;
import java.math.BigDecimal;
import java.io.FileWriter;
import java.io.Writer;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import com.ceri.delta.server.rest.Delta3StudyResource;
import java.net.URLDecoder;
import java.nio.file.*; 
import javax.servlet.RequestDispatcher;
import com.comPath;
import java.net.InetAddress;
import java.net.UnknownHostException;
 

public class DBHelper
{
  private static final Logger logger = Logger.getLogger(DBHelper.class.getName()); private static String[] typeS = { 
      "SET", "ENUM", "VARBINARY", "DECIMAL", "BIGINT", "INT", "MEDIUMINT", "SMALLINT", "TINYINT", "VARCHAR", "CHAR" };

  
  public static String getMetadata(String dbType, String connString, String dbUser, String dbPass) throws Exception {
    String jdbcDriver = "";
    String dataString = "[";
    String schema = null;
    ResultSet  mrs = null;
    Statement st = null;
    DesEncrypter des = new DesEncrypter(ConfigurationJpaController.dbPhrase);
    
    if (dbType.equals("MySQL")) {
      jdbcDriver = "com.mysql.jdbc.Driver";
    }
    else if (dbType.equals("MS-SQL")) {
      jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
      schema = "dbo";
    } else {
      throw new DatabaseNotSupportedException("Database type " + dbType + " is not supported.");
    } 
    
    try {
      Connection conn = ConnectionCache.getInstance().getConnection(jdbcDriver, connString, dbUser, des.decrypt(dbPass));
      st = conn.createStatement();
      mrs = conn.getMetaData().getTables(null, schema, null, new String[] { "TABLE" });
      int j = 0;
      while (mrs.next()) {
        String tableName = "";
        try {
          tableName = mrs.getString(3);
          logger.log(Level.INFO, "SQL Getting metadata for table: " + tableName);
          ResultSet rs = st.executeQuery("select * from " + tableName);
          ResultSetMetaData metadata = rs.getMetaData();
          if (j++ != 0) {
            dataString = dataString + ",";
          }
          dataString = dataString + "{\"text\": \"" + tableName + "\",\"qtip\":\"table\",\"leaf\":false,\"children\":[";
          for (int i = 0; i < metadata.getColumnCount(); i++) {
            if (i != 0) {
              dataString = dataString + ",";
            }
            dataString = dataString + "{\"leaf\":true,\"text\":\"" + metadata.getColumnLabel(i + 1) + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) + "\",\"precision\":\"" + metadata.getPrecision(i + 1) + "\",\"scale\":\"" + metadata.getScale(i + 1) + "\"}";
          } 
          
          dataString = dataString + "]}";
          rs.close();
        } catch (Exception ex) {
          logger.log(Level.SEVERE, "Exception while retrieving db info about table: {0} {1}", new Object[] { tableName, ex });
        } 
      } 
      mrs.close();
      mrs = conn.getMetaData().getTables(null, schema, null, new String[] { "VIEW" });
      while (mrs.next()) {
        String viewName = "";
        try {
          String tableName = mrs.getString(3);
          logger.log(Level.INFO, "SQL Getting metadata for view: " + viewName);
          ResultSet rs = st.executeQuery("select * from " + viewName);
          ResultSetMetaData metadata = rs.getMetaData();
          if (j++ != 0) {
            dataString = dataString + ",";
          }
          dataString = dataString + "{\"text\": \"" + viewName + "\",\"qtip\":\"view\",\"leaf\":false,\"children\":[";
          for (int i = 0; i < metadata.getColumnCount(); i++) {
            if (i != 0) {
              dataString = dataString + ",";
            }
            dataString = dataString + "{\"leaf\":true,\"text\":\"" + metadata.getColumnLabel(i + 1) + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) + "\",\"precision\":\"" + metadata.getPrecision(i + 1) + "\",\"scale\":\"" + metadata.getScale(i + 1) + "\"}";
          } 
          
          dataString = dataString + "]}";
          rs.close();
        } catch (Exception ex) {
          logger.log(Level.SEVERE, "Exception while retrieving db info about view: {0} {1}", new Object[] { viewName, ex });
        } 
      } 
      dataString = dataString + "]";
      return dataString;
    } catch (SQLException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db info about DB: {0} {1}", new Object[] { connString, ex });
      
      ConnectionCache.getInstance().freeConnection(jdbcDriver, connString, dbUser, des.decrypt(dbPass));
      dataString = "";
    } finally {
      if (mrs != null) {
        try {
          mrs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db record set of DB: {0} {1}", new Object[] { connString, sqlEx });
        } 
        mrs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DB: {0} {1}", new Object[] { connString, sqlEx });
        } 
        st = null;
      } 
    } 
    return dataString;
  }


  
  public static int executeSqlStatement(String pu, String sql, Logger logger) throws Exception {
    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory(pu);
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      Query q = em.createNativeQuery(sql);
      int result = q.executeUpdate();
      em.getTransaction().commit();
      return result;
    } catch (Exception ex) {
      em.getTransaction().rollback();
      logger.log(Level.SEVERE, "SQL Exception while executing statement {0} {1}", new Object[] { sql, ex });
      
      throw ex;
    } 
  }


  
  public static int executeSqlStatement(EntityManagerFactory emf, String sql, Logger logger) throws Exception {
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      Query q = em.createNativeQuery(sql);
      int result = q.executeUpdate();
      em.getTransaction().commit();
      return result;
    } catch (Exception ex) {
      em.getTransaction().rollback();
      logger.log(Level.SEVERE, "SQL Exception while executing statement {0} {1}", new Object[] { sql, ex });
      
      throw ex;
    } 
  }


  
  public static Query executeSqlQuery(String pu, String sql, Logger logger) throws Exception {
    EntityManagerFactory emf = PersistenceManager.getInstance().getEntityManagerFactory(pu);
    EntityManager em = emf.createEntityManager();
    try {
      return em.createNativeQuery(sql);
    }
    catch (Exception ex) {
      logger.log(Level.SEVERE, "SQL Exception while executing query {0} {1}", new Object[] { sql, ex });
      
      throw ex;
    } 
  }

  
  public static Query executeSqlQuery(EntityManagerFactory emf, String sql, Logger logger) throws Exception {
    EntityManager em = emf.createEntityManager();
    try {
      return em.createNativeQuery(sql);
    }
    catch (Exception ex) {
      logger.log(Level.SEVERE, "SQL Exception while executing query {0} {1}", new Object[] { sql, ex });
      
      throw ex;
    } 
  }




  
  public static String getTypeWithSize(String type, String size) {
    Arrays.sort(typeS);
    if (Arrays.binarySearch(typeS, type) >= 0) {
      return type + "(" + size + ")";
    }
    return type;
  }

  
  public static String printSQLException(SQLException ex) {
    StringBuilder buffer = new StringBuilder();
    for (Throwable e : ex) {
      if (e instanceof SQLException) {
        buffer.append("SQLState: ").append(((SQLException)e).getSQLState());
        buffer.append(" Error Code: ").append(((SQLException)e).getErrorCode());
        buffer.append(" Message: ").append(e.getMessage());
        Throwable t = ex.getCause();
        while (t != null) {
          buffer.append(" Cause: ").append(t);
          t = t.getCause();
        } 
      } 
    } 
    return buffer.toString();
  }
  
  public static String printPersistenceException(PersistenceException ex) {
    StringBuilder buffer = new StringBuilder();  
    buffer.append("Database message");
    Throwable t = ex.getCause();
    while (t != null) {
      buffer.append(" : ").append(t.getMessage());
      t = t.getCause();
    } 
    return buffer.toString();
  }

  
  public static String getDBObjectFields(String dbType, String connectionInfo, String dbUser, String dbPassword, String objectName) {
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    DesEncrypter des = new DesEncrypter(ConfigurationJpaController.dbPhrase);
    
    try {
      String jdbcDriver = "";
      String dataString = "[";
      String schema = null;
      
      if (dbType.equals("MySQL")) {
        jdbcDriver = "com.mysql.jdbc.Driver";
      }
      else if (dbType.equals("MS-SQL")) {
        jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        schema = "dbo";
      } else {
        throw new DatabaseNotSupportedException("Database type " + dbType + " is not supported.");
      } 

      
      conn = ConnectionCache.getInstance().getConnection(jdbcDriver, connectionInfo, dbUser, des.decrypt(dbPassword));
      st = conn.createStatement();
      int j = 0;
      rs = st.executeQuery("select * from " + objectName);
      ResultSetMetaData metadata = rs.getMetaData();
      for (int i = 0; i < metadata.getColumnCount(); i++) {
        if (i != 0) {
          dataString = dataString + ",";
        }
        logger.log(Level.INFO, "SQL Name: " + metadata.getColumnLabel(i + 1) + ", Type: " + metadata.getColumnTypeName(i + 1).toUpperCase());
        dataString = dataString + "{\"label\":\"" + metadata.getColumnLabel(i + 1) + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) + "\",\"precision\":\"" + metadata.getPrecision(i + 1) + "\",\"scale\":\"" + metadata.getScale(i + 1) + "\"}";
      } 
      
      dataString = dataString + "]";
      return dataString;
    } catch (SQLException ex) {
      logger.log(Level.SEVERE, "SQL Exception while retrieving db info about DBObject: {0} {1}", new Object[] { objectName, ex });
    } catch (DatabaseNotSupportedException ex) {
      logger.log(Level.SEVERE, "SQL Exception while retrieving db info about DBObject: {0} {1}", new Object[] { objectName, ex });
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        rs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        st = null;
      } 
    } 
    return null;
  }

  
  public static String getDBObjectData(Integer currentUserId, int start, int limit, String orderField, String direction, String objectName, String filter) {
    DatabaseMetaData dbMetaData = null;
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    String errorMessage = "";
    String sqlStmt = "";
    String schema = null;
    String driverName = null;
    
    try {
      conn = ConnectionPoolData.getInstance().getConnection();
      dbMetaData = conn.getMetaData();
      driverName = dbMetaData.getDriverName();
      
      String metaDataString = "[";
      String orderBy = "";
      
      if (!"".equals(orderField)) {
        orderBy = " order by " + orderField + " " + direction;
      }
      
      switch (driverName) {
        
        case "Microsoft JDBC Driver 4.0 for SQL Server":
          sqlStmt = "select * from  (select  ROW_NUMBER() OVER(order by id) as row, * from " + objectName + ") as tbl WHERE  row >= " + Integer.toString(start) + " AND row <= " + Integer.toString(limit);
          if (!"".equals(filter)) {
            sqlStmt = sqlStmt + " AND " + filter;
          }
          schema = "dbo";
          break;
        case "MySQL Connector Java":
          if (!"".equals(filter)) {
            sqlStmt = "select * from " + objectName + " where " + filter + " " + orderBy + " LIMIT " + Integer.toString(start) + ", " + Integer.toString(limit); break;
          } 
          sqlStmt = "select * from " + objectName + orderBy + " LIMIT " + Integer.toString(start) + ", " + Integer.toString(limit);
          break;
        
        default:
          throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
      } 
      
      logger.log(Level.SEVERE, "Executing for TableViewer: " + sqlStmt);
      st = conn.createStatement();
      rs = st.executeQuery(sqlStmt);
      
      ResultSetMetaData metadata = rs.getMetaData();
      for (int i = 0; i < metadata.getColumnCount(); i++) {
        if (i != 0) {
          metaDataString = metaDataString + ",";
        }
        logger.log(Level.INFO, "Name: " + metadata.getColumnLabel(i + 1) + ", Type: " + metadata.getColumnTypeName(i + 1).toUpperCase());
        metaDataString = metaDataString + "{\"label\":\"" + metadata.getColumnLabel(i + 1) + "\",\"type\":\"" + getTypeWithSize(metadata.getColumnTypeName(i + 1).toUpperCase(), Integer.toString(metadata.getPrecision(i + 1))) + "\",\"size\":\"" + metadata.getColumnDisplaySize(i + 1) + "\",\"precision\":\"" + metadata.getPrecision(i + 1) + "\",\"scale\":\"" + metadata.getScale(i + 1) + "\"}";
      } 
      
      metaDataString = metaDataString + "]";
      
      String dataString = "[";
      int j = 0;
      while (rs.next()) {
        if (j++ != 0) {
          dataString = dataString + ",";
        }
        dataString = dataString + "{";
        for (int i = 0; i < metadata.getColumnCount(); i++) {
          Timestamp ts; Date date; float f; double d; BigDecimal bd; long l; int integerBI, integerI, integerS, integerT, booleanBit; if (i != 0) {
            dataString = dataString + ",";
          }
          dataString = dataString + "\"" + metadata.getColumnLabel(i + 1) + "\":";
          
          switch (metadata.getColumnTypeName(i + 1).toUpperCase()) {
            
            case "VARCHAR":
            case "CHAR":
              dataString = dataString + "\"" + rs.getString(metadata.getColumnLabel(i + 1)) + "\"";
              break;
            case "BIT":
              booleanBit = rs.getInt(metadata.getColumnLabel(i + 1));
              if (rs.wasNull()) {
                dataString = dataString + "\"null\""; break;
              } 
              dataString = dataString + Integer.toString(booleanBit);
              break;
            
            case "TINYINT":
              integerT = rs.getInt(metadata.getColumnLabel(i + 1));
              dataString = dataString + Integer.toString(integerT);
              break;
            case "SMALLINT":
              integerS = rs.getInt(metadata.getColumnLabel(i + 1));
              dataString = dataString + Integer.toString(integerS);
              break;
            case "INT":
              integerI = rs.getInt(metadata.getColumnLabel(i + 1));
              dataString = dataString + Integer.toString(integerI);
              break;
            case "BIGINT":
              integerBI = rs.getInt(metadata.getColumnLabel(i + 1));
              dataString = dataString + Integer.toString(integerBI);
              break;
            case "LONG":
              l = rs.getLong(metadata.getColumnLabel(i + 1));
              dataString = dataString + Long.toString(l);
              break;
            case "DECIMAL":
              bd = rs.getBigDecimal(metadata.getColumnLabel(i + 1));
              if (bd == null) {
                dataString = dataString + "\"null\""; break;
              } 
              dataString = dataString + "\"" + bd.toString() + "\"";
              break;
            
            case "DOUBLE":
              d = rs.getDouble(metadata.getColumnLabel(i + 1));
              dataString = dataString + Double.toString(d);
              break;
            case "FLOAT":
              f = rs.getFloat(metadata.getColumnLabel(i + 1));
              dataString = dataString + Float.toString(f);
              break;
            case "DATE":
              date = rs.getDate(metadata.getColumnLabel(i + 1));
              if (date == null) {
                dataString = dataString + "\"null\""; break;
              } 
              dataString = dataString + "\"" + date.toString() + "\"";
              break;


            
            case "DATETIME":
              dataString = dataString + "\"" + rs.getString(metadata.getColumnLabel(i + 1)) + "\"";
              break;
            case "TIMESTAMP":
              ts = rs.getTimestamp(metadata.getColumnLabel(i + 1));
              if (ts == null) {
                dataString = dataString + "\"null\""; break;
              } 
              dataString = dataString + "\"" + ts.toString() + "\"";
              break;
          } 
        
        } 
        dataString = dataString + "}";
      } 
      dataString = dataString + "]";
      String returnString = "{\"success\":true,\"totalCount\":" + getObjectCount(conn, objectName, filter) + ",\"metaData\":" + metaDataString + ",\"content\":" + dataString + "}";
      
      return returnString;
    } catch (SQLException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db info and data of DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = ex.getMessage();
    } catch (DatabaseNotSupportedException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db info and data of DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = ex.getMessage();
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        rs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        st = null;
      } 
    } 
    return "{\"success\":false,\"status\":{\"message\":\"" + errorMessage + "\"},\"totalCount\":0,\"metaData\":[],\"content\":[]}";
  }

  
  public static String getDBObjectColumns(String objectName, String filter) {
    DatabaseMetaData dbMetaData = null;
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    String errorMessage = "";
    String sqlStmt = "";
    String schema = null;
    String driverName = null;
    
    try {
      conn = ConnectionPoolData.getInstance().getConnection();
      dbMetaData = conn.getMetaData();
      driverName = dbMetaData.getDriverName();
      
      String metaDataString = "";
      
      switch (driverName) {
        
        case "Microsoft JDBC Driver 4.0 for SQL Server":
          sqlStmt = "select * from " + objectName;
          if (!"".equals(filter) && filter != null) {
            sqlStmt = sqlStmt + " WHERE " + filter;
          }
          schema = "dbo";
          break;
        case "MySQL Connector Java":
          if (!"".equals(filter) && filter != null) {
            sqlStmt = "select * from " + objectName + " where " + filter; break;
          } 
          sqlStmt = "select * from " + objectName;
          break;
        
        default:
          throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
      } 
      
      st = conn.createStatement();
      rs = st.executeQuery(sqlStmt);
      
      ResultSetMetaData metadata = rs.getMetaData();
      for (int i = 0; i < metadata.getColumnCount(); i++) {
        if (i != 0) {
          metaDataString = metaDataString + ",";
        }
        
        metaDataString = metaDataString + metadata.getColumnLabel(i + 1);
      } 
      return metaDataString;
    } catch (SQLException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db column info of DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = ex.getMessage();
    } catch (DatabaseNotSupportedException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db column info of DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = ex.getMessage();
    } finally {
      if (rs != null) { 
        try {
          rs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        rs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        st = null;
      } 
    } 
    return errorMessage;
  }

  
  public static String getDBObjectColumnsJSON(String objectName, String filter) {
    DatabaseMetaData dbMetaData = null;
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    String errorMessage = "";
    String sqlStmt = "";
    String schema = null;
    String driverName = null;
    
    try {
      conn = ConnectionPoolData.getInstance().getConnection();
      dbMetaData = conn.getMetaData();
      driverName = dbMetaData.getDriverName();
      
      String metaDataString = "[";
      
      switch (driverName) {
        
        case "Microsoft JDBC Driver 4.0 for SQL Server":
          sqlStmt = "select * from " + objectName;
          if (!"".equals(filter) && filter != null) {
            sqlStmt = sqlStmt + " WHERE " + filter;
          }
          schema = "dbo";
          break;
        case "MySQL Connector Java":
          if (!"".equals(filter) && filter != null) {
            sqlStmt = "select * from " + objectName + " where " + filter; break;
          } 
          sqlStmt = "select * from " + objectName;
          break;
        
        default:
          throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
      } 
      
      st = conn.createStatement();
      rs = st.executeQuery(sqlStmt);
      
      ResultSetMetaData metadata = rs.getMetaData();
      for (int i = 0; i < metadata.getColumnCount(); i++) {
        if (i != 0) {
          metaDataString = metaDataString + ",";
        }
        
        metaDataString = metaDataString + "{\"name\":\"" + metadata.getColumnLabel(i + 1) + "\"}";
      } 
      metaDataString = metaDataString + "]";
      return metaDataString;
    } catch (SQLException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db column info of DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = ex.getMessage();
    } catch (DatabaseNotSupportedException ex) {
      logger.log(Level.SEVERE, "Exception while retrieving db column info of DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = ex.getMessage();
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        rs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        st = null;
      } 
    } 
    return errorMessage;
  }

  
  public static String getObjectCount(Connection conn, String objectName, String filter) throws SQLException {
    Statement st = null;
    ResultSet rs = null;
    
    try {
      st = conn.createStatement();
      if (!"".equals(filter)) {
        rs = st.executeQuery("select count(*) from " + objectName + " where " + filter);
      } else {
        rs = st.executeQuery("select count(*) from " + objectName);
      } 
      rs.next();
      int count = rs.getInt(1);
      return Integer.toString(count);
    } catch (SQLException ex) {
      Logger.getLogger(DBHelper.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        rs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of DBObject: {0} {1}", new Object[] { objectName, sqlEx });
        } 
        st = null;
      } 
    } 
    return "Error getting table size";
  }

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  /*//Old Method
  public static String getObjectDataIntoDoubleArray(String objectName, String columnString, String filter, String orderField, String direction, double[] data) throws Exception {
    DatabaseMetaData dbMetaData = null;
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    String errorMessage = "OK";
    String sqlStmt = "";
    String schema = null;
    String driverName = null;
    String dataString = "X";
    String colTypeName = "Y";
    try {
        System.out.println("objectName "+objectName);
      conn = ConnectionPoolData.getInstance().getConnection();
      dbMetaData = conn.getMetaData();
      driverName = dbMetaData.getDriverName();
      
      String metaDataString = "[";
      String orderBy = "";
      
      if (!"".equals(orderField)) {
        orderBy = " order by " + orderField + " " + direction;
      }
      
      switch (driverName) {
        
        case "Microsoft JDBC Driver 4.0 for SQL Server":
          sqlStmt = "select " + columnString + " from " + objectName;
          if (!"".equals(filter) && filter != null) {
            sqlStmt = sqlStmt + " WHERE " + filter;
          }
          schema = "dbo";
          break;
        case "MySQL Connector Java":
          if (!"".equals(filter) && filter != null) {
            sqlStmt = "select " + columnString + " from " + objectName + " where " + filter; break;
          } 
          sqlStmt = "select " + columnString + " from " + objectName;
          break;
        
        default:
          throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
      } 
      
      logger.log(Level.INFO, "Executing to populate double array for proxy: " + sqlStmt);
      st = conn.createStatement();
      rs = st.executeQuery(sqlStmt);
      System.out.println("AT DB helper results sql statement +++++++++++++++++++++++++++++++++");
      ResultSetMetaData metadata = rs.getMetaData();
      
      System.out.println("metadata "+metadata);
        try{
             logger.log(Level.INFO, "Generating UUID for file...");
             UUID uuid = UUID.randomUUID();
             String randomUUIDString = uuid.toString();
             System.out.println("Random UUID String = " + randomUUIDString);
             System.out.println("UUID version       = " + uuid.version());
             System.out.println("UUID variant       = " + uuid.variant());
             
             logger.log(Level.INFO, "Generated UUID"+randomUUIDString);
             logger.log(Level.INFO, "Generating file path...");
             
             String webRootpath4 = comPath.class.getResource("").toString();
             System.out.println("Getting class path\n"+webRootpath4);
                    
             String webRootpath5 = webRootpath4.substring(0, webRootpath4.indexOf("WEB-INF/classes/com/"));
             System.out.println("Getting class path webroot\n"+webRootpath5);
                    
             String webRootpath6 = webRootpath5.replace("file:/", "");
             System.out.println("Getting root removing file\n"+webRootpath6);
                    
             String CSV_FILE_PATH2  = webRootpath6+"resources/files/"+randomUUIDString+".csv";
             System.out.println("CSV_FILE_PATH2\n"+CSV_FILE_PATH2);
            
             logger.log(Level.INFO, "File path generated"+CSV_FILE_PATH2); 
            //create csv file
            try {
                   
                    File file1 = new File(CSV_FILE_PATH2); 
                     //Create the file
                    if (file1.createNewFile())
                    {
                        System.out.println("CSV File is created!");
                        logger.log(Level.INFO, "CSV file created."+CSV_FILE_PATH2);
                    } else {
                        System.out.println("File already exists.");
                        logger.log(Level.INFO, "File already exists."+CSV_FILE_PATH2);
                    }
                    
                    CSVWriter writer1 = new CSVWriter(new FileWriter(CSV_FILE_PATH2));
                    logger.log(Level.INFO, "Writing sql results to file");
                    writer1.writeAll(rs, true); 
                    writer1.close();
                    
                    InetAddress  ip = InetAddress.getLocalHost();
                    String hostname = ip.getHostName();
                    System.out.println("Your current IP address : " + ip);
                    System.out.println("Your current Hostname : " + hostname);
                    System.out.println("Only IP address : "+ InetAddress.getLocalHost().getHostAddress());
                    String fileUrl="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/files/"+randomUUIDString+".csv";
                    System.out.println("file URL address:"+fileUrl);
                    logger.log(Level.INFO, "File is available on url"+fileUrl);
                    
                  } catch (IOException e) {
                    System.out.println("An error occurred during creating file");
                    logger.log(Level.INFO, "An error occurred during creating file");
                    e.printStackTrace();
                  }
             
            }catch (Exception e) {
                System.out.println("UUID can not generate");
                logger.log(Level.INFO, "UUID can not generate");
                e.printStackTrace();
              }
      
      int j = 0;
      while (rs.next()) {
        for (int i = 0; i < metadata.getColumnCount(); i++) {
          long l; int integerI; data[j] = j;
          colTypeName = metadata.getColumnTypeName(i + 1);
          
          switch (colTypeName.toUpperCase()) {
            case "VARCHAR":
            case "CHAR":
              try {
                dataString = rs.getString(metadata.getColumnLabel(i + 1));
                if (dataString.charAt(0) == 'Y' || dataString.charAt(0) == 'y' || dataString
                  .charAt(0) == '1' || dataString.charAt(0) == 'T' || dataString
                  .charAt(0) == 't') {
                  data[j] = 1.0D; break;
                } 
                data[j] = 0.0D;
              }
              catch (Exception e) {
                data[j] = -1.0D;
              } 
              break;
            
            case "SMALLINT":
            case "INT":
            case "BIT":
              integerI = rs.getInt(metadata.getColumnLabel(i + 1));
              data[j] = integerI;
              break;
            case "LONG":
              l = rs.getLong(metadata.getColumnLabel(i + 1));
              data[j] = l;
              break;
            case "DECIMAL":
              try {
                BigDecimal bd = rs.getBigDecimal(metadata.getColumnLabel(i + 1));
                data[j] = bd.doubleValue();
              } catch (Exception e) {
                data[j] = -2.0D;
              } 
              break;
            
            case "DOUBLE":
              data[j] = rs.getDouble(metadata.getColumnLabel(i + 1));
              break;
            case "FLOAT":
              data[j] = rs.getFloat(metadata.getColumnLabel(i + 1));
              break;

            
            case "DATE":
              try {
                dataString = rs.getString(metadata.getColumnLabel(i + 1));
                dataString = Util.dateToDigitString(dataString);
                if (dataString == null) {
                  data[j] = 0.0D; break;
                } 
                data[j] = Double.parseDouble(dataString);
              }
              catch (Exception e) {
                data[j] = -3.0D;
              } 
              break;
            
            case "DATETIME":
              dataString = rs.getString(metadata.getColumnLabel(i + 1));
              if (dataString == null) {
                data[j] = 0.0D; break;
              } 
              data[j] = Double.parseDouble(Util.dateToDigitString(dataString));
              break;
            
            case "TIMESTAMP":
              try {
                Timestamp ts = rs.getTimestamp(metadata.getColumnLabel(i + 1));
                dataString = ts.toString();
                if (dataString == null) {
                  data[j] = 0.0D; break;
                } 
                data[j] = Double.parseDouble(dataString);
              }
              catch (Exception e) {
                data[j] = -4.0D;
              } 
              break;
          } 

          
          j++;
        } 
      } 
      return errorMessage;
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while formatting double array from DBObject: {0} {1}", new Object[] { objectName, ex });
      errorMessage = "Exception while formatting double array for " + dataString + " type " + colTypeName + " : " + ex.getMessage();
    } finally {
      try {
        if (rs != null) rs.close(); 
        if (st != null) st.close(); 
        conn.close();
        conn = null;
      } catch (SQLException ex) {
        logger.log(Level.SEVERE, "Exception when closing record set for double array extraction from DBObject: {0} {1}", new Object[] { objectName, ex });
        errorMessage = "Exception when closing record set for double array extraction from DBObject " + objectName + " : " + ex.getMessage();
      } 
    } 
    return errorMessage;
  }*/
  
  public static ResultSet executeJdbcQuery(Configuration conConf, String sqlStmt, Logger logger) throws SQLException, DatabaseNotSupportedException {
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    DesEncrypter des = new DesEncrypter(ConfigurationJpaController.dbPhrase);
    
    try {
      String jdbcDriver = "";
      String dataString = "[";
      String schema = null;
      
      if (conConf.getType().equals("MySQL")) {
        jdbcDriver = "com.mysql.jdbc.Driver";
      }
      else if (conConf.getType().equals("MS-SQL")) {
        jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        schema = "dbo";
      } else {
        throw new DatabaseNotSupportedException("Database type " + conConf.getType() + " is not supported.");
      } 
      
      conn = ConnectionCache.getInstance().getConnection(jdbcDriver, conConf.getConnectionInfo(), conConf.getDbUser(), des.decrypt(conConf.getDbPassword()));
      st = conn.createStatement();
      int j = 0;
      return st.executeQuery(sqlStmt);
    }
    catch (SQLException ex) {
      logger.log(Level.SEVERE, "SQL Exception while JDBC executing query: {0} {1}", new Object[] { sqlStmt, ex });
      throw ex;
    } catch (DatabaseNotSupportedException ex) {
      logger.log(Level.SEVERE, "SQL Exception while JDBC executing query: {0} {1}", new Object[] { sqlStmt, ex });
      throw ex;
    } finally {}
  }








  
  public static boolean executeJdbcStatement(Configuration conConf, String sqlStmt, Logger logger) throws SQLException {
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    DesEncrypter des = new DesEncrypter(ConfigurationJpaController.dbPhrase);
    try {
      String jdbcDriver = "";
      String dataString = "[";
      String schema = null;
      
      if (conConf.getType().equals("MySQL")) {
        jdbcDriver = "com.mysql.jdbc.Driver";
      }
      else if (conConf.getType().equals("MS-SQL")) {
        jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        schema = "dbo";
      } else {
        throw new DatabaseNotSupportedException("Database type " + conConf.getType() + " is not supported.");
      } 
      
      conn = ConnectionCache.getInstance().getConnection(jdbcDriver, conConf.getConnectionInfo(), conConf.getDbUser(), des.decrypt(conConf.getDbPassword()));
      st = conn.createStatement();
      return st.execute(sqlStmt);
    } catch (SQLException ex) {
      logger.log(Level.SEVERE, "SQL Exception while JDBC executing statement: {0} {1}", new Object[] { sqlStmt, ex });
      throw ex;
    } catch (DatabaseNotSupportedException ex) {
      logger.log(Level.SEVERE, "SQL Exception while JDBC executing statement: {0} {1}", new Object[] { sqlStmt, ex });
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of statement: {0} {1}", new Object[] { sqlStmt, sqlEx });
        } 
        rs = null;
      } 
      
      if (st != null) {
        try {
          st.close();
        } catch (SQLException sqlEx) {
          logger.log(Level.SEVERE, "SQL Exception while closing db connection of statement: {0} {1}", new Object[] { sqlStmt, sqlEx });
        } 
        st = null;
      } 
    } 
    return true;
  }
  
  
  //----------------------------new method by Neha
   public static String getObjectDataIntoDoubleArray(String fileId, Integer processId,String objectName, String columnString, String filter, String orderField, String direction, double[] data,int numberOfColumns) throws Exception {
    DatabaseMetaData dbMetaData = null;
    Connection conn = null;
    Statement st = null;
    ResultSet rs = null;
    String errorMessage = "OK";
    String sqlStmt = "";
    String schema = null;
    String driverName = null;
    String dataString = "X";
    String colTypeName = "Y";
    String fileUrl=null;
    String newLine = System.getProperty("line.separator");
    String[] columnHeaders = columnString.split(",");
    String colSeparator =",";
    
    
    try {
       logger.log(Level.INFO, "---------------AT NEW METHOD------------------------getObjectDataIntoDoubleArray");
      conn = ConnectionPoolData.getInstance().getConnection();
      dbMetaData = conn.getMetaData();
      driverName = dbMetaData.getDriverName();
      String metaDataString = "[";
      String orderBy = "";
      String webRootpath4,webRootpath5;
      String webRootpath6 ="";
      
      
      if (!"".equals(orderField)) {
        orderBy = " order by " + orderField + " " + direction;
      }
      
      switch (driverName) {
        
        case "Microsoft JDBC Driver 4.0 for SQL Server":
          sqlStmt = "select " + columnString + " from " + objectName;
          if (!"".equals(filter) && filter != null) {
            sqlStmt = sqlStmt + " WHERE " + filter;
          }
          schema = "dbo";
          break;
        case "MySQL Connector Java":
          if (!"".equals(filter) && filter != null) {
            sqlStmt = "select " + columnString + " from " + objectName + " where " + filter; break;
          } 
          sqlStmt = "select " + columnString + " from " + objectName;
          break;
        
        default:
          throw new DatabaseNotSupportedException("Database driver " + driverName + " is not supported.");
      } 
      
      logger.log(Level.INFO, "Executing to populate double array for proxy: {0}", sqlStmt);
      st = conn.createStatement();
      rs = st.executeQuery(sqlStmt);
      ResultSetMetaData metadata = rs.getMetaData();
            logger.log(Level.INFO, "Generating file path...");
            
            webRootpath4 = comPath.class.getResource("").toString();             
            logger.log(Level.INFO, "Getting class path...{0}",webRootpath4);             
            webRootpath5 = webRootpath4.substring(0, webRootpath4.indexOf("WEB-INF/classes/com/"));
            logger.log(Level.INFO, "Getting class path...{0}",webRootpath5);
            
            //System.out.println("Getting class path webroot\n"+webRootpath5);
            //String webRootpath6 = webRootpath5.replace("file:/", "");
            //System.out.println("Getting root removing file\n"+webRootpath6);
            //String webRootpath6 = webRootpath5.replace("file:", "");
            
            String os  = System.getProperty("os.name").toLowerCase();
            logger.log(Level.INFO, "Analysing type of Operating system...{0}",os);
            
                if (os.contains("win")) {
                   webRootpath6 = webRootpath5.replace("file:/", "");
                   logger.log(Level.INFO, "This is win", webRootpath6); 
                   logger.log(Level.INFO, "File path generated after replacing file: {0}", webRootpath6); 
                }
                else if (os.contains("nix") || os.contains("nux") || os.contains("aix")) { 
                   webRootpath6 = webRootpath5.replace("file:", "");
                   logger.log(Level.INFO, "This is nix", webRootpath6); 
                   logger.log(Level.INFO, "File path generated after replacing file: {0}", webRootpath6); 
                }

                else if (os.contains("mac")) {
                   webRootpath6 = webRootpath5.replace("file:/", "");
                   logger.log(Level.INFO, "This is mac", webRootpath6); 
                   logger.log(Level.INFO, "File path generated after replacing file: {0}", webRootpath6); 
                }
                
                else if (os.contains("sunos")) {
                  webRootpath6 = webRootpath5.replace("file:/", "");
                  logger.log(Level.INFO, "This is sunos", webRootpath6); 
                  logger.log(Level.INFO, "File path generated after replacing file: {0}", webRootpath6); 
                }
             
            String CSV_FILE_PATH2  = webRootpath6+"resources/"+fileId+".csv";
            
            
            logger.log(Level.INFO, "File path generated CSV_FILE_PATH2: {0}", CSV_FILE_PATH2); 
            //create csv file
                    File file1 = new File(CSV_FILE_PATH2); 
                     //Create the file
                    if (file1.createNewFile())
                    {
                        logger.log(Level.INFO, "CSV file created.{0}", CSV_FILE_PATH2);
                    } else {
                        logger.log(Level.INFO, "File already exists.{0}", CSV_FILE_PATH2);
                    }
                    FileWriter fwriter = new FileWriter(file1);
                    logger.log(Level.INFO, "Writing sql results to file");
                    StringBuilder sb = new StringBuilder();
                    fwriter.write(columnString);   
                    sb.append('\n');
                    int j = 0;
                    while (rs.next()) 
                    {
                       for (int i = 0; i < metadata.getColumnCount(); i++) {
                        data[j] = j;
                          colTypeName = metadata.getColumnTypeName(i + 1);                        
                          sb.append(rs.getString(metadata.getColumnLabel(i + 1)));
                          sb.append(",");
                        j++;  
                      } 
                      sb.deleteCharAt(sb.lastIndexOf(","));
                      sb.append('\n');
                    }
              
        fwriter.write(sb.toString());
        logger.log(Level.INFO, "Done writing CSV file created.{0}", CSV_FILE_PATH2);
        fwriter.close();
      return errorMessage;
      
    } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while formatting double array from DBObject: {0} {1}", new Object[] { objectName, ex });
            errorMessage = "Exception while formatting double array for " + dataString + " type " + colTypeName + " : " + ex.getMessage();
    } finally {
            try {
              if (rs != null) rs.close(); 
              if (st != null) st.close(); 
              conn.close();
              conn = null;
            } catch (SQLException ex) {
              logger.log(Level.SEVERE, "Exception when closing record set for double array extraction from DBObject: {0} {1}", new Object[] { objectName, ex });
              errorMessage = "Exception when closing record set for double array extraction from DBObject " + objectName + " : " + ex.getMessage();
            } 
    } 
        return errorMessage;
  }
}
