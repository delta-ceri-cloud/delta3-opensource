/*
 * (C) 2017 CERI-Lahey
 * 
 */
package com.ceri.delta.jpa.study;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.study.Stat;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.study.exceptions.NonexistentEntityException;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.security.exceptions.DeltaOperationNotAllowedException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author mxm29
 */
public class StatJpaController implements PersistenceHelper, Serializable {
    
    @SuppressWarnings("empty-statement")
    public StatJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Stat stat) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(stat);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Stat edit(Stat stat) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            stat = em.merge(stat);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = stat.getIdStat();
                if (findStat(id) == null) {
                    throw new NonexistentEntityException("The stat with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return stat;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Stat stat;
            try {
                stat = em.getReference(Stat.class, id);
                stat.getIdStat();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stat with id " + id + " no longer exists.", enfe);
            }
            em.remove(stat);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findStatEntities() {
        return findStatEntities(true, -1, -1);
    }

    public List<Object> findStatEntities(int maxResults, int firstResult) {
        return findStatEntities(false, maxResults, firstResult);
    }

    private List<Object> findStatEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Stat as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    //-----------------------------------------------------------------------------------------------------------------
    public List<Object> findOrgStatEntities(Integer orgId) {
        return findOrgStatEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgStatEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgStatEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgStatEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Stat as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Stat findStat(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Stat.class, id);
        } finally {
            em.close();
        }
    }

    public int getStatCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Stat as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getStatCountByOrg(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Stat as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }  
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.ceri.delta.entity.study.Stat)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.ceri.delta.entity.study.Stat)object).setCreatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Stat)object).setCreatedBy(currentUserId);        
        ((com.ceri.delta.entity.study.Stat)object).setUpdatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Stat)object).setUpdatedBy(currentUserId);      
        com.ceri.delta.entity.study.Stat obj = edit(((com.ceri.delta.entity.study.Stat)object));
        AuditLogger.log(currentUserId, obj.getIdStat(), "create", obj);
        return obj; 
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.ceri.delta.entity.study.Stat)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((com.ceri.delta.entity.study.Stat)object).setUpdatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Stat)object).setUpdatedBy(currentUserId);      
        com.ceri.delta.entity.study.Stat obj = edit(((com.ceri.delta.entity.study.Stat)object));
        AuditLogger.log(currentUserId, obj.getIdStat(), "update", obj);
        return obj; 
    }

    @Override
    public String getObjectType() {
        return "stats";
    }

    @Override
    public Integer getObjectId(Object subject) {
        return ((Stat)subject).getIdStat();
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findStatEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgStatEntities(maxResults, firstResult, currentOrgId);    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer currentOrgId, Integer idGroup, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object findObjectEntityById(Integer objectId) {
        return findStat(objectId);
    }

    @Override
    public int getObjectCount() {
        return getStatCount();
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountByOrg(Integer currentOrgId) {
        return getStatCountByOrg(currentOrgId);
    }

    @Override
    public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        if ( !Objects.equals(((Stat)subject).getIdOrganization(), currentOrgId) ) {
            throw new DeltaOperationNotAllowedException("Operation not authorized.");             
        }
        AuditLogger.log(currentUserId, ((Stat)subject).getIdStat(), "delete", subject);                   
        destroy(((Stat)subject).getIdStat());
    }
    
}
