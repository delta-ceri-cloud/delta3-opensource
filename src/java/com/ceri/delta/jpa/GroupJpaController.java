/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.GroupTable;
import com.ceri.delta.entity.UserHasRole;
import com.ceri.delta.entity.User;
import com.ceri.delta.entity.UserHasRolePK;
import com.ceri.delta.jpa.UserHasProjectRoleJpaController;
import com.ceri.delta.jpa.UserHasRoleJpaController;
import com.ceri.delta.jpa.UserJpaController;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.server.exceptions.EntityExistsException;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONObject;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class GroupJpaController implements PersistenceHelper, Serializable {

    private EntityManagerFactory emf = null;
    private static UserHasRoleJpaController userhasroleController;
    private static UserJpaController userController;
     private static UserHasProjectRoleJpaController userhasprojectroleController;
              
    public GroupJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("CeriPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(GroupTable groupTable) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(groupTable);
            
            em.getTransaction().commit();
        }
        
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public GroupTable edit(GroupTable groupTable) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            groupTable = em.merge(groupTable);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = groupTable.getIdGroup();
                if (findGroupTable(id) == null) {
                    throw new NonexistentEntityException("The groupTable with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return groupTable;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GroupTable groupTable;
            try {
                groupTable = em.getReference(GroupTable.class, id);
                groupTable.getIdGroup();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The groupTable with id " + id + " no longer exists.", enfe);
            }
            em.remove(groupTable);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public List<Object> findOrgGroupTableEntities(Integer orgId) {
        return findOrgGroupTableEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgGroupTableEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgGroupTableEntities(false, maxResults, firstResult, orgId);
    }
    
    private List<Object> findOrgGroupTableEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from GroupTable as o where organizationId = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
    
    // this is specialized set of methods for Group objects
    public List<Object> findOrgGroupTableEntitiesByType(Integer orgId, String type) {
        return findOrgGroupTableEntitiesByType(true, -1, -1, type, orgId);
    }

    public List<Object> findOrgGroupTableEntitiesByType(int maxResults, int firstResult, String type, Integer orgId) {
        return findOrgGroupTableEntitiesByType(false, maxResults, firstResult, type, orgId);
    }
    
    private List<Object> findOrgGroupTableEntitiesByType(boolean all, int maxResults, int firstResult, String type, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from GroupTable as o where organizationId = " 
                    + orgId.toString() + " type = " + type);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }      
    
    public List<Object> findGroupTableEntities() {
        return findGroupTableEntities(true, -1, -1);
    }

    public List<Object> findGroupTableEntities(int maxResults, int firstResult) {
        return findGroupTableEntities(false, maxResults, firstResult);
    }

    private List<Object> findGroupTableEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from GroupTable as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public GroupTable findGroupTable(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(GroupTable.class, id);
        } finally {
            em.close();
        }
    }

    public int getGroupTableCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from GroupTable as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getOrgGroupTableCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from GroupTable as o where organizationId = " + orgId.toString()).getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }   
    
    @Override
    public GroupTable createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((GroupTable)object).setActive(Boolean.TRUE);                   // activate immediately        
        ((GroupTable)object).setCreatedTS(currentTimestamp);
        ((GroupTable)object).setCreatedBy(currentUserId);        
        ((GroupTable)object).setUpdatedTS(currentTimestamp);
        ((GroupTable)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((GroupTable)object).getIdGroup(), "create", object);
        
        System.out.println("This is at create Object for GroupJPA controller");
       
        System.out.println("This is at create Object for GroupJPA object"+object.toString());
        System.out.println("This is at create Object for GroupJPA currentUserId"+currentUserId);
       
        
        
        
        
        
        
        return edit(((GroupTable)object));            
    }

    @Override
    public GroupTable updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((GroupTable)object).setUpdatedTS(currentTimestamp);
        ((GroupTable)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((GroupTable)object).getIdGroup(), "update", object);        
        return edit(((GroupTable)object));          
      }

    @Override
    public GroupTable createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        
        ((GroupTable)object).setOrganizationId(currentOrgId);
        ((GroupTable)object).setActive(Boolean.TRUE);                   // activate immediately               
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((GroupTable)object).setCreatedTS(currentTimestamp);
        ((GroupTable)object).setCreatedBy(currentUserId);        
        ((GroupTable)object).setUpdatedTS(currentTimestamp);
        ((GroupTable)object).setUpdatedBy(currentUserId);        
        AuditLogger.log(currentUserId, ((GroupTable)object).getIdGroup(), "create", object);
        return edit(((GroupTable)object));            
    }

    
    //@Override
    public String projectALL(String JSONString, Integer currentUserId, Integer currentOrgId) throws NonexistentEntityException, Exception {
       JSONObject responseJSON = null;
        
         responseJSON = new JSONObject(JSONString);
                if ( responseJSON.getBoolean("success") == true ) {
                    JSONArray groupArray = responseJSON.getJSONArray("groups");
                    JSONObject group = (JSONObject)groupArray.get(0);
                    Integer newlyidgroup = group.getInt("idGroup");   
                    Integer reorganizationId = group.getInt("organizationId");
                  
            //---------below is list of all users who have idGroup 0
            System.out.println("==============This is at USERS=========");
            if (userController == null) {
                userController = new UserJpaController();
            }
            List<User> userlist = userController.findUserForall(currentOrgId);
           
            for(int i=0;i<userlist.size();i++){
                System.out.println("userlist userid:"+userlist.get(i).getIdUser());
                System.out.println("==============This is at USERSHASROLE=========");
                if (userhasroleController == null) {
                    userhasroleController = new UserHasRoleJpaController();
                }
                
                List<UserHasRole> ghe = userhasroleController.findGroupHasEntityUserForall(userlist.get(i).getIdUser(), currentOrgId);
                //check if userhasroll null
                if(ghe.isEmpty()!=true){           
                System.out.println("This is ghe size empty"+ghe.isEmpty());

                for(int j=0;j<ghe.size();j++){
                    System.out.println("-----------------------------------------------------");
                    System.out.println("ghe"+ghe.get(j).getUserHasRolePK().getUseridUser());
                    System.out.println("ghe"+ghe.get(j).getUserHasRolePK().getRoleidRole());
                    System.out.println("ghe"+ghe.get(j).getUserHasRolePK().getIdGroup());
                    System.out.println("-----------------------------------------------------");
                    
                    System.out.println("Now assigning project ..................");
                    
                    if (userhasprojectroleController == null){
                    userhasprojectroleController = new UserHasProjectRoleJpaController(); 
                    }
                    
                    UserHasRolePK uhrPK = new UserHasRolePK(ghe.get(j).getUserHasRolePK().getUseridUser(),ghe.get(j).getUserHasRolePK().getRoleidRole(), newlyidgroup);
                    UserHasRole uhr = new UserHasRole(uhrPK);
                    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
                    uhr.setCreatedTS(currentTimestamp);
                    uhr.setUpdatedTS(currentTimestamp);
                    uhr.setCreatedBy(currentUserId);
                    uhr.setUpdatedBy(currentUserId);
                    uhr = userhasprojectroleController.edit(uhr);
                    System.out.println("This is UHR"+uhr);
               
                    }
                }
                else{
                    System.out.println("userlist userid does not have 0 project assigned:"+userlist.get(i).getIdUser());
                }
                
            }
            }
           
        return "ok";            
    }

    
    
    
    
    
    @Override
    public GroupTable updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((GroupTable)object).setUpdatedTS(currentTimestamp);
        ((GroupTable)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((GroupTable)object).getIdGroup(), "update", object);        
        return edit(((GroupTable)object));          
      }
    
    @Override
    public String getObjectType() {
        return "groups";
     }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findGroupTableEntities(maxResults, firstResult);
    }
    
    @Override
    public List<Object> findObjectEntities() {
        return findGroupTableEntities();
    }
    
    @Override
    public int getObjectCount() {
        return getGroupTableCount();
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgGroupTableEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        return findOrgGroupTableEntities(currentOrgId);
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override    
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((GroupTable)subject).getIdGroup(), "delete", subject);
        destroy(((GroupTable)subject).getIdGroup());
    }
    
    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((GroupTable)subject).getIdGroup(), "delete", subject);
        destroy(((GroupTable)subject).getIdGroup());
    }    

    @Override
    public int getObjectCount(int currentUserId) {
        return getGroupTableCount();        
    }

    @Override
    public int getObjectCountByOrg(Integer currentOrgId) {
        return getOrgGroupTableCount(currentOrgId);
    }

    @Override
    public Object findObjectEntityById(Integer objectId) {
        return findGroupTable(objectId);
    }

    @Override
    public Integer getObjectId(Object subject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer currentOrgId, Integer idGroup, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
