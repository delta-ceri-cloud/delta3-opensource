/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.UserHasRole;
import com.ceri.delta.entity.UserHasRolePK;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class UserHasRoleJpaController implements Serializable {
    private EntityManagerFactory emf = null;
    
    public UserHasRoleJpaController() {
        emf = PersistenceManager.getInstance().getEntityManagerFactory("CeriPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserHasRole userHasRole) throws PreexistingEntityException, Exception {
        if (userHasRole.getUserHasRolePK() == null) {
            userHasRole.setUserHasRolePK(new UserHasRolePK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(userHasRole);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserHasRole(userHasRole.getUserHasRolePK()) != null) {
                throw new PreexistingEntityException("UserHasRole " + userHasRole + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public UserHasRole edit(UserHasRole userHasRole) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            userHasRole = em.merge(userHasRole);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserHasRolePK id = userHasRole.getUserHasRolePK();
                if (findUserHasRole(id) == null) {
                    throw new NonexistentEntityException("The userHasRole with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return userHasRole;
    }

    public void destroy(UserHasRolePK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserHasRole userHasRole;
            try {
                userHasRole = em.getReference(UserHasRole.class, id);
                userHasRole.getUserHasRolePK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userHasRole with id " + id + " no longer exists.", enfe);
            }
            em.remove(userHasRole);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserHasRole> findUserHasRoleEntities() {
        return findUserHasRoleEntities(true, -1, -1);
    }

    public List<UserHasRole> findUserHasRoleEntities(int maxResults, int firstResult) {
        return findUserHasRoleEntities(false, maxResults, firstResult);
    }

    private List<UserHasRole> findUserHasRoleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserHasRole as o");
            System.out.println("findUserHasRoleEntities USER HAS JPA maxResults" +maxResults);
            System.out.println("findUserHasRoleEntities USER HAS JPA all" +all);
            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<UserHasRole> findUserHasRoleByUserIdEntities(int useridUser) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("UserHasRole.findByUseridUser");
            q.setParameter("useridUser", useridUser);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public UserHasRole findUserHasRole(UserHasRolePK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserHasRole.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserHasRoleCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from UserHasRole as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<UserHasRole> findGroupHasEntityUserForall(Integer userid, Integer orgid) {
        EntityManager em = getEntityManager();
        try {
            
            System.out.println("userid"+userid);
            System.out.println("orgid"+orgid);
            Query q = em.createQuery("SELECT u FROM UserHasRole u WHERE u.userHasRolePK.useridUser ="+userid+" and u.userHasRolePK.idGroup = 0");
            System.out.println("This is at EntityManager USER HAS ROLE");
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    
    
    
    
    
    
}
