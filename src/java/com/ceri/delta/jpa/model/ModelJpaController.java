/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa.model;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.exceptions.PreexistingEntityException;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.security.exceptions.DeltaOperationNotAllowedException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Coping Systems Inc.
 */
public class ModelJpaController implements PersistenceHelper, Serializable  {
    //Added for project filter
    private static GroupHasEntityJpaController grouphasentityController;
    
    public ModelJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Model model) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(model);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findModel(model.getIdModel()) != null) {
                throw new PreexistingEntityException("Model " + model + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Model edit(Model model) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            model = em.merge(model);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = model.getIdModel();
                if (findModel(id) == null) {
                    throw new NonexistentEntityException("The model with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return model;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Model model;
            try {
                model = em.getReference(Model.class, id);
                model.getIdModel();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The model with id " + id + " no longer exists.", enfe);
            }
            em.remove(model);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findModelEntities() {
        return findModelEntities(true, -1, -1);
    }

    public List<Object> findModelEntities(int maxResults, int firstResult) {
        return findModelEntities(false, maxResults, firstResult);
    }

    private List<Object> findModelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Model as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findOrgModelEntities(Integer orgId) {
        return findOrgModelEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgModelEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgModelEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgModelEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Model as o where organization = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
 
    public List<Object> findModelEntitiesByOrgGroup(Integer orgId, Integer groupId) {
        return findModelEntitiesByOrgGroup(true, -1, -1, orgId, groupId);
    }

    public List<Object> findModelEntitiesByOrgGroup(int maxResults, int firstResult, Integer orgId, Integer groupId) {
        return findModelEntitiesByOrgGroup(false, maxResults, firstResult, orgId, groupId);
    }

    private List<Object> findModelEntitiesByOrgGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
            
           /*
           System.out.println("This is at query find findModelEntitiesByOrgGroup");
           System.out.println("all"+all);
           System.out.println("maxResults"+maxResults);
           System.out.println("orgId"+orgId);
           System.out.println("groupId"+groupId);
           System.out.println("firstResult"+firstResult);
          
          if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
          String mymodel="models";
          List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
          if (ghe.isEmpty()) {
            return null;
          }
          for(int i=0;i<ghe.size();i++){
              System.out.println("ghe"+ghe.get(i));
          }
          
          List<Integer> mylist = new ArrayList<Integer>();

          for (GroupHasEntity t : ghe) {
            int i = t.getGroupHasEntityPK().getEntityidEntity();
            mylist.add(i);            
          }
          
           System.out.println("mylist"+mylist);
           System.out.println("mylist string"+mylist.toString());
           String mylist1= mylist.toString().replace("[", "(");
           String mylist2= mylist1.replace("]", ")");
           System.out.println("mylist after replace"+mylist2); 
          
            Query q = em.createQuery
            ("select object(o) from Model as o where organization = " + orgId.toString() + 
                " and o.idModel in " 
                + mylist2); 

            */
                      
           Query q = em.createQuery("select object(o) from Model as o where organization = " 
                   + orgId.toString() + " and o.idModel in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'models' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")"); 
           
           
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Model findModel(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Model.class, id);
        } finally {
            em.close();
        }
    }

    public List<Model> findModelByName(String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Model.findByName");
            q.setParameter("name", name);
            return q.getResultList();            
        } finally {
            em.close();
        }
    }

    public List<Model> findModelById(Integer id) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Model.findByIdModel");
            q.setParameter("idModel", id);
            return q.getResultList();            
        } finally {
            em.close();
        }
    }
    
    public int getModelCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Model as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getModelCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Model as o where organization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    
 
    public int getModelCountExtendedOrg(Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Model as o where organization = " 
                    + orgId.toString() + whereClause);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getModelCountByOrgGroup(Integer orgId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
            
            /*
            System.out.println("==============This is at getModelCountByOrgGroup=========");
            if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
            
            String mymodel="models";
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
            for(int i=0;i<ghe.size();i++){
                System.out.println("ghe"+ghe.get(i));
            }
            List<Integer> mylist = new ArrayList<Integer>();
            for (GroupHasEntity t : ghe) {
              int i = t.getGroupHasEntityPK().getEntityidEntity();
              mylist.add(i);            
            }

            System.out.println("mylist"+mylist);
            System.out.println("mylist string"+mylist.toString());
            String mylist1= mylist.toString().replace("[", "(");
            String mylist2= mylist1.replace("]", ")");
            System.out.println("mylist after replace"+mylist2);

            
            
            Query q = em.createQuery("select count(o) from Model as o where organization = " + orgId.toString() 
                    + " and o.idModel in " 
                +  mylist2); */
            
            
            Query q = em.createQuery("select count(o) from Model as o where organization = " + orgId.toString() + " and o.idModel in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'models' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")");
            
            
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    
    
    
    public int getModelCountExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            
            Query q ;
            
            System.out.println("==============This is at getModelCountExtendedOrgGroup=========");
            if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
            
            String mymodel="models";
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
            for(int i=0;i<ghe.size();i++){
                System.out.println("ghe"+ghe.get(i));
            }
            
            
            
            List<Integer> mylist = new ArrayList<Integer>();
            for (GroupHasEntity t : ghe) {
              int i = t.getGroupHasEntityPK().getEntityidEntity();
              mylist.add(i);            
            }

            System.out.println("mylist"+mylist);
            System.out.println("mylist string"+mylist.toString());
            String mylist1= mylist.toString().replace("[", "(");
            String mylist2= mylist1.replace("]", ")");
            System.out.println("mylist after replace"+mylist2);

            if(!mylist.isEmpty()){
            
            q = em.createQuery
             ("select count(o) from Model as o where organization = " + orgId.toString() + 
                 " and o.idModel in " 
                 + mylist2 + whereClause); 
            }
            
            else{
                return 0;
            }
            
            
            
            
           /* //----previous code------------
            Query q = em.createQuery("select count(o) from Model as o where organization = " + orgId.toString() + " and o.idModel in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'models' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")" + whereClause); 
            */
            
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }        
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Model)object).setCreatedTS(currentTimestamp);
        ((Model)object).setCreatedBy(currentUserId);        
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);        
        Model obj = edit(((Model)object));
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "create", obj);       
        return obj;  
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);   
        Model obj = edit(((Model)object));        
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "update", obj);
        return obj;     
    }
    
    
    
    
    public String updateProjectAssigned(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        System.out.println("This is update project Assigned at model JPA controller");
        System.out.println("This is update project Assigned object"+object);
        EntityManager em = getEntityManager();
        int result;
 
        if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
         }
        
        
         int ghe = grouphasentityController.getGroupHasEntityCount();
         
//                    getGroupHasEntityCountbyModel();
            System.out.println("This is result back from ghe"+ghe);
         
            return "Ok";     
    }
    
    
    
    

    @Override
    public String getObjectType() {
        return "models";
    }

    public Integer getObjectId(Object subject) {
        return ((Model)subject).getIdModel();
    }
    
    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findModelEntities(maxResults, firstResult);
    }

    @Override
    public List<Object> findObjectEntities() {
         return findModelEntities();
    }

    @Override
    public int getObjectCount() {
        return getModelCount();
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Model)object).setOrganization(currentOrgId);
        ((Model)object).setCreatedTS(currentTimestamp);
        ((Model)object).setCreatedBy(currentUserId);        
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);        
        Model obj = edit(((Model)object));
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "create", obj);       
        return obj;  
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ((Model)object).setOrganization(currentOrgId);    
        ((Model)object).setUpdatedTS(currentTimestamp);
        ((Model)object).setUpdatedBy(currentUserId);   
        Model obj = edit(((Model)object));        
        AuditLogger.log(currentUserId, ((Model)obj).getIdModel(), "update", obj);      
        return obj;
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgModelEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
         return findOrgModelEntities(currentOrgId);
    }
    
    public Object findObjectEntityById(Integer objectId) {
         return findModelById(objectId).get(0);
    }    

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountByOrg(Integer currentOrgId) {
        return getModelCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.security.exceptions.DeltaOperationNotAllowedException, NonexistentEntityException {
        if ( !Objects.equals(((Model)subject).getOrganization(), currentOrgId) ) {
            throw new DeltaOperationNotAllowedException("Operation not authorized.");             
        }
        AuditLogger.log(currentUserId, ((Model)subject).getIdModel(), "delete", subject);                   
        destroy(((Model)subject).getIdModel());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup) {
        return findModelEntitiesByOrgGroup(maxResults, firstResult, currentOrgId, idGroup);
    }

    @Override
    public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) {
        return getModelCountByOrgGroup(currentOrgId, groupId);
    }   

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override    
    public List<Object> findObjectEntitiesExtendedOrg(Integer orgId, String whereClause) {
        return findObjectEntitiesExtendedOrg(true, -1, -1, orgId, whereClause);
    }
    
    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer orgId, String whereClause) {
        return findObjectEntitiesExtendedOrg(false, maxResults, firstResult, orgId, whereClause);
    }
    
    private List<Object> findObjectEntitiesExtendedOrg(boolean all, int maxResults, int firstResult, Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Model as o where organization = " 
                    + orgId.toString() + whereClause);            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        return findObjectEntitiesExtendedOrgGroup(true, -1, -1, orgId, groupId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        return findObjectEntitiesExtendedOrgGroup(false, maxResults, firstResult, orgId, groupId, whereClause);
    }

    private List<Object> findObjectEntitiesExtendedOrgGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
           Query q;  
           
           System.out.println("This is at query find findObjectEntitiesExtendedOrgGroup");
           System.out.println("all"+all);
           System.out.println("maxResults"+maxResults);
           System.out.println("orgId"+orgId);
           System.out.println("groupId"+groupId);
           System.out.println("firstResult"+firstResult);
           System.out.println("whereClause"+whereClause);
          
          if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
          String mymodel="models";
          List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
          
          
          if (ghe.isEmpty()) {
            System.out.println("This is at returning null");
            return new ArrayList<Object>();
            //return null;
          }
          
          
          for(int i=0;i<ghe.size();i++){
              System.out.println("ghe"+ghe.get(i));
          }
          
          List<Integer> mylist = new ArrayList<Integer>();
          for(GroupHasEntity t : ghe) {
            int i = t.getGroupHasEntityPK().getEntityidEntity();
            mylist.add(i);            
          }
           System.out.println("mylist"+mylist);
           System.out.println("mylist string"+mylist.toString());
          
           String mylist1= mylist.toString().replace("[", "(");
           String mylist2= mylist1.replace("]", ")");
           System.out.println("mylist after replace"+mylist2); 
          
            
           
           if(!mylist.isEmpty()){
            q = em.createQuery
            ("select object(o) from Model as o where organization = " + orgId.toString() + 
                " and o.idModel in " 
                + mylist2 + whereClause);   
            }
           else{
               q = em.createQuery("select object(o) from Model as o where organization = " + orgId.toString() + 
                " and o.idModel in " 
                + mylist2);
           }
           
           
           
           /* //previous code 
           Query q = em.createQuery("select object(o) from Model as o where organization = " + orgId.toString() + " and o.idModel in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'models' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")" + whereClause);   */
           
           /*Query q = em.createQuery
            ("select object(o) from Model as o where organization = " + orgId.toString() + 
                " and o.idModel in " 
                + mylist2 + whereClause); */
           
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            
            
            return q.getResultList();
            
        } finally {
            em.close();
        }
    }   

    @Override
    public int getObjectCountExtendedOrg(Integer orgId, String whereClause) {
        return getModelCountExtendedOrg(orgId, whereClause);
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        return getModelCountExtendedOrgGroup(orgId, groupId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
