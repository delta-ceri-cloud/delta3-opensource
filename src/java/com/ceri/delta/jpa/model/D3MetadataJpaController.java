/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.jpa.model;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.model.D3Metadata;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.model.exceptions.NonexistentEntityException;
import com.ceri.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author mxm29
 */
public class D3MetadataJpaController implements PersistenceHelper, Serializable {

    public D3MetadataJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(D3Metadata d3Metadata) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(d3Metadata);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public D3Metadata edit(D3Metadata d3Metadata) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            d3Metadata = em.merge(d3Metadata);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = d3Metadata.getIdMetadata();
                if (findD3Metadata(id) == null) {
                    throw new NonexistentEntityException("The d3Metadata with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return d3Metadata;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            D3Metadata d3Metadata;
            try {
                d3Metadata = em.getReference(D3Metadata.class, id);
                d3Metadata.getIdMetadata();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The d3Metadata with id " + id + " no longer exists.", enfe);
            }
            em.remove(d3Metadata);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<D3Metadata> findD3MetadataEntities() {
        return findD3MetadataEntities(true, -1, -1);
    }

    public List<D3Metadata> findD3MetadataEntities(int maxResults, int firstResult) {
        return findD3MetadataEntities(false, maxResults, firstResult);
    }

    private List<D3Metadata> findD3MetadataEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from D3Metadata as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public D3Metadata findD3Metadata(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(D3Metadata.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<D3Metadata> findD3MetadataByModelId(Integer id) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("D3Metadata.findByModelId");
            q.setParameter("idModel", id);
            return q.getResultList();     
        } finally {
            em.close();
        }
    }    

    public int getD3MetadataCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from D3Metadata as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Object updateObjects(Object o, Integer intgr) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((D3Metadata)object).setCreatedTS(currentTimestamp);
        ((D3Metadata)object).setCreatedBy(currentUserId);        
        ((D3Metadata)object).setUpdatedTS(currentTimestamp);
        ((D3Metadata)object).setUpdatedBy(currentUserId);        
        D3Metadata obj = edit(((D3Metadata)object));
        AuditLogger.log(currentUserId, ((D3Metadata)obj).getIdModel(), "create", obj);       
        return obj;     }

    @Override
    public Object updateObjects(Object o, Integer intgr, Integer intgr1) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getObjectType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getObjectId(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int i, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int i, int i1, Integer intgr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(Integer intgr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int i, int i1, int i2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int i, int i1, int i2, Integer intgr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer intgr, Integer intgr1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int i, int i1, Integer intgr, Integer intgr1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int i, int i1, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int i, int i1, Integer intgr, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(Integer intgr, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer intgr, Integer intgr1, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int i, int i1, Integer intgr, Integer intgr1, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object findObjectEntityById(Integer intgr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountByOrg(Integer intgr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountByOrgGroup(Integer intgr, Integer intgr1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrg(Integer intgr, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer intgr, Integer intgr1, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object o, Integer intgr) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObject(Object o, Integer intgr, Integer intgr1) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object createObjects(Object o, Integer intgr, Integer intgr1) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
