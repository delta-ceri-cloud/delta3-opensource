/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.jpa.model;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.model.D3DescStats;
import com.ceri.delta.entity.model.Model;
import com.ceri.delta.jpa.model.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.model.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author mxm29
 */
public class D3DescStatsJpaController implements Serializable {

    public D3DescStatsJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(D3DescStats d3DescStats) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(d3DescStats);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findD3DescStats(d3DescStats.getIdStats()) != null) {
                throw new PreexistingEntityException("D3DescStats " + d3DescStats + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(D3DescStats d3DescStats) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            d3DescStats = em.merge(d3DescStats);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = d3DescStats.getIdStats();
                if (findD3DescStats(id) == null) {
                    throw new NonexistentEntityException("The d3DescStats with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            D3DescStats d3DescStats;
            try {
                d3DescStats = em.getReference(D3DescStats.class, id);
                d3DescStats.getIdStats();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The d3DescStats with id " + id + " no longer exists.", enfe);
            }
            em.remove(d3DescStats);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<D3DescStats> findD3DescStatsEntities() {
        return findD3DescStatsEntities(true, -1, -1);
    }

    public List<D3DescStats> findD3DescStatsEntities(int maxResults, int firstResult) {
        return findD3DescStatsEntities(false, maxResults, firstResult);
    }

    private List<D3DescStats> findD3DescStatsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from D3DescStats as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public D3DescStats findD3DescStats(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(D3DescStats.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<D3DescStats> findDescStatsByModelId(Integer modelId, boolean flat) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("D3DescStats.findByIdModel");
            q.setParameter("idModel", modelId);
            q.setParameter("flat", flat);
            return q.getResultList();            
        } finally {
            em.close();
        }
    }    

    public int getD3DescStatsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from D3DescStats as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
