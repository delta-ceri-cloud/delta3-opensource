/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ceri.delta.jpa.event;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.events.Notification;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.exceptions.PreexistingEntityException;
import com.ceri.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Boston Advanced Analytics
 */
public class NotificationJpaController implements PersistenceHelper, Serializable {

    public NotificationJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("CeriPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 


    public void create(Notification notification) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(notification);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNotification(notification.getIdNotification()) != null) {
                throw new PreexistingEntityException("Notification " + notification + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Notification edit(Notification notification) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            notification = em.merge(notification);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = notification.getIdNotification();
                if (findNotification(id) == null) {
                    throw new NonexistentEntityException("The notification with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return notification;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Notification notification;
            try {
                notification = em.getReference(Notification.class, id);
                notification.getIdNotification();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The notification with id " + id + " no longer exists.", enfe);
            }
            em.remove(notification);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findNotificationEntities() {
        return findNotificationEntities(true, -1, -1);
    }

    public List<Object> findNotificationEntities(int maxResults, int firstResult) {
        return findNotificationEntities(false, maxResults, firstResult);
    }

    private List<Object> findNotificationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            //System.out.println("This is at findNotificationEntities select o----------------");
            Query q = em.createQuery("select object(o) from Notification as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findUserNotificationEntities(Integer userId) {
        return findUserNotificationEntities(true, -1, -1, userId);
    }

    public List<Object> findUserNotificationEntities(int maxResults, int firstResult, Integer userId) {
        return findUserNotificationEntities(false, maxResults, firstResult, userId);
    }

    private List<Object> findUserNotificationEntities(boolean all, int maxResults, int firstResult, Integer userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Notification as o where User_idUser = " + userId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
 
    public List<Object> findUserNotificationsEntitiesByGroup(Integer userId, Integer groupId, String sort) {
        return findUserNotificationsEntitiesByGroup(true, -1, -1, userId, groupId, sort);
    }

    public List<Object> findUserNotificationsEntitiesByGroup(int maxResults, int firstResult, Integer userId, Integer groupId, String sort) {
        return findUserNotificationsEntitiesByGroup(false, maxResults, firstResult, userId, groupId, sort);
    }

    private List<Object> findUserNotificationsEntitiesByGroup(boolean all, int maxResults, int firstResult, Integer userId, Integer groupId, String sort) {
        EntityManager em = getEntityManager();
        String orderBy = "";
        
//        System.out.println("-----------------findObjectEntitiesExtendedOrgGroup--------------");
//        System.out.println("all"+all);
//        System.out.println("userId"+userId);
//        System.out.println("all"+all);
//        System.out.println("groupId"+groupId);
//        System.out.println("firstResult"+firstResult);
//        System.out.println("findObjectEntitiesByOrgGroup----======"+groupId);
//        
        if ( !"".equals(sort) && sort != null ) {
            orderBy = " order by o." + sort;
        }
        try {
            //Query q = em.createQuery("select object(o) from Event as o where (objectId = " + groupId.toString() + " and objectType = " + objectType + ") or (objectType <> " + objectType + ") or (objectType is null)");
             Query q = em.createQuery("select object(o) from Notification as o"
                    + " where (User_idUser = " 
                    + userId.toString() + ") and (idGroup = " 
                    + groupId.toString() +")" + orderBy);
            
            /*
            Query q = em.createQuery("select object(o) from Notification as o, "
                    + "Event as e where (User_idUser = " + userId.toString() 
                    + ") and (o.idEvent = e.idEvent) and "
                            + "((e.objectId in (select ghe.groupHasEntityPK.entityidEntity "
                            + "from GroupHasEntity as ghe where "
                            + "ghe.groupHasEntityPK.groupidGroup = " + groupId.toString() 
                    + ")))" + orderBy);
            
            */
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Notification findNotification(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Notification.class, id);
        } finally {
            em.close();
        }
    }

    public int getNotificationCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Notification as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
    
    public int getNotificationCount(int userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Notification as o where User_idUser = " + Integer.toString(userId));
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }  
  
    public int getUserNotificationEntitiesByGroupCount(Integer userId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Notification as o, Event as e where (User_idUser = " + userId.toString() 
                    + ") and (o.idEvent = e.idEvent) and ((e.objectId in (select ghe.groupHasEntityPK.entityidEntity from GroupHasEntity as ghe where ghe.groupHasEntityPK.groupidGroup = " + groupId.toString() 
                    + ")))");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }     
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Notification)object).setIdUser(currentUserId);
        ((Notification)object).setCreatedTS(currentTimestamp);
        ((Notification)object).setCreatedBy(currentUserId);        
        Notification obj = edit(((Notification)object));     
        AuditLogger.log(currentUserId, ((Notification)object).getIdNotification(), "create", object);               
        return obj;         
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Notification)object).setUpdatedTS(currentTimestamp);
        ((Notification)object).setUpdatedBy(currentUserId);
        AuditLogger.log(currentUserId, ((Notification)object).getIdNotification(), "update", object);               
        return edit(((Notification)object)); 
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getObjectType() {
        return "notifications";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findNotificationEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findNotificationEntities();
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        return findUserNotificationEntities(maxResults, firstResult, id);
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer userId) {
        return findUserNotificationEntities(maxResults, firstResult, userId);
    }

    @Override
    public List<Object> findObjectEntities(Integer userId) {
        //System.out.println("findObjectEntities Userid :"+userId);
        return findUserNotificationEntities(userId);
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getNotificationCount();
    }
    
    @Override
    public int getObjectCount(int userId) {
        return getNotificationCount(userId);
    }

    @Override
    public int getObjectCountByOrg(Integer userId) {
        // in this case get count by userId not by ordId
        return getNotificationCount(userId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.ceri.delta.entity.events.Notification)subject).getIdNotification(), "delete", subject);
        destroy(((com.ceri.delta.entity.events.Notification)subject).getIdNotification());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object findObjectEntityById(Integer objectId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getObjectId(Object subject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer userId, Integer idGroup, String sort) {
        // in this controller Org is replaced with User, as notifications are per user
        // System.out.println("This is maxResults firstResult userId idGroup sort: "+firstResult+":"+maxResults +" :"+userId+" :"+idGroup+" :"+sort);
        return findUserNotificationsEntitiesByGroup(maxResults, firstResult, userId, idGroup, sort);
    }
    
    @Override
    public int getObjectCountByOrgGroup(Integer userId, Integer groupId) {
        // in this controller Org is replaced with User, as notifications are per user
        return getUserNotificationEntitiesByGroupCount(userId, groupId);
    }   

    public List<Object> findObjectEntitiesByOrgGroup(Integer userId, Integer idGroup, String sort) {
        // in this controller Org is replaced with User, as notifications are per user
        //System.out.println("This is UserId Group orderby :"+userId+" :"+idGroup+" :"+sort);
        
        return findUserNotificationsEntitiesByGroup(userId, idGroup, sort);
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer currentOrgId, Integer idGroup, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
