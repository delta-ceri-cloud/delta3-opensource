/*
 * (C) 2016 CERI-Lahey
 * 
 */
package com.ceri.delta.jpa.matchset;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.matchset.MatchSet;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author mxm29
 */
public class MatchSetJpaController implements Serializable {

    public MatchSetJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MatchSet matchSet) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(matchSet);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public MatchSet edit(MatchSet matchSet) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            matchSet = em.merge(matchSet);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = matchSet.getIdMatchset();
                if (findMatchSet(id) == null) {
                    throw new NonexistentEntityException("The matchSet with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return matchSet;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MatchSet matchSet;
            try {
                matchSet = em.getReference(MatchSet.class, id);
                matchSet.getIdMatchset();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The matchSet with id " + id + " no longer exists.", enfe);
            }
            em.remove(matchSet);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MatchSet> findMatchSetEntities() {
        return findMatchSetEntities(true, -1, -1);
    }

    public List<MatchSet> findMatchSetEntities(int maxResults, int firstResult) {
        return findMatchSetEntities(false, maxResults, firstResult);
    }

    private List<MatchSet> findMatchSetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from MatchSet as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MatchSet findMatchSet(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MatchSet.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<MatchSet> findMatchSetEntitiesForModelAndProject(Integer modelId, Integer projectId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from MatchSet as o where idModel = " + modelId.toString() + " and idGroup = " + projectId.toString());
            return q.getResultList();
        } finally {
            em.close();
        }
    }
        
    public int getMatchSetCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from MatchSet as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<MatchSet> findMatchsetByUniqueId(Integer studyId, Integer orgId, Integer groupId, Integer processId, Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from MatchSet as o where idStudy = " + studyId.toString() 
                    + " and idOrganization = " + orgId.toString()
                    + " and idGroup = " + groupId.toString()
                    + " and idProcess = " + processId.toString()
                    + " and idModel = " + modelId.toString()
                    + " order by idMatchset");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }    

    public MatchSet createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((MatchSet)object).setCreatedTS(currentTimestamp);
        ((MatchSet)object).setCreatedBy(currentUserId);        
        ((MatchSet)object).setUpdatedTS(currentTimestamp);
        ((MatchSet)object).setUpdatedBy(currentUserId);   
        MatchSet ms = edit(((MatchSet)object));
        AuditLogger.log(currentUserId, ms.getIdMatchset(), "create", ms);               
        return ms;            
    }

    public MatchSet updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((MatchSet)object).setUpdatedTS(currentTimestamp);
        ((MatchSet)object).setUpdatedBy(currentUserId);    
        MatchSet ms = edit(((MatchSet)object));
        AuditLogger.log(currentUserId, ms.getIdMatchset(), "update", ms);               
        return ms;          
      }    
}
