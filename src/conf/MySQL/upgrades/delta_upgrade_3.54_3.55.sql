/* 
 * (C) 2018 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Jul 27, 2018
 */

ALTER TABLE `delta3`.`d3_permission` 
ADD COLUMN `Module_idModule` INT(11) NULL AFTER `Role_idRole`;

ALTER TABLE `delta3`.`d3_role` 
DROP COLUMN `moduleId`,
ADD COLUMN `modules` VARCHAR(45) NULL DEFAULT NULL AFTER `type`;

UPDATE `delta3`.`d3_role` SET `modules`='1' WHERE `idRole`='1';
UPDATE `delta3`.`d3_role` SET `modules`='1' WHERE `idRole`='2';
UPDATE `delta3`.`d3_role` SET `modules`='1' WHERE `idRole`='3';
UPDATE `delta3`.`d3_role` SET `modules`='1' WHERE `idRole`='4';
UPDATE `delta3`.`d3_role` SET `modules`='8' WHERE `idRole`='5';
UPDATE `delta3`.`d3_role` SET `modules`='8' WHERE `idRole`='6';
UPDATE `delta3`.`d3_role` SET `modules`='9' WHERE `idRole`='7';


