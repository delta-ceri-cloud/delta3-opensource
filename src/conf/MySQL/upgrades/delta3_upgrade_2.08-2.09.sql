/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Mar 28, 2017
 */

ALTER TABLE `delta3`.`d3_study` 
ADD COLUMN `submittedTS` VARCHAR(45) NULL DEFAULT NULL AFTER `LastProcess_idProcess`;
ALTER TABLE `delta3`.`d3_process` 
ADD COLUMN `submittedTS` TIMESTAMP NULL DEFAULT NULL AFTER `active`;

