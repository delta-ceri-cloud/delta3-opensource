/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Feb 28, 2017
 */

ALTER TABLE `delta3`.`d3_user_has_role` 
CHANGE COLUMN `idGroup` `idGroup` INT(11) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`User_idUser`, `Role_idRole`, `idGroup`);
ALTER TABLE `delta3`.`d3_process` 
CHANGE COLUMN `message` `message` VARCHAR(2048) NULL DEFAULT NULL ;

CREATE TABLE `delta3`.`d3_matchset` (
  `idMatchset` int(11) NOT NULL,
  `idOrganization` int(11) DEFAULT NULL,
  `idModel` int(11) DEFAULT NULL,
  `idStudy` int(11) DEFAULT NULL,
  `idProcess` int(11) DEFAULT NULL,  
  `idGroup` int(11) DEFAULT NULL,  
  `GUID` varchar(45) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `rowCount` int(11) DEFAULT NULL,
  `status` varchar(1024) DEFAULT NULL,
  `localStatPackage` tinyint(1) DEFAULT NULL,
  `remoteStatPackage` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idMatchset`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delta3`.`d3_sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES ('MATCHSET_ID', '1');

