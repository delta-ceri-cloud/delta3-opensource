/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Sep 11, 2018
 */

CREATE TABLE `d3_desc_stats` (
  `idStats` int(11) NOT NULL,
  `idOrganization` int(11) NOT NULL,
  `idModel` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `stats` MEDIUMTEXT DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `flat` tinyint(1) NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idStats`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delta3`.`d3_sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES ('DESCSTATS_ID', '1');

ALTER TABLE `delta3`.`d3_process` 
CHANGE COLUMN `message` `message` VARCHAR(10000) NULL DEFAULT NULL ;

