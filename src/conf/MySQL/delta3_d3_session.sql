-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.17

--
-- Table structure for table `d3_session`
--

DROP TABLE IF EXISTS `d3_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_session` (
  `idSession` int(11) NOT NULL,
  `cookie` varchar(100) DEFAULT NULL,
  `userAlias` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `locale` varchar(45) DEFAULT NULL,
  `emailAddress1` varchar(256) DEFAULT NULL,
  `emailAddress2` varchar(256) DEFAULT NULL,
  `misc` varchar(1024) DEFAULT NULL,  
  `permissions` MEDIUMTEXT NULL DEFAULT NULL,
  `apps` VARCHAR(256) NULL DEFAULT NULL,
  `userAgent` VARCHAR(256) NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `timeoutTS` timestamp NULL DEFAULT NULL,  
  `idOrganization` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idSUser` int(11) NOT NULL,  
  `idApp` int(11) NOT NULL,    
  PRIMARY KEY (`idSession`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


