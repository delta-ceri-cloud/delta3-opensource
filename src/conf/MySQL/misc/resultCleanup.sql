DELETE FROM delta3.d3_logregr_field where idLogregr in (SELECT idLogregr FROM delta3.d3_logregr where idProcess in (SELECT idProcess FROM delta3.d3_process where idOrganization = x and idProcess <= y));
DELETE FROM delta3.d3_logregr_date where idLogregr in (SELECT idLogregr FROM delta3.d3_logregr where idProcess in (SELECT idProcess FROM delta3.d3_process where idOrganization = x and idProcess <= y));
DELETE FROM delta3.d3_logregr where idProcess in (SELECT idProcess FROM delta3.d3_process where idOrganization = x and idProcess <= y);
DELETE FROM delta3.d3_process where idOrganization = x and idProcess <= y;
