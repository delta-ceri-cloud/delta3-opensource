CREATE DEFINER=`root`@`localhost` PROCEDURE `convert_dates`()
BEGIN
DECLARE tableName VARCHAR(50) DEFAULT 'logistic_regression_test_base';
DECLARE columnName VARCHAR(50) DEFAULT 'leadDate';
DECLARE done INTEGER DEFAULT 0;
DECLARE c_id INT(11) DEFAULT 0;
DECLARE c_date VARCHAR(50) DEFAULT '';
DECLARE c_leadDate DATE DEFAULT '2000-01-01';
DECLARE seqDate DATE DEFAULT '2000-01-01';

DECLARE conv_date_cursor CURSOR FOR SELECT * FROM view_temp_date_conv;
 -- declare NOT FOUND handler
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DROP VIEW IF EXISTS view_temp_date_conv;
SET @query = CONCAT('CREATE VIEW view_temp_date_conv as select id, sequencingDate from ', tableName, ' FOR UPDATE');
select @query;
PREPARE stmt from @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

IF EXISTS (select * from information_schema.columns where table_name = tableName and column_name = columnName) THEN
  SET @sql_text1 = CONCAT('alter table ',tableName,' drop column ',columnName);
  PREPARE stmt1 FROM @sql_text1;
  EXECUTE stmt1;
  DEALLOCATE PREPARE stmt1;
END IF;

SET @sql_text2 = CONCAT('alter table ',tableName,' add column ',columnName,' TIMESTAMP NULL DEFAULT NULL AFTER `id`');
PREPARE stmt2 FROM @sql_text2;
EXECUTE stmt2;
DEALLOCATE PREPARE stmt2;

SET @count = 0;
OPEN conv_date_cursor;

get_dates: LOOP
 FETCH conv_date_cursor INTO c_id, c_date;
 IF done THEN
    LEAVE get_dates;
 END IF;

 SET seqDate = STR_TO_DATE(c_date, '%c/%e/%Y');

  SET @sql_text3 = CONCAT('update ',tableName,' set ',columnName,' = "',seqDate, '" where id = ',c_id);
  PREPARE stmt3 FROM @sql_text3;
  EXECUTE stmt3;
  DEALLOCATE PREPARE stmt3;
  
  SET @count = @count + 1;

END LOOP get_dates;

CLOSE conv_date_cursor;
DROP VIEW view_temp_date_conv;
SELECT @COUNT;

END
