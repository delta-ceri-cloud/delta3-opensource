/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Sep 17, 2018
 */
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateUser_OADMIN`(
 IN idOrg INT,
 IN userAlias VARCHAR(50), 
 OUT idUser INT)
BEGIN
DECLARE idUser INT DEFAULT 0;
DECLARE pass VARCHAR(20);
    
    SET pass = 'Delta3pass';
    SELECT seq_count
    INTO idUser
    FROM d3_sequence
    WHERE seq_name = 'USER_ID';

    INSERT INTO `d3_user` VALUES (idUser,UUID(),2,CURRENT_TIMESTAMP(),1,CURRENT_TIMESTAMP(),userAlias,SHA1(pass),'2020-10-10 04:00:00','2015-03-25 17:52:33',0,3,'1',1,'','OADMIN','eng_US','John','Tester','company@xyzsystems.com','','','','d3','','','','',idOrg,1,1);
    SET idUser = idUser + 1;

    UPDATE d3_sequence 
    SET seq_count = idUser
    WHERE seq_name = 'USER_ID';    
    
    SET idUser = idUser - 1;
    
    INSERT INTO `d3_user_has_role` VALUES (idUser,2,0,2,CURRENT_TIMESTAMP(),2,CURRENT_TIMESTAMP()),(idUser,3,0,2,CURRENT_TIMESTAMP(),2,CURRENT_TIMESTAMP()),(idUser,4,0,2,CURRENT_TIMESTAMP(),2,CURRENT_TIMESTAMP());    

END
