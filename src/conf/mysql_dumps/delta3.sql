-- DatAdmin Native MySQL Dump

/*!40101 SET NAMES utf8 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE delta32;
CREATE TABLE `d3_stats` (
  `idStat` int(11) NOT NULL,
  `idOrganization` int(11) NOT NULL,
  `shortName` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `configVersion` varchar(45) DEFAULT NULL,
  `useProxy` tinyint(1) DEFAULT NULL,
  `configuration` text,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idStat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `d3_alert` ( 
    `idAlert` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `priority` varchar(45) NULL, 
    `retryMax` int NULL, 
    `timeOut` int NULL, 
    `ackRequired` tinyint(1) NULL, 
    `sendEmail` tinyint(1) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `EventTemplate_idEventTemplate` int NOT NULL, 
    `User_idUser` int NOT NULL,  
    PRIMARY KEY (`idAlert`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Alert_EventTemplae1_idx` ON `d3_alert` (`EventTemplate_idEventTemplate`);
CREATE INDEX `fk_Alert_Subscriber1_idx` ON `d3_alert` (`User_idUser`);
CREATE TABLE `d3_audit` ( 
    `idAudit` int NOT NULL, 
    `source` varchar(45) NULL, 
    `userAlias` varchar(45) NULL, 
    `userId` int NULL, 
    `organizationId` int NULL, 
    `objectType` varchar(45) NULL, 
    `objectId` int NULL, 
    `action` varchar(45) NULL, 
    `objectContent` mediumtext NULL, 
    `createdTS` timestamp NULL,  
    PRIMARY KEY (`idAudit`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_configuration` ( 
    `idConfiguration` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `type` varchar(12) NULL, 
    `connectionInfo` varchar(256) NULL, 
    `dbUser` varchar(45) NULL, 
    `dbPassword` varchar(45) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `Organization_idOrganization` int NOT NULL,  
    PRIMARY KEY (`idConfiguration`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_event` ( 
    `idEvent` int NOT NULL, 
    `description` text NULL, 
    `objectId` int NULL, 
    `objectType` varchar(45) NULL, 
    `executeBy` int NULL, 
    `executeTS` timestamp NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `Organization_idOrganization` int NULL, 
    `EventTemplate_idEventTemplate` int NOT NULL, 
    `Task_idTask` int NULL,  
    PRIMARY KEY (`idEvent`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Event_EventTemplae1_idx` ON `d3_event` (`EventTemplate_idEventTemplate`);
CREATE TABLE `d3_eventtemplate` ( 
    `idEventTemplate` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `priority` varchar(45) NULL, 
    `actionClass` varchar(200) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `Module_idModule` int NULL,  
    PRIMARY KEY (`idEventTemplate`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_EventTemplae_Module1_idx` ON `d3_eventtemplate` (`Module_idModule`);
CREATE TABLE `d3_filter` ( 
    `idFilter` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `resultCount` int NULL DEFAULT '-1', 
    `type` varchar(45) NULL, 
    `Organization_idOrganization` int NULL, 
    `Group_idGroup` int NULL, 
    `active` tinyint(1) NULL, 
    `Model_idModel` int NOT NULL,  
    PRIMARY KEY (`idFilter`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_filter_field` ( 
    `idFilterField` int NOT NULL, 
    `Filter_idFilter` int NOT NULL, 
    `ModelColumn_idModelColumn` int NOT NULL, 
    `formula` varchar(1024) NULL, 
    `negator` tinyint NULL, 
    `type` varchar(45) NULL, 
    `operator` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idFilterField`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_filter_has_field` ( 
    `Filter_idFilter` int NOT NULL, 
    `ModelColumn_idModelColumn` int NOT NULL, 
    `formula` varchar(1024) NULL, 
    `negator` tinyint NULL, 
    `type` varchar(45) NULL, 
    `operator` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`Filter_idFilter`, `ModelColumn_idModelColumn`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Filter_has_Field_Field1_idx` ON `d3_filter_has_field` (`ModelColumn_idModelColumn`);
CREATE INDEX `fk_Filter_has_Field_Filter1_idx` ON `d3_filter_has_field` (`Filter_idFilter`);
CREATE TABLE `d3_group_has_entity` ( 
    `Group_idGroup` int NOT NULL, 
    `Entity_idEntity` int NOT NULL, 
    `type` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `active` tinyint(1) NULL,  
    PRIMARY KEY (`Group_idGroup`, `Entity_idEntity`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Group_has_Entity_idx` ON `d3_group_has_entity` (`Group_idGroup`);
CREATE TABLE `d3_group_has_user` ( 
    `Group_idGroup` int NOT NULL, 
    `User_idUser` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `active` tinyint(1) NULL,  
    PRIMARY KEY (`Group_idGroup`, `User_idUser`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Group_has_User_User1_idx` ON `d3_group_has_user` (`User_idUser`);
CREATE INDEX `fk_Group_has_User_Group1_idx` ON `d3_group_has_user` (`Group_idGroup`);
CREATE TABLE `d3_group_table` ( 
    `idGroup` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `type` varchar(45) NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `parentId` int NULL, 
    `active` tinyint(1) NULL, 
    `organizationId` int NOT NULL,  
    PRIMARY KEY (`idGroup`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_location` ( 
    `idLocation` int NOT NULL, 
    `GUID` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(45) NULL, 
    `address1` varchar(45) NULL, 
    `address2` varchar(45) NULL, 
    `state` varchar(45) NULL, 
    `county` varchar(45) NULL, 
    `country` varchar(45) NULL, 
    `zip` varchar(20) NULL, 
    `longitude` decimal(10,0) NULL, 
    `latitude` decimal(10,0) NULL, 
    `phoneNumber1` varchar(45) NULL, 
    `phoneNumber2` varchar(45) NULL, 
    `active` tinyint(1) NULL, 
    `Organization_idOrganization` int NOT NULL,  
    PRIMARY KEY (`idLocation`, `Organization_idOrganization`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Location_Organization1_idx` ON `d3_location` (`Organization_idOrganization`);
CREATE TABLE `d3_logregr` ( 
    `idLogregr` int NOT NULL, 
    `name` varchar(100) NULL, 
    `description` varchar(1024) NULL, 
    `status` varchar(45) NULL, 
    `idOrganization` int NOT NULL, 
    `idProcess` int NULL, 
    `idModel` int NULL, 
    `idStudy` int NULL, 
    `ModelColumn_idSequencer` int NULL, 
    `intervalData` varchar(16) NULL, 
    `intervalQuery` varchar(1024) NULL, 
    `formula` mediumtext NULL, 
    `originalLogregrId` int NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idLogregr`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_logregr_date` ( 
    `idLogregrDate` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(1024) NULL, 
    `value` varchar(45) NULL, 
    `ModelColumn_idSequencer` int NULL, 
    `idLogregrField` int NULL, 
    `idLogregr` int NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idLogregrDate`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_logregr_field` ( 
    `idLogregrField` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(1024) NULL, 
    `value` varchar(45) NULL, 
    `ModelColumn_idRisk` int NOT NULL, 
    `idLogregr` int NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idLogregrField`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_metadata` ( 
    `idMetadata` int NOT NULL, 
    `idModel` int NOT NULL, 
    `name` varchar(100) NULL, 
    `data` mediumtext NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idMetadata`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_method` ( 
    `idMethod` int NOT NULL, 
    `idOrganization` int NOT NULL, 
    `idStat` int NULL, 
    `shortName` varchar(45) NULL, 
    `name` varchar(100) NULL, 
    `description` varchar(1024) NULL, 
    `sequenceNumber` int NULL DEFAULT '0', 
    `methodParams` varchar(10000) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idMethod`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_model` ( 
    `idModel` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `idConfiguration` int NULL, 
    `outputName` varchar(45) NULL, 
    `status` varchar(1024) NULL, 
    `isAtomicFinished` tinyint(1) NULL, 
    `isMissingFinished` tinyint(1) NULL, 
    `isVirtualFinished` tinyint(1) NULL, 
    `isSecondaryVirtualFinished` tinyint(1) NULL, 
    `processingId` varchar(45) NULL, 
    `locked` tinyint(1) NULL, 
    `active` tinyint(1) NULL, 
    `Organization_idOrganization` int NOT NULL, 
    `idGroup` int NULL DEFAULT '0', 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idModel`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_model_column` ( 
    `idModelColumn` int NOT NULL, 
    `name` varchar(255) NULL, 
    `description` varchar(255) NULL, 
    `physicalName` varchar(255) NULL, 
    `type` varchar(45) NULL, 
    `defaultValue` varchar(1024) NULL, 
    `defaultValueType` varchar(12) NULL, 
    `keyField` tinyint(1) NULL DEFAULT '0', 
    `insertable` tinyint(1) NULL DEFAULT '1', 
    `atomic` tinyint(1) NULL DEFAULT '1', 
    `sub` tinyint(1) NULL DEFAULT '0', 
    `virtualField` tinyint(1) NULL DEFAULT '0', 
    `virtualOrder` tinyint(1) NULL DEFAULT '0', 
    `rollup` tinyint NULL, 
    `formula` varchar(1024) NULL, 
    `verified` tinyint(1) NULL DEFAULT '0', 
    `fieldClass` varchar(20) NULL, 
    `fieldKind` varchar(20) NULL, 
    `active` tinyint(1) NULL, 
    `ModelTable_idModelTable` int NULL, 
    `Model_idModel` int NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idModelColumn`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_model_key` ( 
    `idModelKey` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idModelKey`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_model_relationship` ( 
    `idModelRelationship` int NOT NULL, 
    `table1` int NULL, 
    `key1` int NULL, 
    `table2` int NULL, 
    `key2` int NULL, 
    `type` varchar(12) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `Model_idModel` int NULL,  
    PRIMARY KEY (`idModelRelationship`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_model_table` ( 
    `idModelTable` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(45) NULL, 
    `physicalName` varchar(256) NULL, 
    `active` tinyint(1) NULL, 
    `primaryTable` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `Model_idModel` int NULL,  
    PRIMARY KEY (`idModelTable`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_module` ( 
    `idModule` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `type` varchar(45) NULL, 
    `parentId` varchar(45) NULL, 
    `active` tinyint(1) NULL, 
    `Organization_idOrganization` int NOT NULL,  
    PRIMARY KEY (`idModule`, `Organization_idOrganization`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Module_Organization1_idx` ON `d3_module` (`Organization_idOrganization`);
CREATE TABLE `d3_notification` ( 
    `idNotification` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `eventData` varchar(1024) NULL, 
    `priority` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `acknowledged` tinyint(1) NULL, 
    `retryCount` int NULL, 
    `timeOut` int NULL, 
    `ackRequired` tinyint(1) NULL, 
    `active` tinyint(1) NULL, 
    `Event_idEvent` int NOT NULL, 
    `NotificationTemplate_idNotificationTemplate` int NULL, 
    `User_idUser` int NOT NULL,  
    PRIMARY KEY (`idNotification`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_notificationtemplate` ( 
    `idNotificationTemplate` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `config1` varchar(45) NULL, 
    `config2` varchar(45) NULL, 
    `config3` varchar(45) NULL, 
    `config4` varchar(45) NULL, 
    `type` varchar(45) NULL,  
    PRIMARY KEY (`idNotificationTemplate`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_organization` ( 
    `idOrganization` int NOT NULL, 
    `GUID` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `name` varchar(45) NULL, 
    `longName` varchar(256) NULL, 
    `type` varchar(45) NULL, 
    `parentId` int NULL, 
    `key1` varchar(256) NULL, 
    `key2` varchar(256) NULL, 
    `active` tinyint(1) NULL,  
    PRIMARY KEY (`idOrganization`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_permission` ( 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `PermTemp_idPermTemp` int NOT NULL, 
    `Role_idRole` int NOT NULL, 
    `Module_idModule` int NULL,  
    PRIMARY KEY (`PermTemp_idPermTemp`, `Role_idRole`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Permission_PermissionTemplate_idx` ON `d3_permission` (`PermTemp_idPermTemp`);
CREATE INDEX `fk_Permission_Role1_idx` ON `d3_permission` (`Role_idRole`);
CREATE TABLE `d3_permissiontemplate` ( 
    `idPermissionTemplate` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(45) NULL, 
    `type` varchar(45) NULL, 
    `tag` varchar(45) NULL, 
    `sequence` int NULL, 
    `active` tinyint(1) NULL, 
    `Module_idModule` int NOT NULL,  
    PRIMARY KEY (`idPermissionTemplate`, `Module_idModule`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_PermissionTemplate_Module1_idx` ON `d3_permissiontemplate` (`Module_idModule`);
CREATE TABLE `d3_person` ( 
    `idPerson` int NOT NULL, 
    `GUID` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `firstName` varchar(45) NULL, 
    `middleName` varchar(45) NULL, 
    `lastName` varchar(45) NULL, 
    `ssn` varchar(45) NULL, 
    `driverLicenseNumber` varchar(45) NULL, 
    `identifier1` varchar(45) NULL, 
    `identifier2` varchar(45) NULL, 
    `active` tinyint(1) NULL, 
    `Location_idLocation` int NOT NULL,  
    PRIMARY KEY (`idPerson`, `Location_idLocation`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Person_Location1_idx` ON `d3_person` (`Location_idLocation`);
CREATE TABLE `d3_process` ( 
    `idProcess` int NOT NULL, 
    `idOrganization` int NULL, 
    `idModel` int NULL, 
    `idStudy` int NULL, 
    `GUID` varchar(45) NULL, 
    `progress` int NULL, 
    `name` varchar(1024) NULL, 
    `data` mediumtext NULL, 
    `description` varchar(1024) NULL, 
    `rowCount` int NULL, 
    `status` varchar(1024) NULL, 
    `message` varchar(2048) NULL, 
    `filterClause` varchar(1024) NULL, 
    `idFilter` int NULL, 
    `localStatPackage` tinyint(1) NULL, 
    `remoteStatPackage` tinyint(1) NULL, 
    `input` mediumtext NULL, 
    `manualExecution` tinyint(1) NULL, 
    `stepNumber` int NULL, 
    `active` tinyint(1) NULL, 
    `submittedTS` timestamp NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idProcess`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_role` ( 
    `idRole` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `type` varchar(45) NULL, 
    `modules` varchar(45) NULL, 
    `parentId` int NULL, 
    `active` tinyint(1) NULL,  
    PRIMARY KEY (`idRole`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_sequence` ( 
    `SEQ_NAME` varchar(20) NOT NULL, 
    `SEQ_COUNT` bigint NOT NULL,  
    PRIMARY KEY (`SEQ_NAME`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_session` ( 
    `idSession` int NOT NULL, 
    `cookie` varchar(100) NULL, 
    `userAlias` varchar(45) NULL, 
    `type` varchar(45) NULL, 
    `locale` varchar(45) NULL, 
    `emailAddress1` varchar(256) NULL, 
    `emailAddress2` varchar(256) NULL, 
    `misc` varchar(1024) NULL, 
    `permissions` mediumtext NULL, 
    `apps` varchar(256) NULL, 
    `userAgent` varchar(256) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `timeoutTS` timestamp NULL, 
    `idOrganization` int NOT NULL, 
    `idUser` int NOT NULL, 
    `idSUser` int NOT NULL, 
    `idApp` int NOT NULL,  
    PRIMARY KEY (`idSession`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_study` ( 
    `idStudy` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(1024) NULL, 
    `status` varchar(1024) NULL, 
    `idOrganization` int NOT NULL, 
    `idGroup` int NULL, 
    `idModel` int NOT NULL, 
    `ModelColumn_idOutcome` int NULL, 
    `ModelColumn_idTreatment` int NULL, 
    `ModelColumn_idKey` int NULL, 
    `ModelColumn_idDate` int NULL, 
    `Stat_idStat` int NULL, 
    `Method_idMethod` int NULL, 
    `Filter_idFilter` int NULL, 
    `methodParams` varchar(10000) NULL, 
    `isMissingDataUsed` tinyint(1) NULL, 
    `isGenericId` tinyint(1) NULL, 
    `isChunking` tinyint(1) NULL, 
    `chunkingBy` varchar(45) NULL, 
    `dumpData` tinyint(1) NULL DEFAULT '0', 
    `sendAllColumns` tinyint(1) NULL DEFAULT '0', 
    `outcomePositive` tinyint(1) NULL DEFAULT '0', 
    `autoExecute` tinyint(1) NOT NULL DEFAULT '1', 
    `startTS` timestamp NULL, 
    `endTS` timestamp NULL, 
    `Alert_idAlert` int NULL, 
    `statPackageVer` varchar(45) NULL, 
    `originalStudyId` int NULL, 
    `active` tinyint(1) NULL, 
    `confidenceInterval` real NULL, 
    `confidenceInterval2` real NULL, 
    `overwriteResults` tinyint(1) NULL, 
    `localStatPackage` tinyint(1) NULL, 
    `remoteStatPackage` tinyint(1) NULL, 
    `LastProcess_idProcess` int NULL, 
    `submittedTS` varchar(45) NULL, 
    `processedTS` timestamp NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idStudy`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_study_has_category` ( 
    `Study_idStudy` int NOT NULL, 
    `ModelColumn_idModelColumn` int NOT NULL, 
    `type` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`Study_idStudy`, `ModelColumn_idModelColumn`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Study_has_Category_Category1_idx` ON `d3_study_has_category` (`ModelColumn_idModelColumn`);
CREATE INDEX `fk_Study_has_Category_Study1_idx` ON `d3_study_has_category` (`Study_idStudy`);
CREATE TABLE `d3_subscriber` ( 
    `idSubscriber` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `User_idUser` int NOT NULL,  
    PRIMARY KEY (`idSubscriber`, `User_idUser`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_Subscriber_User1_idx` ON `d3_subscriber` (`User_idUser`);
CREATE TABLE `d3_task` ( 
    `idTask` int NOT NULL, 
    `name` varchar(45) NULL, 
    `description` varchar(256) NULL, 
    `type` varchar(45) NULL, 
    `Organization_idOrganization` int NOT NULL, 
    `objectType` varchar(45) NULL, 
    `objectId` int NULL, 
    `parameters` varchar(1024) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idTask`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE TABLE `d3_user` ( 
    `idUser` int NOT NULL, 
    `GUID` varchar(45) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL, 
    `alias` varchar(45) NULL, 
    `password` varchar(45) NULL, 
    `passExpirationTS` timestamp NULL, 
    `passChangedTS` timestamp NULL, 
    `passFailureCount` int NULL, 
    `passFailureMax` int NULL, 
    `securityHandler` varchar(45) NULL, 
    `active` tinyint(1) NULL, 
    `status` varchar(45) NULL, 
    `type` varchar(45) NULL, 
    `locale` varchar(45) NULL, 
    `firstName` varchar(45) NULL, 
    `lastName` varchar(45) NULL, 
    `emailAddress1` varchar(256) NULL, 
    `emailAddress2` varchar(256) NULL, 
    `uri1` varchar(256) NULL, 
    `uri2` varchar(256) NULL, 
    `graphPackage` varchar(45) NULL DEFAULT 'd3', 
    `phoneNumber1` varchar(45) NULL, 
    `phoneNumber2` varchar(45) NULL, 
    `alertPreference1` varchar(45) NULL, 
    `alertPreference2` varchar(45) NULL, 
    `Organization_idOrganization` int NOT NULL, 
    `Person_idPerson` int NOT NULL, 
    `Group_idGroup` int NOT NULL,  
    PRIMARY KEY (`idUser`, `Organization_idOrganization`, `Person_idPerson`, `Group_idGroup`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_User_Organization1_idx` ON `d3_user` (`Organization_idOrganization`);
CREATE TABLE `d3_user_has_role` ( 
    `User_idUser` int NOT NULL, 
    `Role_idRole` int NOT NULL, 
    `idGroup` int NOT NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`User_idUser`, `Role_idRole`, `idGroup`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci ;
CREATE INDEX `fk_User_has_Role_Role1_idx` ON `d3_user_has_role` (`Role_idRole`);
CREATE INDEX `fk_User_has_Role_User1_idx` ON `d3_user_has_role` (`User_idUser`);
INSERT INTO `d3_permission` (`createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `PermTemp_idPermTemp`, `Role_idRole`, `Module_idModule`) VALUES
(1, '2013-11-01T14:24:46.000000', 1, '2013-11-01T14:24:46.000000', 1, 1, NULL),
(2, '2017-01-19T10:47:39.000000', 2, '2017-01-19T10:47:39.000000', 1, 4, NULL),
(1, '2013-11-01T14:24:46.000000', 1, '2013-11-01T14:24:46.000000', 2, 1, NULL),
(1, '2013-11-01T14:24:46.000000', 1, '2013-11-01T14:24:46.000000', 3, 1, NULL),
(2, '2017-01-19T10:47:39.000000', 2, '2017-01-19T10:47:39.000000', 3, 4, NULL),
(1, '2013-11-01T14:24:46.000000', 1, '2013-11-01T14:24:46.000000', 4, 1, NULL),
(2, '2017-01-19T10:53:14.000000', 2, '2017-01-19T10:53:14.000000', 5, 3, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 5, 4, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 6, 2, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 6, 4, NULL),
(1, '2014-01-21T12:00:00.000000', 1, '2014-01-21T12:00:00.000000', 7, 1, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 8, 2, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 9, 2, NULL),
(2, '2017-01-19T10:53:14.000000', 2, '2017-01-19T10:53:14.000000', 10, 3, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 10, 4, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 11, 2, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 11, 4, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 12, 2, NULL),
(2, '2017-01-19T10:53:14.000000', 2, '2017-01-19T10:53:14.000000', 12, 3, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 12, 4, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 13, 4, NULL),
(2, '2017-01-19T10:53:14.000000', 2, '2017-01-19T10:53:14.000000', 14, 3, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 14, 4, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 15, 4, NULL),
(6, '2014-09-12T16:42:35.000000', 6, '2014-09-12T16:42:35.000000', 16, 1, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 16, 2, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 16, 4, NULL),
(2, '2017-01-19T09:58:41.000000', 2, '2017-01-19T09:58:41.000000', 18, 1, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 18, 2, NULL),
(2, '2017-01-19T10:53:14.000000', 2, '2017-01-19T10:53:14.000000', 18, 3, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 18, 4, NULL),
(2, '2017-01-19T09:58:41.000000', 2, '2017-01-19T09:58:41.000000', 19, 1, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 19, 2, NULL),
(2, '2017-01-19T10:53:14.000000', 2, '2017-01-19T10:53:14.000000', 19, 3, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 19, 4, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 20, 2, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 20, 4, NULL),
(6, '2014-11-10T09:39:22.000000', 6, '2014-11-10T09:39:22.000000', 21, 1, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 23, 4, NULL),
(2, '2017-01-19T10:04:00.000000', 2, '2017-01-19T10:04:00.000000', 24, 2, NULL),
(2, '2017-01-19T10:47:40.000000', 2, '2017-01-19T10:47:40.000000', 24, 4, NULL),
(2, '2017-01-19T09:58:41.000000', 2, '2017-01-19T09:58:41.000000', 27, 1, NULL),
(2, '2017-01-19T09:58:41.000000', 2, '2017-01-19T09:58:41.000000', 28, 1, NULL),
(2, '2017-04-11T11:02:28.000000', 2, '2017-04-11T11:02:28.000000', 29, 4, NULL),
(2, '2018-04-23T11:44:44.000000', 2, '2018-04-23T11:44:44.000000', 31, 5, NULL),
(2, '2018-04-23T11:44:44.000000', 2, '2018-04-23T11:44:44.000000', 32, 5, NULL),
(2, '2018-04-23T11:44:31.000000', 2, '2018-04-23T11:44:31.000000', 32, 6, NULL),
(2, '2018-07-27T11:45:37.000000', 2, '2018-07-27T11:45:37.000000', 34, 7, NULL),
(2, '2018-07-30T11:49:51.000000', 2, '2018-07-30T11:49:51.000000', 34, 11, 9);
INSERT INTO `d3_organization` (`idOrganization`, `GUID`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `longName`, `type`, `parentId`, `key1`, `key2`, `active`) VALUES
(1, 'b336d3d9-8839-47d4-bcc9-f02bb53c6bd9', 2, '2013-10-18T15:55:27.000000', 2, '2014-06-10T16:57:44.000000', 'DELTA3 System', 'DELTA3 System Organization', 'IT', 0, 'vaJXZZhtpLXECudR7JcZz9fXDDjGMRuQTLEpaIge', NULL, 1);
INSERT INTO `d3_person` (`idPerson`, `GUID`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `firstName`, `middleName`, `lastName`, `ssn`, `driverLicenseNumber`, `identifier1`, `identifier2`, `active`, `Location_idLocation`) VALUES
(1, '0', 1, '2013-10-03T14:53:12.000000', 1, '2013-10-03T14:53:12.000000', 'Empty', 'Empty', 'Empty', '000-00-0000', '000000000', NULL, NULL, 1, 1);
INSERT INTO `d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES
(1, 1, '2013-10-30T14:04:51.000000', 1, '2013-10-30T14:37:09.000000', 'Admin Users', 'administer users', 'menu', 'Users', 90, 1, 1),
(2, 1, '2013-10-30T14:38:39.000000', 1, '2013-10-30T14:43:57.000000', 'Admin Roles', 'administer roles', 'menu', 'Roles', 92, 1, 1),
(3, 1, '2013-10-30T14:41:16.000000', 1, '2013-10-30T14:44:08.000000', 'Admin Groups', 'administer groups', 'menu', 'Groups', 94, 1, 1),
(4, 1, '2013-10-30T14:43:34.000000', 1, '2013-10-30T14:44:15.000000', 'Admin Organizations', 'administer organizations', 'menu', 'Organizations', 96, 1, 1),
(5, 1, '2013-11-01T20:27:14.000000', 1, '2014-01-21T20:36:08.000000', 'Results', 'user of statistical package', 'menu', 'Processes', 30, 1, 2),
(6, 1, '2014-01-20T17:00:00.000000', 1, '2014-01-21T20:36:08.000000', 'Data Models', 'access to Model Builder', 'menu', 'Models', 10, 1, 2),
(7, 1, '2014-01-20T17:00:00.000000', 1, '2014-01-20T17:00:00.000000', 'Permissions', 'administer permission templates', 'menu', 'Permissions', 98, 1, 1),
(8, 1, '2014-03-12T16:00:00.000000', 1, '2014-06-10T21:22:38.000000', 'Table Viewer', 'access to Table Viewer', 'button', 'TableViewer', 1001, 1, 2),
(9, 1, '2014-06-30T16:00:00.000000', 1, '2014-06-30T16:00:00.000000', 'Database Connections', 'program configuration', 'menu', 'Configurations', 84, 1, 1),
(10, 1, '2014-07-03T16:00:00.000000', 1, '2014-07-03T16:00:00.000000', 'Studies', 'tools to create Studies', 'menu', 'Studies', 20, 1, 3),
(11, 1, '2017-01-31T15:00:00.000000', 6, '2017-01-31T15:00:00.000000', 'Stat Method Configurator', 'statistical method configuration', 'button', 'Methods', 86, 1, 1),
(12, 1, '2014-07-11T17:41:00.000000', 1, '2014-07-11T17:41:00.000000', 'Filters', 'administer filters for studies', 'button', 'Filters', 1002, 1, 3),
(13, 1, '2014-07-29T15:11:11.000000', 1, '2014-07-29T15:11:11.000000', 'Delete Studies', 'allow deletion of studies', 'button', 'StudyDelete', 1003, 1, 3),
(14, 1, '2014-08-01T15:11:00.000000', 6, '2014-08-04T13:02:10.000000', 'Stored Formulas', 'create and maintain log regression formulas', 'menu', 'Logregrs', 40, 1, 4),
(15, 6, '2014-09-08T16:42:38.000000', 6, '2014-09-08T16:42:38.000000', 'Delete Processes', 'allow deletion of processes', 'button', 'ProcessDelete', 1004, 1, 1),
(16, 6, '2014-09-12T20:41:44.000000', 6, '2014-09-12T20:41:44.000000', 'Events', 'events, alerts, notifications', 'menu', 'Events', 70, 1, 5),
(17, 6, '2014-09-15T17:29:49.000000', 6, '2014-09-15T17:29:49.000000', 'Event Templates', 'administer event templates', 'menu', 'Eventtemplates', 80, 1, 5),
(18, 6, '2014-09-15T21:02:48.000000', 6, '2014-09-15T21:02:48.000000', 'Setup Alerts', 'set up alerts, receive notifications', 'menu', 'Alerts', 50, 1, 5),
(19, 6, '2014-09-17T19:37:57.000000', 6, '2014-09-17T19:37:57.000000', 'Notifications', 'view and acknowledge notifications', 'menu', 'Notifications', 60, 1, 5),
(20, 6, '2014-11-12T16:13:55.000000', 6, '2014-11-12T16:13:55.000000', 'Delete Models', 'allow deletion of Models', 'button', 'ModelDelete', 1005, 1, 1),
(21, 6, '2014-11-12T16:14:30.000000', 6, '2014-11-12T16:14:30.000000', 'Pseudo', 'become another user', 'button', 'Pseudo', 1006, 1, 1),
(22, 6, '2015-05-26T21:32:02.000000', 6, '2015-05-26T21:32:02.000000', 'Lock Model', 'lock/unlock model', 'button', 'LockModel', 1020, 1, 1),
(23, 6, '2015-08-07T13:37:39.000000', 6, '2015-08-07T13:37:39.000000', 'Move Study', 'allow to move studies to projects', 'column', 'MoveStudy', 1016, 0, 1),
(24, 6, '2015-08-07T13:38:46.000000', 6, '2015-08-07T13:38:46.000000', 'Move Model', 'allow to move model to project', 'column', 'MoveModel', 1017, 0, 1),
(25, 6, '2015-10-28T19:03:48.000000', 6, '2015-10-28T19:46:26.000000', 'Web Services', 'execute web services', 'menu', 'Webservices', 1500, 1, 1),
(26, 6, '2016-02-19T10:18:00.000000', 1, '2016-02-19T10:18:00.000000', 'Health Survey', 'collect health and patient information ', 'menu', 'HealthSurvey', 1600, 1, 7),
(27, 6, '2016-06-30T11:51:50.000000', 6, '2016-06-30T11:52:00.000000', 'Audit', 'Allow access to audit information', 'menu', 'Audits', 1010, 1, 1),
(28, 6, '2016-11-03T11:00:00.000000', 6, '2016-11-03T11:00:00.000000', 'DELTAlytics', 'DELTA Analytics Module', 'menu', 'DAM', 1510, 1, 1),
(29, 6, '2017-01-30T15:22:15.000000', 6, '2017-01-30T15:23:14.000000', 'Statistics Configurator', 'statistical package configuration', 'menu', 'Stats', 84, 1, 1),
(30, 1, '2017-03-15T10:00:00.000000', 1, '2017-03-15T10:00:00.000000', 'Log Level', 'adjust logging cerver logging level', 'button', 'Logging', 1030, 1, 1),
(31, 1, '2018-04-23T11:00:00.000000', 1, '2018-04-23T11:00:00.000000', 'Chart Admin', 'administer chart reviews', 'app', 'ChartAdmin', 1040, 1, 8),
(32, 1, '2018-04-23T11:00:00.000000', 1, '2018-04-23T11:00:00.000000', 'Chart Reviewer', 'review ambulatory charts', 'app', 'ChartReviewer', 1042, 1, 8),
(33, 1, '2018-06-28T09:46:00.000000', 1, '2018-06-28T09:46:00.000000', 'Valueset Admin', 'administer valuesets', 'app', 'ValuesetAdmin', 1044, 1, 9),
(34, 1, '2018-06-28T09:46:00.000000', 1, '2018-06-28T09:46:00.000000', 'Valueset Reviewer', 'review valuesets', 'app', 'ValuesetReviewer', 1046, 1, 9);
INSERT INTO `d3_notificationtemplate` (`idNotificationTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `config1`, `config2`, `config3`, `config4`, `type`) VALUES
(1, 1, '2014-09-17T12:00:00.000000', NULL, NULL, 'email', NULL, NULL, NULL, 'SLOW'),
(2, 1, '2014-09-17T12:00:00.000000', NULL, NULL, 'ui', NULL, NULL, NULL, 'FAST');
INSERT INTO `d3_model_table` (`idModelTable`, `name`, `description`, `physicalName`, `active`, `primaryTable`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `Model_idModel`) VALUES
(1, 'sample_data1', NULL, 'sample_data1', 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000', 1);
INSERT INTO `d3_module` (`idModule`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `parentId`, `active`, `Organization_idOrganization`) VALUES
(1, 1, '2013-10-30T10:00:00.000000', 1, '2013-10-30T12:00:00.000000', 'Administration', 'All administration tasks', 'ADMIN', '0', 1, 1),
(2, 1, '2014-01-21T12:00:00.000000', 1, NULL, 'Model Builder', 'Model Builder', 'APP', '0', 1, 1),
(3, 1, '2014-07-03T12:00:00.000000', 1, NULL, 'Study Manager', 'Module used to create Studies', 'APP', '0', 1, 1),
(4, 1, '2014-07-03T12:00:01.000000', 1, NULL, 'Logistic Regression', 'Logistic Regression Formula Builder', 'APP', '0', 1, 1),
(5, 1, '2014-09-12T12:00:00.000000', 1, NULL, 'Events', 'Events, Alerts and Notifications Module', 'ADMIN', '0', 1, 1),
(6, 1, '2014-09-25T12:00:00.000000', 1, NULL, 'Statistics Manager', 'Interaction with statistical packages, data management.', 'APP', '0', 1, 1),
(7, 1, '2016-02-19T12:00:00.000000', 1, NULL, 'Health Survey', 'External Health Survey Module running in iFrame', 'APP', '0', 1, 1),
(8, 1, '2018-04-23T10:59:00.000000', 1, NULL, 'Chart Review', 'Ambulatory Chart Review', 'APP', '0', 1, 1),
(9, 1, '2018-06-28T09:40:00.000000', 1, NULL, 'LHODE Valueset', 'LHODE Valueset Manager ', 'APP', '0', 1, 1);
INSERT INTO `d3_user_has_role` (`User_idUser`, `Role_idRole`, `idGroup`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`) VALUES
(1, 4, 0, 1, '2017-01-19T10:19:26.000000', 1, '2017-01-19T10:19:26.000000'),
(2, 1, 0, 2, '2014-01-21T16:28:17.000000', 2, '2014-01-21T16:28:17.000000'),
(3, 2, 0, 1, '2017-01-24T16:37:51.000000', 1, '2017-01-24T16:37:51.000000'),
(4, 3, 0, 1, '2017-01-24T17:30:21.000000', 1, '2017-01-24T17:30:21.000000');
INSERT INTO `d3_user` (`idUser`, `GUID`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `alias`, `password`, `passExpirationTS`, `passChangedTS`, `passFailureCount`, `passFailureMax`, `securityHandler`, `active`, `status`, `type`, `locale`, `firstName`, `lastName`, `emailAddress1`, `emailAddress2`, `uri1`, `uri2`, `graphPackage`, `phoneNumber1`, `phoneNumber2`, `alertPreference1`, `alertPreference2`, `Organization_idOrganization`, `Person_idPerson`, `Group_idGroup`) VALUES
(1, '4a48decd-232e-49cc-ae7b-0bf827de6061', 2, '2013-10-03T16:08:53.000000', 1, '2017-01-18T20:36:23.000000', 'csoadmin', 'b00f66d87bb00ca2ccb207bfa3de4110b0da88b9', '2020-10-10T00:00:00.000000', '2015-03-25T13:52:33.000000', 0, 3, '1', 1, '', 'OADMIN', 'eng_US', 'John', 'Smith', 'company@xyzsystems.com', '', '', '', 'd3', '', '', '', '', 1, 1, 1),
(2, '4a48decd-232e-49cc-ae7b-0bf827de6061', 2, '2013-12-31T13:37:29.000000', 1, '2017-01-18T20:36:44.000000', 'cssadmin', 'b00f66d87bb00ca2ccb207bfa3de4110b0da88b9', '2020-10-10T00:00:00.000000', '2015-03-25T13:51:58.000000', 0, 3, '1', 1, '', NULL, 'eng_US', 'System', 'Admin', 'cssadmin@xyzsystems.com', '', '', '', 'd3', '', '', '', '', 1, 0, 0),
(3, '9c653ac2-c5ba-4623-9f6c-6e66a28c060d', 1, '2017-01-24T16:36:49.000000', 1, '2017-01-24T16:37:25.000000', 'dbadmin', 'b00f66d87bb00ca2ccb207bfa3de4110b0da88b9', '2020-01-24T00:00:00.000000', '2017-01-25T09:04:35.000000', 0, 3, '1', 1, '', 'USER', 'eng_US', 'DB', 'Admin', '', '', '', '', 'd3', '', '', '', '', 1, 0, 0),
(4, 'd0b72650-4eda-4543-ad40-abfda11033c8', 1, '2017-01-24T17:29:37.000000', 1, '2017-01-24T17:30:01.000000', 'deltauser', 'b00f66d87bb00ca2ccb207bfa3de4110b0da88b9', '2020-01-24T00:00:00.000000', '2017-01-25T09:03:29.000000', 0, 3, '1', 1, '', 'USER', 'eng_US', 'Delta', 'User', '', '', '', '', 'd3', '', '', '', '', 1, 0, 0);
INSERT INTO `d3_sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES
('ALERT_ID', 1),
('AUDIT_ID', 1),
('CONFIGURATION_ID', 2),
('EVENTTEMPLATE_ID', 18),
('EVENT_ID', 4),
('FILTERFIELD_ID', 1),
('FILTER_ID', 1),
('GROUP_ID', 1),
('LOCATION_ID', 2),
('LOGREGRDATE_ID', 1),
('LOGREGRFIELD_ID', 1),
('LOGREGR_ID', 1),
('MATCHSET_ID', 1),
('METADATA_ID', 1),
('METHOD_ID', 30),
('MODELCOLUMN_ID', 27),
('MODELRELATIONSHIP_ID', 1),
('MODELTABLE_ID', 2),
('MODEL_ID', 2),
('NOTIFICATION_ID', 1),
('ORGANIZATION_ID', 2),
('PERMTEMPLATE_ID', 35),
('PERSON_ID', 1),
('PROCESS_ID', 1),
('ROLE_ID', 5),
('SESSION_ID', 1),
('STAT_ID', 3),
('STUDY_ID', 1),
('TASK_ID', 1),
('USER_ID', 5);
INSERT INTO `d3_role` (`idRole`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `modules`, `parentId`, `active`) VALUES
(1, 1, '2013-10-16T16:18:09.000000', 1, '2013-10-16T16:18:09.000000', 'Sys Admin', 'System Administrator', 'SADMIN', '1', 0, 1),
(2, 1, '2013-10-28T14:03:10.000000', 2, '2017-01-19T10:48:43.000000', 'Database Manager', 'Database Manager', 'USER', '1', 0, 1),
(3, 1, '2013-11-01T16:30:41.000000', 1, '2013-11-01T16:30:41.000000', 'Delta User', 'User of Delta System', 'USER', '1', 0, 1),
(4, 1, '2013-11-01T16:34:35.000000', 1, '2013-11-01T16:34:35.000000', 'Org Admin', 'Organization Administrator', 'OADMIN', '1', 0, 1),
(5, 2, '2018-04-23T11:43:38.000000', 2, '2018-05-16T13:33:09.000000', 'Chart Admin', 'Chart Review Administraor', 'OADMIN', '8', 0, 1),
(6, 2, '2018-04-23T11:44:02.000000', 2, '2018-04-23T11:44:07.000000', 'Chart Reviewer', 'Chart Reviewer', 'USER', '8', 0, 1),
(7, 2, '2018-07-27T11:45:14.000000', 2, '2018-07-27T11:45:14.000000', 'LHODE User', 'LHODE Apps User', 'USER', '9', 0, 1),
(11, 2, '2018-07-30T10:57:10.000000', 2, '2018-07-30T10:57:10.000000', 'LHODE Tester', 'tester of LHODE', 'USER', '9,', 0, 1);
INSERT INTO `d3_eventtemplate` (`idEventTemplate`, `name`, `description`, `priority`, `actionClass`, `active`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `Module_idModule`) VALUES
(1, 'Job Submit', 'Request submitted', '1', NULL, 1, 1, '2014-09-15T13:39:41.000000', 1, '2014-09-15T13:39:41.000000', 6),
(2, 'Results Ready', 'Results received', '1', NULL, 1, 1, '2014-09-15T13:52:52.000000', 1, '2014-09-15T13:52:52.000000', 6),
(3, 'Stat Method Update', 'Statistical method configuration was updated', '1', NULL, 1, 1, '2014-12-23T12:00:00.000000', 1, '2014-12-23T12:00:00.000000', 1),
(4, 'Data Source Update', 'Data Source configuration was updated', '1', NULL, 1, 1, '2014-12-23T12:00:00.000000', 1, '2014-12-23T12:00:00.000000', 1),
(5, 'Stat Method Created', 'Statistical method configuration was created', '1', NULL, 1, 1, '2015-04-23T12:00:00.000000', 1, '2015-04-23T12:00:00.000000', 1),
(6, 'Data Source Created', 'Data Source configuration was created', '1', NULL, 1, 1, '2015-04-23T12:00:00.000000', 1, '2015-04-23T12:00:00.000000', 1),
(7, 'Study Full Processing Start', 'Generate Flat Table and process study', '1', 'com.ceri.delta.server.processor.ProcessStudyAll', 1, 6, '2015-08-11T13:41:35.000000', 1, '2015-08-12T14:00:24.000000', 6),
(8, 'Study Processing Start', 'Process study', '1', 'com.ceri.delta.server.processor.ProcessStudy', 1, 1, '2015-08-13T13:38:35.000000', 1, '2015-08-13T13:38:35.000000', 6),
(9, 'Value Below Zero', 'Value of one of the results is below zero', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(10, 'Value Above Primary CI', 'Value of one of the results is above primary confidence level', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(11, 'Value Above Secondary CI', 'Value of one of the results is above secondary confidence level', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(12, 'Value Below Primary CI', 'Value of one of the results is below primary confidence level', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(13, 'Value Below Secondary CI', 'Value of one of the results is below secondary confidence level', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(14, 'Trending Up', '3 values of the results are increasing ', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(15, 'Trending Down', '3 values of the results are decreasing', '2', NULL, 1, 1, '2015-09-10T12:00:00.000000', 1, '2015-09-10T12:00:00.000000', 6),
(16, 'Stat Package Created', 'Statistical package configuration was created', '1', '', 1, 1, '2017-01-30T16:10:43.000000', 1, '2017-01-30T16:10:43.000000', 1),
(17, 'Stat Package Updated', 'Statistical Package configuration was updated', '1', '', 1, 1, '2017-01-30T16:11:40.000000', 1, '2017-01-30T16:11:40.000000', 1);
INSERT INTO `d3_alert` (`idAlert`, `name`, `description`, `priority`, `retryMax`, `timeOut`, `ackRequired`, `sendEmail`, `active`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `EventTemplate_idEventTemplate`, `User_idUser`) VALUES
(9, 'Job Submit', 'Request submitted', '1', 0, 0, 0, 0, 1, 1, '2014-09-24T10:22:29.000000', 1, '2015-10-09T15:53:26.000000', 1, 1),
(11, 'Value Below Zero', 'Value of one of the results is below zero', '2', 0, 0, 0, 0, 1, 1, '2015-10-09T15:53:10.000000', 1, '2015-10-09T16:03:34.000000', 9, 1),
(12, 'Value Above Primary CI', 'Value of one of the results is above primary confidence level', '2', 0, 0, 0, 0, 1, 1, '2015-10-09T15:54:00.000000', 1, '2015-10-09T16:03:42.000000', 10, 1),
(13, 'Value Above Secondary CI', 'Value of one of the results is above secondary confidence level', '2', 0, 0, 0, 0, 1, 1, '2015-10-09T15:54:28.000000', 1, '2015-10-09T16:03:51.000000', 11, 1),
(14, 'Value Below Secondary CI', 'Value of one of the results is below secondary confidence level', '2', 0, 0, 0, 0, 1, 1, '2015-10-09T15:54:56.000000', 1, '2015-10-09T16:03:59.000000', 13, 1),
(15, 'Value Below Zero', 'Value of one of the results is below zero', '2', 0, 0, 0, 0, 1, 1, '2015-10-09T15:56:47.000000', 1, '2015-10-09T16:04:08.000000', 9, 1),
(16, 'Value Above Secondary CI', 'Value of one of the results is above secondary confidence level', '2', 0, 0, 0, 0, 1, 1, '2015-10-09T15:57:21.000000', 1, '2015-10-09T16:04:14.000000', 11, 1);
INSERT INTO `d3_configuration` (`idConfiguration`, `name`, `description`, `type`, `connectionInfo`, `dbUser`, `dbPassword`, `active`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `Organization_idOrganization`) VALUES
(1, 'Test_Datasource', '', 'MySQL', 'jdbc:mysql://localhost:3306/d3_test_datasets?autoReconnect=true', 'd3TestUser', 'VLnhrwpDdhc=', 1, 3, '2017-01-24T16:44:35.000000', 3, '2017-01-24T16:44:35.000000', 1);
INSERT INTO `d3_method` (`idMethod`, `idOrganization`, `idStat`, `shortName`, `name`, `description`, `sequenceNumber`, `methodParams`, `active`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`) VALUES
(10, 1, 1, 'LR 2 Step', 'logisticRegression', 'Two Step Logistic Regression Analysis', 0, '[{"stepDesc":"This step creates a Logistic Regression Formula","inputs":{"chunkingPeriod":{"values":["month"],"inputText":"Initial Chunking Period","description":"Select the period to determine the LR formula","availableValues":[{"1":"month"},{"2":"quarter"},{"3":"6months"},{"4":"year"}],"type":"MultipleUnique","required":true},"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"estimationPscore":{"condition":"","values":["Logit"],"inputText":"Logistic Estimation","description":"Define how the propensity scores are estimated","availableValues":[{"1":"Logit"},{"2":"Probit"}],"type":"MultipleUnique","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Factor","required":true}},"name":"logisticRegression","stepID":2.1,"active":true,"shortName":"LR Create"},{"stepDesc":"This step analyses data using a Logistic Regression Formula created by step 1","inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","required":true},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","fieldClass":"Sequencer","required":true},"reportingPeriod":{"values":["Quarter"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"model":{"editable":false,"values":[{"name":"value0","estimate":"0.0"}],"inputText":"Model values","description":"Select all the model values for the study from the list","type":"ArrayList<String>","fieldClass":"lrValue","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"studyStartDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study Start Date","description":"Start Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"alphaError":{"values":[0.05],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"studyEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study End Date","description":"End Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true}},"name":"logisticRegressionAnalysis","stepID":2.2,"active":true,"shortName":"LR Analysis"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(11, 1, 1, 'LR Create', 'logisticRegressionCreate', 'Logistic Regression Model', 10, '[{"stepDesc":"This step creates a Logistic Regression Formula","inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"This defines a field in the data that uniquely identifies each case. DA will not validate job where this field doesn\'t uniquely identify cases in the data","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"estimationPscore":{"condition":"","values":["Logit"],"inputText":"Logistic Estimation","description":"Define how the propensity scores are estimated","availableValues":[{"1":"Logit"},{"2":"Probit"}],"type":"MultipleUnique","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list. This is used to generate a model as the DV","type":"string","fieldClass":"Outcome","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list, these covariates are required to generate a model. DA will only validate run if at least one IV is provided.","type":"ArrayList<String>","fieldClass":"Risk Factor","required":true}},"name":"logisticRegression","stepID":1.1,"active":true,"shortName":"LR Create"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(12, 1, 1, 'LR Apply', 'manualLogisticRegression', 'Logistic Regression Analysis with prepopulated model', 20, '[{"stepDesc":"This step analyses data using an existing Logistic Regression Formula","inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","required":true},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","fieldClass":"Sequencer","required":false},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"model":{"values":[{"name":"value0","estimate":"0.0"}],"name":"","inputText":"Model values","description":"Select all the model values for the study from the list","id":"","type":"ArrayList<String>","fieldClass":"lrValue","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"studyStartDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study Start Date","description":"Start Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"alphaError":{"values":[0.05],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"studyEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study End Date","description":"End Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true}},"name":"manualLogisticRegressionAnalysis","stepID":2.2,"active":true,"shortName":"LR Apply"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(13, 1, 1, 'LR Combined', 'logisticRegressionCombined', 'NOT YET FULLY IMPLEMENTED', 30, '[{"inputs":{"rollingWindowEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Rolling Window End Date","description":"Enter End Date for the rolling window","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","required":true},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","fieldClass":"Sequencer","required":false},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"alphaError":{"values":[0.05],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"studyEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study End Date","description":"End Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"rollingWindowDumpWindowTimeUnit":{"values":["Month"],"inputText":"Scoring Window period","description":"This is the window to carve off each time a rolling scoring is done.","availableValues":[{"1":"Year"},{"2":"Month"},{"3":"Week"},{"4":"Quarter"},{"5":"Day"}],"type":"MultipleUnique","required":true},"rollingWindowDumpNumberUnits":{"values":[1],"inputText":"Scoring Window units","description":"Enter the number of number of windows to dump each rolling window cycle.","type":"Integer","required":true},"rollingWindowUse":{"values":[false],"inputText":"Use Rolling Window","description":"Set to use rolling window for scoring the model","type":"boolean","fieldClass":"Parameter","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"rollingWindowScoringWindow":{"values":["Month"],"inputText":"Scoring Window period","description":"This is the window against which scoring is performed in a rolling window.","availableValues":[{"1":"Year"},{"2":"Month"},{"3":"Week"},{"4":"Quarter"},{"5":"Day"}],"type":"MultipleUnique","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"studyStartDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study Start Date","description":"Start Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Factor","required":true},"rollingWindowScoringWindowUnits":{"values":[1],"inputText":"Scoring Window units","description":"Enter the number of windows in the scoring cycle.","type":"Integer","required":true}},"name":"logisticRegressionCombinedAnalysis","stepID":"4.1","active":true,"shortName":"LR Combined"}]', 0, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(14, 1, 1, 'Propensity', 'propensityScoreAnalysis', 'Propensity Score Analysis', 40, '[{"stepDesc":"This step does a full PSM analysis by creating a model, matching and analyzing outcomes","inputs":{"matchOrder":{"condition":"","values":["RANDOM ORDER"],"inputText":"Exposed Match Order","description":"Define how the exposed are ordered prior to matching","availableValues":[{"1":"RANDOM ORDER"},{"2":"RAREST"}],"type":"MultipleUnique","required":true},"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","required":true},"sequencingVariableSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Sequencing Variable","description":"Define a date field used to define matching and reporting periods","type":"string","required":true},"reportingPeriod":{"values":["Quarter"],"inputText":"Reporting Period","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"duplicatePscores":{"condition":"","values":[true],"inputText":"Allow duplicate propensity score values","description":"if true test for control observations with duplicate propensity score values.","type":"boolean","required":true},"matchType":{"condition":"","values":["CLOSEST"],"inputText":"Match Type","description":"Define how the unexposed match is selected","availableValues":[{"1":"CLOSEST"},{"2":"RANDOM"}],"type":"MultipleUnique","required":true},"commonSupport":{"condition":"","values":[false],"inputText":"Impose a Common Support","description":"imposes a common support by dropping treatment observations whose pscore is higher than the maximum or less than the minimum pscore of the controls","type":"boolean","required":true},"alphaError":{"condition":"","values":[0.05],"inputText":"Alpha Error","description":"Define the Alpha error","type":"Double","required":true},"caliper":{"condition":"maximum value allowed is 1.0","values":[0.2],"inputText":"Caliper Width/SD Proportion","description":"when a proportion of the SD is used a default of 0.2 is used","type":"Double","required":true},"dataSplitOption":{"condition":"","values":["Quarter"],"inputText":"Match Within Period","description":"Define a period within which match occurs","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"condition":"","values":["value0"],"inputText":"Exposure Variable","description":"Define an exposure variable","type":"string","fieldClass":"Treatment","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"estimationPscore":{"condition":"","values":["Logit"],"inputText":"Estimation of Propensity Score","description":"Define how the propensity scores are estimated","availableValues":[{"1":"Logit"},{"2":"Probit"}],"type":"MultipleUnique","required":true},"outcomeVariableSelection":{"condition":"Only variables identified as outcomes, the exposure variable can\'t be added here","values":["value0"],"inputText":"Outcome Variables","description":"Select the outcome variable(s) for the study from the list","type":"ArrayList<String>","fieldClass":"Outcome","required":true},"alphaSpending":{"condition":"","values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"independentVariableSelection":{"condition":"","values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Factor","required":true},"caliperType":{"condition":"","values":["Proportion of Logit SD"],"inputText":"Caliper Type","description":"Select the type of caliper-a user defined width or a proportion of the logit standard deviation","availableValues":[{"1":"User Defined Width"},{"2":"Proportion of Logit SD"}],"type":"MultipleUnique","required":true},"matchNumber":{"values":[1],"inputText":"Number of Neighbors","description":"Number of matches between exposed and unexposed","type":"integer","required":true},"replaceMatches":{"condition":"","values":[false],"inputText":"Replace Matches","description":"If yes, a match will be placed back in the match pool","type":"boolean","required":true}},"name":"propensityScoreAnalysis","stepID":5.1,"active":true,"shortName":"Propensity"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(15, 1, 1, 'PA Create', 'propensityAnalysisCreate', 'Logit Model', 50, '[{"stepDesc":"this method creates a logit model","inputs":{"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"exposureVariableSelection":{"condition":"","values":["value0"],"inputText":"Exposure Variable","description":"Define an exposure variable","type":"string","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"estimationPscore":{"condition":"","values":["Logit"],"inputText":"Logistic Estimation","description":"Define how the propensity scores are estimated","availableValues":[{"1":"Logit"},{"2":"Probit"}],"type":"MultipleUnique","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Factor","required":true}},"name":"propensityAnalysisCreate","stepID":6.1,"active":true,"shortName":"Propensity Create"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(16, 1, 1, 'PA Match', 'manualPropensityAnalysis', 'Apply a model to a dataset to estimate pscores', 60, '[{"stepDesc":"this method applies a formula to a dataset and generates propensity scores","inputs":{"matchOrder":{"condition":"","values":["RANDOM ORDER"],"inputText":"Exposed Match Order","description":"Define how the exposed are ordered prior to matching","availableValues":[{"1":"RANDOM ORDER"},{"2":"RAREST"}],"type":"MultipleUnique","required":true},"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","required":true},"reportingPeriod":{"values":["Quarter"],"inputText":"Reporting Period","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"duplicatePscores":{"condition":"","values":[true],"inputText":"Allow duplicate propensity score values","description":"if true test for control observations with duplicate propensity score values.","type":"boolean","required":true},"matchType":{"condition":"","values":["CLOSEST"],"inputText":"Match Type","description":"Define how the unexposed match is selected","availableValues":[{"1":"CLOSEST"},{"2":"RANDOM"}],"type":"MultipleUnique","required":true},"commonSupport":{"condition":"","values":[false],"inputText":"Impose a Common Support","description":"imposes a common support by dropping treatment observations whose pscore is higher than the maximum or less than the minimum pscore of the controls","type":"boolean","required":true},"alphaError":{"condition":"","values":[0.05],"inputText":"Alpha Error","description":"Define the Alpha error","type":"Double","required":true},"caliper":{"condition":"maximum value allowed is 1.0","values":[0.2],"inputText":"Caliper Width/SD Proportion","description":"when a proportion of the SD is used a default of 0.2 is used","type":"Double","required":true},"dataSplitOption":{"condition":"","values":["Quarter"],"inputText":"Match Within Period","description":"Define a period within which match occurs","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"condition":"","values":["value0"],"inputText":"Exposure Variable","description":"Define an exposure variable","type":"string","required":true},"alphaSpending":{"condition":"","values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"model":{"values":[{"name":"value0","estimate":"0.0"}],"name":"","inputText":"Model values","description":"Select all the model values for the study from the list","id":"","type":"ArrayList<String>","fieldClass":"lrValue","required":true},"caliperType":{"condition":"","values":["Proportion of Logit SD"],"inputText":"Caliper Type","description":"Select the type of caliper-a user defined width or a proportion of the logit standard deviation","availableValues":[{"1":"User Defined Width"},{"2":"Proportion of Logit SD"}],"type":"MultipleUnique","required":true},"matchNumber":{"values":[1],"inputText":"Number of Neighbors","description":"Number of matches between exposed and unexposed","type":"Double","required":true},"replaceMatches":{"condition":"","values":[false],"inputText":"Replace Matches","description":"If yes, a match will be placed back in the match pool","type":"boolean","required":true}},"name":"manualPropensityAnalysis","stepID":7.1,"active":true,"shortName":"Propensity Apply"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(17, 1, 1, 'PA Outcomes', 'propensityScoreOutcomes', 'Propensity Score Matching Outcome analysis using a matchset', 70, '[{"stepDesc":"outcome analysis","inputs":{"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","required":true},"sequencingVariableSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Sequencing Variable","description":"Define a date field used to define matching and reporting periods","type":"string","required":true},"reportingPeriod":{"values":["Quarter"],"inputText":"Reporting Period","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"outcomeVariableSelection":{"condition":"Only variables identified as outcomes, the exposure variable can\'t be added here","values":["value0"],"inputText":"Outcome Variables","description":"Select the outcome variable(s) for the study from the list","type":"ArrayList<String>","fieldClass":"Outcome","required":true},"alphaSpending":{"condition":"","values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"alphaError":{"condition":"","values":[0.05],"inputText":"Alpha Error","description":"Define the Alpha error","type":"Double","required":true},"matchSet":{"condition":"","values":["value0"],"inputText":"Matched Dataset","description":"Provide a matched dataset","type":"String","required":true}},"name":"propensityScoreOutcomes","stepID":11.1,"active":true,"shortName":"Propensity"}]', 1, 1, '2017-10-26T09:34:10.000000', 1, '2017-10-26T09:34:10.000000'),
(18, 1, 1, 'UnMatched Survival', 'survivalAnalysisUnmatched', 'Implements unmatched survival analysis', 80, '[{"stepDesc":"This step implements unmatched survival analysis","inputs":{"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","required":true},"sequencingVariableSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Sequencing Variable","description":"Define a date field used to define matching and reporting periods","type":"string","required":true},"exposureVariableSelection":{"condition":"","values":["value0"],"inputText":"Exposure Variable","description":"Define an exposure variable","type":"String","required":true},"exposureDateSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Exposure Date","description":"Define a date field used for the exposure date","type":"string","required":true},"outcomeVariableSelection":{"condition":"Only variables identified as outcomes, the exposure variable can\'t be added here","values":["value0"],"inputText":"Outcome Variables","description":"Select the outcome variable for the study from the list","type":"String","required":true},"outcomeDateSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Outcome Date","description":"Define a date field used outcome date","type":"string","required":true},"durationType":{"condition":"","values":["Month"],"inputText":"Duration Type","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true}},"name":"sva1","stepID":9.1,"active":true,"shortName":"UnMatched survival"}]', 1, 1, '2017-10-26T09:34:11.000000', 1, '2017-10-26T09:34:11.000000'),
(19, 1, 1, 'Matched Survival', 'survivalAnalysisMatched', 'Full PSM analysis: creating a model, matching and analyzing outcomes', 90, '[{"stepDesc":"This step does a full PSM analysis by creating a model, matching and analyzing outcomes","inputs":{"matchOrder":{"condition":"","values":["RANDOM ORDER"],"inputText":"Exposed Match Order","description":"Define how the exposed are ordered prior to matching","availableValues":[{"1":"RANDOM ORDER"},{"2":"RAREST"}],"type":"MultipleUnique","required":true},"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","required":true},"sequencingVariableSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Sequencing Variable","description":"Define a date field used to define matching and reporting periods","type":"string","required":true},"reportingPeriod":{"values":["Quarter"],"inputText":"Reporting Period","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"exposureDateSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Exposure Date","description":"Define a date field used for the exposure date","type":"string","required":true},"duplicatePscores":{"condition":"","values":[true],"inputText":"Allow duplicate propensity score values","description":"if true test for control observations with duplicate propensity score values.","type":"boolean","required":true},"matchType":{"condition":"","values":["CLOSEST"],"inputText":"Match Type","description":"Define how the unexposed match is selected","availableValues":[{"1":"CLOSEST"},{"2":"RANDOM"}],"type":"MultipleUnique","required":true},"outcomeDateSelection":{"condition":"this field must be a valid date field","values":["value0"],"inputText":"Outcome Date","description":"Define a date field used outcome date","type":"string","required":true},"commonSupport":{"condition":"","values":[false],"inputText":"Impose a Common Support","description":"imposes a common support by dropping treatment observations whose pscore is higher than the maximum or less than the minimum pscore of the controls","type":"boolean","required":true},"alphaError":{"condition":"","values":[0.05],"inputText":"Alpha Error","description":"Define the Alpha error","type":"Double","required":true},"durationType":{"condition":"","values":["Month"],"inputText":"Survival Duration Type","description":"Select the period type for the study from the list","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"caliper":{"condition":"maximum value allowed is 1.0","values":[0.2],"inputText":"Caliper Width/SD Proportion","description":"when a proportion of the SD is used a default of 0.2 is used","type":"Double","required":true},"dataSplitOption":{"condition":"","values":["Quarter"],"inputText":"Match Within Period","description":"Define a period within which match occurs","availableValues":[{"1":"Week"},{"2":"Month"},{"3":"Quarter"},{"4":"Half"},{"5":"Year"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"condition":"","values":["value0"],"inputText":"Exposure Variable","description":"Define an exposure variable","type":"string","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"estimationPscore":{"condition":"","values":["Logit"],"inputText":"Estimation of Propensity Score","description":"Define how the propensity scores are estimated","availableValues":[{"1":"Logit"},{"2":"Probit"}],"type":"MultipleUnique","required":true},"outcomeVariableSelection":{"condition":"Only variables identified as outcomes, the exposure variable can\'t be added here","values":["value0"],"inputText":"Outcome Variables","description":"Select the outcome variable for the study from the list","type":"String","required":true},"alphaSpending":{"condition":"","values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"independentVariableSelection":{"condition":"","values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","required":true},"caliperType":{"condition":"","values":["Proportion of Logit SD"],"inputText":"Caliper Type","description":"Select the type of caliper-a user defined width or a proportion of the logit standard deviation","availableValues":[{"1":"User Defined Width"},{"2":"Proportion of Logit SD"}],"type":"MultipleUnique","required":true},"matchNumber":{"values":[1],"inputText":"Number of Neighbors","description":"Number of matches between exposed and unexposed","type":"Double","required":true},"replaceMatches":{"condition":"","values":[false],"inputText":"Replace Matches","description":"If yes, a match will be placed back in the match pool","type":"boolean","required":true}},"name":"svam","stepID":5.1,"active":true,"shortName":"Matched Survival"}]', 1, 1, '2017-10-26T09:34:11.000000', 1, '2017-10-26T09:34:11.000000'),
(20, 1, 1, 'RA-SPRT', 'riskAdjustedSPRT', 'Risk Adjusted SPRT', 100, '[{"stepDesc":"","inputs":{"studyUniqueId":{"condition":"Each case must be uniquely identified by this field","values":["value0"],"inputText":"Unique Case Identifier","description":"Select the unique id for the study","type":"String","required":true},"sequencingVariableSelection":{"condition":"","values":["value0"],"inputText":"Sequencing Variable","description":"Select the sequencing variable for the study from the list","type":"string","required":true},"automaticVariableSelection":{"values":["None"],"inputText":"Use Automatic Variable Selection (AVS)","description":"Set to on to use Automatic Variable Selection","availableValues":[{"1":"None"},{"2":"Lasso Regression"}],"type":"MultipleUnique","fieldClass":"Parameter","required":true},"outcomeVariableSelection":{"condition":"Only variables identified as outcomes, the exposure variable can\'t be added here","values":["value0"],"inputText":"Outcome Variables","description":"Select the outcome variable(s) for the study from the list","type":"String","required":true},"oddsRatio":{"condition":"","values":[0],"inputText":"Odds Ratio","description":"Enter odds ratio for the study","type":"Double","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","required":true},"alphaError":{"condition":"","values":[0.05],"inputText":"Alpha Error","description":"Enter alpha error for the study","type":"Double","required":true},"betaError":{"condition":"","values":[0.05],"inputText":"Beta Error","description":"Enter beta error for the study","type":"Double","required":true},"numberPeriods":{"condition":"","values":[0],"inputText":"Number of Periods","description":"Number of periods returned in result set","type":"Integer","required":true}},"name":"riskAdjustedSPRT","stepID":8.1,"active":true,"shortName":"Risk Adjusted SPRT"}]', 1, 1, '2017-10-26T09:34:11.000000', 1, '2017-10-26T09:34:11.000000'),
(21, 1, 2, 'LR Create', 'logisticRegressionCreate', 'NOT YET FULLY IMPLEMENTED', 0, '[{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":false},"automaticVariableSelection":{"values":[false],"inputText":"Use Automatic Variable Selection","description":"Set to on to use Automatic Variable Selection","type":"boolean","fieldClass":"Parameter","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Adjustment","required":true}},"name":"logisticRegression","active":true}]', 0, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(22, 1, 2, 'LR 2 Step', 'logisticRegression', 'Two step Logistic Regression', 10, '[{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":false},"automaticVariableSelection":{"values":[false],"inputText":"Use Automatic Variable Selection","description":"Set to on to use Automatic Variable Selection","type":"boolean","fieldClass":"Parameter","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Adjustment","required":true}},"name":"logisticRegression","active":true},{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","required":false},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","fieldClass":"Sequencer","required":false},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"model":{"values":[{"name":"value0","estimate":"0.0"}],"inputText":"Model values","description":"Select all the model values for the study from the list","type":"ArrayList<String>","fieldClass":"lrValue","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"studyStartDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study Start Date","description":"Start Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"alphaError":{"values":[0],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"studyEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study End Date","description":"End Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true}},"name":"logisticRegressionAnalysis","active":true}]', 1, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(23, 1, 2, 'LR Apply', 'manualLogisticRegression', 'Logistic Regression Analysis with prepopulated model', 20, '[{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","required":false},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","fieldClass":"Sequencer","required":false},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"model":{"values":[{"name":"value0","estimate":"0.0"}],"name":"","inputText":"Model values","description":"Select all the model values for the study from the list","id":"","type":"ArrayList<String>","fieldClass":"lrValue","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"studyStartDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study Start Date","description":"Start Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"alphaError":{"values":[0],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"studyEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study End Date","description":"End Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true}},"name":"manualLogisticRegressionAnalysis","active":true}]', 1, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(24, 1, 2, 'LR Combined', 'logisticRegressionCombined', 'NOT YET FULLY IMPLEMENTED', 30, '[{"inputs":{"rollingWindowEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Rolling Window End Date","description":"Enter End Date for the rolling window","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","required":false},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","fieldClass":"Sequencer","required":false},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"alphaError":{"values":[0],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"studyEndDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study End Date","description":"End Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"rollingWindowDumpWindowTimeUnit":{"values":["Month"],"inputText":"Scoring Window period","description":"This is the window to carve off each time a rolling scoring is done.","availableValues":[{"1":"Year"},{"2":"Month"},{"3":"Week"},{"4":"Quarter"},{"5":"Day"}],"type":"MultipleUnique","required":true},"rollingWindowDumpNumberUnits":{"values":[1],"inputText":"Scoring Window units","description":"Enter the number of number of windows to dump each rolling window cycle.","type":"Integer","required":true},"rollingWindowUse":{"values":[false],"inputText":"Use Rolling Window","description":"Set to use rolling window for scoring the model","type":"boolean","fieldClass":"Parameter","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"rollingWindowScoringWindow":{"values":["Month"],"inputText":"Scoring Window period","description":"This is the window against which scoring is performed in a rolling window.","availableValues":[{"1":"Year"},{"2":"Month"},{"3":"Week"},{"4":"Quarter"},{"5":"Day"}],"type":"MultipleUnique","required":true},"automaticVariableSelection":{"values":[false],"inputText":"Use Automatic Variable Selection","description":"Set to on to use Automatic Variable Selection","type":"boolean","fieldClass":"Parameter","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"studyStartDate":{"values":["Y-m-d H:i:s.u"],"inputText":"Study Start Date","description":"Start Date for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Adjustment","required":true},"rollingWindowScoringWindowUnits":{"values":[1],"inputText":"Scoring Window units","description":"Enter the number of windows in the scoring cycle.","type":"Integer","required":true}},"name":"logisticRegressionCombinedAnalysis","active":true}]', 0, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(25, 1, 2, 'Propensity', 'propensityScoreAnalysis', 'Propensity Score Analysis', 40, '[{"inputs":{"matchOrder":{"values":["value0"],"inputText":"Match Order","description":"Type of the match between exposed and unexposed","availableValues":[{"1":"RANDOMORDER"}],"type":"MultipleUnique","required":true},"studyUniqueId":{"values":["value0"],"inputText":"UniqueID","description":"Select the unique id for the study","type":"String","required":false},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","required":true},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"matchType":{"values":["value0"],"inputText":"Match Type","description":"Type of the match between exposed and unexposed","availableValues":[{"1":"CLOSEST"},{"2":"RANDOM"}],"type":"MultipleUnique","required":true},"calipers":{"values":[0],"inputText":"Calipers","description":"Enter the calipers used to adjust matching value.","type":"Double","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"DependentVariable","description":"Select the dependent variable for the study from the list","type":"string","required":true},"alphaError":{"values":[0],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"rollingWindowUse":{"values":[false],"inputText":"Use Rolling Window","description":"Set to use rolling window for scoring the model","type":"boolean","fieldClass":"Parameter","required":true},"dataSplitOption":{"values":["value0"],"inputText":"SplitOption","description":"Select the split option for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"IndependentVariables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","required":true},"caliperType":{"values":["value0"],"inputText":"CaliperType","description":"Select the type of caliper from the available values","availableValues":[{"1":"EXPOSUREPROBABILITY"},{"2":"LOGITSD"}],"type":"MultipleUnique","required":true},"matchNumber":{"values":[0],"inputText":"NumberofMatches","description":"Number of matches between exposed and unexposed","type":"Double","required":true}},"name":"propensityScoreAnalysis","active":true}]', 1, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(26, 1, 2, 'PA Create', 'propensityAnalysisCreate', 'NOT YET FULLY IMPLEMENTED', 50, '[{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":false},"automaticVariableSelection":{"values":[false],"inputText":"Use Automatic Variable Selection","description":"Set to on to use Automatic Variable Selection","type":"boolean","fieldClass":"Parameter","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","fieldClass":"Outcome","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","fieldClass":"Risk Adjustment","required":true}},"name":"propensityAnaysisCreate","active":true}]', 0, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(27, 1, 2, 'PA Apply', 'manualPropensityAnalysis', 'NOT YET FULLY IMPLEMENTED', 60, '[{"inputs":{"matchOrder":{"values":["value0"],"inputText":"Match Order","description":"Type of the match between exposed and unexposed","availableValues":[{"1":"RANDOMORDER"},{"2":"RAREST"}],"type":"MultipleUnique","required":true},"studyUniqueId":{"values":["value0"],"inputText":"UniqueID","description":"Select the unique id for the study","type":"String","required":false},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","required":true},"reportingPeriod":{"values":["value0"],"inputText":"Period type","description":"Select the period type for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"matchType":{"values":["value0"],"inputText":"Match Type","description":"Type of the match between exposed and unexposed","availableValues":[{"1":"CLOSEST"},{"2":"RANDOM"}],"type":"MultipleUnique","required":true},"calipers":{"values":[0],"inputText":"Calipers","description":"Enter the calipers used to adjust matching value.","type":"Double","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"DependentVariable","description":"Select the dependent variable for the study from the list","type":"string","required":true},"alphaError":{"values":[0],"inputText":"Alpha Error","description":"Alpha error for Primary Confidence Interval","type":"Double","required":true},"rollingWindowUse":{"values":[false],"inputText":"Use Rolling Window","description":"Set to use rolling window for scoring the model","type":"boolean","fieldClass":"Parameter","required":true},"dataSplitOption":{"values":["value0"],"inputText":"SplitOption","description":"Select the split option for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"alphaSpending":{"values":[true],"inputText":"Alpha Spending","description":"Use Alpha Spending to calculate Secondary Confidence Interval","type":"boolean","fieldClass":"Parameter","required":true},"model":{"values":[{"name":"value0","estimate":"0.0"}],"name":"","inputText":"Model values","description":"Select all the model values for the study from the list","id":"","type":"ArrayList<String>","fieldClass":"lrValue","required":true},"caliperType":{"values":["value0"],"inputText":"CaliperType","description":"Select the type of caliper from the available values","availableValues":[{"1":"EXPOSUREPROBABILITY"},{"2":"LOGITSD"}],"type":"MultipleUnique","required":true},"matchNumber":{"values":[0],"inputText":"NumberofMatches","description":"Number of matches between exposed and unexposed","type":"Double","required":true}},"name":"manualPropensityAnalysis","active":true}]', 0, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(28, 1, 2, 'SPRT', 'riskAdjustedSPRT', 'NOT YET FULLY IMPLEMENTED', 70, '[{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","validationRegEx":"<optional validation for permissible values for client-level validation>","required":true},"sequencingVariableSelection":{"values":["value0"],"inputText":"Sequencing Variable","description":"Select the sequencing variable for the study from the list","type":"string","required":true},"rollingWindowTimeUnit":{"values":["Month"],"inputText":"Training Window period","description":"This is the window against which scoring is performed in a rolling window.","availableValues":[{"1":"Year"},{"2":"Month"},{"3":"Week"},{"4":"Quarter"},{"5":"Day"}],"type":"MultipleUnique","required":true},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","required":true},"alphaError":{"values":[0],"inputText":"Alpha Error","description":"Enter alpha error for the study","type":"Double","required":true},"numberPeriods":{"values":[100],"inputText":"Number of Periods","description":"Number of periods returned in result set","type":"Integer","required":true},"rollingWindowUnitQuantity":{"values":[1],"inputText":"Training Window units","description":"Enter the number of windows in the training cycle.","type":"Integer","required":true},"rollingWindowUse":{"values":[false],"inputText":"Use Rolling Window","description":"Set to use rolling window for scoring the model","type":"boolean","fieldClass":"Parameter","required":true},"dataSplitOption":{"values":["value0"],"inputText":"Split Option","description":"Select the split option for the study from the list","availableValues":[{"1":"SingleSplit"},{"2":"Half"},{"3":"Year"},{"4":"Month"},{"5":"Week"},{"6":"Quarter"},{"7":"MultiSplit"}],"type":"MultipleUnique","required":true},"automaticVariableSelection":{"values":[false],"inputText":"Use Automatic Variable Selection","description":"Set to on to use Automatic Variable Selection","type":"boolean","fieldClass":"Parameter","required":true},"oddsRatio":{"values":[0],"inputText":"Odds Ratio","description":"Enter odds ratio for the study","type":"Double","required":true},"independentVariableSelection":{"values":["value0"],"inputText":"Independent Variables","description":"Select all the independent variables for the study from the list","type":"ArrayList<String>","required":true},"betaError":{"values":[0],"inputText":"Beta Error","description":"Enter beta error for the study","type":"Double","required":true}},"name":"riskAdjustedSPRT","active":true}]', 0, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000'),
(29, 1, 2, 'Survival', 'survivalAnalysis', 'NOT YET FULLY IMPLEMENTED', 80, '[{"inputs":{"studyUniqueId":{"values":["value0"],"inputText":"Unique ID","description":"Select the unique id for the study","type":"String","fieldClass":"Case ID","validationRegEx":"<optional validation for permissible values for client-level validation>","required":false},"sequencingVariableSelection":{"values":["value0"],"inputText":"SequencingVariable","description":"Select the sequencing variable for the study from the list","type":"string","required":false},"stop":{"values":[],"inputText":"Stop value for time","description":"Stop value for time","type":"String","fieldClass":"Parameter","required":true},"exposureVariableSelection":{"values":["value0"],"inputText":"ExposureVariable","description":"Select the exposure variable for the study from the list","type":"string","required":true},"start":{"values":[],"inputText":"Start value for time","description":"Start value for time","type":"String","fieldClass":"Parameter","required":false},"dependentVariableSelection":{"values":["value0"],"inputText":"Dependent Variable","description":"Select the dependent variable for the study from the list","type":"string","required":true}},"name":"survivalAnalysis","active":true}]', 0, 1, '2017-10-26T09:34:45.000000', 1, '2017-10-26T09:34:45.000000');
INSERT INTO `d3_model_column` (`idModelColumn`, `name`, `description`, `physicalName`, `type`, `defaultValue`, `defaultValueType`, `keyField`, `insertable`, `atomic`, `sub`, `virtualField`, `virtualOrder`, `rollup`, `formula`, `verified`, `fieldClass`, `fieldKind`, `active`, `ModelTable_idModelTable`, `Model_idModel`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`) VALUES
(1, 'married', '', 'married', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(2, 'gender_x', '', 'gender_x', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(3, 'risk1_enum', '', 'risk1_enum', 'INT(11)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(4, 'risk2_enum', '', 'risk2_enum', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(5, 'risk3_enum', '', 'risk3_enum', 'INT(11)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(6, 'age', '', 'age', 'INT(11)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(7, 'duration', '', 'duration', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(8, 'treatment1', '', 'treatment1', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Treatment', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(9, 'treatment2', '', 'treatment2', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Treatment', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(10, 'duration_gt24', '', 'duration_gt24', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(11, 'race1', '', 'race1', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(12, 'race2', '', 'race2', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(13, 'race3', '', 'race3', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(14, 'race4', '', 'race4', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(15, 'insurance1', '', 'insurance1', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(16, 'insurance2', '', 'insurance2', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(17, 'insurance3', '', 'insurance3', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(18, 'insurance4', '', 'insurance4', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(19, 'risk4_dich', '', 'risk4_dich', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(20, 'risk5_dich', '', 'risk5_dich', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(21, 'risk6_dich', '', 'risk6_dich', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(22, 'month', '', 'month', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Risk Factor', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(23, 'outcome1', '', 'outcome1', 'SMALLINT(6)', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Outcome', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(24, 'caseid', '', 'caseid', 'VARCHAR(50)', NULL, NULL, 1, 1, 1, 0, 0, 0, 0, '', 0, 'Case ID', 'Continuous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(25, 'durationgte12', 'duration >=12', 'virtual', 'INTEGER(11)', NULL, NULL, 0, 1, 0, 0, 1, 0, 0, 'duration>=12', 1, 'Risk Factor', 'Dichotomous', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000'),
(26, 'ctrdate', '', 'ctrdate', 'DATE', NULL, NULL, 0, 1, 1, 0, 0, 0, 0, '', 0, 'Sequencer', 'Date', 1, 1, 1, 3, '2017-01-24T16:49:21.000000', 3, '2017-01-24T16:49:21.000000');
INSERT INTO `d3_model` (`idModel`, `name`, `description`, `idConfiguration`, `outputName`, `status`, `isAtomicFinished`, `isMissingFinished`, `isVirtualFinished`, `isSecondaryVirtualFinished`, `processingId`, `locked`, `active`, `Organization_idOrganization`, `idGroup`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`) VALUES
(1, 'DELTA3_SampleModel', 'DELTA3 Sample Model for Users', 1, 'ft1484858971815', 'Data Model imported', 0, 0, 0, 0, '2017-01-23 14:27:40.491', 0, 1, 1, 0, 2, '2017-01-24T16:49:21.000000', 2, '2017-01-24T16:52:01.000000');
INSERT INTO `d3_location` (`idLocation`, `GUID`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `address1`, `address2`, `state`, `county`, `country`, `zip`, `longitude`, `latitude`, `phoneNumber1`, `phoneNumber2`, `active`, `Organization_idOrganization`) VALUES
(1, '0', 1, '2013-10-03T14:53:12.000000', 0, '2013-10-03T14:53:12.000000', 'Empty', 'Empty', 'Empty', 'Empty', 'Empty', 'Empty', 'Empty', '00000', NULL, NULL, NULL, NULL, 1, 1);
ALTER TABLE `d3_event` ADD CONSTRAINT `fk_Event_EventTemplae1` FOREIGN KEY (`EventTemplate_idEventTemplate`) REFERENCES `d3_eventtemplate`(`idEventTemplate`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_eventtemplate` ADD CONSTRAINT `fk_EventTemplae_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `d3_module`(`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_filter_has_field` ADD CONSTRAINT `fk_Filter_has_Field_Filter1` FOREIGN KEY (`Filter_idFilter`) REFERENCES `d3_filter`(`idFilter`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_group_has_user` ADD CONSTRAINT `fk_Group_has_User_Group1` FOREIGN KEY (`Group_idGroup`) REFERENCES `d3_group_table`(`idGroup`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_group_has_user` ADD CONSTRAINT `fk_Group_has_User_User1` FOREIGN KEY (`User_idUser`) REFERENCES `d3_user`(`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_location` ADD CONSTRAINT `fk_Location_Organization1` FOREIGN KEY (`Organization_idOrganization`) REFERENCES `d3_organization`(`idOrganization`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_module` ADD CONSTRAINT `fk_Module_Organization1` FOREIGN KEY (`Organization_idOrganization`) REFERENCES `d3_organization`(`idOrganization`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_permission` ADD CONSTRAINT `fk_Permission_Role1` FOREIGN KEY (`Role_idRole`) REFERENCES `d3_role`(`idRole`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_permissiontemplate` ADD CONSTRAINT `fk_PermissionTemplate_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `d3_module`(`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_person` ADD CONSTRAINT `fk_Person_Location1` FOREIGN KEY (`Location_idLocation`) REFERENCES `d3_location`(`idLocation`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_study_has_category` ADD CONSTRAINT `fk_Study_has_Category_Category1` FOREIGN KEY (`ModelColumn_idModelColumn`) REFERENCES `d3_model_column`(`idModelColumn`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_study_has_category` ADD CONSTRAINT `fk_Study_has_Category_Study1` FOREIGN KEY (`Study_idStudy`) REFERENCES `d3_study`(`idStudy`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_subscriber` ADD CONSTRAINT `fk_Subscriber_User1` FOREIGN KEY (`User_idUser`) REFERENCES `d3_user`(`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_user` ADD CONSTRAINT `fk_User_Organization1` FOREIGN KEY (`Organization_idOrganization`) REFERENCES `d3_organization`(`idOrganization`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_user_has_role` ADD CONSTRAINT `fk_User_has_Role_Role1` FOREIGN KEY (`Role_idRole`) REFERENCES `d3_role`(`idRole`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d3_user_has_role` ADD CONSTRAINT `fk_User_has_Role_User1` FOREIGN KEY (`User_idUser`) REFERENCES `d3_user`(`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION

;/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
