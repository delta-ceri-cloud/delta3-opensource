The Data Evaluation and Longitudinal Trend Analysis system, in its third iteration, 
DELTA version 3.00, is developed as a fully open-source safety surveillance system designed to provide linking to source data, 
configuration, analysis and visualization for surveillance studies.  
It is a multi-tenant system which means that it handles a separation of data and users between multiple organizations, 
each with independent site and user administration.
Database connections allow the site administrator to map to multiple data stores as a central data repository separate from system files. 

DELTA is an open source project but at the moment direct contributions to the source code are not enabled. Feedback can be submitted through the UI on each page.

DELTA was developed using Oracle NetBeans 8 and it comes with project setup that can easily be opened with this IDE. 
Basic knowledge of Linux or Windows operating systems is required to build and deploy DELTA.

Open source credits and GNU GPL acknowledgments go to:

    Oracle Inc. for MySQL, Netbeans and Java
    Sencha Inc. for Ext JS.
    Hazelcast Inc. for Hazecast distributed data grid
    Apache Software Foundation for Tomcat





